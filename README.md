Here are the steps to run the tests in discipl-python/test/test_Core.py

1. Clone the repository
2. start a command-line prompt and go to the directory C:\....\discipl-python
3. type the following command: python -m src.Ephemeral.MyServer.py<br/>
   Note1: This command is case sensitive<br/>
   Note2: This command will run the server in the background as a localhost
4. start a second command-line prompt go to the directory C:\....\discipl-python
5. type the following command: python -m unittest test/test_Core.py<br/>
   Note3: This command is case sensitive

If there are no errors in the output of the second command-line prompt, the tests succeeded.

It could be the case that certain modules are missing. In the command prompt type: "pip install 'module name'" to install missing modules
