import os
import unittest
import asyncio
import json

from test.testUtils import expectCheckActionResult, runScenario, runOnModel, expectModelFact, expectModelDuty, expectModelActDetails, takeAction, expectData, expectPotentialActs, expectActiveDuties, expectAvailableActs, factResolverFactory, expectRetrievedFactFunction, takeFailingAction, expectPotentialAct
from src.DisciplLawReg.Utils.FactResolver import FactResolver

model = {
    'acts': [
      {
        'act': '<<subsidie aanvragen>>',
        'actor': '[burger]',
        'action': '[aanvragen]',
        'object': '[verzoek]',
        'recipient': '[ambtenaar]',
        'preconditions': '[bedrag]',
        'create': [
          '[aanvraag]'
        ],
        'terminate': [],
        'sources': [],
        'explanation': ''
      },
      {
        'act': '<<subsidie aanvraag intrekken>>',
        'actor': '[burger with aanvraag]',
        'action': '[intrekken]',
        'object': '[aanvraag]',
        'recipient': '[ambtenaar]',
        'preconditions': '[]',
        'create': [],
        'terminate': [
          '[aanvraag]'
        ],
        'sources': [],
        'explanation': ''
      },
      {
        'act': '<<subsidie aanvraag toekennen>>',
        'actor': '[ambtenaar]',
        'action': '[toekennen]',
        'object': '[aanvraag]',
        'recipient': '[burger]',
        'preconditions': {
          'expression': 'LESS_THAN',
          'operands': [
            '[bedrag projection]',
            {
              'expression': 'LITERAL',
              'operand': 500
            }
          ]
        },
        'create': [],
        'terminate': [
          '[aanvraag]'
        ],
        'sources': [],
        'explanation': ''
      }
    ],
    'facts': [
      {
        'fact': '[bedrag]',
        'function': '[]',
        'sources': []
      },
      {
        'fact': '[aanvraag]',
        'function': {
          'expression': 'CREATE',
          'operands': [
            '[bedrag]',
            '[burger]'
          ]
        },
        'sources': []
      },
      {
        'fact': '[bedrag projection]',
        'function': {
          'expression': 'PROJECTION',
          'context': [
            '[aanvraag]'
          ],
          'operand': '[bedrag]'
        },
        'sources': []
      },
      {
        'fact': '[burger with aanvraag]',
        'function': {
          'expression': 'PROJECTION',
          'context': [
            '[aanvraag]'
          ],
          'operand': '[burger]'
        },
        'sources': []
      },
      {
        'fact': '[burger]',
        'function': '[]',
        'sources': []
      },
      {
        'fact': '[verzoek]',
        'function': '[]',
        'sources': []
      },
      {
        'fact': '[ambtenaar]',
        'function': '[]',
        'sources': []
      },
      {
        'fact': '[aanvragen]',
        'function': '[]',
        'sources': []
      },
      {
        'fact': '[toekennen]',
        'function': '[]',
        'sources': []
      }
    ],
    'duties': []
  }

class TestCore(unittest.TestCase):
    def setUp(self):
        self.i = 0

    def calculatorModel (self, precondition):
        return {
          'acts': [
            {
              'act': '<<give a number>>',
              'actor': '[actor1]',
              'action': 'action',
              'object': '[calculator]',
              'recipient': '[actor1]',
              'preconditions': '[number]',
              'create': [
                '[paper]'
              ],
              'terminate': [],
              'sources': [],
              'explanation': ''
            },
            {
              'act': '<<accept number>>',
              'actor': '[actor1]',
              'action': 'accept',
              'object': '[calculator]',
              'recipient': '[actor1]',
              'preconditions': precondition,
              'create': [],
              'terminate': [],
              'sources': [],
              'explanation': ''
            }
          ],
          'facts': [
            {
              'fact': '[actor1]',
              'explanation': '',
              'function': '[]',
              'sources': []
            },
            {
              'fact': '[calculator]',
              'explanation': '',
              'function': '[]',
              'sources': []
            },
            {
              'fact': '[paper]',
              'explanation': '',
              'function': {
                'expression': 'CREATE',
                'operands': [
                  '[number]'
                ]
              },
              'sources': []
            },
            {
              'fact': '[number]',
              'explanation': '',
              'function': '[]',
              'sources': []
            }
          ],
          'duties': []
        }

'should be able to have multiple of the same type of actor'
class TestProjectionExpression1(TestCore):
  def test_Check_Action_Model(self):
    tasks = [self.asyncTestProjectionExpression1()]
    loop = asyncio.get_event_loop()
    loop.set_debug(True)
    loop.run_until_complete(asyncio.wait(tasks))
    # loop.close()

  async def asyncTestProjectionExpression1(self):
    facts = {'[bedrag]': 50, '[verzoek]': True}
    result, test = await runScenario(
      model,
      {'ambtenaar': ['[ambtenaar]'], 'burger1': ['[burger]'], 'burger2': ['[burger]']},
      [
        expectAvailableActs('burger1', ['<<subsidie aanvragen>>'], factResolverFactory(facts)),
        takeAction('burger1', '<<subsidie aanvragen>>', factResolverFactory(facts)),
        expectAvailableActs('burger1', ['<<subsidie aanvraag intrekken>>']),
        expectAvailableActs('burger2', []),
        expectAvailableActs('ambtenaar', ['<<subsidie aanvraag toekennen>>'])
      ]
      )
    self.assertEqual(test, True)

'should only be able to take action on fact you have a relation to'
class TestProjectionExpression2(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestProjectionExpression2()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestProjectionExpression2(self):
      facts = {'[bedrag]': 50, '[verzoek]': True}
      result, test = await runScenario(
        model,
        {'ambtenaar': ['[ambtenaar]'], 'burger1': ['[burger]'], 'burger2': ['[burger]']},
        [
          expectAvailableActs('burger1', ['<<subsidie aanvragen>>'], factResolverFactory(facts)),
          takeAction('burger1', '<<subsidie aanvragen>>', factResolverFactory(facts)),
          takeFailingAction('burger2', '<<subsidie aanvraag intrekken>>',
                            'Action <<subsidie aanvraag intrekken>> is not allowed due to actor',
                            factResolverFactory(facts)),
          takeAction('burger1', '<<subsidie aanvragen>>', factResolverFactory(facts)),
          takeAction('burger2', '<<subsidie aanvragen>>', factResolverFactory(facts)),
          takeFailingAction('burger2', '<<subsidie aanvraag intrekken>>',
                            'Action <<subsidie aanvraag intrekken>> is not allowed due to actor',
                            factResolverFactory(facts, {'[aanvraag]': 1})),
          takeAction('burger2', '<<subsidie aanvraag intrekken>>', factResolverFactory(facts, {'[aanvraag]': 2}))
        ]
        )
      self.assertEqual(test, True)

'should call getPotentialActs multiple times without breaking PROJECTION expressions'
class TestProjectionExpression3(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestProjectionExpression3()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestProjectionExpression3(self):
      result, test = await runScenario(
        model,
        {'ambtenaar': ['[ambtenaar]'], 'burger': ['[burger]']},
        [
          expectPotentialActs('burger', ['<<subsidie aanvragen>>']),
          expectPotentialActs('ambtenaar', [])
        ]
        )
      self.assertEqual(test, True)

'should get the projected property'
class TestProjectionExpression4(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestProjectionExpression4()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestProjectionExpression4(self):
      completeFacts = {'[verzoek]': True, '[bedrag]': 500}
      result, test = await runScenario(
        model,
        {'ambtenaar': ['[ambtenaar]'], 'burger': ['[burger]']},
        [
          takeAction('burger', '<<subsidie aanvragen>>', factResolverFactory(completeFacts)),
          takeAction('ambtenaar', '<<subsidie aanvraag toekennen>>', factResolverFactory(completeFacts))
        ]
      )
      self.assertEqual(test, True)

'should be able to take an action after object is created from other action'
class TestProjectionExpression5(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestProjectionExpression5()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestProjectionExpression5(self):
      result, test = await runScenario(
        model,
        { 'ambtenaar': ['[ambtenaar]'], 'burger': ['[burger]'] },
          [
            takeAction('burger', '<<subsidie aanvragen>>', factResolverFactory({ '[verzoek]': True, '[bedrag]': 50 })),
            expectAvailableActs('ambtenaar', ['<<subsidie aanvraag toekennen>>']),
            expectPotentialActs('burger', ['<<subsidie aanvragen>>']),
            expectAvailableActs('burger', ['<<subsidie aanvraag intrekken>>']),
            takeAction('ambtenaar', '<<subsidie aanvraag toekennen>>', factResolverFactory({ '[verzoek]': True })),
            expectAvailableActs('ambtenaar', [])
          ]
        )
      self.assertEqual(test, True)

'should check actors match for create expressions'
class TestProjectionExpression6(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestProjectionExpression6()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestProjectionExpression6(self):
      result, test = await runScenario(
          model,
          { 'ambtenaar': ['[ambtenaar]'], 'burger1': ['[burger]'], 'burger2': ['[burger]'] },
          [
            takeAction('burger1', '<<subsidie aanvragen>>', factResolverFactory({ '[verzoek]': True, '[bedrag]': 50 })),
            takeAction('burger2', '<<subsidie aanvragen>>', factResolverFactory({ '[verzoek]': True, '[bedrag]': 51 })),
            expectPotentialActs('burger1', ['<<subsidie aanvragen>>', '<<subsidie aanvraag intrekken>>']),
            expectPotentialActs('burger2', ['<<subsidie aanvragen>>', '<<subsidie aanvraag intrekken>>']),
            expectPotentialActs('ambtenaar', ['<<subsidie aanvraag toekennen>>'])
          ]
        )
      self.assertEqual(test, True)

'EQUAL should return undefined when a PROJECTION expression returns undefined'
class TestProjectionExpression7(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestProjectionExpression7()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestProjectionExpression7(self):
      model = self.calculatorModel({
        'expression': 'EQUAL',
        'operands': [
          {
            'expression': 'LITERAL',
            'operand': 10
          },
          {
            'expression': 'PROJECTION',
            'context': [
              '[paper]'
            ],
            'operand': '[number]'
          }
        ]
      })

      facts = {'[number]': 10, '[calculator]': True}
      result, test = await runScenario(
        model,
        {'Actor': ['[actor1]']},
        [
          takeAction('Actor', '<<give a number>>', factResolverFactory(facts)),
          expectPotentialAct('Actor', '<<accept number>>'),
          takeAction('Actor', '<<give a number>>', factResolverFactory(facts)),
          expectPotentialAct('Actor', '<<accept number>>')
        ]
      )
      self.assertEqual(test, True)

'LESS THAN should return undefined when a PROJECTION expression returns undefined'
class TestProjectionExpression8(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestProjectionExpression8()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestProjectionExpression8(self):
      model = self.calculatorModel({
        'expression': 'LESS_THAN',
        'operands': [
          {
            'expression': 'LITERAL',
            'operand': 1
          },
          {
            'expression': 'PROJECTION',
            'context': [
              '[paper]'
            ],
            'operand': '[number]'
          }
        ]
      })

      facts = { '[number]': 10, '[calculator]': True }
      result, test = await runScenario(
          model,
          { 'Actor': ['[actor1]'] },
          [
            takeAction('Actor', '<<give a number>>', factResolverFactory(facts)),
            expectPotentialAct('Actor', '<<accept number>>'),
            takeAction('Actor', '<<give a number>>', factResolverFactory(facts)),
            expectPotentialAct('Actor', '<<accept number>>')
          ]
        )
      self.assertEqual(test, True)

'SUM should return undefined when a PROJECTION expression returns undefined'
class TestProjectionExpression9(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestProjectionExpression9()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestProjectionExpression9(self):
      model = self.calculatorModel({
          'expression': 'EQUAL',
          'operands': [
            {
              'expression': 'LITERAL',
              'operand': 11
            },
            {
              'expression': 'SUM',
              'operands': [
                {
                  'expression': 'LITERAL',
                  'operand': 1
                },
                {
                  'expression': 'PROJECTION',
                  'context': [
                    '[paper]'
                  ],
                  'operand': '[number]'
                }
              ]
            }
          ]
        })

      facts = { '[number]': 10, '[calculator]': True }
      result, test = await runScenario(
          model,
          { 'Actor': ['[actor1]'] },
          [
            takeAction('Actor', '<<give a number>>', factResolverFactory(facts)),
            expectPotentialAct('Actor', '<<accept number>>'),
            takeAction('Actor', '<<give a number>>', factResolverFactory(facts)),
            expectPotentialAct('Actor', '<<accept number>>')
          ]
        )
      self.assertEqual(test, True)

'PRODUCT should return undefined when a PROJECTION expression returns undefined'
class TestProjectionExpression10(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestProjectionExpression10()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestProjectionExpression10(self):
      model = self.calculatorModel({
          'expression': 'EQUAL',
          'operands': [
            {
              'expression': 'LITERAL',
              'operand': 20
            },
            {
              'expression': 'PRODUCT',
              'operands': [
                {
                  'expression': 'LITERAL',
                  'operand': 2
                },
                {
                  'expression': 'PROJECTION',
                  'context': [
                    '[paper]'
                  ],
                  'operand': '[number]'
                }
              ]
            }
          ]
        })

      facts = { '[number]': 10, '[calculator]': True }
      result, test = await runScenario(
        model,
        {'Actor': ['[actor1]']},
        [
          takeAction('Actor', '<<give a number>>', factResolverFactory(facts)),
          expectPotentialAct('Actor', '<<accept number>>'),
          takeAction('Actor', '<<give a number>>', factResolverFactory(facts)),
          expectPotentialAct('Actor', '<<accept number>>')
        ]
      )
      self.assertEqual(test, True)

'Should be able to chose one of the previously created facts'
class TestProjectionExpression11(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestProjectionExpression11()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestProjectionExpression11(self):
      model = self.calculatorModel({
          'expression': 'EQUAL',
          'operands': [
            {
              'expression': 'LITERAL',
              'operand': 10
            },
            {
              'expression': 'PROJECTION',
              'context': [
                '[paper]'
              ],
              'operand': '[number]'
            }
          ]
        })

      facts = { '[number]': 10, '[calculator]': True }
      result, test = await runScenario(
        model,
        {'Actor': ['[actor1]']},
        [
          takeAction('Actor', '<<give a number>>', factResolverFactory(facts)),
          takeAction('Actor', '<<give a number>>', factResolverFactory(facts)),
          takeAction('Actor', '<<accept number>>', factResolverFactory(facts, {'[paper]': 1}))
        ]
      )
      self.assertEqual(test, True)

'Should be able to have expression in operand with single scope'
class TestProjectionExpression12(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestProjectionExpression12()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestProjectionExpression12(self):
      model = self.calculatorModel({
          'expression': 'EQUAL',
          'operands': [
            {
              'expression': 'LITERAL',
              'operand': 20
            },
            {
              'expression': 'PROJECTION',
              'context': [
                '[paper]'
              ],
              'operand': {
                'expression': 'PRODUCT',
                'operands': [
                  '[number]',
                  {
                    'expression': 'LITERAL',
                    'operand': 2
                  }
                ]
              }
            }
          ]
        })

      facts = { '[number]': 10, '[calculator]': True }
      result, test = await runScenario(
          model,
          {'Actor': ['[actor1]']},
          [
            takeAction('Actor', '<<give a number>>', factResolverFactory(facts)),
            takeAction('Actor', '<<accept number>>', factResolverFactory(facts))
          ]
        )
      self.assertEqual(test, True)

'Should be able to have expression in operand with all scope'
class TestProjectionExpression13(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestProjectionExpression13()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestProjectionExpression13(self):
      model = self.calculatorModel({
        'expression': 'EQUAL',
        'operands': [
          {
            'expression': 'LITERAL',
            'operand': 20
          },
          {
            'expression': 'PROJECTION',
            'scope': 'all',
            'context': [
              '[paper]'
            ],
            'operand': {
              'expression': 'SUM',
              'operands': [
                '[number]'
              ]
            }
          }
        ]
      })

      facts1 = {'[number]': 12, '[calculator]': True}
      facts2 = {'[number]': 5, '[calculator]': True}
      facts3 = {'[number]': 3, '[calculator]': True}
      facts4 = {'[calculator]': True}

      result, test = await runScenario(
          model,
          { 'Actor': ['[actor1]'] },
          [
            takeAction('Actor', '<<give a number>>', factResolverFactory(facts1)),
            takeAction('Actor', '<<give a number>>', factResolverFactory(facts2)),
            takeAction('Actor', '<<give a number>>', factResolverFactory(facts3)),
            takeAction('Actor', '<<accept number>>', factResolverFactory(facts4))
          ]
        )
      self.assertEqual(test, True)

'Should be able to have expression in operand with some scope'
class TestProjectionExpression14(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestProjectionExpression14()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestProjectionExpression14(self):
      model = self.calculatorModel({
        'expression': 'EQUAL',
        'operands': [
          {
            'expression': 'LITERAL',
            'operand': 20
          },
          {
            'expression': 'PROJECTION',
            'scope': 'some',
            'context': [
              '[paper]'
            ],
            'operand': {
              'expression': 'SUM',
              'operands': [
                '[number]'
              ]
            }
          }
        ]
      })

      facts1 = {'[number]': 15, '[calculator]': True}
      facts2 = {'[number]': 5, '[calculator]': True}
      facts = {'[number]': 10, '[calculator]': True}

      result, test = await runScenario(
        model,
        {'Actor': ['[actor1]']},
        [
          takeAction('Actor', '<<give a number>>', factResolverFactory(facts1)),
          takeAction('Actor', '<<give a number>>', factResolverFactory(facts2)),
          takeAction('Actor', '<<give a number>>', factResolverFactory(facts)),
          takeAction('Actor', '<<give a number>>', factResolverFactory(facts)),
          takeAction('Actor', '<<accept number>>', factResolverFactory(facts, {'[paper]': [0, 1]}))
        ]
        )
      self.assertEqual(test, True)

'should project a series of CREATE facts'
class TestProjectionExpression15(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestProjectionExpression15()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestProjectionExpression15(self):
        model = {
            'acts': [
                {
                    'act': '<<persoonlijk gegevens invullen>>',
                    'actor': '[burger]',
                    'recipient': '[ambtenaar]',
                    'object': '[verzoek]',
                    'preconditions': '[naam]',
                    'create': [
                        '[persoonlijke gegevens]'
                    ]
                },
                {
                    'act': '<<subsidie aanvragen>>',
                    'actor': '[burger]',
                    'recipient': '[ambtenaar]',
                    'object': '[verzoek]',
                    'preconditions': {
                        'expression': 'AND',
                        'operands': [
                            '[persoonlijke gegevens]',
                            '[bedrag]'
                        ]
                    },
                    'create': [
                        '[aanvraag]'
                    ]
                },
                {
                    'act': '<<subsidie aanvraag toekennen>>',
                    'actor': '[ambtenaar]',
                    'object': '[aanvraag]',
                    'preconditions': {
                        'expression': 'EQUAL',
                        'operands': [
                            {
                                'expression': 'PROJECTION',
                                'context': [
                                    '[aanvraag]',
                                    '[persoonlijke gegevens]'
                                ],
                                'operand': '[naam]'
                            },
                            {
                                'expression': 'LITERAL',
                                'operand': 'Discipl'
                            }
                        ]
                    },
                    'recipient': '[burger]'
                }
            ],
            'facts': [
                {
                    'fact': '[aanvraag]',
                    'function': {
                        'expression': 'CREATE',
                        'operands': [
                            '[persoonlijke gegevens]',
                            '[bedrag]'
                        ]
                    }
                },
                {
                    'fact': '[persoonlijke gegevens]',
                    'function': {
                        'expression': 'CREATE',
                        'operands': [
                            '[naam]'
                        ]
                    }
                }
            ],
            'duties': []
        }

        completeFacts = { '[burger]': True, '[ambtenaar]': True, '[verzoek]': True, '[naam]': 'Discipl', '[bedrag]': 500 }

        result, test = await runScenario(
            model,
            { 'burger': ['[burger]'], 'ambtenaar': ['[ambtenaar]'] },
            [
                takeAction('burger', '<<persoonlijk gegevens invullen>>', factResolverFactory(completeFacts)),
                takeAction('burger', '<<subsidie aanvragen>>', factResolverFactory(completeFacts)),
                takeAction('ambtenaar', '<<subsidie aanvraag toekennen>>', factResolverFactory(completeFacts))
            ]
        )
        self.assertEqual(test, True)

'should project a series of CREATE facts car accident example'
class TestProjectionExpression16(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestProjectionExpression16()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestProjectionExpression16(self):
        model = {
            'acts': [
                {
                    'act': '<<create driver>>',
                    'actor': '[police officer]',
                    'recipient': '[police officer]',
                    'object': '[object]',
                    'preconditions': '[license]',
                    'create': ['[driver]']
                },
                {
                    'act': '<<create passenger>>',
                    'actor': '[police officer]',
                    'recipient': '[police officer]',
                    'object': '[object]',
                    'preconditions': '[license]',
                    'create': ['[passenger]']
                },
                {
                    'act': '<<create victims car>>',
                    'actor': '[police officer]',
                    'recipient': '[police officer]',
                    'object': '[object]',
                    'preconditions': {
                        'expression': 'AND',
                        'operands': [
                            '[driver]', '[license plate]', '[passengers]'
                        ]
                    },
                    'create': ['[victims car]']
                },
                {
                    'act': '<<create causing car>>',
                    'actor': '[police officer]',
                    'recipient': '[police officer]',
                    'object': '[object]',
                    'preconditions': {
                        'expression': 'AND',
                        'operands': [
                            '[driver]', '[license plate]', '[passengers]'
                        ]
                    },
                    'create': ['[causing car]']
                },
                {
                    'act': '<<create car accident>>',
                    'actor': '[police officer]',
                    'recipient': '[police officer]',
                    'object': '[object]',
                    'preconditions': {
                        'expression': 'AND',
                        'operands': ['[causing car]', '[victims car]']
                    },
                    'create': ['[car accident]']
                },
                {
                    'act': '<<check license of causing driver>>',
                    'actor': '[police officer]',
                    'recipient': '[police officer]',
                    'object': '[object]',
                    'preconditions': {
                        'expression': 'EQUAL',
                        'operands': [
                            {
                                'expression': 'LITERAL',
                                'operand': 'causing license'
                            },
                            {
                                'expression': 'PROJECTION',
                                'context': [
                                    '[car accident]',
                                    '[causing car]',
                                    '[driver]'
                                ],
                                'operand': '[license]'
                            }
                        ]
                    },
                    'create': []
                }
            ],
            'facts': [
                {
                    'fact': '[driver]',
                    'function': {
                        'expression': 'CREATE',
                        'operands': [
                            '[license]'
                        ]
                    }
                },
                {
                    'fact': '[passengers]',
                    'function': {
                        'expression': 'CREATE',
                        'operands': [
                            '[license]'
                        ]
                    }
                },
                {
                    'fact': '[causing car]',
                    'function': {
                        'expression': 'CREATE',
                        'operands': [
                            '[driver]',
                            '[license plate]',
                            '[passengers]'
                        ]
                    }
                },
                {
                    'fact': '[victims car]',
                    'function': {
                        'expression': 'CREATE',
                        'operands': [
                            '[driver]',
                            '[license plate]',
                            '[passengers]'
                        ]
                    }
                },
                {
                    'fact': '[police officer]',
                    'function': '[]'
                },
                {
                    'fact': '[object]',
                    'function': {
                        'expression': 'LITERAL',
                        'operand': True
                    }
                },
                {
                    'fact': '[car-accident]',
                    'function': {
                        'expression': 'CREATE',
                        'operands': [
                            '[causing car]',
                            '[victims car]'
                        ]
                    }
                },
                {
                    'fact': '[passengers]',
                    'function': {
                        'expression': 'PROJECTION',
                        'scope': 'some',
                        'context': ['[passenger]'],
                        'operand': '[passenger]'
                    }
                }
            ],
            'duties': []
        }

        result, test = await runScenario(
            model,
            { 'officer': ['[police officer]'] },
            [
                takeAction('officer', '<<create driver>>', factResolverFactory({ '[license]': 'causing license' })),
                takeAction('officer', '<<create driver>>', factResolverFactory({ '[license]': 'victim license' })),
                takeAction('officer', '<<create driver>>', factResolverFactory({ '[license]': 'random license' })),
                takeAction('officer', '<<create passenger>>', factResolverFactory({ '[license]': 'passenger1 license' })),
                takeAction('officer', '<<create passenger>>', factResolverFactory({ '[license]': 'passenger2 license' })),
                takeAction('officer', '<<create passenger>>', factResolverFactory({ '[license]': 'passenger3 license' })),
                takeAction('officer', '<<create causing car>>', factResolverFactory({ '[license plate]': 'causing plate' }, { '[driver]': 0, '[passenger]': [3] })),
                takeAction('officer', '<<create victims car>>', factResolverFactory({ '[license plate]': 'victims plate' }, { '[driver]': 1, '[passenger]': [4, 5] })),
                takeAction('officer', '<<create car accident>>', factResolverFactory({})),
                takeAction('officer', '<<check license of causing driver>>', factResolverFactory({}))
            ]
        )
        self.assertEqual(test, True)

'should not allow an act if the projection failed'
class TestProjectionExpression17(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestProjectionExpression17()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestProjectionExpression17(self):
        model = {
            'acts': [
                {
                    'act': '<<subsidie aanvraag toekennen>>',
                    'actor': '[ambtenaar]',
                    'object': '[aanvraag]',
                    'preconditions': {
                        'expression': 'EQUAL',
                        'operands': [
                            {
                                'expression': 'PROJECTION',
                                'context': [
                                    '[aanvraag]'
                                ],
                                'operand': '[niet bestaand bedrag]'
                            },
                            {
                                'expression': 'LITERAL',
                                'operand': 500
                            }
                        ]
                    },
                    'recipient': '[burger]'
                }
            ],
            'facts': [
                {
                    'fact': '[aanvraag]',
                    'function': {
                        'expression': 'CREATE',
                        'operands': [
                            '[bedrag]'
                        ]
                    }
                }
            ],
            'duties': []
        }

        completeFacts = {'[burger]': True, '[ambtenaar]': True, '[verzoek]': True, '[bedrag]': 500}

        result, test = await runScenario(
            model,
            {'burger': ['[burger]'], 'ambtenaar': ['[ambtenaar]']},
            [
                takeFailingAction('burger', '<<subsidie aanvraag toekennen>>',
                                  'Action <<subsidie aanvraag toekennen>> is not allowed due to object',
                                  factResolverFactory(completeFacts))
            ]
        )
        self.assertEqual(test, True)