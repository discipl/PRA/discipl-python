from src.DisciplLawReg.LawReg import LawReg
from src.DisciplLawReg.Utils.Util import Util
from src.DisciplLawReg.Utils.FactResolver import FactResolver
import asyncio
from mpmath import *

'''
* @typedef Step
* @property {function(lawReg: LawReg, ssids: ssid[], prevLink: string, stepIndex: number, actionLinks: string[], modelLink: string): Promise<string>} execute
'''

'''
* @param {string} actor - actor name
* @param {string} act - description of the act to be taken
* @param {function(*=): function(*): Promise<*|undefined>} factResolverFactory - Function used create a fact resolver
* @returns {Step}
'''
class takeAction:
  def __init__(self, actor, act, factResolverFactory = FactResolver(None, True, False)):
    self.actor = actor
    self.act = act
    self.factResolverFactory = factResolverFactory

  async def execute(self, lawReg, ssids, link, index, actionLinks, modelLink = None):
    try:
        self.factResolverFactory.actionLinks = actionLinks
        resolver = self.factResolverFactory
        #console.log('resolver', resolver)
        actionLink = await lawReg.take(ssids[self.actor], link, self.act, resolver)
        actionLinks.append(actionLink)
        return actionLink, True
    except:
      raise Exception('TakeAction failed')
    return None, False
'''
 * @param {string} actor - actor name
 * @param {string} act - description of the act to be taken
 * @param {string} message - the failure message
 * @param {function(*=): function(*): Promise<*|undefined>} factResolverFactory - Function used create a fact resolver
 * @returns {Step}
'''
class takeFailingAction:
  def __init__(self, actor, act, message, factResolverFactory = FactResolver(None, True, False)):
    self.actor = actor
    self.act = act
    self.message = message
    self.factResolverFactory = factResolverFactory

  async def execute(self, lawReg, ssids, link, index, actionLinks, modelLink = None):
      try:
        self.factResolverFactory.actionLinks = actionLinks
        return await lawReg.take(ssids[self.actor], link, self.act, self.factResolverFactory), False
      except Exception as e:
        i = 0
        #raise Exception('takeFailingAction failed')
        return link, True
      return link, False

'''
* @param {string} actor - actor name
* @param {string[]} acts - description of the act that is expected
* @param {function(*=): function(*): Promise<*|undefined>} factResolverFactory - Function used create a fact resolver
* @returns {Step}
'''
class expectPotentialActs:
  def __init__(self, actor, acts, factResolverFactory = FactResolver(None, True, None)):
    self.actor = actor
    self.acts = acts
    self.factResolverFactory = factResolverFactory
    self.expect = False

  async def execute(self, lawReg, ssids, link, index, actionLinks, modelLink = None):
    self.factResolverFactory.actionLinks = actionLinks
    result = await lawReg.getPotentialActsWithResolver(link, ssids[self.actor], self.factResolverFactory)  # .map((actInfo) => actInfo.act)
    actsResult = []
    for index in range(len(result)):
      for key, value in result[index].items():
        if key == 'act':
          actsResult.append(value)
    self.expect = actsResult == self.acts
    return link, self.expect


'''
* @param {string} actor - actor name
* @param {string} act - description of the act that is expected
* @param {function(*=): function(*): Promise<*|undefined>} factResolverFactory - Function used create a fact resolver
* @returns {Step}
'''
class expectPotentialAct:
  def __init__(self, actor, act, factResolverFactory = FactResolver(None, True, None)):
    self.actor = actor
    self.act = act
    self.factResolverFactory = factResolverFactory
    self.expect = False

  async def execute(self, lawReg, ssids, link, index, actionLinks, modelLink = None):
    self.factResolverFactory.actionLinks = actionLinks
    result = await lawReg.getPotentialActsWithResolver(link, ssids[self.actor], self.factResolverFactory)
    actResult = []
    for index in range(len(result)):
      for key, value in result[index].items():
        if key == 'act':
          actResult.append(value)
    self.expect = self.act in actResult
    return link, self.expect

'''
* @param {string} actor - actor name
* @param {string[]} acts - description of the act that is expected
* @param {function(*=): function(*): Promise<*|undefined>} factResolverFactory - Function used create a fact resolver
* @returns {Step}
'''
class expectAvailableActs:
  def __init__(self, actor, acts, factResolverFactory = FactResolver(None, True, None)):
    self.actor = actor
    self.acts = acts
    self.factResolverFactory = factResolverFactory

  async def execute(self, lawReg, ssids, link, index, actionLinks, modelLink = None):
      self.factResolverFactory.actionLinks = actionLinks
      result = await lawReg.getAvailableActsWithResolver(link, ssids[self.actor], self.factResolverFactory)
      #.map((actInfo) => actInfo.act)
      actsResult = []
      for index in range(len(result)):
        for key, value in result[index].items():
          if key == 'act':
            actsResult.append(value)
      self.expect = actsResult == self.acts
      return link, self.expect

'''
* @param {string} actor - actor name
* @param {string[]} duties - description of the act that is expected
* @returns {Step}
'''
class expectActiveDuties:
  def __init__(self, actor, duties):
    self.actor = actor
    self.duties = duties

  async def execute(self, lawReg, ssids, link, index, actionLinks, modelLink = None):
    result = await lawReg.getActiveDuties(link, ssids[self.actor])#.map((dutoInfo) => dutoInfo.duty)
    dutiesResult = []
    for index in range(len(result)):
      for key, value in result[index].items():
        if key == 'duty':
          dutiesResult.append(value)
    self.expect = dutiesResult == self.duties
    return link, self.expect

'''
* @param {string} actor - actor name
* @param {string} act - the previous act
* @param {function(object[], string[]):object} factsSupplied - function that returns the expected supplied acts
* @return {Step}
'''
class expectData:
  def __init__(self, actor, act, factsSupplied):
    self.actor = actor
    self.act = act
    self.factsSupplied = factsSupplied
    self.expect = False

  async def execute(self, lawReg, ssids, link, index, actionLinks, modelLink):
      await runOnModel(self.actor, self.checkData).execute(lawReg, ssids, link, index, actionLinks, modelLink)
      return link, self.expect

  async def checkData(self, model, lawReg, ssids, link, index, actionLinks, modelLink):
    core = lawReg.getAbundanceService().getCoreAPI()
    if self.actor in ssids:
      action = await core.get(link, ssids[self.actor])
    else:
      action = await core.get(link, None)
    found = False
    for index in range(len(model['acts'])):
      for key, value in model['acts'][index].items():
        if self.act in key:
          actLink = value
          found = True
          break
      if found:
        break
    #actLink = Object.values(model.acts.find(anAct= > Object.keys(anAct).includes(act)))[0]
    data = {
      'DISCIPL_FLINT_ACT_TAKEN': actLink,
      'DISCIPL_FLINT_FACTS_SUPPLIED': self.factsSupplied(ssids, actionLinks),
      'DISCIPL_FLINT_GLOBAL_CASE': actionLinks[0],
      'DISCIPL_FLINT_PREVIOUS_CASE': actionLinks[len(actionLinks) - 2]
    }
    self.expect = action['claim']['data'] == data
    if self.expect:
      print('checkFact True')
    else:
      print('checkFact False')
    return self.expect

'''
* @param {string} actor - actor name
* @param {string} fact - the fact to check
* @param {any} aFunction - the expected function for the fact
* @return {Step}
'''
class expectRetrievedFactFunction:
  def __init__(self, actor, fact, aFunction):
    self.actor = actor
    self.fact = fact
    self.aFunction = aFunction

  async def execute(self, lawReg, ssids, link, index, actionLinks, modelLink):
      core = lawReg.getAbundanceService().getCoreAPI()
      retrievedModel = await core.get(modelLink)
      rFacts = retrievedModel['claim']['data']['DISCIPL_FLINT_MODEL']['facts']
      found = False
      for localDict in rFacts:
        for key, value in localDict.items():
          if self.fact in key:
            rFactLink = value
            found = True
            break
        if found:
          break
      #rFactLink = rFacts.find(aFact => Object.keys(aFact).includes(fact))[fact]
      rFact = await core.get(rFactLink, ssids[self.actor])
      #expect(rFact.data['DISCIPL_FLINT_FACT'].function).to.deep.equal(aFunction, `ExpectRetrievedFactFunction${fact} Step failed. Step Index ${index}`)
      if rFact['claim']['data']['DISCIPL_FLINT_FACT']['function'] == self.aFunction:
        return link, True
      return link, False

class expectCheckActionResult:
  def __init__(self, actor, act, checkActionResult, factResolverFactory, earlyEscape = False):
    self.actor = actor
    self.act = act
    self.checkActionResult = checkActionResult
    self.factResolverFactory = factResolverFactory
    self.earlyEscape = earlyEscape

  async def execute(self, lawReg, ssids, link, index, actionLinks, modelLink):
    core = lawReg.getAbundanceService().getCoreAPI()
    retrievedModel = await core.get(modelLink)
    acts = retrievedModel['claim']['data']['DISCIPL_FLINT_MODEL']['acts']
    for key, value in acts[0].items():
      if self.act in key:
        actLink = value
        break
    #actLink = Object.values(acts.find(anAct => Object.keys(anAct).includes(self.act)))[0]
    self.factResolverFactory.actionLinks = actionLinks
    result = await lawReg._getActionChecker().checkAction(modelLink, actLink, ssids[self.actor], { 'factResolver': self.factResolverFactory }, self.earlyEscape)
    #expect(result).to.deep.equal(checkActionResult, `ExpectCheckActionResult${act} Step failed. Step Index ${index}`)
    if result == self.checkActionResult:
      return link, True
    return link, False

'''
 * @param {string} actor - actor name
 * @param {function(*)} aFunction - the function to run
 * @return {Step}
'''
class runOnModel:
  def __init__(self, actor, aFunction):
    self.actor = actor
    self.aFunction = aFunction

  async def execute(self, lawReg, ssids, link, index, actionLinks, modelLink):
    core = lawReg.getAbundanceService().getCoreAPI()
    retrievedModel = await core.get(modelLink)
    rModel = retrievedModel['claim']['data']['DISCIPL_FLINT_MODEL']
    if asyncio.iscoroutinefunction(self.aFunction):
      test = await self.aFunction(rModel, lawReg, ssids, link, index, actionLinks, modelLink)
    else:
      test = self.aFunction(rModel, lawReg, ssids, link, index, actionLinks, modelLink)
    return link, test

'''
* @param {string} actor - actor name
* @param {string} fact - fact to check
* @param {any} factValue - expected value of fact
* @return {Step}
'''
class expectModelFact:
  def __init__(self, actor, fact, factValue):
    self.actor = actor
    self.fact = fact
    self.factValue = factValue
    self.expect = False

  async def execute(self, lawReg, ssids, link, index, actionLinks, modelLink):
      await runOnModel(self.actor, self.checkFact).execute(lawReg, ssids, link, index, actionLinks, modelLink)
      return link, self.expect

  async def checkFact(self, model, lawReg, ssids, link, index, actionLinks, modelLink):
    core = lawReg.getAbundanceService().getCoreAPI()
    found = False
    for index in range(len(model['facts'])):
      for key, value in model['facts'][index].items():
        if self.fact in key:
          rFactLink = value
          found = True
          break
      if found:
        break
    #rFactLink = model.facts.find(aFact= > Object.keys(aFact).includes(fact))[fact]
    rFact = await core.get(rFactLink, ssids[self.actor])
    self.expect = rFact['claim']['data']['DISCIPL_FLINT_FACT'] == self.factValue
    if self.expect:
      print('checkFact True')
    else:
      print('checkFact False')
    return self.expect

'''
* @param {string} actor - actor name
* @param {string} duty - duty to check
* @param {any} dutyValue - expected value of duty
* @return {Step}
'''
class expectModelDuty:
  def __init__(self, actor, duty, dutyValue):
    self.actor = actor
    self.duty = duty
    self.dutyValue = dutyValue
    self.expect = False

  async def execute(self, lawReg, ssids, link, index, actionLinks, modelLink):
      await runOnModel(self.actor, self.checkDuty).execute(lawReg, ssids, link, index, actionLinks, modelLink)
      return link, self.expect

  async def checkDuty(self, model, lawReg, ssids, link, index, actionLinks, modelLink):
    core = lawReg.getAbundanceService().getCoreAPI()
    found = False
    for index in range(len(model['duties'])):
      for key, value in model['duties'][index].items():
        if self.duty in key:
          rDutyLink = value
          found = True
          break
      if found:
        break
    #rDutyLink = model.duties.find(aDuty= > Object.keys(aDuty).includes(duty))[duty]
    rDuty = await core.get(rDutyLink, ssids[self.actor])
    self.expect = rDuty['claim']['data']['DISCIPL_FLINT_DUTY'] == self.dutyValue
    if self.expect:
      print('checkDuty True')
    else:
      print('checkDuty False')
    return self.expect
'''
* @param {string} actor - actor name
* @param {string} act - act to check
* @param {any} actValue - expected value of act
* @return {Step}
'''
class expectModelActDetails:
  def __init__(self, actor, act, actValue):
    self.actor = actor
    self.act = act
    self.actValue = actValue
    self.expect = False

  async def execute(self, lawReg, ssids, link, index, actionLinks, modelLink):
      await runOnModel(self.actor, self.checkAct).execute(lawReg, ssids, link, index, actionLinks, modelLink)
      return link, self.expect

  async def checkAct(self, model, lawReg, ssids, link, index, actionLinks, modelLink):
    found = False
    for index in range(len(model['acts'])):
      for key, value in model['acts'][index].items():
        if self.act in key:
          rActLink = value
          found = True
          break
      if found:
        break
    #rActLink = model.acts.find(anAct= > Object.keys(anAct).includes(act))[act]
    rAct = await lawReg.getActDetails(rActLink, ssids[self.actor])
    self.expect = rAct == self.actValue
    if self.expect:
      print('checkAct True')
    else:
      print('checkAct False')
    return self.expect

'''
* @param {object} facts
* @param {Object<string, number[]|number>} cases - Object where the keys are createable facts and the values are the action index to use for that fact
* @return {function(*=): function(*): Promise<*|undefined>}
'''
class factResolverFactory:
  def __init__(self, facts = {}, cases = {}, actionLinks = []):
    self.facts = facts
    self.cases = cases
    self.actionLinks = actionLinks

  async def resolveFacts(self, fact, listNames, listIndices, possibleCreatingActions):
    if isinstance(fact, dict):
      return None

    if fact in self.cases:
      actionIndex = self.cases[fact]
    else:
      actionIndex = None

    if type(actionIndex) == int or type(actionIndex) == float or type(actionIndex) == mpf:
      # 0 is global link
      link = self.actionLinks[actionIndex + 1]
      return link
    if isinstance(actionIndex, list):
      # 0 is global link
      links = []
      for index in range(len(actionIndex)):
        links.append(self.actionLinks[actionIndex[index] + 1])
      #links = actionIndex.map((index) => actionLinks[index + 1])
      return links
    if fact in self.facts and self.facts[fact] != None:
      if isinstance(self.facts[fact], list):
        return self.facts[fact].pop(0)
      else:
        return self.facts[fact]
    return None

'''
 * @param {boolean} acceptAll - if it should return true or false for all
 * @return {function(*=): function(*): Promise<*|undefined>}

class SimpleFactResolverFactory:
  def __init__(self, result):
    self.resutl = result

  def resolveFacts(self):
    return self.result
'''
'''
 * run scenario
 * @param {object} model FlintModel
 * @param {Object.<string, string[]>} actors Object with keys as actors and values as facts that apply to the actor.
 * @param {Step[]} steps Steps
 * @param {Object.<string, *>} extraFacts
'''
async def runScenario (model, actors, steps, extraFacts = {}):
  lawReg = LawReg()
  util = Util(lawReg)
  core = lawReg.getAbundanceService().getCoreAPI()
  needSsid = await core.newSsid('ephemeral')
  await core.allow(needSsid)

  actorNames = []
  for key, value in actors.items():
    if key != 'ANYONE':
      actorNames.append(key)
  actorVal = _actorFactFunctionSpec(actors)
  _addExtraFacts(actorVal, extraFacts)
  ssids, modelLink = await util.setupModel(model, actorNames, actorVal, False)
  globalLink = await core.claim(needSsid, {
    'need': {
      'DISCIPL_FLINT_MODEL_LINK': modelLink
    }
  })

  actionLinks = [globalLink]

  resultLink, resultTest = await reduceSteps(steps, globalLink, lawReg, ssids, actionLinks, modelLink)

  return resultLink, resultTest
  '''
  return steps.reduce(async (previousValue, currentValue, index) => {
    const prevLink = await previousValue
    console.log('------- Executing step:', index + 1)
    const nextLink = await currentValue.execute(lawReg, ssids, prevLink, index + 1, actionLinks, modelLink)
    console.log('------- Executed step:', index + 1)
    return nextLink
  }, globalLink)
  '''

async def reduceSteps(steps, initialValue, lawReg, ssids, actionLinks, modelLink):
  result = initialValue
  resultTest = True
  previousValue = initialValue
  index = 0
  for step in steps:
    prevLink = previousValue
    print('------- Executing step:', index + 1)
    nextLink, test = await step.execute(lawReg, ssids, prevLink, index + 1, actionLinks, modelLink)
    print('------- Executed step:', index + 1)
    previousValue = nextLink
    if test == False:
      resultTest = False
  return previousValue, resultTest

'''
 * @param {Object<string, string[]>} actors Object with keys as actors and values as facts that apply to the actor.
 * @return {Object<string, string[]>} Object with keys as facts and values as actors that the fact applies to.
 * @private
'''
def _actorFactFunctionSpec (actors):
  actorVal = {}
  for key, value in actors.items():
    for fact in value:
      if not fact in actorVal:
        x = []
      x.append(key)
      actorVal[fact] = x
  return actorVal

'''
 * @param {Object<string, string[]>} actorVal
 * @param {Object<string, *>} extraFacts
 * @private
'''
def _addExtraFacts (actorVal, extraFacts):
  for key, value in extraFacts.items():
    actorVal[key] = value
