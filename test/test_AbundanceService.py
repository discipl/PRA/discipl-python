import unittest
import asyncio
from src.Core import DisciplCore
from src.AbundanceService.AbundanceService import AbundanceService

class TestCore(unittest.TestCase):
    def setUp(self):
        self.core = DisciplCore()

class AbundanceServiceTest(TestCore):
    def test_abundance_service(self):
        tasks = [self.asyncAbundanceServiceTest()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))

    # should be able to claim something and listen to the connector with a filter on a predicate without an ssid
    async def asyncAbundanceServiceTest(self):
        svc = AbundanceService(self.core)
        bsn = '123123123'

        await svc.attendTo1('ephemeral', 'beer', ['BSN'])
        await svc.need1('ephemeral', 'beer')

        await svc.attendTo2('ephemeral', 'beer', ['BSN'])
        await svc.need2('ephemeral', 'beer')

        await svc.attendTo3('ephemeral', 'beer', ['BSN'])
        need = await svc.need3('ephemeral', 'beer')

        await svc.attendTo4('ephemeral', 'beer', ['BSN'])

        await svc.observeOffer1(svc.ssid3['did'], svc.ssid4)

        attendDetails = await svc.attendTo5()

        await svc.getCoreAPI().claim(need['myPrivateSsid'], { 'BSN': bsn })

        nlxSsid = await svc.getCoreAPI().newSsid('ephemeral')
        nlxClaimLink = await svc.getCoreAPI().claim(nlxSsid, {'BSN': bsn, 'woonplaats': 'Haarlem'})
        await svc.getCoreAPI().allow(nlxSsid, nlxClaimLink, attendDetails['theirPrivateDid'])

        await svc.offer(attendDetails['myPrivateSsid'], nlxClaimLink)

        result = await svc.observeOffer2(svc.ssid3['did'], svc.ssid4)

        self.assertEqual(result['claim']['data'], {'BSN': '123123123', 'woonplaats': 'Haarlem'})
