import unittest
import asyncio
from src.Core import DisciplCore
import sys
import time
from mpmath import *
from functools import reduce

from datetime import datetime, timedelta
from src.FileWriter import *
from src.SsidObserver import *
import base64
from src.SubjectFilter import *
from src.BaseConnector.Base_Connector import BaseConnector
from src.Ephemeral.EphemeralConnector import EphemeralConnector
from src.Ephemeral.EphemeralStorage import EphemeralStorage
from src.AbundanceService.AbundanceService import AbundanceService

from src.Ephemeral.MyClient import MyClient

from src.DisciplLawReg.Utils.Array_Util import arrayToObject
from src.DisciplLawReg.Utils.Big_Util import BigUtil

EPHEMERAL_ENDPOINT = 'https://localhost:8000'
EPHEMERAL_WEBSOCKET_ENDPOINT = 'ws://localhost:8000'

from src.Flint.Law import Law
from src.Flint.Laws.CVDR616379Flint import CVDR616379Flint

class TestCore(unittest.TestCase):
    def setUp(self):
        self.core = DisciplCore()

        myLaw1 = Law("C:\\Users\\W.Meesen\\PycharmProjects\\discipl-python\\src\\Flint\\\FlintLawFiles\\CVDR642743.flint.json")
        myLaw1.parseLawJson()

        myLaw2 = Law("C:\\Users\\W.Meesen\\PycharmProjects\\discipl-python\\src\\Flint\\\FlintLawFiles\\CVDR616379.flint.json")
        myLaw2.parseLawJson()

        birthDatesChildren = []
        strDateOfBirth = '2009/10/20'
        birthDatesChildren.append(datetime.strptime(strDateOfBirth, "%Y/%m/%d"))
        strDateOfBirth = '2013/10/20'
        birthDatesChildren.append(datetime.strptime(strDateOfBirth, "%Y/%m/%d"))

        CVDR616379 = CVDR616379Flint(myLaw2, birthDatesChildren, False, 2040, 0, 1300, 1700)

        kindTest = CVDR616379.isMinderjarige(birthDatesChildren[0])

        #law, birthdatesChildren, isAlleenstaande, inkomen, totaleKinderbijslag, bijstandsnormAlleenstaande, bijstandsnormGehuwden):
        toeslagTotaal = CVDR616379.toeslag()

        CVDR616379.setPeilDatum(datetime.now())

        i = 0

# should be able to get the connector asynchronously
class TestAddTwoFloats(TestCore):
    def test_get_connector(self):

        tasks = [self.asyncAddTwoFloats()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        #loop.close()

    async def asyncAddTwoFloats(self):
        result = BigUtil.add(1, 2)
        self.assertEqual(result, 3)
        self.assertEqual(type(result), int)

class TestAddOneFloatOneBig(TestCore):
    def test_get_connector(self):
        tasks = [self.asyncAddOneFloatOneBig()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncAddOneFloatOneBig(self):
        result = BigUtil.add(1, mpf(2))
        self.assertEqual(result, 3)
        self.assertEqual(type(result), mpf)

class TestAddOneNoneOneNoNone(TestCore):
    def test_get_connector(self):
        tasks = [self.asyncAddOneNoneOneNoNone()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncAddOneNoneOneNoNone(self):
        result = BigUtil.add(None, mpf(2))
        self.assertEqual(result, None)

class TestMultiplyTwoFloats(TestCore):
    def test_get_connector(self):

        tasks = [self.asyncMultiplyTwoFloats()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        #loop.close()

    async def asyncMultiplyTwoFloats(self):
        result = BigUtil.multiply(2, 3)
        self.assertEqual(result, 6)
        self.assertEqual(type(result), int)

class TestMultiplyOneBigOneFloat(TestCore):
    def test_get_connector(self):
        tasks = [self.asyncMultiplyOneBigOneFloat()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncMultiplyOneBigOneFloat(self):
        result = BigUtil.multiply(2, mpf(3))
        self.assertEqual(result, 6)
        self.assertEqual(type(result), mpf)

class TestMultiplyOneNoneOneNoNone(TestCore):
    def test_get_connector(self):
        tasks = [self.asyncMultiplyOneNoneOneNoNone()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncMultiplyOneNoneOneNoNone(self):
        result = BigUtil.multiply(None, mpf(2))
        self.assertEqual(result, None)

class TestEqualityBigFloat(TestCore):
    def test_get_connector(self):
        tasks = [self.asyncTestEqualityBigFloat()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestEqualityBigFloat(self):
        result = BigUtil.equal(mpf(2), 2)
        self.assertEqual(result, True)

class TestLessThanBigFloat1(TestCore):
    def test_get_connector(self):
        tasks = [self.asyncTestLessThanBigFloat()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestLessThanBigFloat(self):
        result = BigUtil.lessThan(mpf(1.999), 2)
        self.assertEqual(result, True)

class TestLessThanBigFloat2(TestCore):
    def test_get_connector(self):
        tasks = [self.asyncTestLessThanBigFloat()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestLessThanBigFloat(self):
        result = BigUtil.lessThan(mpf(2.001), 2)
        self.assertEqual(result, False)