import os
import unittest
import asyncio
import json

from test.testUtils import expectCheckActionResult, runScenario, runOnModel, expectModelFact, expectModelDuty, expectModelActDetails, takeAction, expectData, expectPotentialActs, expectActiveDuties, expectAvailableActs, factResolverFactory, expectRetrievedFactFunction, takeFailingAction
from src.DisciplLawReg.Utils.FactResolver import FactResolver

class TestCore(unittest.TestCase):
    def setUp(self):
        self.i = 0

    async def mathExpressionTest(self, precondition, facts, reason = None):
        model = {
            'acts': [
              {
                'act': '<<compute mathematical expression>>',
                'actor': '[mathematician]',
                'object': '[expression]',
                'recipient': '[user]',
                'preconditions': precondition
              }
            ],
            'facts': [],
            'duties': []
            }

        completeFacts = { '[expression]': True, '[user]': True, '[mathematician]': True, **facts }

        if reason == None:
            result, test = await runScenario(
                model,
                { 'mathematician': [] },
                [
                    takeAction('mathematician', '<<compute mathematical expression>>', factResolverFactory(completeFacts))
                ])
        else:
            result, test = await runScenario(
                model,
                {'mathematician': []},
                [
                    takeFailingAction('mathematician', '<<compute mathematical expression>>',
                                             'Action <<compute mathematical expression>> is not allowed reason',
                                             factResolverFactory(completeFacts))
            ])
        self.assertEqual(test, True)


'should be able to compare numbers'
class TestScenarioMathExpression1(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestMathExpression1()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestMathExpression1(self):
        await self.mathExpressionTest({
                'expression': 'LESS_THAN',
                'operands': [
                    '[three]',
                    '[five]'
                ]},
                {
                    '[three]': 3,
                    '[five]': 5
                })

'should be able to compare literals'
class TestScenarioMathExpression2(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestMathExpression2()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestMathExpression2(self):
        await self.mathExpressionTest({
            'expression': 'LESS_THAN',
            'operands': [
                {
                    'expression': 'LITERAL',
                    'operand': 3
                },
                {
                    'expression': 'LITERAL',
                    'operand': 5
                }
            ]
            },
            {})

'should be able to compare numbers with a false result'
class TestScenarioMathExpression3(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestMathExpression3()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestMathExpression3(self):
        await self.mathExpressionTest({
            'expression': 'LESS_THAN',
            'operands': [
                '[five]',
                '[three]'
            ]
            },
            {
                '[three]': 3,
                '[five]': 5
            },
            'due to preconditions'
        )


'should be able to compare numbers equality with a false result'
class TestScenarioMathExpression4(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestMathExpression4()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestMathExpression4(self):
        await self.mathExpressionTest({
            'expression': 'EQUAL',
            'operands': [
                '[dozen]',
                '[thirteen]'
            ]
            },
            {
                '[dozen]': 12,
                '[thirteen]': 13
            },
            'due to preconditions'
            )

'should be able to add numbers'
class TestScenarioMathExpression5(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestMathExpression5()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestMathExpression5(self):
        await self.mathExpressionTest({
            'expression': 'EQUAL',
            'operands': [
                {
                    'expression': 'SUM',
                    'operands': [
                    '[three]', '[five]'
                        ]
                },
                '[eight]'
                ]
            },
            {
                '[three]': 3,
                '[five]': 5,
                '[eight]': 8
            })

'should be able to add in a list'
class TestScenarioMathExpression6(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestMathExpression6()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestMathExpression6(self):
        await self.mathExpressionTest({
            'expression': 'EQUAL',
            'operands': [
                {
                    'expression': 'SUM',
                    'operands': [
                    {
                        'expression': 'LIST',
                        'items': '[number]'
                    }
                    ]
                },
                '[eight]'
                ]
            },
            {
                '[number]': [3, 5, False],
                '[eight]': 8
            })

'should be able to add numbers with a false result'
class TestScenarioMathExpression7(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestMathExpression7()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestMathExpression7(self):
        await self.mathExpressionTest({
            'expression': 'EQUAL',
            'operands': [
                {
                    'expression': 'SUM',
                    'operands': [
                        '[three]', '[five]'
                        ]
                },
                '[nine]'
                ]
                },
                    {
                        '[three]': 3,
                        '[five]': 5,
                        '[nine]': 9
                    },
                    'due to preconditions'
                )

'should be able to determine the minimum of numbers'
class TestScenarioMathExpression8(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestMathExpression8()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestMathExpression8(self):
        await self.mathExpressionTest({
            'expression': 'EQUAL',
            'operands': [
                {
                    'expression': 'MIN',
                    'operands': [
                        '[three]', '[five]'
                    ]
                },
                '[three]'
            ]
            },
            {
                '[three]': 3,
                '[five]': 5
            })

'should be able to evaluate a literal boolean'
class TestScenarioMathExpression9(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestMathExpression9()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestMathExpression9(self):
        await self.mathExpressionTest({
            'expression': 'LITERAL',
            'operand': True
            },
            {})

'should be able to multiply in a list'
class TestScenarioMathExpression10(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestMathExpression10()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestMathExpression10(self):
        await self.mathExpressionTest({
            'expression': 'EQUAL',
            'operands': [
                {
                    'expression': 'PRODUCT',
                    'operands': [
                        {
                            'expression': 'LIST',
                            'items': '[number]'
                        }
                    ]
                },
                '[fifteen]'
                ]
            },
                {
                '[number]': [3, 5, False],
                '[fifteen]': 15
            })

'should be able to multiply numbers with arbitrary precision'
class TestScenarioMathExpression11(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestMathExpression11()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestMathExpression11(self):
        await self.mathExpressionTest({
            'expression': 'EQUAL',
            'operands': [
              {
                'expression': 'PRODUCT',
                'operands': [
                  {
                    'expression': 'LITERAL',
                    'operand': 1.15
                  }, '[400]', '[100]'
                ]
              },
              '[46000]'
            ]
          },
          {
            '[400]': 400,
            '[100]': 100,
            '[46000]': 46000
          })

'should be able to multiply numbers with a false result'
class TestScenarioMathExpression12(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestMathExpression12()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestMathExpression12(self):
        await self.mathExpressionTest({
            'expression': 'EQUAL',
            'operands': [
                {
                'expression': 'PRODUCT',
                'operands': [
                    '[three]', '[five]'
                    ]
                },
                '[fourteen]'
                ]
            },
            {
                '[three]': 3,
                '[five]': 5,
                '[fourteen]': 14
            },
        'due to preconditions'
        )

'should be able to determine the maximum of numbers'
class TestScenarioMathExpression13(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestMathExpression13()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestMathExpression13(self):
        await self.mathExpressionTest({
            'expression': 'EQUAL',
            'operands': [
            {
                'expression': 'MAX',
                'operands': [
                    '[three]', '[five]'
                ]
            },
            '[five]'
            ]
            },
            {
                '[three]': 3,
                '[five]': 5
            })

'should be able to determine the minimum of numbers'
class TestScenarioMathExpression14(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestMathExpression14()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestMathExpression14(self):
        await self.mathExpressionTest({
            'expression': 'EQUAL',
            'operands': [
            {
                'expression': 'MIN',
                'operands': [
                    '[three]', '[five]'
                ]
            },
            '[three]'
            ]
            },
            {
                '[three]': 3,
                '[five]': 5
            })

'should throw an error with unknown expressions'
class TestScenarioMathExpression15(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestMathExpression15()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestMathExpression15(self):
        try:
            await self.mathExpressionTest({
                    'expression': 'BANANAS',
                    'operands': [
                        '[three]', '[five]'
                    ]
                    },
                    {
                        '[three]': 3,
                        '[five]': 5
                    })
        except Exception as e:
            error = e

        self.assertEqual(error.args[0], 'TakeAction failed')

'should fail to compare non numeric operands'
class TestScenarioMathExpression16(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestMathExpression16()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestMathExpression16(self):
        await self.mathExpressionTest({
            'expression': 'LESS_THAN',
            'operands': [
            {
                'expression': 'LITERAL',
                'operand': False
            },
            {
                'expression': 'LITERAL',
                'operand': 5
            }
            ]
            },
            {},
            'due to preconditions')

        await self.mathExpressionTest({
            'expression': 'LESS_THAN',
            'operands': [
            {
                'expression': 'LITERAL',
                'operand': 'three'
            },
            {
                'expression': 'LITERAL',
                'operand': 5
            }
            ]
            },
            {},
            'due to preconditions')

'should support numbers in or expressions'
class TestScenarioMathExpression17(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestMathExpression17()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestMathExpression17(self):
        await self.mathExpressionTest({
            'expression': 'OR',
            'operands': [
                '[three]',
                '[five]'
            ]
            },
                {
                    '[three]': 3,
                    '[five]': 5
                }
            )
