import os
import unittest
import asyncio
import json

from src.DisciplLawReg.LawReg import LawReg
from src.DisciplLawReg.Utils.Util import Util
from src.DisciplLawReg.Utils.Identity_Util import IdentityUtil

lawReg = LawReg()
core = lawReg.getAbundanceService().getCoreAPI()

wd = os.getcwd()
path = wd + "\\flint-example-lerarenbeurs.json"
f = open(path, encoding="utf8")
lb = json.load(f)
f.close()

class belanghebbendeFactresolver1:
    def __init__(self):
        self.facts = None

    async def resolveFacts(self, fact, listNames, listIndices, possibleCreatingActions):
        if type(fact) == str:
            return fact == '[verzoek een besluit te nemen]'
        return False

class belanghebbendeFactresolver2:
    def __init__(self):
        self.facts = None

    async def resolveFacts(self, fact, listNames, listIndices, possibleCreatingActions):
        if type(fact) == str:
          return fact == '[subsidie voor studiekosten]' or \
            fact == '[ingevuld aanvraagformulier studiekosten op de website van de Dienst Uitvoering Onderwijs]' or\
            fact == '[indienen 1 april tot en met 30 juni, voorafgaand aan het studiejaar waarvoor subsidie wordt aangevraagd]'
        return False

class belanghebbendeFactresolver3:
    def __init__(self):
        self.facts = None

    async def resolveFacts(self, fact, listNames, listIndices, possibleCreatingActions):
        if type(fact) == str:
            return fact == '[aanvraagformulieren op de website van de Dienst Uitvoering Onderwijs]' or \
            fact == '[ingevuld aanvraagformulier studiekosten op de website van de Dienst Uitvoering Onderwijs]' or \
            fact == '[inleveren]' or \
            fact == '[indienen 1 april tot en met 30 juni, voorafgaand aan het studiejaar waarvoor subsidie wordt aangevraagd]'
        return False

class belanghebbendeFactresolver4:
    def __init__(self):
        self.facts = None

    async def resolveFacts(self, fact, listNames, listIndices, possibleCreatingActions):
        if type(fact) == str:
            return fact == '[aanvraagformulieren op de website van de Dienst Uitvoering Onderwijs]' or \
                fact == '[ingevuld aanvraagformulier studiekosten op de website van de Dienst Uitvoering Onderwijs]' or \
                fact == '[inleveren]' or \
                fact == '[indienen 1 april tot en met 30 juni, voorafgaand aan het studiejaar waarvoor subsidie wordt aangevraagd]' or \
                fact == '[binnen twee maanden na het verstrekken van de subsidie]'
        return False

class belanghebbendeFactresolver5:
    def __init__(self):
        self.facts = None

    async def resolveFacts(self, fact, listNames, listIndices, possibleCreatingActions):
        if type(fact) == str:
            return fact == '[ingevuld aanvraagformulier studiekosten op de website van de Dienst Uitvoering Onderwijs]' or \
                fact == '[inleveren]' or \
                fact == '[indienen 1 april tot en met 30 juni, voorafgaand aan het studiejaar waarvoor subsidie wordt aangevraagd]'
        return False

class belanghebbendeFactresolver8:
    def __init__(self):
        self.facts = None

    async def resolveFacts(self, fact, listNames, listIndices, possibleCreatingActions):
        if type(fact) == str:
            return fact == '[aanvraagformulieren op de website van de Dienst Uitvoering Onderwijs]' or \
                fact == '[ingevuld aanvraagformulier studiekosten op de website van de Dienst Uitvoering Onderwijs]' or \
                fact == '[inleveren]' or \
                fact == '[indienen 1 april tot en met 30 juni, voorafgaand aan het studiejaar waarvoor subsidie wordt aangevraagd]'
        return False

class belanghebbendeFactresolver9:
    def __init__(self):
        self.facts = None

    async def resolveFacts(self, fact, listNames, listIndices, possibleCreatingActions):
        if type(fact) == str:
            return fact == '[aanvraagformulieren op de website van de Dienst Uitvoering Onderwijs]' or \
                fact == '[ingevuld aanvraagformulier studiekosten op de website van de Dienst Uitvoering Onderwijs]' or \
                fact == '[inleveren]' or \
                fact == '[indienen 1 april tot en met 30 juni, voorafgaand aan het studiejaar waarvoor subsidie wordt aangevraagd]' or \
                fact == '[binnen twee maanden na het verstrekken van de subsidie]'
        return False

class bestuursorgaanFactresolver1:
    def __init__(self):
        self.facts = None

    async def resolveFacts(self, fact, listNames, listIndices, possibleCreatingActions):
        if type(fact) == str:
            return fact == '[persoon wiens belang rechtstreeks bij een besluit is betrokken]' or \
                   fact == '[aanvrager heeft de gelegenheid gehad de aanvraag aan te vullen]' or \
                   fact == '[de aanvraag is binnen de afgelopen 4 weken aangevuld]'
        return False


class bestuursorgaanFactresolver2:
    def __init__(self):
        self.facts = None

    async def resolveFacts(self, fact, listNames, listIndices, possibleCreatingActions):
        if type(fact) == str:
          return fact == '[subsidie lerarenbeurs]' or \
            fact == '[subsidie voor bacheloropleiding leraar]' or \
            fact == '[leraar voldoet aan bevoegdheidseisen]' or \
            fact == '[leraar die bij aanvang van het studiejaar waarvoor de subsidie bestemd de graad Bachelor mag voeren]' or \
            fact == '[leraar die op het moment van de subsidieaanvraag in dienst is bij een werkgever]' or \
            fact == '[leraar werkt bij een of meer bekostigde onderwijsinstellingen]' or \
            fact == '[leraar die voor minimaal twintig procent van zijn werktijd is belast met lesgebonden taken]' or \
            fact == '[leraar die pedagogisch-didactisch verantwoordelijk is voor het onderwijs]' or \
            fact == '[leraar die ingeschreven staat in registerleraar.nl]' or \
            fact == '[subsidie wordt verstrekt voor één studiejaar en voor één opleiding]' or \
            fact == '[minister verdeelt het beschikbare bedrag per doelgroep over de aanvragen]' or \
            fact == '[template voor aanvraagformulieren studiekosten]'
        return False


class bestuursorgaanFactresolver3:
    def __init__(self):
        self.facts = None

    async def resolveFacts(self, fact, listNames, listIndices, possibleCreatingActions):
        if type(fact) == str:
            return fact == '[template voor aanvraagformulieren op de website van de Dienst Uitvoering Onderwijs]'or \
                fact == '[schriftelijke beslissing van een bestuursorgaan]'or \
                fact == '[beslissing inhoudende een publiekrechtelijke rechtshandeling]'or \
                fact == '[subsidie lerarenbeurs]'or \
                fact == '[subsidie voor bacheloropleiding leraar]'or \
                fact == '[leraar voldoet aan bevoegdheidseisen]'or \
                fact == '[leraar werkt bij een of meer bekostigde onderwijsinstellingen]'or \
                fact == '[leraar die bij aanvang van het studiejaar waarvoor de subsidie bestemd de graad Bachelor mag voeren]'or \
                fact == '[leraar die op het moment van de subsidieaanvraag in dienst is bij een werkgever]'or \
                fact == '[leraar die voor minimaal twintig procent van zijn werktijd is belast met lesgebonden taken]'or \
                fact == '[leraar die pedagogisch-didactisch verantwoordelijk is voor het onderwijs]'or \
                fact == '[leraar die ingeschreven staat in registerleraar.nl]'or \
                fact == '[subsidie wordt verstrekt voor één studiejaar en voor één opleiding]'or \
                fact == '[minister verdeelt het beschikbare bedrag per doelgroep over de aanvragen]'or \
                fact == '[hoogte van de subsidie voor studiekosten]'or \
                fact == '[template voor aanvraagformulieren studiekosten]'

        return False

class bestuursorgaanFactresolver4:
    def __init__(self):
        self.facts = None

    async def resolveFacts(self, fact, listNames, listIndices, possibleCreatingActions):
        if type(fact) == str:
            return fact == '[template voor aanvraagformulieren op de website van de Dienst Uitvoering Onderwijs]' or \
                fact == '[schriftelijke beslissing van een bestuursorgaan]' or \
                fact == '[beslissing inhoudende een publiekrechtelijke rechtshandeling]' or \
                fact == '[subsidie lerarenbeurs]' or \
                fact == '[subsidie voor bacheloropleiding leraar]' or \
                fact == '[leraar voldoet aan bevoegdheidseisen]' or \
                fact == '[leraar werkt bij een of meer bekostigde onderwijsinstellingen]' or \
                fact == '[leraar die bij aanvang van het studiejaar waarvoor de subsidie bestemd de graad Bachelor mag voeren]' or \
                fact == '[leraar die op het moment van de subsidieaanvraag in dienst is bij een werkgever]' or \
                fact == '[leraar die voor minimaal twintig procent van zijn werktijd is belast met lesgebonden taken]' or \
                fact == '[leraar die pedagogisch-didactisch verantwoordelijk is voor het onderwijs]' or \
                fact == '[leraar die ingeschreven staat in registerleraar.nl]' or \
                fact == '[subsidie wordt verstrekt voor één studiejaar en voor één opleiding]' or \
                fact == '[minister verdeelt het beschikbare bedrag per doelgroep over de aanvragen]' or \
                fact == '[hoogte van de subsidie voor studiekosten]' or \
                fact == '[template voor aanvraagformulieren studiekosten]'
        return False

class bestuursorgaanFactresolver5:
    def __init__(self):
        self.facts = None

    async def resolveFacts(self, fact, listNames, listIndices, possibleCreatingActions):
        if type(fact) == str:
            return fact == '[template voor aanvraagformulieren op de website van de Dienst Uitvoering Onderwijs]' or \
                fact == '[schriftelijke beslissing van een bestuursorgaan]' or \
                fact == '[beslissing inhoudende een publiekrechtelijke rechtshandeling]' or \
                fact == '[subsidie lerarenbeurs]' or \
                fact == '[subsidie voor bacheloropleiding leraar]' or \
                fact == '[leraar voldoet aan bevoegdheidseisen]' or \
                fact == '[leraar werkt bij een of meer bekostigde onderwijsinstellingen]' or \
                fact == '[leraar die bij aanvang van het studiejaar waarvoor de subsidie bestemd de graad Bachelor mag voeren]' or \
                fact == '[leraar die op het moment van de subsidieaanvraag in dienst is bij een werkgever]' or \
                fact == '[leraar die voor minimaal twintig procent van zijn werktijd is belast met lesgebonden taken]' or \
                fact == '[leraar die pedagogisch-didactisch verantwoordelijk is voor het onderwijs]' or \
                fact == '[leraar die ingeschreven staat in registerleraar.nl]' or \
                fact == '[subsidie wordt verstrekt voor één studiejaar en voor één opleiding]' or \
                fact == '[minister verdeelt het beschikbare bedrag per doelgroep over de aanvragen]' or \
                fact == '[hoogte van de subsidie voor studiekosten]' or \
                fact == '[subsidieverlening aan een leraar]' or \
                fact == '[template voor aanvraagformulieren studiekosten]'
        return False

class bestuursorgaanFactresolver6:
    def __init__(self):
        self.facts = None

    async def resolveFacts(self, fact, listNames, listIndices, possibleCreatingActions):
        if type(fact) == str:
            return fact == '[template voor aanvraagformulieren studiekosten]' or fact == '[minister van Onderwijs, Cultuur en Wetenschap]'
        return False

class bestuursorgaanFactresolver7:
    def __init__(self):
        self.facts = None

    async def resolveFacts(self, fact, listNames, listIndices, possibleCreatingActions):
        if type(fact) == str:
            return fact == '[template voor aanvraagformulieren studiekosten]' or fact == '[minister van Onderwijs, Cultuur en Wetenschap]'
        return False


class bestuursorgaanFactresolver8:
    def __init__(self):
        self.facts = None

    async def resolveFacts(self, fact, listNames, listIndices, possibleCreatingActions):
        if type(fact) == str:
          return fact == '[template voor aanvraagformulieren op de website van de Dienst Uitvoering Onderwijs]' or \
            fact == '[schriftelijke beslissing van een bestuursorgaan]' or \
            fact == '[beslissing inhoudende een publiekrechtelijke rechtshandeling]' or \
            fact == '[subsidie lerarenbeurs]' or \
            fact == '[subsidie voor bacheloropleiding leraar]' or \
            fact == '[leraar voldoet aan bevoegdheidseisen]' or \
            fact == '[leraar werkt bij een of meer bekostigde onderwijsinstellingen]' or \
            fact == '[leraar die bij aanvang van het studiejaar waarvoor de subsidie bestemd de graad Bachelor mag voeren]' or \
            fact == '[leraar die op het moment van de subsidieaanvraag in dienst is bij een werkgever]' or \
            fact == '[leraar die voor minimaal twintig procent van zijn werktijd is belast met lesgebonden taken]' or \
            fact == '[leraar die pedagogisch-didactisch verantwoordelijk is voor het onderwijs]' or \
            fact == '[leraar die ingeschreven staat in registerleraar.nl]' or \
            fact == '[subsidie wordt verstrekt voor één studiejaar en voor één opleiding]' or \
            fact == '[minister verdeelt het beschikbare bedrag per doelgroep over de aanvragen]' or \
            fact == '[hoogte van de subsidie voor studiekosten]' or \
            fact == '[template voor aanvraagformulieren studiekosten]'
        return False

class bestuursorgaanFactresolver9:
    def __init__(self):
        self.facts = None

    async def resolveFacts(self, fact, listNames, listIndices, possibleCreatingActions):
        if type(fact) == str:
            return fact == '[template voor aanvraagformulieren op de website van de Dienst Uitvoering Onderwijs]' or \
            fact == '[schriftelijke beslissing van een bestuursorgaan]' or \
            fact == '[beslissing inhoudende een publiekrechtelijke rechtshandeling]' or \
            fact == '[subsidie lerarenbeurs]' or \
            fact == '[subsidie voor bacheloropleiding leraar]' or \
            fact == '[leraar voldoet aan bevoegdheidseisen]' or \
            fact == '[leraar werkt bij een of meer bekostigde onderwijsinstellingen]' or \
            fact == '[leraar die bij aanvang van het studiejaar waarvoor de subsidie bestemd de graad Bachelor mag voeren]' or \
            fact == '[leraar die op het moment van de subsidieaanvraag in dienst is bij een werkgever]' or \
            fact == '[leraar die voor minimaal twintig procent van zijn werktijd is belast met lesgebonden taken]' or \
            fact == '[leraar die pedagogisch-didactisch verantwoordelijk is voor het onderwijs]' or \
            fact == '[leraar die ingeschreven staat in registerleraar.nl]' or \
            fact == '[subsidie wordt verstrekt voor één studiejaar en voor één opleiding]' or \
            fact == '[minister verdeelt het beschikbare bedrag per doelgroep over de aanvragen]' or \
            fact == '[hoogte van de subsidie voor studiekosten]' or \
            fact == '[template voor aanvraagformulieren studiekosten]'
        return False

class TestCore(unittest.TestCase):
    def setUp(self):
        self.factFunctionSpec = {
            '[persoon wiens belang rechtstreeks bij een besluit is betrokken]': 'belanghebbende',
            '[degene die voldoet aan bevoegdheidseisen gesteld in]': 'belanghebbende',
            '[artikel 3 van de Wet op het primair onderwijs]': 'belanghebbende',
            '[artikel 3 van de Wet op de expertisecentra]': 'belanghebbende',
            '[artikel XI van de Wet op de beroepen in het onderwijs]': 'belanghebbende',
            '[artikel 3 van de Wet primair onderwijs BES]': 'belanghebbende',
            '[is benoemd of tewerkgesteld zonder benoeming als bedoeld in artikel 33 van de Wet op het voortgezet onderwijs]': 'belanghebbende',
            '[artikel 4.2.1. van de Wet educatie en beroepsonderwijs]': 'belanghebbende',
            '[artikel 80 van de Wet voortgezet onderwijs BES]': 'belanghebbende',
            '[artikel 4.2.1 van de Wet educatie beroepsonderwijs BES]': 'belanghebbende',
            '[die lesgeeft in het hoger onderwijs]': 'belanghebbende',
            '[orgaan]': 'bestuursorgaan',
            '[rechtspersoon die krachtens publiekrecht is ingesteld]': 'bestuursorgaan',
            '[met enig openbaar gezag bekleed]': 'bestuursorgaan',
            '[artikel 1 van de Wet op het primair onderwijs]': 'bevoegdGezag',
            '[artikel 1 van de Wet op de expertisecentra]': 'bevoegdGezag',
            '[artikel 1 van de Wet op het voortgezet onderwijs]': 'bevoegdGezag',
            '[artikel 1.1.1., onderdeel w, van de Wet educatie en beroepsonderwijs]': 'bevoegdGezag',
            '[artikel 1 van de Wet primair onderwijs BES]': 'bevoegdGezag',
            '[artikel 1 van de Wet voortgezet onderwijs BES]': 'bevoegdGezag',
            '[artikel 1.1.1, van de Wet educatie en beroepsonderwijs BES]': 'bevoegdGezag',
            '[instellingsbestuur bedoeld in artikel 1.1, onderdeel j, van de Wet op het hoger onderwijs en wetenschappelijk onderzoek]': 'bevoegdGezag',
            '[minister van Onderwijs, Cultuur en Wetenschap]': 'bestuursorgaan',
            '[persoon]': 'ANYONE'
        }

        self.factFunctionSpec2 = {
            '[persoon wiens belang rechtstreeks bij een besluit is betrokken]': ['belanghebbende1', 'belanghebbende2'],
            '[degene die voldoet aan bevoegdheidseisen gesteld in]': ['belanghebbende1', 'belanghebbende2'],
            '[artikel 3 van de Wet op het primair onderwijs]': ['belanghebbende1', 'belanghebbende2'],
            '[artikel 3 van de Wet op de expertisecentra]': ['belanghebbende1', 'belanghebbende2'],
            '[artikel XI van de Wet op de beroepen in het onderwijs]': ['belanghebbende1', 'belanghebbende2'],
            '[artikel 3 van de Wet primair onderwijs BES]': ['belanghebbende1', 'belanghebbende2'],
            '[is benoemd of tewerkgesteld zonder benoeming als bedoeld in artikel 33 van de Wet op het voortgezet onderwijs]': [
                'belanghebbende1', 'belanghebbende2'],
            '[artikel 4.2.1. van de Wet educatie en beroepsonderwijs]': ['belanghebbende1', 'belanghebbende2'],
            '[artikel 80 van de Wet voortgezet onderwijs BES]': ['belanghebbende1', 'belanghebbende2'],
            '[artikel 4.2.1 van de Wet educatie beroepsonderwijs BES]': ['belanghebbende1', 'belanghebbende2'],
            '[die lesgeeft in het hoger onderwijs]': ['belanghebbende1', 'belanghebbende2'],
            '[orgaan]': 'bestuursorgaan',
            '[rechtspersoon die krachtens publiekrecht is ingesteld]': 'bestuursorgaan',
            '[met enig openbaar gezag bekleed]': 'bestuursorgaan',
            '[artikel 1 van de Wet op het primair onderwijs]': 'bevoegdGezag',
            '[artikel 1 van de Wet op de expertisecentra]': 'bevoegdGezag',
            '[artikel 1 van de Wet op het voortgezet onderwijs]': 'bevoegdGezag',
            '[artikel 1.1.1., onderdeel w, van de Wet educatie en beroepsonderwijs]': 'bevoegdGezag',
            '[artikel 1 van de Wet primair onderwijs BES]': 'bevoegdGezag',
            '[artikel 1 van de Wet voortgezet onderwijs BES]': 'bevoegdGezag',
            '[artikel 1.1.1, van de Wet educatie en beroepsonderwijs BES]': 'bevoegdGezag',
            '[instellingsbestuur bedoeld in artikel 1.1, onderdeel j, van de Wet op het hoger onderwijs en wetenschappelijk onderzoek]': 'bevoegdGezag',
            '[minister van Onderwijs, Cultuur en Wetenschap]': 'bestuursorgaan',
            '[persoon]': 'ANYONE'
        }

    async def setupModel(self):
        util = Util(lawReg)
        return await util.setupModel(lb, ['belanghebbende', 'bestuursorgaan', 'bevoegdGezag'], self.factFunctionSpec, False)

    async def setupModel2(self):
        util = Util(lawReg)
        return await util.setupModel(lb, ['belanghebbende1', 'belanghebbende2', 'bestuursorgaan', 'bevoegdGezag'], self.factFunctionSpec2, False)

'should be able to take an action where the object originates from another action - LERARENBEURS'
class TestLerarenBeurs1(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestLerarenBeurs1()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestLerarenBeurs1(self):
        ssids, modelLink = await self.setupModel()

        retrievedModel = await core.get(modelLink)
        needSsid = await core.newSsid('ephemeral')

        await core.allow(needSsid)
        needLink = await core.claim(needSsid, {
            'need': {
                'act': '<<indienen verzoek een besluit te nemen>>',
                'DISCIPL_FLINT_MODEL_LINK': modelLink
            }
        })

        actionLink = await lawReg.take(ssids['belanghebbende'], needLink, '<<indienen verzoek een besluit te nemen>>',
                                     belanghebbendeFactresolver1())

        secondActionLink = await lawReg.take(ssids['bestuursorgaan'], actionLink,
                                         '<<besluiten de aanvraag niet te behandelen>>', bestuursorgaanFactresolver1())

        self.assertEqual(type(secondActionLink), str)

        action = await core.get(secondActionLink, ssids['bestuursorgaan'])

        for act in retrievedModel['claim']['data']['DISCIPL_FLINT_MODEL']['acts']:
            for key, value in act.items():
                if '<<besluiten de aanvraag niet te behandelen>>' in key:
                    expectedActLink = value
        #list(retrievedModel['claim']['data']['DISCIPL_FLINT_MODEL']['acts'].filter(item= > Object.keys(item).includes('<<besluiten de aanvraag niet te behandelen>>')))

        self.assertEqual(action['claim']['data'],{
            'DISCIPL_FLINT_ACT_TAKEN': expectedActLink,
            'DISCIPL_FLINT_FACTS_SUPPLIED': {
                '[aanvraag]': actionLink,
                '[bestuursorgaan]': IdentityUtil.identityExpression(ssids['bestuursorgaan']['did']),
                '[aanvrager heeft de gelegenheid gehad de aanvraag aan te vullen]': True,
                '[aanvrager heeft voldaan aan enig wettelijk voorschrift voor het in behandeling nemen van de aanvraag]': False,
                '[de aanvraag is binnen de afgelopen 4 weken aangevuld]': True
            },
            'DISCIPL_FLINT_GLOBAL_CASE': needLink,
            'DISCIPL_FLINT_PREVIOUS_CASE': actionLink
            })

        takenActs = await lawReg.getActions(secondActionLink, ssids['belanghebbende'])
        self.assertEqual(takenActs,
            [{'act': '<<indienen verzoek een besluit te nemen>>', 'link': actionLink},
            {'act': '<<besluiten de aanvraag niet te behandelen>>', 'link': secondActionLink}])


'should be able to determine available actions'
class TestLerarenBeurs2(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestLerarenBeurs2()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestLerarenBeurs2(self):
        ssids, modelLink = await self.setupModel()

        needSsid = await core.newSsid('ephemeral')

        await core.allow(needSsid)
        needLink = await core.claim(needSsid, {
            'need': {
                'act': '<<indienen verzoek een besluit te nemen>>',
                'DISCIPL_FLINT_MODEL_LINK': modelLink
            }
        })

        allowedActs = await lawReg.getAvailableActs(needLink, ssids['belanghebbende'],
                                                    ['[verzoek een besluit te nemen]'],
                                                    ['[bij wettelijk voorschrift is anders bepaald]'])

        for key, value in allowedActs[0].items():
            if key == 'act':
                allowedActNames = value
        self.assertEqual(allowedActNames, '<<indienen verzoek een besluit te nemen>>')

'should be able to determine available actions with nonfacts'
class TestLerarenBeurs3(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestLerarenBeurs3()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestLerarenBeurs3(self):
        ssids, modelLink = await self.setupModel()

        needSsid = await core.newSsid('ephemeral')

        await core.allow(needSsid)
        needLink = await core.claim(needSsid, {
            'need': {
                'act': '<<indienen verzoek een besluit te nemen>>',
                'DISCIPL_FLINT_MODEL_LINK': modelLink
            }
        })

        allowedActs = await lawReg.getAvailableActs(needLink, ssids['belanghebbende'], [],
                                                    ['[verzoek een besluit te nemen]'])

        allowedActNames = []
        if len(allowedActs) > 0:
            for key, value in allowedActs[0].items():
                if key == 'act':
                    allowedActNames = value

        #allowedActNames = allowedActs.map((act) = > act.act)
        self.assertEqual(allowedActNames, [])
        #expect(allowedActNames).to.deep.equal([])


'should be able to determine potentially available actions'
class TestLerarenBeurs4(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestLerarenBeurs4()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestLerarenBeurs4(self):
        ssids, modelLink = await self.setupModel()

        needSsid = await core.newSsid('ephemeral')

        await core.allow(needSsid)
        needLink = await core.claim(needSsid, {
            'need': {
                'act': '<<indienen verzoek een besluit te nemen>>',
                'DISCIPL_FLINT_MODEL_LINK': modelLink
            }
        })

        allowedActs = await lawReg.getPotentialActs(needLink, ssids['belanghebbende'], [])

        allowedActNames = []
        if len(allowedActs) > 0:
            for key, value in allowedActs[0].items():
                if key == 'act':
                    allowedActNames.append(value)
        #allowedActNames = allowedActs.map((act) = > act.act)

        self.assertEqual(allowedActNames, ['<<indienen verzoek een besluit te nemen>>'])

'should be able to determine potentially available actions with non-facts'
class TestLerarenBeurs5(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestLerarenBeurs5()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestLerarenBeurs5(self):
        ssids, modelLink = await self.setupModel()

        needSsid = await core.newSsid('ephemeral')

        await core.allow(needSsid)
        needLink = await core.claim(needSsid, {
            'need': {
                'act': '<<indienen verzoek een besluit te nemen>>',
                'DISCIPL_FLINT_MODEL_LINK': modelLink
            }
        })

        allowedActs = await lawReg.getPotentialActs(needLink, ssids['belanghebbende'], [],
                                                    ['[verzoek een besluit te nemen]'])

        allowedActNames = []
        if len(allowedActs) > 0:
            for key, value in allowedActs[0].items():
                if key == 'act':
                    allowedActNames.append(value)

        self.assertEqual(allowedActNames, [])

'should be able to determine potentially available actions from another perspective'
class TestLerarenBeurs6(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestLerarenBeurs6()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestLerarenBeurs6(self):
        ssids, modelLink = await self.setupModel()

        needSsid = await core.newSsid('ephemeral')

        await core.allow(needSsid)
        needLink = await core.claim(needSsid, {
            'need': {
                'act': '<<indienen verzoek een besluit te nemen>>',
                'DISCIPL_FLINT_MODEL_LINK': modelLink
            }
        })

        allowedActs = await lawReg.getPotentialActs(needLink, ssids['bestuursorgaan'], [])

        allowedActNames = []
        for i in range(len(allowedActs)):
            for key, value in allowedActs[i].items():
                if key == 'act':
                    allowedActNames.append(value)

        self.assertEqual(allowedActNames, [
            '<<vaststellen formulier voor verstrekken van gegevens>>',
            '<<minister treft betalingsregeling voor het terugbetalen van de subsidie voor studiekosten>>',
            '<<minister laat een of meer bepalingen van de subsidieregeling lerarenbeurs buiten toepassing>>',
            '<<minister wijkt af van een of meer bepalingen van de subsidieregeling lerarenbeurs>>',
            '<<minister van OCW verdeelt het beschikbare bedrag voor de subsidieregeling lerarenbeurs per doelgroep>>',
            '<<minister van OCW verdeelt concreet het beschikbare budget in een studiejaar per soort onderwijs>>',
            '<<minister van OCW berekent de hoogte van de subsidie voor studiekosten>>',
            '<<minister van OCW berekent de hoogte van de subsidie voor studieverlof>>',
            '<<aanvraagformulieren verstrekken voor subsidie studiekosten op de website van de DUO>>',
            '<<aanvraagformulieren verstrekken voor subsidie studieverlof op de website van de DUO>>'
        ])

'should perform multiple acts for a happy flow in the context of Lerarenbeurs'
class TestLerarenBeurs7(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestLerarenBeurs7()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestLerarenBeurs7(self):
        ssids, modelLink = await self.setupModel()

        retrievedModel = await core.get(modelLink)
        needSsid = await core.newSsid('ephemeral')

        await core.allow(needSsid)
        needLink = await core.claim(needSsid, {
            'need': {
                'act': '<<leraar vraagt subsidie voor studiekosten aan>>',
                'DISCIPL_FLINT_MODEL_LINK': modelLink
            }
        })

        actionLink = await lawReg.take(ssids['bestuursorgaan'], needLink, '<<aanvraagformulieren verstrekken voor subsidie studiekosten op de website van de DUO>>', bestuursorgaanFactresolver2())

        actionLink2 = await lawReg.take(ssids['belanghebbende'], actionLink, '<<leraar vraagt subsidie voor studiekosten aan>>', belanghebbendeFactresolver2())

        actionLink3 = await lawReg.take(ssids['bestuursorgaan'], actionLink2, '<<minister verstrekt subsidie lerarenbeurs aan leraar>>', bestuursorgaanFactresolver2())

        action = await core.get(actionLink3, ssids['bestuursorgaan'])

        expectedActLink = []
        for i in range(len(retrievedModel['claim']['data']['DISCIPL_FLINT_MODEL']['acts'])):
            for key, value in retrievedModel['claim']['data']['DISCIPL_FLINT_MODEL']['acts'][i].items():
                if '<<minister verstrekt subsidie lerarenbeurs aan leraar>>' in key:
                    expectedActLink.append(value)
        #expectedActLink = retrievedModel['claim']['data']['DISCIPL_FLINT_MODEL']['acts']
         #   .filter(item => Object.keys(item).includes('<<minister verstrekt subsidie lerarenbeurs aan leraar>>'))

        self.assertEqual(action['claim']['data'], {
            'DISCIPL_FLINT_ACT_TAKEN': expectedActLink[0],
            'DISCIPL_FLINT_FACTS_SUPPLIED': {
                '[minister van Onderwijs, Cultuur en Wetenschap]': IdentityUtil.identityExpression(ssids['bestuursorgaan']['did']),
                '[aanvraag]': actionLink2,
                '[budget volledig benut]': False,
                '[leraar die bij aanvang van het studiejaar waarvoor de subsidie bestemd de graad Bachelor mag voeren]': True,
                '[leraar die ingeschreven staat in registerleraar.nl]': True,
                '[leraar die op het moment van de subsidieaanvraag in dienst is bij een werkgever]': True,
                '[leraar die pedagogisch-didactisch verantwoordelijk is voor het onderwijs]': True,
                '[leraar die voor minimaal twintig procent van zijn werktijd is belast met lesgebonden taken]': True,
                '[leraar is aangesteld als ambulant begeleider]': False,
                '[leraar is aangesteld als intern begeleider]': False,
                '[leraar is aangesteld als remedial teacher]': False,
                '[leraar is aangesteld als zorgcoördinator]': False,
                '[leraar ontvangt van de minister een tegemoetkoming in de studiekosten voor het volgen van de opleiding]': False,
                '[leraar werkt bij een of meer bekostigde onderwijsinstellingen]': True,
                '[minister verdeelt het beschikbare bedrag per doelgroep over de aanvragen]': True,
                '[subsidie lerarenbeurs]': True,
                '[subsidie voor bacheloropleiding leraar]': True,
                '[subsidie wordt verstrekt voor één studiejaar en voor één opleiding]': True
            },
            'DISCIPL_FLINT_GLOBAL_CASE': needLink,
            'DISCIPL_FLINT_PREVIOUS_CASE': actionLink2
        })

'should perform an extended happy flow in the context of Lerarenbeurs'
class TestLerarenBeurs8(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestLerarenBeurs8()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestLerarenBeurs8(self):
        ssids, modelLink = await self.setupModel()

        retrievedModel = await core.get(modelLink)
        needSsid = await core.newSsid('ephemeral')

        await core.allow(needSsid)
        needLink = await core.claim(needSsid, {
            'need': {
                'act': '<<aanvraagformulieren lerarenbeurs verstrekken>>',
                'DISCIPL_FLINT_MODEL_LINK': modelLink
            }
        })

        actionLink = await lawReg.take(ssids['bestuursorgaan'], needLink,
                                       '<<aanvraagformulieren verstrekken voor subsidie studiekosten op de website van de DUO>>',
                                       bestuursorgaanFactresolver3())

        actionLink2 = await lawReg.take(ssids['belanghebbende'], actionLink,
                                        '<<leraar vraagt subsidie voor studiekosten aan>>', belanghebbendeFactresolver3())
        actionLink3 = await lawReg.take(ssids['bestuursorgaan'], actionLink2,
                                        '<<minister verstrekt subsidie lerarenbeurs aan leraar>>',
                                        bestuursorgaanFactresolver3())
        actionLink4 = await lawReg.take(ssids['bestuursorgaan'], actionLink3,
                                        '<<minister van OCW berekent de hoogte van de subsidie voor studiekosten>>',
                                        bestuursorgaanFactresolver3())

        thirdAction = await core.get(actionLink2, ssids['bestuursorgaan'])

        expectedThirdActLink = []
        for i in range(len(retrievedModel['claim']['data']['DISCIPL_FLINT_MODEL']['acts'])):
            for key, value in retrievedModel['claim']['data']['DISCIPL_FLINT_MODEL']['acts'][i].items():
                if '<<leraar vraagt subsidie voor studiekosten aan>>' in key:
                    expectedThirdActLink.append(value)

        #expectedThirdActLink = retrievedModel.data['DISCIPL_FLINT_MODEL'].acts
        #   .filter(item= > Object.keys(item).includes('<<leraar vraagt subsidie voor studiekosten aan>>'))

        lastAction = await core.get(actionLink4, ssids['bestuursorgaan'])

        expectedActLink = []
        for i in range(len(retrievedModel['claim']['data']['DISCIPL_FLINT_MODEL']['acts'])):
            for key, value in retrievedModel['claim']['data']['DISCIPL_FLINT_MODEL']['acts'][i].items():
                if '<<minister van OCW berekent de hoogte van de subsidie voor studiekosten>>' in key:
                    expectedActLink.append(value)

        #expectedActLink = retrievedModel.data['DISCIPL_FLINT_MODEL'].acts
        #.filter(item= > Object.keys(item).includes(
        #    '<<minister van OCW berekent de hoogte van de subsidie voor studiekosten>>'))

        self.assertEqual(thirdAction['claim']['data'], {
            'DISCIPL_FLINT_ACT_TAKEN': expectedThirdActLink[0],
            'DISCIPL_FLINT_FACTS_SUPPLIED': {
                '[leraar]': IdentityUtil.identityExpression(ssids['belanghebbende']['did']),
                '[aanvraagformulieren studiekosten op de website van de Dienst Uitvoering Onderwijs]': actionLink,
                '[indienen 1 april tot en met 30 juni, voorafgaand aan het studiejaar waarvoor subsidie wordt aangevraagd]': True,
                '[ingevuld aanvraagformulier studiekosten op de website van de Dienst Uitvoering Onderwijs]': True
            },
            'DISCIPL_FLINT_GLOBAL_CASE': needLink,
            'DISCIPL_FLINT_PREVIOUS_CASE': actionLink
        })

        self.assertEqual(lastAction['claim']['data'], {
            'DISCIPL_FLINT_ACT_TAKEN': expectedActLink[0],
            'DISCIPL_FLINT_FACTS_SUPPLIED': {
                '[minister van Onderwijs, Cultuur en Wetenschap]': IdentityUtil.identityExpression(
                    ssids['bestuursorgaan']['did']),
                '[hoogte van de subsidie voor studiekosten]': True
            },
            'DISCIPL_FLINT_GLOBAL_CASE': needLink,
            'DISCIPL_FLINT_PREVIOUS_CASE': actionLink3
        })

'should perform an extended flow where teacher withdraws request in the context of Lerarenbeurs'
class TestLerarenBeurs9(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestLerarenBeurs9()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestLerarenBeurs9(self):
        ssids, modelLink = await self.setupModel()

        retrievedModel = await core.get(modelLink)
        needSsid = await core.newSsid('ephemeral')

        await core.allow(needSsid)
        needLink = await core.claim(needSsid, {
            'need': {
                'act': '<<aanvraagformulieren lerarenbeurs verstrekken>>',
                'DISCIPL_FLINT_MODEL_LINK': modelLink
            }
        })

        actionLink = await lawReg.take(ssids['bestuursorgaan'], needLink,
                                       '<<aanvraagformulieren verstrekken voor subsidie studiekosten op de website van de DUO>>',
                                       bestuursorgaanFactresolver4())

        actionLink2 = await lawReg.take(ssids['belanghebbende'], actionLink,
                                        '<<leraar vraagt subsidie voor studiekosten aan>>', belanghebbendeFactresolver4())
        actionLink3 = await lawReg.take(ssids['belanghebbende'], actionLink2,
                                        '<<leraar trekt aanvraag subsidie voor studiekosten in>>',
                                        belanghebbendeFactresolver4())

        self.assertEqual(type(actionLink3), str)

        lastAction = await core.get(actionLink3, ssids['bestuursorgaan'])

        #expectedActLink = retrievedModel.data['DISCIPL_FLINT_MODEL'].acts
        #.filter(item= > Object.keys(item).includes('<<leraar trekt aanvraag subsidie voor studiekosten in>>'))

        expectedActLink = []
        for i in range(len(retrievedModel['claim']['data']['DISCIPL_FLINT_MODEL']['acts'])):
            for key, value in retrievedModel['claim']['data']['DISCIPL_FLINT_MODEL']['acts'][i].items():
                if '<<leraar trekt aanvraag subsidie voor studiekosten in>>' in key:
                    expectedActLink.append(value)

        self.assertEqual(lastAction['claim']['data'], {
            'DISCIPL_FLINT_ACT_TAKEN': expectedActLink[0],
            'DISCIPL_FLINT_FACTS_SUPPLIED': {
                '[leraar met aanvraag]': IdentityUtil.identityExpression(ssids['belanghebbende']['did']),
                '[aanvraag subsidie voor studiekosten]': actionLink2,
                '[aanvraag]': actionLink2,
                '[binnen twee maanden na het verstrekken van de subsidie]': True
            },
            'DISCIPL_FLINT_GLOBAL_CASE': needLink,
            'DISCIPL_FLINT_PREVIOUS_CASE': actionLink2
        })


'should perform an extended flow where minister refuses the request because they already got financing in the context of Lerarenbeurs'
class TestLerarenBeurs10(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestLerarenBeurs10()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestLerarenBeurs10(self):
        ssids, modelLink = await self.setupModel()

        retrievedModel = await core.get(modelLink)
        needSsid = await core.newSsid('ephemeral')

        await core.allow(needSsid)
        needLink = await core.claim(needSsid, {
            'need': {
                'act': '<<aanvraagformulieren lerarenbeurs verstrekken>>',
                'DISCIPL_FLINT_MODEL_LINK': modelLink
            }
        })

        actionLink = await lawReg.take(ssids['bestuursorgaan'], needLink, '<<aanvraagformulieren verstrekken voor subsidie studiekosten op de website van de DUO>>', bestuursorgaanFactresolver5())

        actionLink2 = await lawReg.take(ssids['belanghebbende'], actionLink, '<<leraar vraagt subsidie voor studiekosten aan>>', belanghebbendeFactresolver5())
        actionLink3 = await lawReg.take(ssids['bestuursorgaan'], actionLink2, '<<minister verstrekt subsidie lerarenbeurs aan leraar>>', bestuursorgaanFactresolver5())

        actionLink4 = await lawReg.take(ssids['bestuursorgaan'], actionLink3, '<<minister van OCW weigert subsidieverlening aan een leraar>>', bestuursorgaanFactresolver5())

        lastAction = await core.get(actionLink4, ssids['bestuursorgaan'])

        #const expectedActLink = retrievedModel.data['DISCIPL_FLINT_MODEL'].acts
        #.filter(item => Object.keys(item).includes('<<minister van OCW weigert subsidieverlening aan een leraar>>'))

        expectedActLink = []
        for i in range(len(retrievedModel['claim']['data']['DISCIPL_FLINT_MODEL']['acts'])):
            for key, value in retrievedModel['claim']['data']['DISCIPL_FLINT_MODEL']['acts'][i].items():
                if '<<minister van OCW weigert subsidieverlening aan een leraar>' in key:
                    expectedActLink.append(value)

        self.assertEqual(lastAction['claim']['data'], {
            'DISCIPL_FLINT_ACT_TAKEN': expectedActLink[0],
            'DISCIPL_FLINT_FACTS_SUPPLIED': {
                '[minister van Onderwijs, Cultuur en Wetenschap]': IdentityUtil.identityExpression(ssids['bestuursorgaan']['did']),
                '[besluit tot verlenen subsidie voor studiekosten van een leraar in verband met het volgen van een opleiding]': actionLink3,
                '[subsidieverlening aan een leraar]': True
            },
            'DISCIPL_FLINT_GLOBAL_CASE': needLink,
            'DISCIPL_FLINT_PREVIOUS_CASE': actionLink3
        })

'should be able to get available actions after fact created from another action'
class TestLerarenBeurs11(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestLerarenBeurs11()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestLerarenBeurs11(self):
        ssids, modelLink = await self.setupModel()

        needSsid = await core.newSsid('ephemeral')
        await core.allow(needSsid)

        needLink = await core.claim(needSsid, {
            'need': {
                'act': '<<aanvraagformulieren lerarenbeurs verstrekken>>',
                'DISCIPL_FLINT_MODEL_LINK': modelLink
            }
        })

        actionLink = await lawReg.take(ssids['bestuursorgaan'], needLink,
                                       '<<aanvraagformulieren verstrekken voor subsidie studiekosten op de website van de DUO>>',
                                       bestuursorgaanFactresolver6())

        availableActs = await lawReg.getAvailableActs(actionLink, ssids['belanghebbende'], [], [])

        self.assertEqual(availableActs, [])

'should be able to get potential actions after fact created from another action'
class TestLerarenBeurs12(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestLerarenBeurs12()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestLerarenBeurs12(self):
        ssids, modelLink = await self.setupModel()

        needSsid = await core.newSsid('ephemeral')
        await core.allow(needSsid)

        needLink = await core.claim(needSsid, {
            'need': {
                'act': '<<aanvraagformulieren lerarenbeurs verstrekken>>',
                'DISCIPL_FLINT_MODEL_LINK': modelLink
            }
        })

        actionLink = await lawReg.take(ssids['bestuursorgaan'], needLink,
                                       '<<aanvraagformulieren verstrekken voor subsidie studiekosten op de website van de DUO>>',
                                       bestuursorgaanFactresolver7())

        potentialActs = await lawReg.getPotentialActs(actionLink, ssids['belanghebbende'], [], [])

        potentialActNames = []
        for i in range(len(potentialActs)):
            for key, value in potentialActs[i].items():
                if key == "act":
                    potentialActNames.append(value)
        #potentialActNames = potentialActs.map((act) = > act.act)

        self.assertEqual(potentialActNames, [
            '<<indienen verzoek een besluit te nemen>>',
            '<<leraar vraagt subsidie voor studiekosten aan>>'
        ])

'should perform an extended happy flow in the context of Lerarenbeurs'
class TestLerarenBeurs13(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestLerarenBeurs13()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestLerarenBeurs13(self):
        ssids, modelLink = await self.setupModel2()

        retrievedModel = await core.get(modelLink)
        needSsid = await core.newSsid('ephemeral')

        await core.allow(needSsid)
        needLink = await core.claim(needSsid, {
            'need': {
                'act': '<<aanvraagformulieren lerarenbeurs verstrekken>>',
                'DISCIPL_FLINT_MODEL_LINK': modelLink
            }
        })

        actionLink = await lawReg.take(ssids['bestuursorgaan'], needLink,
                                       '<<aanvraagformulieren verstrekken voor subsidie studiekosten op de website van de DUO>>',
                                       bestuursorgaanFactresolver8())

        actionLink2 = await lawReg.take(ssids['belanghebbende1'], actionLink,
                                        '<<leraar vraagt subsidie voor studiekosten aan>>', belanghebbendeFactresolver8())
        actionLink3 = await lawReg.take(ssids['bestuursorgaan'], actionLink2,
                                        '<<minister verstrekt subsidie lerarenbeurs aan leraar>>',
                                        bestuursorgaanFactresolver8())
        actionLink4 = await lawReg.take(ssids['bestuursorgaan'], actionLink3,
                                        '<<minister van OCW berekent de hoogte van de subsidie voor studiekosten>>',
                                        bestuursorgaanFactresolver8())

        thirdAction = await core.get(actionLink2, ssids['bestuursorgaan'])

        expectedThirdActLink = []
        for i in range(len(retrievedModel['claim']['data']['DISCIPL_FLINT_MODEL']['acts'])):
            for key, value in retrievedModel['claim']['data']['DISCIPL_FLINT_MODEL']['acts'][i].items():
                if '<<leraar vraagt subsidie voor studiekosten aan>>' in key:
                    expectedThirdActLink.append(value)

        #expectedThirdActLink = retrievedModel.data['DISCIPL_FLINT_MODEL'].acts
        #.filter(item= > Object.keys(item).includes('<<leraar vraagt subsidie voor studiekosten aan>>'))

        lastAction = await core.get(actionLink4, ssids['bestuursorgaan'])

        expectedActLink = []
        for i in range(len(retrievedModel['claim']['data']['DISCIPL_FLINT_MODEL']['acts'])):
            for key, value in retrievedModel['claim']['data']['DISCIPL_FLINT_MODEL']['acts'][i].items():
                if '<<minister van OCW berekent de hoogte van de subsidie voor studiekosten>>' in key:
                    expectedActLink.append(value)

        #expectedActLink = retrievedModel.data['DISCIPL_FLINT_MODEL'].acts
        #.filter(item= > Object.keys(item).includes(
        #    '<<minister van OCW berekent de hoogte van de subsidie voor studiekosten>>'))

        self.assertEqual(thirdAction['claim']['data'], {
            'DISCIPL_FLINT_ACT_TAKEN': expectedThirdActLink[0],
            'DISCIPL_FLINT_FACTS_SUPPLIED': {
                '[leraar]': IdentityUtil.identityExpression(ssids['belanghebbende1']['did']),
                '[aanvraagformulieren studiekosten op de website van de Dienst Uitvoering Onderwijs]': actionLink,
                '[indienen 1 april tot en met 30 juni, voorafgaand aan het studiejaar waarvoor subsidie wordt aangevraagd]': True,
                '[ingevuld aanvraagformulier studiekosten op de website van de Dienst Uitvoering Onderwijs]': True
            },
            'DISCIPL_FLINT_GLOBAL_CASE': needLink,
            'DISCIPL_FLINT_PREVIOUS_CASE': actionLink
        })

        self.assertEqual(lastAction['claim']['data'], {
            'DISCIPL_FLINT_ACT_TAKEN': expectedActLink[0],
            'DISCIPL_FLINT_FACTS_SUPPLIED': {
                '[minister van Onderwijs, Cultuur en Wetenschap]': IdentityUtil.identityExpression(
                    ssids['bestuursorgaan']['did']),
                '[hoogte van de subsidie voor studiekosten]': True
            },
            'DISCIPL_FLINT_GLOBAL_CASE': needLink,
            'DISCIPL_FLINT_PREVIOUS_CASE': actionLink3
        })

'should perform an extended flow where teacher withdraws request in the context of Lerarenbeurs'
class TestLerarenBeurs14(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestLerarenBeurs14()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestLerarenBeurs14(self):
        ssids, modelLink = await self.setupModel2()

        retrievedModel = await core.get(modelLink)
        needSsid = await core.newSsid('ephemeral')

        await core.allow(needSsid)
        needLink = await core.claim(needSsid, {
            'need': {
                'act': '<<aanvraagformulieren lerarenbeurs verstrekken>>',
                'DISCIPL_FLINT_MODEL_LINK': modelLink
            }
        })

        actionLink = await lawReg.take(ssids['bestuursorgaan'], needLink,
                                       '<<aanvraagformulieren verstrekken voor subsidie studiekosten op de website van de DUO>>',
                                       bestuursorgaanFactresolver9())

        actionLink2 = await lawReg.take(ssids['belanghebbende1'], actionLink,
                                        '<<leraar vraagt subsidie voor studiekosten aan>>', belanghebbendeFactresolver9())

        availableActInfos1 = await lawReg.getAvailableActsWithResolver(actionLink2, ssids['belanghebbende2'],
                                                                   belanghebbendeFactresolver9())

        availableActs = []
        for i in range(len(availableActInfos1)):
            for key, value in availableActInfos1[i].items():
                if key == 'act':
                    availableActs.append(value)

        if '<<leraar trekt aanvraag subsidie voor studiekosten in>>' in availableActs or \
                'leraar2 should not be able to access aanvraag of leraar1' in availableActs:
            self.assertEqual(True, False)

        actionLink3 = await lawReg.take(ssids['belanghebbende1'], actionLink2,
                                        '<<leraar trekt aanvraag subsidie voor studiekosten in>>',
                                        belanghebbendeFactresolver9())

        availableActInfos2 = await lawReg.getAvailableActsWithResolver(actionLink2, ssids['belanghebbende2'],
                                                                    belanghebbendeFactresolver9())
        availableActs1 = []
        for i in range(len(availableActInfos2)):
            for key, value in availableActInfos2[i].items():
                if key == 'act':
                    availableActs1.append(value)

        if '<<leraar trekt aanvraag subsidie voor studiekosten in>>' in availableActs1 or \
                'aanvraag subsidie intrekken should destroy aanvraag' in availableActs1:
            self.assertEqual(True, False)

        self.assertEqual(type(actionLink3), str)

        lastAction = await core.get(actionLink3, ssids['bestuursorgaan'])

        expectedActLink = []
        for i in range(len(retrievedModel['claim']['data']['DISCIPL_FLINT_MODEL']['acts'])):
            for key, value in retrievedModel['claim']['data']['DISCIPL_FLINT_MODEL']['acts'][i].items():
                if key == '<<leraar trekt aanvraag subsidie voor studiekosten in>>':
                    expectedActLink.append(value)

        self.assertEqual(lastAction['claim']['data'], {
            'DISCIPL_FLINT_ACT_TAKEN': expectedActLink[0],
            'DISCIPL_FLINT_FACTS_SUPPLIED': {
                '[leraar met aanvraag]': IdentityUtil.identityExpression(ssids['belanghebbende1']['did']),
                '[aanvraag subsidie voor studiekosten]': actionLink2,
                '[aanvraag]': actionLink2,
                '[binnen twee maanden na het verstrekken van de subsidie]': True
            },
            'DISCIPL_FLINT_GLOBAL_CASE': needLink,
            'DISCIPL_FLINT_PREVIOUS_CASE': actionLink2
        })


'''' 
per wet een class
per fact/act/duty een class
'''