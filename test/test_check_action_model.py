import os
import unittest
import asyncio
import json

from test.testUtils import expectCheckActionResult, runScenario
from src.DisciplLawReg.Utils.FactResolver import FactResolver

#import * as log from 'loglevel'
#log.getLogger('disciplLawReg').setLevel('warn')


checkActionModel = {
      'acts': [{
        'act': '<<ingezetene kan verwelkomst van overheid aanvragen>>',
        'action': '[aanvragen]',
        'actor': '[aanvrager]',
        'object': '[verwelkomst]',
        'recipient': '[overheid]',
        'preconditions': '[]',
        'create': '<verwelkomen>',
        'terminate': '',
        'reference': 'art 2.1',
        'sourcetext': '',
        'explanation': '',
        'version': '2-[19980101]-[jjjjmmdd]',
        'juriconnect': 'jci1.3:c:BWBR0005537&hoofdstuk=1&titeldeel=1.1&artikel=1:3&lid=3&z=2017-03-01&g=2017-03-01'
      }],
      'facts': [{
        'explanation': '',
        'fact': '[belanghebbende]',
        'function': '[persoon wiens belang rechtstreeks bij een besluit is betrokken]',
        'reference': 'art. 1:2 lid 1 Awb',
        'version': '2-[19940101]-[jjjjmmdd]',
        'juriconnect': 'jci1.3:c:BWBR0005537&hoofdstuk=1&titeldeel=1.1&artikel=1:2&lid=1&z=2017-03-10&g=2017-03-10',
        'sourcetext': '{Onder belanghebbende wordt verstaan: degene wiens belang rechtstreeks bij een besluit is betrokken}'
      }, {
        'explanation': '',
        'fact': '[aanvrager]',
        'function': '[]',
        'reference': 'art 3:41 lid 1 Awb',
        'version': '',
        'juriconnect': '',
        'sourcetext': ''
      }, {
        'explanation': '',
        'fact': '[toezending besluit aan aanvrager]',
        'function': '[]',
        'reference': 'art 3:41 lid 1 Awb',
        'version': '',
        'juriconnect': '',
        'sourcetext': ''
      }, {
        'explanation': '',
        'fact': '[toezending besluit aan meer belanghebbenden]',
        'function': '[]',
        'reference': 'art 3:41 lid 1 Awb',
        'version': '',
        'juriconnect': '',
        'sourcetext': ''
      }, {
        'explanation': '',
        'fact': '[uitreiking besluit aan aanvrager]',
        'function': '[]',
        'reference': 'art 3:41 lid 1 Awb',
        'version': '',
        'juriconnect': '',
        'sourcetext': ''
      }, {
        'explanation': '',
        'fact': '[uitreiking besluit aan meer belanghebbenden]',
        'function': '[]',
        'reference': 'art 3:41 lid 1 Awb',
        'version': '',
        'juriconnect': '',
        'sourcetext': ''
      }],
      'duties': []
    }



class TestCore(unittest.TestCase):
    def setUp(self):
        self.i = 0

# should be able to get the connector asynchronously
class TestScenario1(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestCheckActionModel()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestCheckActionModel(self):
        result, test = await runScenario(
            checkActionModel,
            {'aanvrager': ['[aanvrager]']},
            [
                expectCheckActionResult(
                    'aanvrager',
                    '<<ingezetene kan verwelkomst van overheid aanvragen>>',
                    {
                        'invalidReasons': [],
                        'valid': True
                    },
                    FactResolver(None, True, True)
                )
            ]
        )
        self.assertEqual(test, True)

class TestScenario2(TestCore):
    def test_Check_Action_Model2(self):
        tasks = [self.asyncTestCheckActionModel2()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestCheckActionModel2(self):
        result, test = await runScenario(
            checkActionModel,
            {'aanvrager': ['[aanvrager]']},
            [
                expectCheckActionResult(
                    'aanvrager',
                    '<<ingezetene kan verwelkomst van overheid aanvragen>>',
                    {
                        'invalidReasons': ['object',
                                           'recipient'],
                        'valid': False
                    },
                    FactResolver(None, True, False)
                )
            ]
        )
        self.assertEqual(test, True)

class TestScenario3(TestCore):
    def test_Check_Action_Model3(self):
        tasks = [self.asyncTestCheckActionModel3()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestCheckActionModel3(self):
        result, test = await runScenario(
            checkActionModel,
            {'aanvrager': ['[aanvrager]']},
            [
                expectCheckActionResult(
                    'aanvrager',
                    '<<ingezetene kan verwelkomst van overheid aanvragen>>',
                    {
                        'invalidReasons': ['object'],
                        'valid': False
                    },
                    FactResolver(None, True, False),
                    True
                )
            ]
        )
        self.assertEqual(test, True)