import unittest
import asyncio
from src.Core import DisciplCore
import sys
import time
from mpmath import *

from datetime import datetime, timedelta
from src.FileWriter import *
from src.SsidObserver import *
import base64
from src.SubjectFilter import *
from src.BaseConnector.Base_Connector import BaseConnector
from src.Ephemeral.EphemeralConnector import EphemeralConnector
from src.Ephemeral.EphemeralStorage import EphemeralStorage
from src.AbundanceService.AbundanceService import AbundanceService

from src.Ephemeral.MyClient import MyClient

from src.DisciplLawReg.Utils.Array_Util import arrayToObject
from src.DisciplLawReg.Utils.Big_Util import BigUtil

EPHEMERAL_ENDPOINT = 'https://localhost:8000'
EPHEMERAL_WEBSOCKET_ENDPOINT = 'ws://localhost:8000'

class TestCore(unittest.TestCase):
    def setUp(self):
        self.core = DisciplCore()
        '''
        #arrayToObject(["hallo", "dit is", "een test"])
        result = BigUtil.add(mpf(2),3)
        print(type(result))
        print(result)

        result = BigUtil.add(2, 3)
        print(type(result))
        print(result)
        '''
        #self.server = MyServer()
        #original_stdout = sys.stdout  # Save a reference to the original standard output
"""
            #msg, dgst = encode()
            #verify(msg, dgst)
    
            #signed, verify_key_bytes = signAMessage("Attack by Dawn")
            #verifySignature(signed, verify_key_bytes)
    
            #encrypted, pkbob, skalice = signAMessageBox("Kill all humans")
            #verifySignatureBox(encrypted, pkbob, skalice)

            #someTestingWithSigningKey()
            with open('filename.txt', 'w') as f:
                sys.stdout = f  # Change the standard output to the file we created.
                message = "Python is fun"
                message_bytes = message.encode('ascii')
                base64_bytes = base64.b64encode(message_bytes)
                base64_message = base64_bytes.decode('ascii')
                
                print(message)
                print(message_bytes)
                print(base64_bytes)
                print(base64_message)

                sys.stdout = original_stdout  # Reset the standard output to its original value
"""

# should be able to get the connector asynchronously
class TestInitConnector(TestCore):
    def test_get_connector(self):

        tasks = [self.asyncTestGetConnector('ephemeral')]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        #loop.close()

    async def asyncTestGetConnector(self, connectorName):
        connector = await self.core.getConnector(connectorName)
        self.assertEqual(connector.getName(), 'ephemeral')

        connector = await self.core.getConnector(connectorName)
        self.assertEqual(connector.getName(), 'ephemeral')
        self.assertEqual(len(self.core.disciplCoreConnectors), 1)

class TestInitSSID(TestCore):
    def test_get_ssid(self):
        tasks = [self.asyncTestInitSSID()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        #loop.close()

    async def asyncTestInitSSID(self):
        ssid = await self.core.newSsid('ephemeral')

        writeToFile(ssid, "UnitTests.txt")
        appendToFile(type(ssid['privkey']), "UnitTests.txt")

        self.assertEqual(type(ssid['did']) == str, True)
        self.assertEqual(ssid['did'].startswith('did:discipl:ephemeral:'), True)


class TestCreateClaim(TestCore):
    def test_create_claim(self):
        tasks = [self.asyncTestCreateClaim()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        #loop.close()

    async def asyncTestCreateClaim(self):
        ssid = await self.core.newSsid('ephemeral')
        claimlink = await self.core.claim(ssid, {'need': 'beer'})

        writeToFile(claimlink, "UnitTests.txt")
        #print(claimlink)
        self.assertEqual(type(claimlink) == str, True)

# should be able to get a claim added through claim, with link to previous
class TestCreateTwoClaims(TestCore):
    def test_create_two_claims(self):
        tasks = [self.asyncTestCreateTwoClaims()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        #loop.close()

    async def asyncTestCreateTwoClaims(self):
        ssid = await self.core.newSsid('ephemeral')
        claimlink1 = await self.core.claim(ssid, {'need': 'beer'})
        claimlink2 = await self.core.claim(ssid, {'need': 'wine'})

        claim = await self.core.get(claimlink2, ssid)

        self.assertEqual(claim['claim']['data']['need'], 'wine')
        self.assertEqual(claim['claim']['previous'], claimlink1)

# should be able to attest to a second claim in a chain
class TestAttestToSecondClaim(TestCore):
    def test_attest_second_claim(self):
        tasks = [self.asyncTestAttestToSecondClaim()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestAttestToSecondClaim(self):
        ssid = await self.core.newSsid('ephemeral')
        await self.core.claim(ssid, {'need': 'beer'})
        claimlink2 = await self.core.claim(ssid, {'need': 'wine'})

        attestorSsid = await self.core.newSsid('ephemeral')

        attestationLink = await self.core.attest(attestorSsid, 'agree', claimlink2)
        attestation = await self.core.get(attestationLink, attestorSsid)

        self.assertEqual(attestation['claim']['data']['agree'], claimlink2)
        self.assertEqual(attestation['claim']['previous'], None)

# should be able to verify an attestation
class TestVerifyAttestation(TestCore):
    def test_attest_second_claim(self):
        tasks = [self.asyncTestVerifyAttestation()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestVerifyAttestation(self):
        ssid = await self.core.newSsid('ephemeral')
        await self.core.claim(ssid, {'need': 'beer'})
        claimlink2 = await self.core.claim(ssid, {'need': 'wine'})

        attestorSsid = await self.core.newSsid('ephemeral')
        await self.core.allow(attestorSsid)

        await self.core.attest(attestorSsid, 'agree', claimlink2)
        verifiedAttestor = await self.core.verify('agree', claimlink2, [ssid, ssid['did'], None, 'did:discipl:ephemeral:1234', attestorSsid['did']], ssid)

        self.assertEqual(verifiedAttestor, attestorSsid['did'])

# should be able to not verify an attestation of a revoked claim
class TestVerifyAttestationRevoked(TestCore):
    def test_attest_second_claim(self):
        tasks = [self.asyncTestVerifyAttestationRevoked()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestVerifyAttestationRevoked(self):
        ssid = await self.core.newSsid('ephemeral')
        claimlink1 = await self.core.claim(ssid, {'need': 'beer'})
        claimlink2 = await self.core.claim(ssid, {'need': 'wine'})

        attestorSsid = await self.core.newSsid('ephemeral')
        allowlink = await self.core.allow(attestorSsid)

        attestationLink = await self.core.attest(attestorSsid, 'agree', claimlink2)

        getattestationLink = await self.core.get(attestationLink, attestorSsid)
        prevOfattest = getattestationLink['claim']['previous']

        claimlink3 = await self.core.revoke(ssid, claimlink2)
        getclaimlink3 = await self.core.get(claimlink3, ssid)
        prevOf3 = getclaimlink3['claim']['previous']

        verifiedAttestor = await self.core.verify('agree', claimlink2, [attestorSsid['did']], ssid)

        # The first ssid that is valid and proves the attestation should be returned
        # but none such was given so it should not find any matching attestor
        self.assertEqual(verifiedAttestor, None)
        #print(verifiedAttestor)

# be able to observe
class TestObserve(TestCore):
    def test_observe(self):
        tasks = [self.asyncTestObserve()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestObserve(self):
        ssid = await self.core.newSsid('ephemeral')
        claimlink1 = await self.core.claim(ssid, {'need': 'beer'})
        await self.core.observe(ssid['did'], ssid)
        claimlink2 = await self.core.claim(ssid, {'need': 'wine'})

        claim = await self.core.getFirstClaim(ssid['did'], ssid['did'])
        self.assertEqual(claim['claim']['data'], {'need': 'wine'})

# be able to observe and subscribe
class TestObserveAndSubscribe(TestCore):
    def test_observe_and_subscribe(self):
        tasks = [self.asyncTestObserveAndSubscribe()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestObserveAndSubscribe(self):
        ssid = await self.core.newSsid('ephemeral')
        claimlink1 = await self.core.claim(ssid, {'need': 'beer'})
        await self.core.observe(ssid['did'], ssid)
        await self.core.subscribe(ssid, ssid)
        claimlink2 = await self.core.claim(ssid, {'need': 'wine'})

        notification = await self.core.getLastNotification(ssid, ssid)
        print(notification['claim']['data'])

        self.assertEqual(notification['claim']['data'], {'need': 'wine'})

# be able to observe historically
class TestObserveHistorically(TestCore):
    def test_observe_historically(self):
        tasks = [self.asyncTestObserveHistorically()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestObserveHistorically(self):
        connName = 'ephemeral'
        ssid = await self.core.newSsid(connName)
        claimlink1 = await self.core.claim(ssid, {'need': 'beer'})
        await self.core.observe(ssid['did'], ssid, {}, True)
        claimlink2 = await self.core.claim(ssid, {'need': 'wine'})

        result = await self.core.getClaimsHistorical(2, ssid, ssid)

        self.assertEqual(result[0]['claim']['data'], {'need': 'beer'})
        self.assertEqual(result[1]['claim']['data'], {'need': 'wine'})

# be able to observe historically with a filter
class TestObserveHistoricallyWithFilter(TestCore):
    def test_observe_historically_with_filter(self):
        tasks = [self.asyncTestObserveHistoricallyWithFilter()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestObserveHistoricallyWithFilter(self):
        connName = 'ephemeral'
        ssid = await self.core.newSsid(connName)
        claimlink1 = await self.core.claim(ssid, {'need': 'beer'})
        claimlink2 = await self.core.claim(ssid, {'need': 'wine'})
        claimlink3 = await self.core.claim(ssid, {'need': 'tea'})

        await self.core.observe(ssid['did'], ssid, { 'need': 'beer' }, True)

        result = await self.core.getClaimsHistorical(1, ssid, ssid)

        #print(result)
        self.assertEqual(result[0]['claim']['data'], {'need': 'beer'})

# be able to observe historically with a filter on the predicate
class TestObserveHistoricallyWithFilterOnPredicate(TestCore):
    def test_observe_historically_with_filter(self):
        tasks = [self.asyncTestObserveHistoricallyWithFilterOnPredicate()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestObserveHistoricallyWithFilterOnPredicate(self):
        connName = 'ephemeral'
        ssid = await self.core.newSsid(connName)
        claimlink1 = await self.core.claim(ssid, {'need': 'beer'})
        claimlink2 = await self.core.claim(ssid, {'desire': 'wine'})
        claimlink3 = await self.core.claim(ssid, {'need': 'wine'})

        await self.core.observe(ssid['did'], ssid, { 'desire': None }, True)

        result = await self.core.getClaimsHistorical (2, ssid, ssid)

        #print(result)
        self.assertEqual(result[0]['claim']['data'], {'desire': 'wine'})
        self.assertEqual(result[0]['claim']['previous'], claimlink1)

# should not be able to observe historically without an ssid
class TestObserveHistoricallyWithoutSsid(TestCore):
    def test_observe_historically_without_ssid(self):
        tasks = [self.asyncTestObserveHistoricallyWithoutSsid()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestObserveHistoricallyWithoutSsid(self):
        exception = None
        try:
            subjectFilter = await self.core.observe(None, {}, False)
        except Exception as e:
            exception = str(e)

        self.assertEqual(exception, 'Observe without did or connector is not supported')

# should be able to export linked verifiable claim channels given a did
class TestExportWithDid(TestCore):
    def test_export_with_did(self):
        tasks = [self.asyncTestExportWithDid()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestExportWithDid(self):
        connName = 'ephemeral'
        ssid = await self.core.newSsid(connName)
        claimlink1 = await self.core.claim(ssid, {'need': 'beer'})
        claimlink2 = await self.core.claim(ssid, {'need': 'wine'})

        allowlink = await self.core.allow(ssid)

        attestorSsid = await self.core.newSsid('ephemeral')

        attestationLink = await self.core.attest(attestorSsid, 'agree', claimlink2)
        exportedData = await self.core.exportLD(attestorSsid['did'], attestorSsid, 4, None, [], False)

        #print(exportedData)

        self.assertEqual(exportedData[attestorSsid['did']][0], { attestationLink: { 'agree': { ssid['did']: [ { claimlink2: { 'need': 'wine' } } ] } } })

# should be able to export multiple (linked) verifiable claims in a channel in order
class TestMultipleExportsWithDid(TestCore):
    def test_export_with_did(self):
        tasks = [self.asyncTestMultipleExportsWithDid()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestMultipleExportsWithDid(self):
        connName = 'ephemeral'
        ssid = await self.core.newSsid(connName)
        claimlink1 = await self.core.claim(ssid, {'need': 'beer'})
        claimlink2 = await self.core.claim(ssid, {'need': 'wine'})

        ssid2 = await self.core.newSsid(connName)
        allowlink = await self.core.allow(ssid2)
        claimlink3 = await self.core.claim(ssid2, {'need': 'water'})

        attestationLink = await self.core.attest(ssid, 'agree', claimlink3)
        exportedData = await self.core.exportLD(ssid['did'], ssid, 4, None, [], False)

        print(exportedData)

        self.assertEqual(exportedData[ssid['did']][0], { claimlink1: {'need': 'beer'}})
        self.assertEqual(exportedData[ssid['did']][1], { claimlink2: {'need': 'wine'}})
        self.assertEqual(exportedData[ssid['did']][2], { attestationLink: { 'agree': { ssid2['did']: [ { claimlink3: { 'need': 'water' } } ] } } })

# should be able to export more complex json objects as data
class TestComplexData(TestCore):
    def test_export_with_did(self):
        tasks = [self.asyncTestComplexData()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestComplexData(self):
        connName = 'ephemeral'
        ssid = await self.core.newSsid(connName)
        allowlink = await self.core.allow(ssid)
        claimlink1 = await self.core.claim(ssid, {'need': 'wine'})
        claimlink2 = await self.core.claim(ssid, [{'need': 'beer'}, claimlink1])

        ssid2 = await self.core.newSsid(connName)
        attestationLink = await self.core.attest(ssid2, 'agree', claimlink2)
        exportedData = await self.core.exportLD(ssid2['did'], ssid2, 4, None, [], False)

        self.assertEqual(exportedData[ssid2['did']][0], {attestationLink: {'agree': {ssid['did']: [{claimlink2: [{'need': 'beer'}, {ssid['did']: [{claimlink1: {'need': 'wine'}}]}]}]}}})

# should be able to import multiple verifiable claims in multiple channels in order (in ephemeral connector)
class TestImportMultipleChannels(TestCore):
    def test_import_with_did(self):
        tasks = [self.asyncTestImportMultipleChannels()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestImportMultipleChannels(self):
        connName = 'ephemeral'
        ssid = await self.core.newSsid(connName)
        ssid2 = await self.core.newSsid(connName)
        claimLink1 = await self.core.claim(ssid, {'need': 'food'})
        claimLink2 = await self.core.claim(ssid, {'match': 'link'})
        claimLink3 = await self.core.claim(ssid, {'allow': 'some'})
        claimLink4 = await self.core.claim(ssid2, {'require': 'drink'})
        claimLink5 = await self.core.claim(ssid2, {'solved': 'problem'})
        claimLink6 = await self.core.claim(ssid2, {'attendTo': 'wishes'})

        ld = await self.core.exportLD(ssid['did'], ssid, 4, None, [], False)
        ld2 = await self.core.exportLD(ssid2['did'], ssid2, 4, None, [], False)

        connector = EphemeralConnector('warn', EphemeralStorage())
        self.core.registerConnector(connName, connector)

        channelData = await self.core.exportLD(ssid['did'], ssid, 4, None, [], False)
        self.assertEqual(len(channelData[ssid['did']]), 0)

        channelData = await self.core.exportLD(ssid2['did'], ssid2, 4, None, [], False)
        self.assertEqual(len(channelData[ssid2['did']]), 0)

        result = await self.core.importLD({**ld, **ld2})
        self.assertEqual(result, True)

        channelData = await self.core.exportLD(ssid['did'], ssid, 4, None, [], False)
        self.assertEqual(channelData[ssid['did']][0][claimLink1], {'need': 'food'})
        self.assertEqual(channelData[ssid['did']][1][claimLink2], {'match': 'link'})
        self.assertEqual(channelData[ssid['did']][2][claimLink3], {'allow': 'some'})

        channelData = await self.core.exportLD(ssid2['did'], ssid2, 4, None, [], False)
        self.assertEqual(channelData[ssid2['did']][0][claimLink4], {'require': 'drink'})
        self.assertEqual(channelData[ssid2['did']][1][claimLink5], {'solved': 'problem'})
        self.assertEqual(channelData[ssid2['did']][2][claimLink6], {'attendTo': 'wishes'})


# should be able to import attested (linked) claims in multiple channels (in ephemeral connector)
class TestImportAttestedClaimsInMultipleChannels(TestCore):
    def test_import_with_did(self):
        tasks = [self.asyncTestImportAttestedClaimsInMultipleChannels()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestImportAttestedClaimsInMultipleChannels(self):
        connName = 'ephemeral'
        ssid = await self.core.newSsid('ephemeral')
        ssid2 = await self.core.newSsid('ephemeral')
        ssid3 = await self.core.newSsid('ephemeral')

        await self.core.allow(ssid)
        await self.core.allow(ssid2)

        link = await self.core.claim(ssid, {'need': 'food'})
        link2 = await self.core.claim(ssid2, {'match': link})
        link3 = await self.core.claim(ssid3, {'solved': link2})

        ld = await self.core.exportLD(link3, ssid3, 4, None, [], False)

        connector = EphemeralConnector('warn', EphemeralStorage())
        self.core.registerConnector(connName, connector)

        channelData = await self.core.exportLD(ssid['did'], ssid, 4, None, [], False)
        self.assertEqual(len(channelData[ssid['did']]), 0)
        channelData2 = await self.core.exportLD(ssid2['did'], ssid, 4, None, [], False)
        self.assertEqual(len(channelData2[ssid2['did']]), 0)
        channelData3 = await self.core.exportLD(link3, ssid, 4, None, [], False)
        self.assertEqual(channelData3, {})

        result = await self.core.importLD(ld, ssid['did'])
        self.assertEqual(result, True)

        channelData = await self.core.exportLD(ssid['did'], ssid, 4, None, [], False)
        self.assertEqual(channelData[ssid['did']][0][link], {'need': 'food'})

        channelData2 = await self.core.exportLD(ssid2['did'], ssid, 4, None, [], False)
        self.assertEqual(channelData2[ssid2['did']][0][link2], {'match': {ssid['did']: [{link: {'need': 'food'}}]}})

        channelData3 = await self.core.exportLD(link3, ssid, 4, None, [], False)
        self.assertEqual(channelData3[ssid3['did']][0][link3], {'solved': {ssid2['did']: [{link2: {'match': {ssid['did']: [{link: {'need': 'food'}}]}}}]}})

class EphemeralTestPresentAName(TestCore):
    def test_import_with_did(self):
        tasks = [self.asyncTestPresentAName()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))

    # should present a name
    async def asyncTestPresentAName(self):
        ephemeralConnector = EphemeralConnector()
        self.assertEqual(ephemeralConnector.getName(), 'ephemeral')

class EphemeralTestGenerateSsid(TestCore):
    def test_import_with_did(self):
        tasks = [self.asyncTestGenerateSsid()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))

    # should be able to generate an ssid
    async def asyncTestGenerateSsid(self):
        ephemeralConnector = EphemeralConnector()
        identity = await ephemeralConnector.newIdentity()

        self.assertEqual(type(identity['did']), str)
        self.assertEqual(len(identity['did']), 116)

class EphemeralTestDeepCopy(TestCore):
    def test_import_with_did(self):
        tasks = [self.asyncTestDeepCopy()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))

    # should return deep copy
    async def asyncTestDeepCopy(self):
        ephemeralConnector = EphemeralConnector()
        identity = await ephemeralConnector.newIdentity()

        data = {'need': 'beer', 'wants': ['one', 'two']}
        claimLink = await ephemeralConnector.claim(identity['did'], identity['privkey'], identity['signingkey'], data)

        await ephemeralConnector.claim(identity['did'], identity['privkey'], identity['signingkey'], {BaseConnector.ALLOW(): {}})

        self.assertEqual(type(claimLink), str)
        self.assertEqual(len(claimLink), 111)
    
        fetchedClaim1 = await ephemeralConnector.get(claimLink)
        fetchedClaim1['claim']['data']['wants'] = []
    
        fetchedClaim2 = await ephemeralConnector.get(claimLink)
        self.assertEqual(fetchedClaim2['claim']['data'], { 'need': 'beer', 'wants': ['one', 'two']})
        self.assertEqual(fetchedClaim2['claim']['previous'], None)

class EphemeralClaimDeepCopy(TestCore):
    def test_import_with_did(self):
        tasks = [self.asyncClaimDeepCopy()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))

    # should claim deep copy
    async def asyncClaimDeepCopy(self):
        ephemeralConnector = EphemeralConnector()
        identity = await ephemeralConnector.newIdentity()

        data = {'need': 'beer', 'wants': ['one', 'two']}
        claimLink = await ephemeralConnector.claim(identity['did'], identity['privkey'], identity['signingkey'], data)
        data['wants'] = []

        await ephemeralConnector.claim(identity['did'], identity['privkey'], identity['signingkey'],
                                       {BaseConnector.ALLOW(): {}})

        self.assertEqual(type(claimLink), str)
        self.assertEqual(len(claimLink), 111)

        claim = await ephemeralConnector.get(claimLink)

        self.assertEqual(claim['claim']['data'],{ 'need': 'beer', 'wants': ['one', 'two']})
        self.assertEqual(claim['claim']['previous'], None)

class EphemeralImportIdentityWithPublicKey(TestCore):
    def test_import_with_did(self):
        tasks = [self.asyncImportIdentityWithPublicKey()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))

    # should be able to import an identity with a public key
    async def asyncImportIdentityWithPublicKey(self):
        cert = '-----BEGIN RSA PUBLIC KEY-----\n' +\
            'MEgCQQCM3t09Mld5/LNvcIymJTtPz1jpRpqvoFnrSJIq5E2VdCuSB+xIbUEUF/cj\n' +\
            'PfTyW9/meR2Y5EcrPGQR1ybMWkD5AgMBAAE=\n' +\
            '-----END RSA PUBLIC KEY-----\n'

        ephemeralConnector = EphemeralConnector()
        identity = await ephemeralConnector.newIdentity({ 'cert': cert })

        self.assertEqual(identity['privkey'], None)
        self.assertEqual(identity['did'], 'did:discipl:ephemeral:crt:LS0tLS1CRUdJTiBSU0EgUFVCTElDIEtFWS0tLS0tCk1FZ0NRUUNNM3QwOU1sZDUvTE52Y0l5bUpUdFB6MWpwUnBxdm9GbnJTSklxNUUyVmRDdVNCK3hJYlVFVUYvY2oKUGZUeVc5L21lUjJZNUVjclBHUVIxeWJNV2tENUFnTUJBQUU9Ci0tLS0tRU5EIFJTQSBQVUJMSUMgS0VZLS0tLS0K')
        self.assertEqual(identity['metadata'],  { 'cert': cert })

class EphemeralDetectWrongSignature(TestCore):
    def test_import_with_did(self):
        tasks = [self.asyncDetectWrongSignature()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))

    # should be able to detect wrong signature when getting a claim
    async def asyncDetectWrongSignature(self):

        ephemeralClient = EphemeralStorage()

        # correct signature = 'w7Bdy4LH7e5lJOE42svYhtIxfUMQSZKZYAEBfQvPYp2hEJNwjgbrR6DOVYaj5oLmM8A9E7TM+ZC+r9QN+J31AQ=='
        reference = 'LqdPcmk2UXQsswczOsjtOazw8imOp91pBitbXHGVovmZmoNDUgmg0lvXU6ulmbHOlnWog7fwQeih/Z7T3ynPAA=='
        pubkey = 'ec:QzLsk5Y29Uslkt3zr3apDcdHAswTNip7XNupAbrZXyE=ec:v81YSQAdXs/RPrHRluvbYcAginHW0U+qm+lCplvB4qU='
        signature = 'x7Bdy4LH7e5lJOE42svYhtIxfUMQSZKZYAEBfQvPYp2hEJNwjgbrR6DOVYaj5oLmM8A9E7TM+ZC+r9QN+J31AQ=='

        self.assertEqual(await ephemeralClient.get(reference, pubkey, signature), None)

class EphemeralTestWebsocket(TestCore):
    def test_import_with_did(self):
        tasks = [self.asyncTestWebsocket()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))

    async def asyncTestWebsocket(self):
        mc = MyClient(EPHEMERAL_ENDPOINT,EPHEMERAL_WEBSOCKET_ENDPOINT)
        result = await mc.echo("Hello World!")
        self.assertEqual(result, "Hello World! this is an echo")

class EphemeralRemoveStaleIdentities(TestCore):
    def test_import_with_did(self):
        tasks = [self.asyncRemoveStaleIdentities()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))

    # should remove stale identities
    async def asyncRemoveStaleIdentities(self):
        ephemeralConnector = EphemeralConnector()
        ephemeralConnector.configure(EPHEMERAL_ENDPOINT, EPHEMERAL_WEBSOCKET_ENDPOINT)
        #await ephemeralConnector.ephemeralClient.reset()

        identity = await ephemeralConnector.newIdentity()
        claimLink = await ephemeralConnector.claim(identity['did'], identity['privkey'], identity['signingkey'], {'THIS': 'WILL_DISAPPEAR'})

        claim = await ephemeralConnector.get(claimLink, identity['did'], identity['privkey'], identity['signingkey'])
        self.assertEqual(claim['claim']['data'], {'THIS': 'WILL_DISAPPEAR'})
        self.assertEqual(claim['claim']['previous'], None)

        await ephemeralConnector.ephemeralClient.clean()

        claimAfterTimeOut = await ephemeralConnector.get(claimLink, identity['did'], identity['privkey'], identity['signingkey'])
        self.assertEqual(claimAfterTimeOut['claim']['data'], {'THIS': 'WILL_DISAPPEAR'})
        self.assertEqual(claimAfterTimeOut['claim']['previous'], None)

        await ephemeralConnector.deleteAllFromCache()

        claimAfterClearingCache = await ephemeralConnector.get(claimLink, identity['did'], identity['privkey'], identity['signingkey'])
        self.assertEqual(claimAfterClearingCache, None)

class EphemeralRemoveObserversOfStaleIdentities(TestCore):
    def test_import_with_did(self):
        tasks = [self.asyncRemoveObserversOfStaleIdentities()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))

    # should remove observers of stale identities
    async def asyncRemoveObserversOfStaleIdentities(self):
        ephemeralConnector = EphemeralConnector()
        ephemeralConnector.configure(EPHEMERAL_ENDPOINT, EPHEMERAL_WEBSOCKET_ENDPOINT)
        await ephemeralConnector.ephemeralClient.reset()

        identity = await ephemeralConnector.newIdentity()

        await ephemeralConnector.observe(None, {'some': 'filter'}, identity['did'], identity['privkey'], identity['signingkey'], False)
        await ephemeralConnector.subscribe(None, identity)

        self.assertEqual(await ephemeralConnector.getGlobalObserverCount(), 1)

        await ephemeralConnector.ephemeralClient.clean()

        self.assertEqual(await ephemeralConnector.getGlobalObserverCount(), 0)

class EphemeralTestWithAndWithoutCaching(TestCore):
    def test_import_with_did(self):
        tasks = [self.asyncTestWithAndWithoutCaching()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))

    # testing how it works with and without caching
    async def asyncTestWithAndWithoutCaching(self):
        ephemeralConnector = EphemeralConnector()
        ephemeralConnector.configure(EPHEMERAL_ENDPOINT, EPHEMERAL_WEBSOCKET_ENDPOINT)
        #await ephemeralConnector.ephemeralClient.reset()

        identity1 = await ephemeralConnector.newIdentity()
        claimLink1 = await ephemeralConnector.claim(identity1['did'], identity1['privkey'], identity1['signingkey'], {'need': 'beer'})
        claim1 = await ephemeralConnector.get(claimLink1, identity1['did'], identity1['privkey'], identity1['signingkey'])

        self.assertEqual(claim1['claim']['data'], {'need': 'beer'})

        ephemeralConnectorWithoutCaching = EphemeralConnector()
        ephemeralConnectorWithoutCaching.configure(EPHEMERAL_ENDPOINT, EPHEMERAL_WEBSOCKET_ENDPOINT, False)
        await ephemeralConnectorWithoutCaching.ephemeralClient.reset()

        identity2 = await ephemeralConnectorWithoutCaching.newIdentity()
        claimLink2 = await ephemeralConnectorWithoutCaching.claim(identity2['did'], identity2['privkey'], identity2['signingkey'],
                                                    {'need': 'wine'})
        claim2 = await ephemeralConnectorWithoutCaching.get(claimLink2, identity2['did'], identity2['privkey'],
                                              identity2['signingkey'])

        self.assertEqual(claim2['claim']['data'], {'need': 'wine'})

        await ephemeralConnector.ephemeralClient.clean()
        await ephemeralConnectorWithoutCaching.ephemeralClient.clean()

        claim1 = await ephemeralConnector.get(claimLink1, identity1['did'], identity1['privkey'], identity1['signingkey'])
        self.assertEqual(claim1['claim']['data'], {'need': 'beer'})

        claim2 = await ephemeralConnectorWithoutCaching.get(claimLink2, identity2['did'], identity2['privkey'], identity2['signingkey'])
        self.assertEqual(claim2, None)

class EphemeralTestClaimSomething(TestCore):
    def test_import_with_did(self):
        tasks = [self.asyncTestClaimSomething()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))

    # should be able to claim something
    async def asyncTestClaimSomething(self):
        ephemeralConnector = EphemeralConnector()
        ephemeralConnector.configure(EPHEMERAL_ENDPOINT, EPHEMERAL_WEBSOCKET_ENDPOINT)
        await ephemeralConnector.ephemeralClient.reset()
        await ephemeralConnector.ephemeralClient.clean()

        identity = await ephemeralConnector.newIdentity()
        claimLink = await ephemeralConnector.claim(identity['did'], identity['privkey'], identity['signingkey'], {'need': 'beer'})
        claimLink2 = await ephemeralConnector.claim(identity['did'], identity['privkey'], identity['signingkey'], {'need': 'wine'})

        self.assertEqual(type(claimLink), str)

        claim = await ephemeralConnector.get(claimLink, identity['did'], identity['privkey'], identity['signingkey'])

        self.assertEqual(claim['claim']['data'], {'need': 'beer'})
        self.assertEqual(claim['claim']['previous'], None)

        # should be able to claim multiple things

class EphemeralTestClaimMultipleNoServer(TestCore):
    def test_import_with_did(self):
        tasks = [self.asyncTestClaimMultipleNoServer()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))

    async def asyncTestClaimMultipleNoServer(self):
        ssid = await self.core.newSsid('ephemeral')
        claimlink1 = await self.core.claim(ssid, {'need': 'beer'})
        claimlink2 = await self.core.claim(ssid, {'need': 'wine'})

        await self.core.claim(ssid, {BaseConnector.ALLOW(): {}})

        claim = await self.core.get(claimlink1)

        self.assertEqual(claim['claim']['data'], {'need': 'beer'})
        self.assertEqual(claim['claim']['previous'], None)

class EphemeralTestClaimMultiple(TestCore):
    def test_import_with_did(self):
        tasks = [self.asyncTestClaimMultiple()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))

    # should be able to claim multiple things
    async def asyncTestClaimMultiple(self):
        ephemeralConnector = EphemeralConnector()
        ephemeralConnector.configure(EPHEMERAL_ENDPOINT, EPHEMERAL_WEBSOCKET_ENDPOINT)
        await ephemeralConnector.ephemeralClient.reset()
        await ephemeralConnector.ephemeralClient.clean()

        identity = await ephemeralConnector.newIdentity()
        claimLink = await ephemeralConnector.claim(identity['did'], identity['privkey'], identity['signingkey'], {'need': 'beer'})
        claimLink2 = await ephemeralConnector.claim(identity['did'], identity['privkey'], identity['signingkey'], {'need': 'wine'})
        claimLink3 = await ephemeralConnector.claim(identity['did'], identity['privkey'], identity['signingkey'], {BaseConnector.ALLOW(): None})

        self.assertEqual(type(claimLink), str)

        claim = await ephemeralConnector.get(claimLink, identity['did'], identity['privkey'], identity['signingkey'])

        self.assertEqual(claim['claim']['data'], {'need': 'beer'})
        self.assertEqual(claim['claim']['previous'], None)

        claim2 = await ephemeralConnector.get(claimLink2, identity['did'], identity['privkey'], identity['signingkey'])

        self.assertEqual(claim2['claim']['data'], {'need': 'wine'})
        self.assertEqual(claim2['claim']['previous'], claimLink)

class EphemeralTestClaimSameThingTwice(TestCore):
    def test_import_with_did(self):
        tasks = [self.asyncTestClaimSameThingTwice()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))

    # should be stable if the same thing is claimed twice
    async def asyncTestClaimSameThingTwice(self):
        ephemeralConnector = EphemeralConnector()
        ephemeralConnector.configure(EPHEMERAL_ENDPOINT, EPHEMERAL_WEBSOCKET_ENDPOINT)
        await ephemeralConnector.ephemeralClient.reset()
        await ephemeralConnector.ephemeralClient.clean()

        identity = await ephemeralConnector.newIdentity()
        claimLink = await ephemeralConnector.claim(identity['did'], identity['privkey'], identity['signingkey'], {'need': 'beer'})

        self.assertEqual(type(claimLink), str)

        claim = await ephemeralConnector.get(claimLink, identity['did'], identity['privkey'], identity['signingkey'])

        self.assertEqual(claim['claim']['data'], {'need': 'beer'})
        self.assertEqual(claim['claim']['previous'], None)

        claimLink2 = await ephemeralConnector.claim(identity['did'], identity['privkey'], identity['signingkey'],
                                                   {'need': 'beer'})

        self.assertEqual(type(claimLink2), str)

        claim2 = await ephemeralConnector.get(claimLink2, identity['did'], identity['privkey'], identity['signingkey'])

        self.assertEqual(claim2['claim']['data'], {'need': 'beer'})
        self.assertEqual(claim2['claim']['previous'], None)

        self.assertEqual(claimLink, claimLink2)

class EphemeralTestClaimSomethingComplicated(TestCore):
    def test_import_with_did(self):
        tasks = [self.asyncTestClaimSomethingComplicated()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))

    # should be able to claim something complicated
    async def asyncTestClaimSomethingComplicated(self):
        ephemeralConnector = EphemeralConnector()
        ephemeralConnector.configure(EPHEMERAL_ENDPOINT, EPHEMERAL_WEBSOCKET_ENDPOINT)
        await ephemeralConnector.ephemeralClient.reset()
        await ephemeralConnector.ephemeralClient.clean()

        identity = await ephemeralConnector.newIdentity()
        claimLink = await ephemeralConnector.claim(identity['did'], identity['privkey'], identity['signingkey'],
                                                   {'need': {'for' : 'speed'}})

        self.assertEqual(type(claimLink), str)

        await ephemeralConnector.claim(identity['did'], identity['privkey'], identity['signingkey'], {BaseConnector.ALLOW(): {}})

        claim = await ephemeralConnector.get(claimLink, identity['did'], identity['privkey'],
                                             identity['signingkey'])

        self.assertEqual(claim['claim']['data'], {'need': {'for' : 'speed'}})
        self.assertEqual(claim['claim']['previous'], None)

class EphemeralTestNotBeingAbleToClaimWithWrongKey(TestCore):
    def test_import_with_did(self):
        tasks = [self.asyncTestNotBeingAbleToClaimWithWrongKey()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))

    # should not be able to claim something with a wrong key
    async def asyncTestNotBeingAbleToClaimWithWrongKey(self):
        ephemeralConnector = EphemeralConnector()
        ephemeralConnector.configure(EPHEMERAL_ENDPOINT, EPHEMERAL_WEBSOCKET_ENDPOINT)
        await ephemeralConnector.ephemeralClient.reset()
        await ephemeralConnector.ephemeralClient.clean()

        identity = await ephemeralConnector.newIdentity()
        wrongIdentity = await ephemeralConnector.newIdentity()

        claimLink = await ephemeralConnector.claim(identity['did'], wrongIdentity['privkey'], wrongIdentity['signingkey'],
                                                   {'need': 'beer'})

        self.assertEqual(claimLink, None)

class EphemeralTestNotBeingAbleToAccessClaimWithWrongKey(TestCore):
    def test_import_with_did(self):
        tasks = [self.asyncTestNotBeingAbleToAccessClaimWithWrongKey()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))

    # should not be able to access a claim with a wrong key
    async def asyncTestNotBeingAbleToAccessClaimWithWrongKey(self):
        ephemeralConnector = EphemeralConnector()
        ephemeralConnector.configure(EPHEMERAL_ENDPOINT, EPHEMERAL_WEBSOCKET_ENDPOINT)
        await ephemeralConnector.ephemeralClient.reset()
        await ephemeralConnector.ephemeralClient.clean()

        identity = await ephemeralConnector.newIdentity()
        wrongIdentity = await ephemeralConnector.newIdentity()

        claimLink = await ephemeralConnector.claim(identity['did'], identity['privkey'],
                                                   identity['signingkey'],
                                                   {'need': 'beer'})

        claim = await ephemeralConnector.get(claimLink, identity['did'], wrongIdentity['privkey'],
                                             wrongIdentity['signingkey'])

        self.assertEqual(claim, None)
        # should not be able to access a claim with a wrong key

class EphemeralTestBeingAbleToGrantAccessToClaim(TestCore):
    def test_import_with_did(self):
        tasks = [self.asyncTestBeingAbleToGrantAccessToClaim()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))

    # should be able to claim something and grant a specific did access to the claim
    async def asyncTestBeingAbleToGrantAccessToClaim(self):
        ephemeralConnector = EphemeralConnector()
        ephemeralConnector.configure(EPHEMERAL_ENDPOINT, EPHEMERAL_WEBSOCKET_ENDPOINT)
        #await ephemeralConnector.ephemeralClient.reset()
        #await ephemeralConnector.ephemeralClient.clean()

        identity = await ephemeralConnector.newIdentity()
        accessorIdentity = await ephemeralConnector.newIdentity()

        claimLink = await ephemeralConnector.claim(identity['did'], identity['privkey'],
                                                   identity['signingkey'],
                                                   {'need': 'beer'})

        # Check that without authentication it is not possible to obtain the claim
        claim = await ephemeralConnector.get(claimLink, accessorIdentity['did'], accessorIdentity['privkey'], accessorIdentity['signingkey'])
        self.assertEqual(claim, None)

        allowClaimLink = await ephemeralConnector.claim(identity['did'], identity['privkey'], identity['signingkey'],
                                                        {BaseConnector.ALLOW(): {'scope': claimLink,
                                                                                 'did': accessorIdentity['did']}})

        claim = await ephemeralConnector.get(claimLink, accessorIdentity['did'], accessorIdentity['privkey'], accessorIdentity['signingkey'])

        self.assertEqual(claim['claim']['data'], {'need': 'beer'})
        self.assertEqual(claim['claim']['previous'], None)

        allowClaim = await ephemeralConnector.get(allowClaimLink, accessorIdentity['did'], accessorIdentity['privkey'], accessorIdentity['signingkey'])
        self.assertEqual(allowClaim, None)

class EphemeralTestBeingAbleToGrantAccessToChannel(TestCore):
    def test_import_with_did(self):
        tasks = [self.asyncTestBeingAbleToGrantAccessToChannel()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))

    # should be able to claim something and grant a specific did access to the channel
    async def asyncTestBeingAbleToGrantAccessToChannel(self):
        ephemeralConnector = EphemeralConnector()
        ephemeralConnector.configure(EPHEMERAL_ENDPOINT, EPHEMERAL_WEBSOCKET_ENDPOINT)

        identity = await ephemeralConnector.newIdentity()
        accessorIdentity = await ephemeralConnector.newIdentity()

        claimLink = await ephemeralConnector.claim(identity['did'], identity['privkey'],
                                                   identity['signingkey'],
                                                   {'need': 'beer'})

        # Check that without authentication it is not possible to obtain the claim
        claim = await ephemeralConnector.get(claimLink, accessorIdentity['did'], accessorIdentity['privkey'],
                                             accessorIdentity['signingkey'])
        self.assertEqual(claim, None)

        allowClaimLink = await ephemeralConnector.claim(identity['did'], identity['privkey'],\
                                                   identity['signingkey'], {\
            BaseConnector.ALLOW(): {'did': accessorIdentity['did'], 'datetime': datetime.now() - timedelta(hours=1)}})

        authorizedClaim = await ephemeralConnector.get(claimLink, accessorIdentity['did'], accessorIdentity['privkey'],
                                                   accessorIdentity['signingkey'])
        self.assertEqual(authorizedClaim['claim']['data'], {'need': 'beer'})
        self.assertEqual(authorizedClaim['claim']['previous'], None)

class EphemeralTestGettingReferenceToLastClaim(TestCore):
    def test_import_with_did(self):
        tasks = [self.asyncTestGettingReferenceToLastClaim()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))

    # should be able to obtain a reference to the last claim
    async def asyncTestGettingReferenceToLastClaim(self):
        ephemeralConnector = EphemeralConnector()
        ephemeralConnector.configure(EPHEMERAL_ENDPOINT, EPHEMERAL_WEBSOCKET_ENDPOINT)

        identity = await ephemeralConnector.newIdentity()

        allowClaimLink = await ephemeralConnector.claim(identity['did'], identity['privkey'], identity['signingkey'], {BaseConnector.ALLOW(): {}})

        claimLink = await ephemeralConnector.claim(identity['did'], identity['privkey'],
                                                   identity['signingkey'],
                                                   {'need': 'beer'})

        latestClaimLink = await ephemeralConnector.getLatestClaim(identity['did'])

        self.assertEqual(latestClaimLink['claim']['data'], {'need': 'beer'})
        self.assertEqual(latestClaimLink['claim']['previous'], allowClaimLink)

class EphemeralTestGettingDidOfAClaimLink(TestCore):
    def test_import_with_did(self):
        tasks = [self.asyncTestGettingDidOfAClaimLink()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))

    # should be able to get the ssid from a claim reference
    async def asyncTestGettingDidOfAClaimLink(self):
        ephemeralConnector = EphemeralConnector()
        ephemeralConnector.configure(EPHEMERAL_ENDPOINT, EPHEMERAL_WEBSOCKET_ENDPOINT)

        identity = await ephemeralConnector.newIdentity()

        allowClaimLink = await ephemeralConnector.claim(identity['did'], identity['privkey'], identity['signingkey'], {BaseConnector.ALLOW(): {}})

        claimLink = await ephemeralConnector.claim(identity['did'], identity['privkey'],
                                                   identity['signingkey'],
                                                   {'need': 'beer'})

        claimLinkDid = await ephemeralConnector.getDidOfClaim(claimLink)

        self.assertEqual(claimLinkDid, identity['did'])

class EphemeralTestCLaimAndListenToConnector(TestCore):
    def test_import_with_did(self):
        tasks = [self.asyncTestCLaimAndListenToConnector()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))

    # should be able to claim something and listen to the connector
    async def asyncTestCLaimAndListenToConnector(self):
        ephemeralConnector = EphemeralConnector()
        ephemeralConnector.configure(EPHEMERAL_ENDPOINT, EPHEMERAL_WEBSOCKET_ENDPOINT)

        identity = await ephemeralConnector.newIdentity()

        allowClaimLink = await ephemeralConnector.claim(identity['did'], identity['privkey'],
                                                        identity['signingkey'], {BaseConnector.ALLOW(): {}})

        await ephemeralConnector.observe(identity['did'])

        claimLink = await ephemeralConnector.claim(identity['did'], identity['privkey'],
                                                   identity['signingkey'],
                                                   {'need': 'beer'})

        claims = await ephemeralConnector.getObservedClaims(1, identity)

        self.assertEqual(claims[0]['claim']['data'], {'need': 'beer'})
        self.assertEqual(claims[0]['claim']['previous'], allowClaimLink)

class EphemeralTestClaimsAndListenToConnectorForMultipleClaims(TestCore):
    def test_import_with_did(self):
        tasks = [self.asyncTestClaimsAndListenToConnectorForMultipleClaims()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))

    # should be able to claim something and listen to the connector for multiple claims
    async def asyncTestClaimsAndListenToConnectorForMultipleClaims(self):
        ephemeralConnector = EphemeralConnector()
        ephemeralConnector.configure(EPHEMERAL_ENDPOINT, EPHEMERAL_WEBSOCKET_ENDPOINT)

        identity = await ephemeralConnector.newIdentity()

        allowClaimLink = await ephemeralConnector.claim(identity['did'], identity['privkey'],
                                                        identity['signingkey'], {BaseConnector.ALLOW(): {}})

        await ephemeralConnector.observe(identity['did'])

        claimLink1 = await ephemeralConnector.claim(identity['did'], identity['privkey'],
                                                   identity['signingkey'],
                                                   {'need': 'beer'})
        claimLink2 = await ephemeralConnector.claim(identity['did'], identity['privkey'],
                                                    identity['signingkey'],
                                                    {'need': 'wine'})
        claims = await ephemeralConnector.getObservedClaims(2, identity)

        self.assertEqual(claims[0]['claim']['data'], {'need': 'beer'})
        self.assertEqual(claims[0]['claim']['previous'], allowClaimLink)
        self.assertEqual(claims[1]['claim']['data'], {'need': 'wine'})
        self.assertEqual(claims[1]['claim']['previous'], claimLink1)

class EphemeralTestAccessOnObservedClaims(TestCore):
    def test_import_with_did(self):
        tasks = [self.asyncTestAccessOnObservedClaims()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))

    # should be able to control access on observed claims
    async def asyncTestAccessOnObservedClaims(self):
        ephemeralConnector = EphemeralConnector()
        ephemeralConnector.configure(EPHEMERAL_ENDPOINT, EPHEMERAL_WEBSOCKET_ENDPOINT)

        identity = await ephemeralConnector.newIdentity()


        await ephemeralConnector.observe(identity['did'])

        claimLink1 = await ephemeralConnector.claim(identity['did'], identity['privkey'],
                                                   identity['signingkey'],
                                                   {'need': 'beer'})
        allowClaimLink = await ephemeralConnector.claim(identity['did'], identity['privkey'],
                                                        identity['signingkey'], {BaseConnector.ALLOW(): {}})
        claimLink2 = await ephemeralConnector.claim(identity['did'], identity['privkey'],
                                                    identity['signingkey'],
                                                    {'need': 'wine'})
        claims = await ephemeralConnector.getObservedClaims(3, identity)

        self.assertEqual(claims[0]['claim']['data'], {BaseConnector.ALLOW(): {}})
        self.assertEqual(claims[0]['claim']['previous'], claimLink1)
        self.assertEqual(claims[1]['claim']['data'], {'need': 'wine'})
        self.assertEqual(claims[1]['claim']['previous'], allowClaimLink)

class EphemeralTestAccessOnObservedClaimsWithDid(TestCore):
    def test_import_with_did(self):
        tasks = [self.asyncTestAccessOnObservedClaimsWithDid()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))

    # should be able to control access on observed claims to a specific did
    async def asyncTestAccessOnObservedClaimsWithDid(self):
        ephemeralConnector = EphemeralConnector()
        ephemeralConnector.configure(EPHEMERAL_ENDPOINT, EPHEMERAL_WEBSOCKET_ENDPOINT)

        identity = await ephemeralConnector.newIdentity()
        accessorIdentity = await ephemeralConnector.newIdentity()

        await ephemeralConnector.observe(identity['did'], {'need': None}, accessorIdentity['did'], accessorIdentity['privkey'], accessorIdentity['signingkey'])

        claimLink = await ephemeralConnector.claim(identity['did'], identity['privkey'], identity['signingkey'], {'need': 'beer'})

        claim1 = await ephemeralConnector.get(claimLink, accessorIdentity['did'], accessorIdentity['privkey'],
                                              accessorIdentity['signingkey'])


        allowClaimLink = await ephemeralConnector.claim(identity['did'], identity['privkey'],
            identity['signingkey'], {BaseConnector.ALLOW(): {'did': accessorIdentity['did']}})

        claimLink2 = await ephemeralConnector.claim(identity['did'], identity['privkey'], identity['signingkey'], {'need': 'wine'})

        claims = await ephemeralConnector.getObservedClaims(3, identity, accessorIdentity)

        self.assertEqual(claims[0]['claim']['data'], {'need': 'wine'})
        self.assertEqual(claims[0]['claim']['previous'], allowClaimLink)

        claim1 = await ephemeralConnector.get(claimLink, accessorIdentity['did'], accessorIdentity['privkey'], accessorIdentity['signingkey'])
        claim2 = await ephemeralConnector.get(claimLink, identity['did'], identity['privkey'],
                                        identity['signingkey'])

        self.assertEqual(claim1, None)
        self.assertEqual(claim2['claim']['data'], {'need': 'beer'})

class EphemeralTestShouldNotObserveClaimsWithWrongKey(TestCore):
    def test_import_with_did(self):
        tasks = [self.asyncTestShouldNotObserveClaimsWithWrongKey()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))

    # should not be able to observe claims with a faulty key
    async def asyncTestShouldNotObserveClaimsWithWrongKey(self):
        ephemeralConnector = EphemeralConnector()
        ephemeralConnector.configure(EPHEMERAL_ENDPOINT, EPHEMERAL_WEBSOCKET_ENDPOINT)

        identity = await ephemeralConnector.newIdentity()
        wrongIdentity = await ephemeralConnector.newIdentity()

        observeResult = await ephemeralConnector.observe(identity['did'], {}, identity['did'], identity['privkey'], identity['signingkey'])

        self.assertEqual(observeResult, True)

        observeResult = await ephemeralConnector.observe(identity['did'], {}, identity['did'], wrongIdentity['privkey'],
                                                         wrongIdentity['signingkey'])

        self.assertEqual(observeResult, False)

class EphemeralTestShouldImportClaimWithReferenceAndImportUnderSameClaimId(TestCore):
    def test_import_with_did(self):
        tasks = [self.asyncTestShouldImportClaimWithReferenceAndImportUnderSameClaimId()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))

    # should be able to import a claim using the signature from reference and importing it under same claim id
    async def asyncTestShouldImportClaimWithReferenceAndImportUnderSameClaimId(self):
        ephemeralConnector = EphemeralConnector()
        ephemeralConnector.configure(EPHEMERAL_ENDPOINT, EPHEMERAL_WEBSOCKET_ENDPOINT)

        identity = await ephemeralConnector.newIdentity()
        claimLink = await ephemeralConnector.claim(identity['did'], identity['privkey'], identity['signingkey'],
                                                   {'need': 'beer'})

        allowClaimLink = await ephemeralConnector.claim(identity['did'], identity['privkey'],
                                                        identity['signingkey'], {BaseConnector.ALLOW(): {}})

        claim = await ephemeralConnector.get(claimLink)
        self.assertEqual(claim['claim']['data'], {'need': 'beer'})

        # Purposefully create local-memory connector
        ephemeralConnector = EphemeralConnector('warn', EphemeralStorage())
        identity2 = await ephemeralConnector.newIdentity({ 'did': identity['did'], 'privkey': identity['privkey'], 'signingkey': identity['signingkey']})

        await ephemeralConnector.claim(identity['did'], identity['privkey'], identity['signingkey'], { BaseConnector.ALLOW(): {} })
        claimLink2 = await ephemeralConnector.get(claimLink)
        self.assertEqual(claimLink2, None)

        result = await ephemeralConnector.importConnector(identity['did'], claimLink, claim['claim']['data'])
        self.assertEqual(claimLink, result)
        c = await ephemeralConnector.get(result)
        self.assertEqual(c['claim']['data'], { 'need': 'beer' })

class EphemeralTestShouldImportClaimWithReferenceAndImportUnderOriginalOwner(TestCore):
    def test_import_with_did(self):
        tasks = [self.asyncTestShouldImportClaimWithReferenceAndImportUnderOriginalOwner()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))

    # should be able to import a claim using the signature from reference and be able to be accessed by the original owner
    async def asyncTestShouldImportClaimWithReferenceAndImportUnderOriginalOwner(self):
        ephemeralConnector = EphemeralConnector()
        ephemeralConnector.configure(EPHEMERAL_ENDPOINT, EPHEMERAL_WEBSOCKET_ENDPOINT)

        identity = await ephemeralConnector.newIdentity()
        claimLink = await ephemeralConnector.claim(identity['did'], identity['privkey'], identity['signingkey'],
                                                   {'need': 'beer'})

        claim = await ephemeralConnector.get(claimLink, identity['did'], identity['privkey'], identity['signingkey'])
        self.assertEqual(claim['claim']['data'], {'need': 'beer'})

        # Purposefully create local-memory connector
        ephemeralConnector = EphemeralConnector('warn', EphemeralStorage())
        identity2 = await ephemeralConnector.newIdentity({ 'did': identity['did'], 'privkey': identity['privkey'], 'signingkey': identity['signingkey']})

        claimLink2 = await ephemeralConnector.get(claimLink)
        self.assertEqual(claimLink2, None)

        result = await ephemeralConnector.importConnector(identity['did'], claimLink, claim['claim']['data'])
        self.assertEqual(claimLink, result)
        c = await ephemeralConnector.get(result, identity['did'], identity['privkey'], identity['signingkey'])
        self.assertEqual(c['claim']['data'], { 'need': 'beer' })

class EphemeralTestShouldImportClaimWithReferenceAndImportAccessedByImporter(TestCore):
    def test_import_with_did(self):
        tasks = [self.asyncTestShouldImportClaimWithReferenceAndImportAccessedByImporter()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))

    # should be able to import a claim using the signature from reference and be able to be accessed by the importer
    async def asyncTestShouldImportClaimWithReferenceAndImportAccessedByImporter(self):
        ephemeralConnector = EphemeralConnector()
        ephemeralConnector.configure(EPHEMERAL_ENDPOINT, EPHEMERAL_WEBSOCKET_ENDPOINT)

        identity = await ephemeralConnector.newIdentity()
        claimLink = await ephemeralConnector.claim(identity['did'], identity['privkey'], identity['signingkey'],
                                                   {'need': 'beer'})

        claim = await ephemeralConnector.get(claimLink, identity['did'], identity['privkey'], identity['signingkey'])
        self.assertEqual(claim['claim']['data'], {'need': 'beer'})

        # Purposefully create local-memory connector
        ephemeralConnector = EphemeralConnector('warn', EphemeralStorage())
        identity2 = await ephemeralConnector.newIdentity({ 'did': identity['did'], 'privkey': identity['privkey'], 'signingkey': identity['signingkey']})

        claimLink2 = await ephemeralConnector.get(claimLink)
        self.assertEqual(claimLink2, None)

        importerIdentity = await ephemeralConnector.newIdentity()

        result = await ephemeralConnector.importConnector(identity['did'], claimLink, claim['claim']['data'], importerIdentity['did'])
        c = await ephemeralConnector.get(result, importerIdentity['did'], importerIdentity['privkey'], importerIdentity['signingkey'])
        self.assertEqual(result, claimLink)
        self.assertEqual(c['claim']['data'], { 'need': 'beer' })

class EphemeralTestShouldObserveConnectorWide(TestCore):
    def test_import_with_did(self):
        tasks = [self.asyncTestShouldObserveConnectorWide()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))

    # should be able to observe connector-wide
    async def asyncTestShouldObserveConnectorWide(self):
        ephemeralConnector = EphemeralConnector()
        ephemeralConnector.configure(EPHEMERAL_ENDPOINT, EPHEMERAL_WEBSOCKET_ENDPOINT)

        identity = await ephemeralConnector.newIdentity()
        observeResult = await ephemeralConnector.observe(None, {'need': 'beer'})

        allowClaimLink = await ephemeralConnector.claim(identity['did'], identity['privkey'],
                                                        identity['signingkey'], {BaseConnector.ALLOW(): {}})

        claimLink = await ephemeralConnector.claim(identity['did'], identity['privkey'], identity['signingkey'],
                                                   {'need': 'beer'})

        claims = await ephemeralConnector.getObservedClaims(2, identity)

        pubkey = BaseConnector.referenceFromDid(identity['did'])

        self.assertGreater(len(claims), 0)
        count = 0
        for i in range(len(claims)):
            if claims[i]['pubkey'] == BaseConnector.referenceFromDid(identity['did']):
                self.assertEqual(claims[i]['claim']['data'], {'need': 'beer'})
                self.assertEqual(claims[i]['claim']['previous'], allowClaimLink)
                count += 1
        self.assertEqual(count, 1)

class EphemeralTestShouldObserveConnectorWideWithCredentials(TestCore):
    def test_import_with_did(self):
        tasks = [self.asyncTestShouldObserveConnectorWideWithCredentials()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))

    # should be able to observe connector-wide with credentials
    async def asyncTestShouldObserveConnectorWideWithCredentials(self):
        ephemeralConnector = EphemeralConnector()
        ephemeralConnector.configure(EPHEMERAL_ENDPOINT, EPHEMERAL_WEBSOCKET_ENDPOINT)

        identity = await ephemeralConnector.newIdentity()
        observeResult = await ephemeralConnector.observe(None, {'need': None}, identity['did'], identity['privkey'], identity['signingkey'])

        identity2 = await ephemeralConnector.newIdentity()

        allowClaimLink2 = await ephemeralConnector.claim(identity2['did'], identity2['privkey'],
                                                        identity2['signingkey'], {BaseConnector.ALLOW(): {}})
        claimLink2 = await ephemeralConnector.claim(identity2['did'], identity2['privkey'], identity2['signingkey'],
                                                    {'need': 'beer'})

        identity3 = await ephemeralConnector.newIdentity()
        allowClaimLink3 = await ephemeralConnector.claim(identity3['did'], identity3['privkey'],
                                                         identity3['signingkey'], {BaseConnector.ALLOW(): {}})

        claimLink3 = await ephemeralConnector.claim(identity3['did'], identity3['privkey'], identity3['signingkey'],
                                            {'need': 'wine'})

        claims = await ephemeralConnector.getObservedClaims(4, None, identity)

        self.assertEqual(len(claims), 2)
        self.assertEqual(claims[0]['claim']['data'], {'need': 'beer'})
        self.assertEqual(claims[0]['claim']['previous'], allowClaimLink2)
        self.assertEqual(claims[1]['claim']['data'], {'need': 'wine'})
        self.assertEqual(claims[1]['claim']['previous'], allowClaimLink3)

class EphemeralTestShouldClaimAndListenToConnectorWithFilter(TestCore):
    def test_import_with_did(self):
        tasks = [self.asyncTestShouldClaimAndListenToConnectorWithFilter()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))

    # should be able to claim something and listen to the connector with a filter
    async def asyncTestShouldClaimAndListenToConnectorWithFilter(self):
        ephemeralConnector = EphemeralConnector()
        ephemeralConnector.configure(EPHEMERAL_ENDPOINT, EPHEMERAL_WEBSOCKET_ENDPOINT)

        identity = await ephemeralConnector.newIdentity()
        observeResult = await ephemeralConnector.observe(identity['did'], {'need': 'wine'})

        await ephemeralConnector.claim(identity['did'], identity['privkey'], identity['signingkey'], {BaseConnector.ALLOW(): {}})

        claimLink = await ephemeralConnector.claim(identity['did'], identity['privkey'], identity['signingkey'], {'need': 'beer'})
        claimLink2 = await ephemeralConnector.claim(identity['did'], identity['privkey'], identity['signingkey'], {'need': 'wine'})
        await ephemeralConnector.claim(identity['did'], identity['privkey'], identity['signingkey'], {'need': 'tea'})

        claims = await ephemeralConnector.getObservedClaims(3, identity)

        self.assertEqual(len(claims), 1)
        self.assertEqual(claims[0]['claim']['data'], {'need': 'wine'})
        self.assertEqual(claims[0]['claim']['previous'], claimLink)

class EphemeralTestShouldClaimAndListenToConnectorWithFilterOnPredicate(TestCore):
    def test_import_with_did(self):
        tasks = [self.asyncTestShouldClaimAndListenToConnectorWithFilterOnPredicate()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))

    # should be able to claim something and listen to the connector with a filter on a predicate
    async def asyncTestShouldClaimAndListenToConnectorWithFilterOnPredicate(self):
        ephemeralConnector = EphemeralConnector()
        ephemeralConnector.configure(EPHEMERAL_ENDPOINT, EPHEMERAL_WEBSOCKET_ENDPOINT)

        identity = await ephemeralConnector.newIdentity()
        identity2 = await ephemeralConnector.newIdentity()

        observeResult = await ephemeralConnector.observe(identity2['did'], {'need': None}, identity['did'], identity['privkey'], identity['signingkey'])

        await ephemeralConnector.claim(identity2['did'], identity2['privkey'], identity2['signingkey'], {BaseConnector.ALLOW(): {'did': identity['did']}})

        claimLink = await ephemeralConnector.claim(identity2['did'], identity2['privkey'], identity2['signingkey'], {'desire': 'beer'})
        claimLink2 = await ephemeralConnector.claim(identity2['did'], identity2['privkey'], identity2['signingkey'], {'need': 'wine'})
        await ephemeralConnector.claim(identity2['did'], identity2['privkey'], identity2['signingkey'], {'desire': 'tea'})

        claims = await ephemeralConnector.getObservedClaims(3, identity2, identity)

        self.assertEqual(len(claims), 1)
        self.assertEqual(claims[0]['claim']['data'], {'need': 'wine'})
        self.assertEqual(claims[0]['claim']['previous'], claimLink)

class EphemeralTestShouldClaimAndListenToConnectorWithFilterOnPredicateWithoutSsid(TestCore):
    def test_import_with_did(self):
        tasks = [self.asyncTestShouldClaimAndListenToConnectorWithFilterOnPredicateWithoutSsid()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))

    # should be able to claim something and listen to the connector with a filter on a predicate without an ssid
    async def asyncTestShouldClaimAndListenToConnectorWithFilterOnPredicateWithoutSsid(self):
        ephemeralConnector = EphemeralConnector()
        ephemeralConnector.configure(EPHEMERAL_ENDPOINT, EPHEMERAL_WEBSOCKET_ENDPOINT)

        identity = await ephemeralConnector.newIdentity()
        observeResult = await ephemeralConnector.observe(None, {'need': None})

        await ephemeralConnector.claim(identity['did'], identity['privkey'], identity['signingkey'], {BaseConnector.ALLOW(): {}})

        claimLink = await ephemeralConnector.claim(identity['did'], identity['privkey'], identity['signingkey'], {'desire': 'beer'})
        claimLink2 = await ephemeralConnector.claim(identity['did'], identity['privkey'], identity['signingkey'], {'need': 'wine'})
        await ephemeralConnector.claim(identity['did'], identity['privkey'], identity['signingkey'], {'desire': 'tea'})

        claims = await ephemeralConnector.getObservedClaims(100, identity)

        self.assertGreater(len(claims), 0)
        count = 0
        for i in range(len(claims)):
            if claims[i]['pubkey'] == BaseConnector.referenceFromDid(identity['did']):
                self.assertEqual(claims[i]['claim']['data'], {'need': 'wine'})
                self.assertEqual(claims[i]['claim']['previous'], claimLink)
                count += 1
        self.assertEqual(count, 1)
