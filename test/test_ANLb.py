import os
import unittest
import asyncio
import json

# eslint-env mocha
from src.DisciplLawReg.Utils.Util import Util
from src.DisciplLawReg.LawReg import LawReg
import logging
#import * as log from 'loglevel'
#log.getLogger('disciplLawReg').setLevel('warn')
lawReg = LawReg()

util = Util(lawReg)

actors = ['RVO', 'collectief']

factFunctionSpec = {
  '[RVO]': 'RVO',
  '[agrarisch collectief]': 'collectief'
}

wd = os.getcwd()
path = wd + "\\ANLb.flint.json"
f = open(path, encoding="utf8")
ANLb = json.load(f)
f.close()

scenarios = [
  {
    'name': 'moet een collectief een aanvraag kunnen doen',
    'acts': [
      {
        'act': '<<indienen betalingsaanvraag>>',
        'actor': 'collectief'
      }
    ],
    'facts': {
      '[toegekende gebiedsaanvraag]': True,
      '[De identiteit van de begunstigde]': True,
      '[De naam van de regeling]': True,
      '[De totale omvang in hectares in 2 decimalen per leefgebied/deelgebied waarvoor betaling wordt gevraagd]': True,
      '[De ligging (geometrie).]': [True, False],
      '[Het leefgebied waartoe het perceel behoort.]': True,
      '[Indien van toepassing het deelgebied waartoe het perceel behoort.]': True,
      '[Omvang van de beheerde oppervlakte (in hectares in 2 decimalen voor landbouwgrond, die exact past bij de geometrie (uit eerste punt).]': True,
      '[Aard van het grondgebruik (grasland, bouwland, landschapselement of water).]': True,
      '[De identificatie van niet-landbouwgrond die voor steun in aanmerking komt (subsidiabele landschapselementen (met uitzondering van hoogstamboomgaard, natuurvriendelijke oever en solitaire boom) en water).]': True,
      '[Verklaring van het agrarisch collectief dat de individuele deelnemers weten wat de verplichtingen en consequenties zijn]': True,
      '[De unieke identificatie van iedere deelnemer van het agrarisch collectief]': True,
      '[Aanvraagnummer gebiedsaanvraag (provincie) als bewijsstuk om te bepalen of de begunstigde voor betaling in aanmerking komt.]': True,
      '[De bewijsstukken die nodig zijn om te bepalen of de aanspraak op de steun/bijstand kan worden gemaakt. Hier moet worden gedacht aan stukken die de inhoud van de betalingsaanvraag onderbouwen]': True,
      '[betaalverzoek is tussen 1 maart en 15 mei ingediend]': True,
      '[goedgekeurde gebiedsaanvraag]': True
    }
  }

]





class TestCore(unittest.TestCase):
    def setUp(self):
        self.i = 0

# should be able to get the connector asynchronously
class TestScenario(TestCore):
    def test_ANLB_Scenario(self):
        tasks = [self.asyncTestANLBScenario()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestANLBScenario(self):
        ssids, modelLink = await util.setupModel(ANLb, actors, factFunctionSpec, False)
        for scenario in scenarios:
            caseLinks, connector, ssids = await util.scenarioTest(ssids, modelLink, scenario['acts'], scenario['facts'], False)
            claim = await connector.get(caseLinks[0], ssids[0]['did'], ssids[0]['privkey'], ssids[0]['signingkey'])

            self.assertEqual(len(claim['claim']['data']['DISCIPL_FLINT_FACTS_SUPPLIED']), 12)
            self.assertEqual('[agrarisch collectief]' in claim['claim']['data']['DISCIPL_FLINT_FACTS_SUPPLIED'], True)

'''
    ssids, modelLink
    describe('in het ANLb model', () => {
      before(async () => {
        ({ ssids, modelLink } = await util.setupModel(ANLb, actors, factFunctionSpec, true))
      })

      for (const scenario of scenarios) {
        it(scenario.name, async () => {
          await util.scenarioTest(ssids, modelLink, scenario.acts, scenario.facts, true)
        })
      }
    })
'''