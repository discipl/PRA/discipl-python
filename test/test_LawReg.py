import os
import unittest
import asyncio
import json

from test.testUtils import expectCheckActionResult, runScenario, runOnModel, expectModelFact, expectModelDuty, expectModelActDetails, takeAction, expectData, expectPotentialActs, expectActiveDuties, expectAvailableActs, factResolverFactory, expectRetrievedFactFunction
from src.DisciplLawReg.Utils.FactResolver import FactResolver
from src.DisciplLawReg.Utils.Identity_Util import IdentityUtil

wd = os.getcwd()
path = wd + "\\flint-example-awb.json"
f = open(path, encoding="utf8")
awb = json.load(f)
f.close()

class TestCore(unittest.TestCase):
    def setUp(self):
        self.i = 0

class LocalFactResolver1:
    def __init__(self):
        self.facts = None

    async def resolveFacts(self, fact, listNames, listIndices, possibleCreatingActions):
        if isinstance(listNames, list) and len(listNames) > 0 and listNames[0] == 'leeftijden':
            if listIndices[0] == 0:
                return 8
            elif listIndices[0] == 1:
                return 12
            else:
                return False
        return True

class LocalFactResolver2:
    def __init__(self):
        self.facts = None

    async def resolveFacts(self, fact, listNames, listIndices, possibleCreatingActions):
        if isinstance(listNames, list) and len(listNames) > 0 and listNames[0] == 'kinderen':
            if listIndices[0] == 0 and listIndices[1] == 0:
                return 'BSc Technische Wiskunde'
            elif listIndices[0] == 1 and listIndices[1] == 0:
                return 'MSc Applied Mathematics'
            else:
                return False
        return True

class LocalFactResolver3:
    def __init__(self):
        self.facts = None

    async def resolveFacts(self, fact, listNames, listIndices, possibleCreatingActions):
        if fact in ['[dough]', '[bakery]', '[baker]']:
            return True
        # Last option corresponds to first bake action because this array is populated backwards
        return possibleCreatingActions[1]

# should publish small example
class TestScenarioLawReg1(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestLawReg1()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    def checkKeys(self, modelReference, lawReg, ssids, link, index, actionLinks, modelLink):
        for key, value in modelReference.items():
            if not key in ['model', 'acts', 'facts', 'duties']:
                print('checkKeys ' + key + ' not in modelReference')
                return False
        return True
        #Object.keys(modelReference)).to.have.members(['model', 'acts', 'facts', 'duties'])

    async def asyncTestLawReg1(self):
        model = {
            'model': 'Fictieve verwelkomingsregeling Staat der Nederlanden',
            'acts': [
                {
                    'act': '<<ingezetene kan verwelkomst van overheid aanvragen>>',
                    'action': '[aanvragen]',
                    'actor': '[ingezetene]',
                    'object': '[verwelkomst]',
                    'recipient': '[overheid]',
                    'preconditions': '',
                    'create': '<verwelkomen>',
                    'terminate': '',
                    'reference': 'art 2.1',
                    'sourcetext': '',
                    'explanation': '',
                    'version': '2-[19980101]-[jjjjmmdd]',
                    'juriconnect': 'jci1.3:c:BWBR0005537&hoofdstuk=1&titeldeel=1.1&artikel=1:3&lid=3&z=2017-03-01&g=2017-03-01'
                }, {
                    'act': '<<fdsafadsf >>',
                    'action': '[fdsa]',
                    'actor': '[ingezetene]',
                    'object': '[verwelkomst]',
                    'recipient': '[overheid]',
                    'preconditions': '',
                    'create': '<verwelkomen>',
                    'terminate': '',
                    'reference': 'art 2.1',
                    'sourcetext': '',
                    'explanation': '',
                    'version': '2-[19980101]-[jjjjmmdd]',
                    'juriconnect': 'jci1.3:c:BWBR0005537&hoofdstuk=1&titeldeel=1.1&artikel=1:3&lid=3&z=2017-03-01&g=2017-03-01'
                }, {
                    'act': '<<fdsafadsf fdas>>',
                    'action': '[fdsa fads]',
                    'actor': '[ingezetene]',
                    'object': '[verwelkomst]',
                    'recipient': '[overheid]',
                    'preconditions': '',
                    'create': '<verwelkomen>',
                    'terminate': '',
                    'reference': 'art 2.1',
                    'sourcetext': '',
                    'explanation': '',
                    'version': '2-[19980101]-[jjjjmmdd]',
                    'juriconnect': 'jci1.3:c:BWBR0005537&hoofdstuk=1&titeldeel=1.1&artikel=1:3&lid=3&z=2017-03-01&g=2017-03-01'
                }
            ],
            'facts': [
                {'fact': '[ingezetene]', 'function': '', 'reference': 'art 1.1'},
                {'fact': '[overheid]', 'function': '[aangesteld als ambtenaar]', 'reference': 'art 1.2'},
                {'fact': '[betrokkene]', 'function': '[ingezetene] OF [overheid]', 'reference': 'art 1.3'},
                {'fact': '[klacht]', 'function': '', 'reference': 'art 1.4'},
                {'fact': '[verwelkomst]', 'function': '', 'reference': 'art 1.5'},
                {'fact': '[binnen 14 dagen na aanvragen]', 'function': '', 'reference': 'art 2.2'},
                {'fact': '[na 14 dagen geen verwelkomst]', 'function': '', 'reference': 'art 3.1'}
            ],
            'duties': [
                {
                    'duty': '<verwelkomen binnen 14 dagen na aanvragen>',
                    'duty-holder': '[overheid]',
                    'claimant': '[ingezetene]',
                    'create': '<<verwelkomen>>',
                    'enforce': '<<klagen>>',
                    'terminate': '',
                    'reference': 'art 2.2, art 3.1',
                    'sourcetext': '',
                    'explanation': '',
                    'version': '2-[19980101]-[jjjjmmdd]',
                    'juriconnect': 'jci1.3:c:BWBR0005537&hoofdstuk=1&titeldeel=1.1&artikel=1:3&lid=3&z=2017-03-01&g=2017-03-01'
                }
            ]
        }
        result, test = await runScenario(
            model,
            { 'actor': [] },
            [
              runOnModel('actor', self.checkKeys),
              expectModelFact('actor', '[betrokkene]', { 'fact': '[betrokkene]', 'function': '[ingezetene] OF [overheid]', 'reference': 'art 1.3' }),
              expectModelDuty('actor', '<verwelkomen binnen 14 dagen na aanvragen>', {
                'duty': '<verwelkomen binnen 14 dagen na aanvragen>',
                'duty-holder': '[overheid]',
                'claimant': '[ingezetene]',
                'create': '<<verwelkomen>>',
                'enforce': '<<klagen>>',
                'terminate': '',
                'reference': 'art 2.2, art 3.1',
                'sourcetext': '',
                'explanation': '',
                'version': '2-[19980101]-[jjjjmmdd]',
                'juriconnect': 'jci1.3:c:BWBR0005537&hoofdstuk=1&titeldeel=1.1&artikel=1:3&lid=3&z=2017-03-01&g=2017-03-01'
              }),
              expectModelActDetails('actor', '<<ingezetene kan verwelkomst van overheid aanvragen>>', {
                'act': '<<ingezetene kan verwelkomst van overheid aanvragen>>',
                'action': '[aanvragen]',
                'actor': '[ingezetene]',
                'object': '[verwelkomst]',
                'recipient': '[overheid]',
                'preconditions': '',
                'create': '<verwelkomen>',
                'terminate': '',
                'reference': 'art 2.1',
                'sourcetext': '',
                'explanation': '',
                'version': '2-[19980101]-[jjjjmmdd]',
                'juriconnect': 'jci1.3:c:BWBR0005537&hoofdstuk=1&titeldeel=1.1&artikel=1:3&lid=3&z=2017-03-01&g=2017-03-01'
                })
            ]
        )
        self.assertEqual(test, True)

# should be able to take an action
class TestScenarioLawReg2(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestLawReg2()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    def factsSupplied(self, actors, notUsed):
        return {
            '[overheid]': True,
            '[verwelkomst]': True,
            '[ingezetene]': IdentityUtil.identityExpression(actors['ingezetene']['did'])
            }

    async def asyncTestLawReg2(self):
        model = { 'model': 'Fictieve verwelkomingsregeling Staat der Nederlanden',
            'acts': [
              {
                'act': '<<ingezetene kan verwelkomst van overheid aanvragen>>',
                'action': '[aanvragen]',
                'actor': '[ingezetene]',
                'object': '[verwelkomst]',
                'recipient': '[overheid]',
                'preconditions': '[]',
                'create': '<verwelkomen>',
                'terminate': '',
                'reference': 'art 2.1',
                'sourcetext': '',
                'explanation': '',
                'version': '2-[19980101]-[jjjjmmdd]',
                'juriconnect': 'jci1.3:c:BWBR0005537&hoofdstuk=1&titeldeel=1.1&artikel=1:3&lid=3&z=2017-03-01&g=2017-03-01'
              }],
            'facts': [
              { 'fact': '[ingezetene]', 'function': '[]', 'reference': '' }
            ],
            'duties': []
          }

        result, test = await runScenario(
            model,
            { 'ingezetene': ['[ingezetene]'] },
            [
              takeAction('ingezetene', '<<ingezetene kan verwelkomst van overheid aanvragen>>', FactResolver(None, True, True)),
              expectData('ingezetene', '<<ingezetene kan verwelkomst van overheid aanvragen>>', self.factsSupplied)
            ]
          )
        self.assertEqual(test, True)

'should allow any actor to take an action where the fact is true for ANYONE'
class TestScenarioLawReg3(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestLawReg3()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    def factsSupplied1(self, actors, notUsed):
        return {
                '[overheid]': True,
                '[verwelkomst]': True,
                '[ingezetene]': IdentityUtil.identityExpression(actors['someone']['did'])
                }

    def factsSupplied2(self, actors, notUsed):
        return {
                '[overheid]': True,
                '[verwelkomst]': True,
                '[ingezetene]': IdentityUtil.identityExpression(actors['someone else']['did'])
                }

    async def asyncTestLawReg3(self):
        model = {'model': 'Fictieve verwelkomingsregeling Staat der Nederlanden',
            'acts': [
            {
                'act': '<<ingezetene kan verwelkomst van overheid aanvragen>>',
                'action': '[aanvragen]',
                'actor': '[ingezetene]',
                'object': '[verwelkomst]',
                'recipient': '[overheid]',
                'preconditions': '[]',
                'create': '<verwelkomen>',
                'terminate': '',
                'reference': 'art 2.1',
                'sourcetext': '',
                'explanation': '',
                'version': '2-[19980101]-[jjjjmmdd]',
                'juriconnect': 'jci1.3:c:BWBR0005537&hoofdstuk=1&titeldeel=1.1&artikel=1:3&lid=3&z=2017-03-01&g=2017-03-01'
            }],
            'facts': [
                { 'fact': '[ingezetene]', 'function': '[]', 'reference': '' }
            ],
            'duties': []
          }
        completedFacts = { '[verwelkomst]': True, '[overheid]': True }
        result, test = await runScenario(model,
                { 'someone': [], 'someone else': [], 'ANYONE': ['[ingezetene]'] },
                [
                    takeAction('someone', '<<ingezetene kan verwelkomst van overheid aanvragen>>', FactResolver(completedFacts)),
                    expectData('someone', '<<ingezetene kan verwelkomst van overheid aanvragen>>', self.factsSupplied1),
                    takeAction('someone else', '<<ingezetene kan verwelkomst van overheid aanvragen>>', FactResolver(completedFacts)),
                    expectData('someone else', '<<ingezetene kan verwelkomst van overheid aanvragen>>', self.factsSupplied2)
                ]
            )
        self.assertEqual(test, True)

'should allow any actor to take an action where the fact is true for ANYONE'
class TestScenarioLawReg4(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestLawReg4()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestLawReg4(self):
        model = {
                'model': 'Fictieve verwelkomingsregeling Staat der Nederlanden',
                'acts': [
                    {
                        'act': '<<ingezetene kan verwelkomst van overheid aanvragen>>',
                        'action': '[aanvragen]',
                        'actor': '[ingezetene]',
                        'object': '[verwelkomst]',
                        'recipient': '[overheid]',
                        'preconditions': '[]',
                        'create': '<verwelkomen>',
                        'terminate': '',
                        'reference': 'art 2.1',
                        'sourcetext': '',
                        'explanation': '',
                        'version': '2-[19980101]-[jjjjmmdd]',
                        'juriconnect': 'jci1.3:c:BWBR0005537&hoofdstuk=1&titeldeel=1.1&artikel=1:3&lid=3&z=2017-03-01&g=2017-03-01'
                        }],
                'facts': [
                  { 'fact': '[ingezetene]', 'function': '[]', 'reference': '' }
                        ],
                'duties': []
              }

        result, test = await runScenario(
            model,
            { 'ingezetene1': ['[ingezetene]'], 'ingezetene2': ['[ingezetene]'] },
            [
              expectPotentialActs('ingezetene1', ['<<ingezetene kan verwelkomst van overheid aanvragen>>']),
              expectPotentialActs('ingezetene2', ['<<ingezetene kan verwelkomst van overheid aanvragen>>'])
            ])
        self.assertEqual(test, True)

'should be able to take an action with one fact set to multiple actors'
class TestScenarioLawReg5(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestLawReg5()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    def factsSupplied(self, actors, notUsed):
        return {
                '[ingezetene]': IdentityUtil.identityExpression(actors['ingezetene1']['did']),
                '[verwelkomst]': True
               }

    async def asyncTestLawReg5(self):
        model = {
                'model': 'Fictieve verwelkomingsregeling Staat der Nederlanden',
                'acts': [
                    {
                        'act': '<<ingezetene kan verwelkomst van overheid aanvragen>>',
                        'action': '[aanvragen]',
                        'actor': '[ingezetene]',
                        'object': '[verwelkomst]',
                        'recipient': '[overheid]',
                        'preconditions': '[]',
                        'create': '<verwelkomen>',
                        'terminate': '',
                        'reference': 'art 2.1',
                        'sourcetext': '',
                        'explanation': '',
                        'version': '2-[19980101]-[jjjjmmdd]',
                        'juriconnect': 'jci1.3:c:BWBR0005537&hoofdstuk=1&titeldeel=1.1&artikel=1:3&lid=3&z=2017-03-01&g=2017-03-01'
                        }],
                    'facts': [
                        { 'fact': '[ingezetene]', 'function': '[]', 'reference': '' },
                        { 'fact': '[overheid]', 'function': '[]', 'reference': '' }
                        ],
                    'duties': []
                    }
        result, test = await runScenario(
            model,
            { 'ingezetene1': ['[ingezetene]'], 'ingezetene2': ['[ingezetene]'], 'overheid': ['[overheid]'] },
            [
              takeAction('ingezetene1', '<<ingezetene kan verwelkomst van overheid aanvragen>>', FactResolver({ '[verwelkomst]': True })),
              expectData('ingezetene1', '<<ingezetene kan verwelkomst van overheid aanvragen>>', self.factsSupplied)
            ])
        self.assertEqual(test, True)


'should be able to take an action with an async factresolver'
class TestScenarioLawReg6(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestLawReg6()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    def factsSupplied(self, actors, notUsed):
        return {
            '[overheid]': True,
            '[verwelkomst]': True,
            '[ingezetene]': IdentityUtil.identityExpression(actors['ingezetene']['did'])
            }

    async def asyncTestLawReg6(self):
        model = {
            'model': 'Fictieve verwelkomingsregeling Staat der Nederlanden',
            'acts': [
                {
                    'act': '<<ingezetene kan verwelkomst van overheid aanvragen>>',
                    'action': '[aanvragen]',
                    'actor': '[ingezetene]',
                    'object': '[verwelkomst]',
                    'recipient': '[overheid]',
                    'preconditions': '[]',
                    'create': '<verwelkomen>',
                    'terminate': '',
                    'reference': 'art 2.1',
                    'sourcetext': '',
                    'explanation': '',
                    'version': '2-[19980101]-[jjjjmmdd]',
                    'juriconnect': 'jci1.3:c:BWBR0005537&hoofdstuk=1&titeldeel=1.1&artikel=1:3&lid=3&z=2017-03-01&g=2017-03-01'
                }],
            'facts': [
                { 'fact': '[ingezetene]', 'function': '[]', 'reference': '' }
                ],
            'duties': []
            }
        result, test = await runScenario(
            model,
                { 'ingezetene': ['[ingezetene]'] },
                [
                  takeAction('ingezetene', '<<ingezetene kan verwelkomst van overheid aanvragen>>', FactResolver(None, True, True)),
                  expectData('ingezetene', '<<ingezetene kan verwelkomst van overheid aanvragen>>', self.factsSupplied)
                ]
            )
        self.assertEqual(test, True)

'should be able to take an action with a list'
class TestScenarioLawReg7(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestLawReg7()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    def factsSupplied(self, actors, notUsed):
        return {
            '[ouder]': IdentityUtil.identityExpression(actors['actor']['did']),
            '[overheid]': True,
            '[verzoek]': True,
            'leeftijden': [
                {
                    '[leeftijd]': 8
                },
                {
                    '[leeftijd]': 12
                },
                {
                    '[leeftijd]': False
                }
                ]
            }

    async def asyncTestLawReg7(self):
        model = {
            'model': 'Fictieve kinderbijslag',
            'acts': [
                {
                    'act': '<<kinderbijslag aanvragen>>',
                    'action': '[aanvragen]',
                    'actor': '[ouder]',
                    'object': '[verzoek]',
                    'recipient': '[overheid]',
                    'preconditions': {
                        'expression': 'LIST',
                        'name': 'leeftijden',
                        'items': '[leeftijd]'
                        },
                    'create': [],
                    'terminate': [],
                    'reference': 'art 2.1',
                    'sourcetext': '',
                    'explanation': '',
                    'version': '2-[19980101]-[jjjjmmdd]',
                    'juriconnect': 'jci1.3:c:BWBR0005537&hoofdstuk=1&titeldeel=1.1&artikel=1:3&lid=3&z=2017-03-01&g=2017-03-01'
                }],
            'facts': [
              { 'fact': '[ingezetene]', 'function': '[]', 'reference': '' }
                ],
            'duties': []
            }
        result, test = await runScenario(
                model,
                {'actor': []},
                [
                    takeAction('actor', '<<kinderbijslag aanvragen>>', LocalFactResolver1()),
                    expectData('ingezetene', '<<kinderbijslag aanvragen>>', self.factsSupplied)
                ]
            )
        self.assertEqual(test, True)


'should be able to take an action with a nested list'
class TestScenarioLawReg8(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestLawReg8()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    def factsSupplied(self, actors, notUsed):
        return {'[ouder]': IdentityUtil.identityExpression(actors['actor']['did']),
                '[overheid]': True,
                '[verzoek]': True,
                'kinderen': [
                    {
                      'diplomas': [
                        {
                          '[diploma]': 'BSc Technische Wiskunde'
                        },
                        {
                          '[diploma]': False
                        }
                      ]
                    },
                    {
                      'diplomas': [
                        {
                          '[diploma]': 'MSc Applied Mathematics'
                        },
                        {
                          '[diploma]': False
                        }
                      ]
                    },
                    {
                      'diplomas': [
                        {
                          '[diploma]': False
                        }
                      ]
                    }
                  ]
                }

    async def asyncTestLawReg8(self):
        model = {
            'model': 'Fictieve kinderbijslag',
            'acts': [
                {
                    'act': '<<kinderbijslag aanvragen>>',
                    'action': '[aanvragen]',
                    'actor': '[ouder]',
                    'object': '[verzoek]',
                    'recipient': '[overheid]',
                    'preconditions': {
                        'expression': 'LIST',
                        'name': 'kinderen',
                        'items': {
                            'expression': 'LIST',
                            'name': 'diplomas',
                            'items': '[diploma]'
                            }
                        },
                    'create': [],
                    'terminate': [],
                    'reference': 'art 2.1',
                    'sourcetext': '',
                    'explanation': '',
                    'version': '2-[19980101]-[jjjjmmdd]',
                    'juriconnect': 'jci1.3:c:BWBR0005537&hoofdstuk=1&titeldeel=1.1&artikel=1:3&lid=3&z=2017-03-01&g=2017-03-01'
                }],
            'facts': [
                { 'fact': '[ingezetene]', 'function': '[]', 'reference': '' }
                ],
            'duties': []
            }
        result, test = await runScenario(
                model,
                {'actor': []},
                [
                    takeAction('actor', '<<kinderbijslag aanvragen>>', LocalFactResolver2()),
                    expectData('ingezetene', '<<kinderbijslag aanvragen>>', self.factsSupplied)
                ]
            )
        self.assertEqual(test, True)

verwelkomingsregeling = {
      'model': 'Fictieve verwelkomingsregeling Staat der Nederlanden',
      'acts': [
        {
          'act': '<<ingezetene kan verwelkomst van overheid aanvragen>>',
          'action': '[aanvragen]',
          'actor': '[ingezetene]',
          'object': '[verwelkomst]',
          'recipient': '[overheid]',
          'preconditions': '[]',
          'create': '<verwelkomen>',
          'terminate': '',
          'reference': 'art 2.1',
          'sourcetext': '',
          'explanation': '',
          'version': '2-[19980101]-[jjjjmmdd]',
          'juriconnect': 'jci1.3:c:BWBR0005537&hoofdstuk=1&titeldeel=1.1&artikel=1:3&lid=3&z=2017-03-01&g=2017-03-01'
        },
        {
          'act': '<<ingezetene geeft aan dat verwelkomen niet nodig is>>',
          'action': '[aangeven]',
          'actor': '[ingezetene]',
          'object': '[verwelkomst]',
          'recipient': '[overheid]',
          'preconditions': '[]',
          'create': '',
          'terminate': '<verwelkomen>',
          'reference': 'art 2.1',
          'sourcetext': '',
          'explanation': '',
          'version': '2-[19980101]-[jjjjmmdd]',
          'juriconnect': 'jci1.3:c:BWBR0005537&hoofdstuk=1&titeldeel=1.1&artikel=1:3&lid=3&z=2017-03-01&g=2017-03-01'
        }],
      'facts': [
        { 'fact': '[ingezetene]', 'function': '[]', 'reference': 'art 1.1' },
        { 'fact': '[overheid]', 'function': '[]', 'reference': '' }
      ],
      'duties': [
        {
          'duty': '<verwelkomen>',
          'duty-components': '',
          'duty-holder': '[overheid]',
          'claimant': '[ingezetene]',
          'create': '<<ingezetene kan verwelkomst van overheid aanvragen>>>',
          'terminate': '<<ingezetene geeft aan dat verwelkomen niet nodig is>>',
          'version': '',
          'reference': '',
          'juriconnect': '',
          'sourcetext': '',
          'explanation': ''
        }
      ]
    }

'should be able to determine active duties'
class TestScenarioLawReg9(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestLawReg9()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestLawReg9(self):
        result, test = await runScenario(verwelkomingsregeling,
            { 'ingezetene': ['[ingezetene]'], 'overheid': ['[overheid]'] },
            [
              takeAction('ingezetene', '<<ingezetene kan verwelkomst van overheid aanvragen>>', FactResolver(None, True, True)),
              expectActiveDuties('ingezetene', []),
              expectActiveDuties('overheid', ['<verwelkomen>'])
            ]
            )
        self.assertEqual(test, True)

'should be able to determine active duties being terminated'
class TestScenarioLawReg10(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestLawReg10()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestLawReg10(self):
        result, test = await runScenario(verwelkomingsregeling,
            {'overheid': [], 'ingezetene': []},
            [
                takeAction('ingezetene', '<<ingezetene kan verwelkomst van overheid aanvragen>>', FactResolver(None, True, True)),
                takeAction('ingezetene', '<<ingezetene geeft aan dat verwelkomen niet nodig is>>', FactResolver(None, True, True)),
                expectActiveDuties('ingezetene', []),
                expectActiveDuties('overheid', [])
            ]
            )
        self.assertEqual(test, True)

'should be able to determine available acts'
class TestScenarioLawReg11(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestLawReg11()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestLawReg11(self):
        model = {
            'model': 'Fictieve verwelkomingsregeling Staat der Nederlanden',
            'acts': [
                {
                    'act': '<<ingezetene kan verwelkomst van overheid aanvragen>>',
                    'action': '[aanvragen]',
                    'actor': '[ingezetene]',
                    'object': '[aanvraag verwelkomst]',
                    'recipient': '[overheid]',
                    'preconditions': '[]',
                    'create': '<verwelkomen>',
                    'terminate': '',
                    'reference': 'art 2.1',
                    'sourcetext': '',
                    'explanation': '',
                    'version': '2-[19980101]-[jjjjmmdd]',
                    'juriconnect': 'jci1.3:c:BWBR0005537&hoofdstuk=1&titeldeel=1.1&artikel=1:3&lid=3&z=2017-03-01&g=2017-03-01'
                }],
            'facts': [
                {'fact': '[ingezetene]', 'function': '[]', 'reference': 'art 1.1'},
                {'fact': '[overheid]', 'function': '[]', 'reference': ''},
                {'fact': '[verwelkomst]', 'function': {'expression': 'CREATE', 'operands': []}, 'reference': ''}
            ],
            'duties': []
            }
        result, test = await runScenario(model,
            {'ingezetene': ['[ingezetene]'], 'overheid': ['[overheid]']},
            [
                expectAvailableActs('ingezetene', []),
                expectAvailableActs('ingezetene', ['<<ingezetene kan verwelkomst van overheid aanvragen>>'], factResolverFactory({'[aanvraag verwelkomst]': True}))
            ]
            )
        self.assertEqual(test, True)

'should be able to determine possible actions with a list'
class TestScenarioLawReg12(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestLawReg12()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestLawReg12(self):
        model = {
            'model': 'Fictieve kinderbijslag',
            'acts': [
                {
                'act': '<<kinderbijslag aanvragen>>',
                'action': '[aanvragen]',
                'actor': '[ouder]',
                'object': '[verzoek]',
                'recipient': '[overheid]',
                'preconditions': {
                  'expression': 'LIST',
                  'name': 'leeftijden',
                  'items': '[leeftijd]'
                },
                'create': [],
                'terminate': [],
                'reference': 'art 2.1',
                'sourcetext': '',
                'explanation': '',
                'version': '2-[19980101]-[jjjjmmdd]',
                'juriconnect': 'jci1.3:c:BWBR0005537&hoofdstuk=1&titeldeel=1.1&artikel=1:3&lid=3&z=2017-03-01&g=2017-03-01'
                }],
            'facts': [
                { 'fact': '[ingezetene]', 'function': '[]', 'reference': '' }
                ],
            'duties': []
            }
        result, test = await runScenario(
            model,
            { 'actor': [] },
            [
                expectAvailableActs('actor', []),
                expectPotentialActs('actor', ['<<kinderbijslag aanvragen>>'])
            ]
            )
        self.assertEqual(test, True)

'should be able to determine possible actions with a less than'
class TestScenarioLawReg13(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestLawReg13()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestLawReg13(self):
        model  = {
            'model': 'Fictieve kinderbijslag',
            'acts': [
            {
                'act': '<<kinderbijslag aanvragen>>',
                'action': '[aanvragen]',
                'actor': '[ouder]',
                'object': '[verzoek]',
                'recipient': '[overheid]',
                'preconditions': {
                    'expression': 'LESS_THAN',
                    'operands': [
                        '[kinderen]',
                        '[honderd]'
                    ]
                },
                'create': [],
                'terminate': [],
                'reference': 'art 2.1',
                'sourcetext': '',
                'explanation': '',
                'version': '2-[19980101]-[jjjjmmdd]',
                'juriconnect': 'jci1.3:c:BWBR0005537&hoofdstuk=1&titeldeel=1.1&artikel=1:3&lid=3&z=2017-03-01&g=2017-03-01'
                }],
            'facts': [],
            'duties': []
            }

        result, test = await runScenario(
            model,
            {'actor': []},
            [
                expectAvailableActs('actor', []),
                expectPotentialActs('actor', ['<<kinderbijslag aanvragen>>'])
            ]
            )
        self.assertEqual(test, True)


'should be able to determine possible actions with multiple options for created facts'
class TestScenarioLawReg14(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestLawReg14()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestLawReg14(self):
        model  = {
        'model': 'Fictieve kinderbijslag',
        'acts': [
            {
                'act': '<<kinderbijslag aanvragen>>',
                'actor': '[ouder]',
                'object': '[verzoek]',
                'recipient': '[Minister]',
                'preconditions': {
                    'expression': 'LITERAL',
                    'operand': True
                    },
                'create': ['[aanvraag]']
            },
            {
                'act': '<<aanvraag kinderbijslag toekennen>>',
                'actor': '[Minister]',
                'object': '[aanvraag]',
                'recipient': '[ouder]',
                'preconditions': {
                    'expression': 'LITERAL',
                    'operand': True
                }
            }
            ],
        'facts': [
          {
            'fact': '[aanvraag]',
            'function': {
                'expression': 'CREATE',
                'operands': []
                }
          }
        ],
        'duties': []
        }

        result, test = await runScenario(
            model,
            {'actor': []},
            [
                takeAction('actor', '<<kinderbijslag aanvragen>>', FactResolver(None, True, True)),
                takeAction('actor', '<<kinderbijslag aanvragen>>', FactResolver(None, True, True)),
                expectAvailableActs('actor', []),
                expectPotentialActs('actor', ['<<kinderbijslag aanvragen>>', '<<aanvraag kinderbijslag toekennen>>'])
            ]
            )

        self.assertEqual(test, True)


'should not show an act as available when only a not prevents it'
class TestScenarioLawReg15(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestLawReg15()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestLawReg15(self):
        model = {
            'model': 'Fictieve verwelkomingsregeling Staat der Nederlanden',
            'acts': [
                {
                    'act': '<<ingezetene kan verwelkomst van overheid aanvragen>>',
                    'action': '[aanvragen]',
                    'actor': '[ingezetene]',
                    'object': '[aanvraag verwelkomst]',
                    'recipient': '[overheid]',
                    'preconditions': 'NIET [langer dan een jaar geleden gearriveerd]',
                    'create': '<verwelkomen>',
                    'terminate': '',
                    'reference': 'art 2.1',
                    'sourcetext': '',
                    'explanation': '',
                    'version': '2-[19980101]-[jjjjmmdd]',
                    'juriconnect': 'jci1.3:c:BWBR0005537&hoofdstuk=1&titeldeel=1.1&artikel=1:3&lid=3&z=2017-03-01&g=2017-03-01'
                }],
            'facts': [
                { 'fact': '[ingezetene]', 'function': '[]', 'reference': 'art 1.1' },
                { 'fact': '[overheid]', 'function': '[]', 'reference': '' },
                { 'fact': '[verwelkomst]', 'function': { 'expression': 'CREATE', 'operands': [] }, 'reference': '' }
                ],
            'duties': []
            }

        result, test = await runScenario(
            model,
            {'ingezetene': ['[ingezetene]'], 'overheid': ['[overheid]']},
            [
                expectAvailableActs('ingezetene', []),
                expectAvailableActs('ingezetene', [], factResolverFactory({'[aanvraag verwelkomst]': True}))
            ]
            )

        self.assertEqual(test, True)

'should be able to take an action dependent on recursive facts'
class TestScenarioLawReg16(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestLawReg16()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    def factsSupplied(self, actors, notUsed):
        return {
            '[belanghebbende]': IdentityUtil.identityExpression(actors['actor']['did']),
            '[bij wettelijk voorschrift is anders bepaald]': False,
            '[verzoek een besluit te nemen]': True,
            '[wetgevende macht]': True
        }

    async def asyncTestLawReg16(self):
        completedFacts = {
            '[verzoek een besluit te nemen]': True,
            '[wetgevende macht]': True,
            '[bij wettelijk voorschrift is anders bepaald]': False
            }

        result, test = await runScenario(
            awb,
            {'actor': ['[persoon wiens belang rechtstreeks bij een besluit is betrokken]']},
            [
                takeAction('actor', '<<indienen verzoek een besluit te nemen>>', factResolverFactory(completedFacts)),
                expectData('actor', '<<indienen verzoek een besluit te nemen>>', self.factsSupplied)
            ]
            )

        self.assertEqual(test, True)

'should be able to take an action dependent on recursive facts'
class TestScenarioLawReg17(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestLawReg17()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    def factsSupplied(self, actors, actionLinks):
        return {
            '[bestuursorgaan]': IdentityUtil.identityExpression(actors['bestuursorgaan']['did']),
            '[aanvraag]': actionLinks[len(actionLinks) - 2],
            '[aanvraag is geheel of gedeeltelijk geweigerd op grond van artikel 2:15 Awb]': True,
            '[persoon wiens belang rechtstreeks bij een besluit is betrokken]': True,
            '[wetgevende macht]': True
        }

    async def asyncTestLawReg17(self):
        belanghebbendeFacts = {
            '[persoon wiens belang rechtstreeks bij een besluit is betrokken]': True,
            '[verzoek een besluit te nemen]': True,
            '[wetgevende macht]': True,
            '[bij wettelijk voorschrift is anders bepaald]': False
            }

        bestuursorgaanFacts = {
            '[persoon wiens belang rechtstreeks bij een besluit is betrokken]': True,
            '[wetgevende macht]': True,
            '[aanvraag is geheel of gedeeltelijk geweigerd op grond van artikel 2:15 Awb]': True
            }

        result, test = await runScenario(
            awb,
            {'belanghebbende': [], 'bestuursorgaan': []},
            [
                takeAction('belanghebbende', '<<indienen verzoek een besluit te nemen>>', factResolverFactory(belanghebbendeFacts)),
                takeAction('bestuursorgaan', '<<besluiten de aanvraag niet te behandelen>>', factResolverFactory(bestuursorgaanFacts)),
                expectData('ingezetene', '<<besluiten de aanvraag niet te behandelen>>', self.factsSupplied)
            ])
        self.assertEqual(test, True)

'should be able to fill functions of single and multiple facts'
class TestScenarioLawReg18(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestLawReg18()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    async def asyncTestLawReg18(self):
        model = {
            'acts': [],
            'facts': [
                {
                    'explanation': '',
                    'fact': '[belanghebbende]',
                    'function': '[persoon wiens belang rechtstreeks bij een besluit is betrokken]',
                    'reference': 'art. 1:2 lid 1 Awb',
                    'version': '2-[19940101]-[jjjjmmdd]',
                    'juriconnect': 'jci1.3:c:BWBR0005537&hoofdstuk=1&titeldeel=1.1&artikel=1:2&lid=1&z=2017-03-10&g=2017-03-10',
                    'sourcetext': '{Onder belanghebbende wordt verstaan: degene wiens belang rechtstreeks bij een besluit is betrokken}'
                },
                {
                    'explanation': '',
                    'fact': '[toezending besluit aan aanvrager]',
                    'function': '[]',
                    'reference': 'art 3:41 lid 1 Awb',
                    'version': '',
                    'juriconnect': '',
                    'sourcetext': ''
                },
                {
                    'explanation': '',
                    'fact': '[toezending besluit aan meer belanghebbenden]',
                    'function': '[]',
                    'reference': 'art 3:41 lid 1 Awb',
                    'version': '',
                    'juriconnect': '',
                    'sourcetext': ''
                },
                {
                    'explanation': '',
                    'fact': '[uitreiking besluit aan aanvrager]',
                    'function': '[]',
                    'reference': 'art 3:41 lid 1 Awb',
                    'version': '',
                    'juriconnect': '',
                    'sourcetext': ''
                },
                {
                    'explanation': '',
                    'fact': '[uitreiking besluit aan meer belanghebbenden]',
                    'function': '[]',
                    'reference': 'art 3:41 lid 1 Awb',
                    'version': '',
                    'juriconnect': '',
                    'sourcetext': ''
                }
            ],
            'duties': []
            }

        result, test = await runScenario(
            model,
            {'actor': ['[uitreiking besluit aan aanvrager]', '[toezending besluit aan aanvrager]']},
            [
                expectRetrievedFactFunction('actor', '[uitreiking besluit aan aanvrager]',
                                            {
                                                'expression': 'OR',
                                                'operands': [
                                                    'IS:did:discipl:ephemeral:1234'
                                                ]
                                            }),
                expectRetrievedFactFunction('actor', '[toezending besluit aan aanvrager]',
                                            {
                                                'expression': 'OR',
                                                'operands': [
                                                    'IS:did:discipl:ephemeral:1234'
                                                ]
                                            })
            ],
            {
                '[uitreiking besluit aan aanvrager]': 'IS:did:discipl:ephemeral:1234',
                '[toezending besluit aan aanvrager]': 'IS:did:discipl:ephemeral:1234'
            }
        )

        self.assertEqual(test, True)

'should be able to perform an action where the object originates from one of two other actions'
class TestScenarioLawReg19(TestCore):
    def test_Check_Action_Model(self):
        tasks = [self.asyncTestLawReg19()]
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(tasks))
        # loop.close()

    def factsSupplied1(self, actors, actionLinks):
        return {
            '[baker]': IdentityUtil.identityExpression(actors['baker']['did']),
            '[bakery]': True,
            '[cookie]': actionLinks[1]
            }

    def factsSupplied2(self, actors, actionLinks):
        return {
            '[baker]': IdentityUtil.identityExpression(actors['baker']['did']),
            '[bakery]': True,
            '[cookie]': actionLinks[2]
            }

    async def asyncTestLawReg19(self):
        model = {
            'acts': [
                {
                    'act': '<<bake cookie>>',
                    'actor': '[baker]',
                    'object': '[dough]',
                    'recipient': '[bakery]',
                    'preconditions': '[]',
                    'create': ['[cookie]']
                },
                {
                    'act': '<<eat cookie>>',
                    'actor': '[baker]',
                    'object': '[cookie]',
                    'recipient': '[bakery]',
                    'preconditions': '[]',
                    'terminate': ['[cookie]']
                }
            ],
            'facts': [
                {
                    'fact': '[cookie]',
                    'function': {
                        'expression': 'CREATE',
                        'operands': []
                    }
                }
            ],
            'duties': []
        }

        result, test = await runScenario(
            model,
            {'baker': ['[baker]']},
            [
                takeAction('baker', '<<bake cookie>>', LocalFactResolver3()),
                takeAction('baker', '<<bake cookie>>', LocalFactResolver3()),
                takeAction('baker', '<<eat cookie>>', LocalFactResolver3()),
                expectData('baker', '<<eat cookie>>', self.factsSupplied1),

                takeAction('baker', '<<eat cookie>>', LocalFactResolver3()),
                expectData('baker', '<<eat cookie>>', self.factsSupplied2),
            ])

        self.assertEqual(test, True)

#dynamisch functies/classes inladen
#meerdere flint files, die aan elkaar kunnen refereren
#elk flint model koppelen aan wetsbron (source)