import sys
from nacl.public import PrivateKey, PublicKey
from nacl.signing import SigningKey
from nacl.signing import VerifyKey
from nacl.encoding import Base64Encoder
from nacl.bindings.utils import sodium_memcmp
from src.Ephemeral.crypto.CryptoUtil import CryptoUtil
import nacl.hash
import base64
import os
from reactivex import create, Observer, Subject

def writeToFile(message, filename):
    original_stdout = sys.stdout
    with open(filename, 'w') as f:
        sys.stdout = f  # Change the standard output to the file we created.

        print(message)
        sys.stdout = original_stdout  # Reset the standard output to its original value

def appendToFile(message, filename):
    original_stdout = sys.stdout
    with open(filename, 'a') as f:
        sys.stdout = f  # Change the standard output to the file we created.

        print(message)
        sys.stdout = original_stdout  # Reset the standard output to its original value

def encode():
    HASHER = nacl.hash.sha256
    # could be nacl.hash.sha512 or nacl.hash.blake2b instead

    # define a 1024 bytes log message
    msg = 2 * b'256 BytesMessage'
    digest = HASHER(msg, encoder=nacl.encoding.HexEncoder)

    # now send msg and digest to the user
    print(nacl.encoding.HexEncoder.encode(msg))
    print(digest)

    return nacl.encoding.HexEncoder.encode(msg), digest

def verify(received_msg, dgst):
    HASHER = nacl.hash.sha256

    msg = nacl.encoding.HexEncoder.decode(received_msg)

    print(received_msg)
    print(msg)
    shortened = msg[:-1]
    modified = b'modified' + msg[:-8]

    orig_dgs = HASHER(msg, encoder=nacl.encoding.HexEncoder)
    shrt_dgs = HASHER(shortened, encoder=nacl.encoding.HexEncoder)
    mdfd_dgs = HASHER(modified, encoder=nacl.encoding.HexEncoder)

    if sodium_memcmp(orig_dgs, dgst):
        print("are the same")
        print(orig_dgs)
        print(dgst)

def signAMessage(message):
    # Generate a new random signing key
    signing_key = SigningKey.generate()

    # Sign a message with the signing key
    md = CryptoUtil.objectToDigest("Attack at Dawn")
    signed = signing_key.sign(md)

    # Obtain the verify key for a given signing key
    verify_key = signing_key.verify_key

    # Serialize the verify key to send it to a third party
    verify_key_bytes = verify_key.encode()

    return signed, verify_key_bytes

def verifySignature(signed, verify_key_bytes):
    # Create a VerifyKey object from a hex serialized public key
    verify_key = VerifyKey(verify_key_bytes)

    # Check the validity of a message's signature
    # The message and the signature can either be passed together, or
    # separately if the signature is decoded to raw bytes.
    # These are equivalent:
    verify_key.verify(signed)
    verify_key.verify(signed.message, signed.signature)

    md = CryptoUtil.objectToDigest("Attack at Dawn")
    verify = verify_key.verify(md, signed.signature)

    # Alter the signed message text
    #forged = signed[:-1] + bytes([int(signed[-1]) ^ 1])
    # Will raise nacl.exceptions.BadSignatureError, since the signature check
    # is failing
    #verify_key.verify(forged)

def someTestingWithSigningKey():
    # Generate a new signing key
    signing_key_original = nacl.signing.SigningKey.generate()
    # Obtain its verify key (which we will use to test the reloaded signing key)
    verify_key = signing_key_original.verify_key
    # Export the signing key to hex (binary)
    private_key_bytes = signing_key_original.encode(encoder=nacl.encoding.HexEncoder)
    # Save the signing key to the environment variables
    if os.supports_bytes_environ:
        # The operating system supports hex-encoded environment variables
        os.environb['private_key_bytes'] = private_key_bytes
    else:
        # The operating system does not support hex-encoded environment variables - decode to utf-8
        private_key_utf8 = private_key_bytes.decode('utf-8')
        os.environ['private_key_utf8'] = private_key_utf8

    # Now reverse the process (by loading a signing key from the appropriate environment variable)
    if os.supports_bytes_environ:
        # The operating system supports hex-encoded environment variable
        private_key_bytes_loaded = os.environb['private_key_bytes']
    else:
        # The operating system does not support hex-encoded environment variables - look for the utf-8 encoded private key
        private_key_utf8_loaded = os.environ['private_key_utf8']
        private_key_bytes_loaded = str.encode(private_key_utf8_loaded)

    # Create a PyNaCL signing key from the loaded private key
    signing_key_loaded = nacl.signing.SigningKey(private_key_bytes_loaded, encoder=nacl.encoding.HexEncoder)
    # Sign a new message
    signed_with_loaded = signing_key_loaded.sign(b"PyNaCl signing keys can be exported, saved and loaded")
    # Verify the new signed message with the original verify key (the one we created with signing_key_original)
    print(verify_key.verify(signed_with_loaded))

def doSomeWork():
    source = Subject();

    source.on_next("alpha")

    wouter = LocalObserver("Wouter")
    harry = LocalObserver("Harry")

    source.subscribe(wouter)
    source.subscribe(harry)

    source.on_next("beta")

    #    on_next=lambda i: print("Received {0}".format(i)), \
    #    on_error=lambda e: print("Error Occurred: {0}".format(e)), \
    #    on_completed=lambda: print("Done!"), \
    #    )

class LocalObserver(Observer):
    def __init__(self, name):
        Observer.__init__(self)
        self.name = name

    def on_next(self, value):
        print("{0} Received {1}".format(self.name, value))

    def on_error(self, value):
        print("Error {0}".format(value))

    def on_completed(self):
        print("Done!")