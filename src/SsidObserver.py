from reactivex import create, Observer, Subject

class SsidObserver(Observer):
    def __init__(self, ssid, accessorSsid):
        Observer.__init__(self)
        self.ssid = ssid
        self.accessorSsid = accessorSsid
        self.notifications = []

    def on_next(self, value):
        print("{0} Received {1}".format(self.ssid, value))
        self.notifications.append(value)

    def on_error(self, value):
        print("Error {0}".format(value))

    def on_completed(self):
        print("Done!")

    def getLastNotification(self):
        return self.notifications[-1]