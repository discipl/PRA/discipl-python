from src.BaseConnector.Base_Connector import BaseConnector
from src.Core import DisciplCore

#import { map } from 'rxjs/operators'
#import { DisciplCore, ObserveResult } from '@discipl/core'

ABUNDANCE_SERVICE_NEED_PREDICATE = 'need'
ABUNDANCE_SERVICE_ATTENDTO_PREDICATE = 'attendTo'
ABUNDANCE_SERVICE_MATCH_PREDICATE = 'matchedNeed'
ABUNDANCE_SERVICE_OFFER_PREDICATE = 'offer'
ABUNDANCE_SERVICE_REQUIRE_PREDICATE = 'require'
ABUNDANCE_SERVICE_REFER_TO_PREDICATE = 'referTo'
ABUNDANCE_SERVICE_REFERRED_FROM_PREDICATE = 'referredFrom'

class AbundanceService:
    def __init__(self, core = DisciplCore()):
        self.core = core
        self.ssid1 = None
        self.ssid2 = None
        self.ssid3 = None
        self.ssid4 = None
        self.matchClaim = None
        self.serviceInfoClaim = None

    #
    # retrieve the loaded discipl core api object used by this module
    # @return {DisciplCore}
    #
    def getCoreAPI (self):
        return self.core

    '''
    * @typedef {Object} AttendResult
    * @property {object} ssid - Ssid created to publically register attendance
    * @property {core.ObservableResult} observableResult - Can be subscribed on to implement serving of the need
    '''

    '''
    * Register an abundance service by creating a new ssid that claims the service and what needs it attends to in itś
    * channel on the platform corresponding to the given connector
    *
    * It will automatically set up private channels with matched needing parties
    *
    * @param {string} connectorName
    * @param {string} what - Identifier of the need being attended to
    * @param {string[]} requirements - Hints to what information is required to fulfill the need
    * @returns {AttendResult}
    '''
    async def attendTo1 (self, connectorName, what, requirements):
        self.ssid1 = await self.core.newSsid(connectorName)
        await self.core.allow(self.ssid1)
        await self.core.claim(self.ssid1, {ABUNDANCE_SERVICE_ATTENDTO_PREDICATE: what })

        await self.core.observe(None, { 'did': None, 'privkey': None, 'signingkey': None }, {ABUNDANCE_SERVICE_NEED_PREDICATE: what }, False, await self.core.getConnector(connectorName))

    async def attendTo2(self, connectorName, what, requirements):
        #claim = await self.core.getFirstClaim(self.ssid1['did'], self.ssid1['did'])
        claim = await self.core.getLastClaim(None, self.ssid1['did'])
        self.ssid3 = await self.refer(self.ssid1, claim['did'])

        # ssid2 want that ssid3 can follow claims of ssid2 with filter referTo: {}
        await self.core.observe(claim['did'], self.ssid3, {ABUNDANCE_SERVICE_REFER_TO_PREDICATE: None}, False)

        await self.match(self.ssid3, claim['did'])

    async def attendTo3(self, connectorName, what, requirements):
        await self.core.observe(self.ssid4, self.ssid3)

        await self.core.allow(self.ssid3, None, self.ssid4['did'])

    async def attendTo4(self, connectorName, what, requirements):
        await self.require(self.ssid3, requirements)

    async def attendTo5(self):
        return {
            'theirPrivateDid': self.ssid4['did'],
            'myPrivateSsid': self.ssid3
            }

    '''
    * @typedef {Object} NeedResult
    * @property {object} needSsid - Ssid created to publically express the need
    * @property {object} myPrivateSsid - Ssid created to privately communicate with party serving need
    * @property {string} theirPrivateDid - Did created to match need in private
    * @property {Promise<object>} serviceInformationPromise - Promise with the first claim made in the private channel (likely require)
    */
    '''
    '''
    * Register a need by creating a new ssid that claims the need in itś channel on the platform corresponding to the given connector
    *
    * It will then set up a private channel to communicate with the party that matches the need
    *
    * @param {string} connectorName
    * @param {string} what - Identifier of the need being expressed
    * @returns {NeedResult}
    '''

    async def need1(self, connectorName, what):
        self.ssid2 = await self.core.newSsid(connectorName)

        await self.core.observe(None, self.ssid2, {ABUNDANCE_SERVICE_MATCH_PREDICATE: self.ssid2['did']}, False,
                                await self.core.getConnector(connectorName))

        await self.core.allow(self.ssid2)
        await self.core.claim(self.ssid2, {ABUNDANCE_SERVICE_NEED_PREDICATE: what})

    async def need2(self, connectorName, what):
        self.matchClaim = await self.core.getLastClaim(self.ssid3['did'], self.ssid2['did'])

        # ssid3 wants that ssid2 can follow all claims of ssid3
        await self.core.observe(self.ssid3['did'], self.ssid2)

        self.ssid4 = await self.refer(self.ssid2, self.matchClaim['did'])


    async def need3(self, connectorName, what):
        self.serviceInfoClaim = await self.core.getLastClaim(self.ssid3['did'], self.ssid2['did'])

        return {
            'needSsid': self.ssid2,
            'myPrivateSsid': self.ssid4,
            'theirPrivateDid': self.ssid3['did'],
            'serviceInformationPromise': self.serviceInfoClaim['claim']['data']
        }

    '''
    * Register a match between a abundance service and a need attending to it. Note that matches can be
    * registered between needs and services that are not really registered to be attending to those needs.
    * Registering the match by a service will trigger actors observing their need
    '''
    async def match (self, referralSsid, didInNeed):
        return await self.core.attest(referralSsid, ABUNDANCE_SERVICE_MATCH_PREDICATE, didInNeed)

    async def require (self, ssid, requirements):
        return await self.core.claim(ssid, { ABUNDANCE_SERVICE_REQUIRE_PREDICATE: requirements })

    '''
    * Offer a fulfillment of a need. self must be a link to another claim
    *
    * @param {object} privateSsid
    * @param {string} link
    * @returns {Promise<string>} - The resulting attestation link
    '''
    async def offer (self, privateSsid, link):
        return await self.core.attest(privateSsid, ABUNDANCE_SERVICE_OFFER_PREDICATE, link)

    '''
    * Observe an offer being made in the private channel
    *
    * @param {string} did - Of the party making the offer
    * @param {object} ssid - That allows access to the offer
    * @returns {Promise<{data: Object, previous: string}>} - The contents of the linked offer
    '''
    async def observeOffer1 (self, did, ssid):
        await self.core.observe(did, ssid, { ABUNDANCE_SERVICE_OFFER_PREDICATE: None })

    async def observeOffer2(self, did, ssid):
        offer = await self.core.getFirstClaim(did, ssid['did'])
        link = offer['claim']['data'][ABUNDANCE_SERVICE_OFFER_PREDICATE]
        claim = await self.core.get(link, ssid)
        return claim

    async def refer (self, originSsid, targetDid):
        # TODO: When refer is needed to another platform, allow configuration of this variable
        connectorName = BaseConnector.getConnectorName(originSsid['did'])

        referralSsid = await self.core.newSsid(connectorName)
        await self.core.allow(referralSsid, None, targetDid)

        await self.core.claim(referralSsid, { ABUNDANCE_SERVICE_REFERRED_FROM_PREDICATE: originSsid['did']})
        await self.core.claim(originSsid, { ABUNDANCE_SERVICE_REFER_TO_PREDICATE: referralSsid['did'] })

        return referralSsid


