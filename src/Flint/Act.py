class Act:
    def __init__(self, act, actor, object, recipient, precondition, create, terminate, sources, explanation):
        self.act = act
        self.actor = actor
        self.object = object
        self.recipient = recipient
        self.precondition = precondition
        self.create = create
        self.terminate = terminate
        self.sources = sources
        self.explanation = explanation
