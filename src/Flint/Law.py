import json
from src.Flint.Act import Act
from src.Flint.Fact import Fact
from src.Flint.Source import Source

class Law:
    def __init__(self, jsonFilePath):
        path = jsonFilePath
        f = open(path, encoding="utf8")
        self.lawJson = json.load(f)
        f.close()

        self.source = None
        self.acts = []
        self.facts = []
        self.duties = []

    def parseLawJson(self):
        for key, value in self.lawJson.items():
            if key == "source":
                self.source = self.parseSource(value)
            if key == "acts":
                for i in range(len(value)):
                    self.acts.append(self.parseAct(value[i]))
            if key == "facts":
                for i in range(len(value)):
                    self.facts.append(self.parseFact(value[i]))
            if key == "duties":
                for i in range(len(value)):
                    self.duties.append(self.parseDuty(value[i]))

    def parseSource(self, dictSource):
        validFrom = ""
        validTo = ""
        citation = ""
        juriconnect = ""
        text = ""
        for sourceKey, sourceValue in dictSource.items():
            if sourceKey == "validFrom":
                validFrom = sourceValue
            if sourceKey == "validTo":
                validTo = sourceValue
            if sourceKey == "citation":
                citation = sourceValue
            if sourceKey == "juriconnect":
                juriconnect = sourceValue
            if sourceKey == "text":
                text = sourceValue
        return Source(validFrom, validTo, citation, juriconnect, text)

    def parseAct(self, dictAct):
        act = ""
        actor = ""
        object = ""
        recipient = ""
        precondition = ""
        create = ""
        terminate = ""
        sources =[]
        explanation = ""
        for actKey, actValue in dictAct.items():
            if actKey == "act":
                act = actValue
            if actKey == "actor":
                actor = actValue
            if actKey == "object":
                object = actValue
            if actKey == "recipient":
                recipient = actValue
            if actKey == "precondition":
                precondition = actValue
            if actKey == "create":
                create = actValue
            if actKey == "terminate":
                terminate = actValue
            if actKey == "sources":
                for i in range(len(actValue)):
                    source = self.parseSource(actValue[i])
                    sources.append(source)
            if actKey == "explanation":
                explanation = actValue
        return Act(act, actor, object, recipient, precondition, create, terminate, sources, explanation)

    def parseFact(self, dictFact):
        fact = ""
        function = ""
        sources = []
        explanation = ""
        for factKey, factValue in dictFact.items():
            if factKey == "fact":
                fact = factValue
            if factKey == "function":
                function = factValue
            if factKey == "sources":
                for i in range(len(factValue)):
                    source = self.parseSource(factValue[i])
                    sources.append(source)
            if factKey == "explanation":
                explanation = factValue
        return Fact(fact, function, sources, explanation)

    def parseDutyAct(self, dictDuty):
        duty = ""
        enforce == ""
        dutyHolder = ""
        claiment = ""
        create = ""
        terminate = ""
        sources = []
        explanatio = ""
        for dutyKey, dutyValue in dictDuty.items():
            if dutyKey == "duty":
                duty = dutyValue
            if dutyKey == "enforce":
                enforce = dutyValue
            if dutyKey == "duty-holder":
                dutyHolder = dutyValue
            if dutyKey == "claiment":
                claiment = dutyValue
            if dutyKey == "create":
                create = dutyValue
            if dutyKey == "terminate":
                terminate = dutyValue
            if dutyKey == "sources":
                for i in range(len(dutyValue)):
                    source = self.parseSource(dutyValue[i])
                    sources.append(source)
            if dutyKey == "explanation":
                explanation = dutyValue
        return Duty(duty, enforce, dutyHolder, claiment, create, terminate, sources, explanation)

    def getSource(self):
        return self.source

    def getAct(self, act):
        for i in range(len(self.acts)):
            if self.acts[i].act == act:
                return self.acts[i]

    def getFact(self, fact):
        for i in range(len(self.facts)):
            if self.facts[i].fact == fact:
                return self.facts[i]