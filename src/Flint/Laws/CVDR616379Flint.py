from datetime import date, datetime, timedelta
from dateutil.relativedelta import relativedelta

class CVDR616379Flint:
    def __init__(self, law, birthdatesChildren, isAlleenstaande, inkomen, totaleKinderbijslag, bijstandsnormAlleenstaande, bijstandsnormGehuwden):
        self.law = law

        self.birthdatesChildren = []
        for i in range(len(birthdatesChildren)):
            if self.isMinderjarige(birthdatesChildren[i]):
                self.birthdatesChildren.append(birthdatesChildren[i])

        self.isAlleenstaande = isAlleenstaande
        self.inkomen = inkomen
        self.totaleKinderbijslag = totaleKinderbijslag
        self.bijstandsnormAlleenstaande = bijstandsnormAlleenstaande
        self.bijstandsnormGehuwden = bijstandsnormGehuwden

        self.peilDatum = None
        self.refertePeriode = None

    def source(self):
        return self.law.getSource()

    def verzoekIndienenAct(self):
        return self.law.getAct("[wet].<<verzoek indienen>>")

    def wetFact(self):
        return self.law.getFact("[wet]")

    def gemeenteUtrechtFact(self):
        return self.law.getFact("[Gemeente Utrecht]")

    def collegeFact(self):
        return self.law.getFact("[college]")

    def inkomenFact(self):
        return self.law.getFact("[inkomen]")

    def gezinFact(self):
        return self.law.getFact("[gezin]")

    def individueleInkomenstoeslagFact(self):
        return self.law.getFact("[Individuele Inkomenstoeslag]")

    def peildatumFact(self):
        return self.law.getFact("[peildatum]")

    def refertePeriodeFact(self):
        return self.law.getFact("[referteperiode]")

    def kindFact(self):
        return self.law.getFact("[kind]")

    def formulierFact(self):
        return self.law.getFact("[formulier]")

    def langdurigLaagInkomenFact(self):
        return self.law.getFact("[landurig laag inkomen]")

    def landurigLaagInkomenAleenstaandeOfGezinFact(self):
        return self.law.getFact("[landurig laag inkomen aleenstaande of gezin]")

    def langdurigLaagInkomenAlleenstaandeFact(self):
        return self.law.getFact("[langdurig laag inkomen alleenstaande]")

    def langdurigLaagInkomenGezinFact(self):
        return self.law.getFact("[langdurig laag inkomen gezin]")

    def nadereRegelsFact(self):
        return self.law.getFact("[nadere regels]")

    def toeslagFact(self):
        return self.law.getFact("[toeslag]")

    def toeslagAlleenstaandeFact(self):
        return self.law.getFact("[toeslag alleenstaande]")

    def toeslagAlleenstaandeOuderMetUitsluitendKinderenJongerDan12JaarFact(self):
        return self.law.getFact("[toeslag alleenstaande ouder met uitsluitend kinderen jonger dan 12 jaar]")

    def toeslagAlleenstaandeOuderMetTenMinste1KindVan12JaarOfOuderFact(self):
        return self.law.getFact("[toeslag alleenstaande ouder met ten minste 1 kind van 12 jaar of ouder]")

    def toeslagGehuwdenOfSamenwonendenZonderKinderenOfMetUitsluitendKinderenJongerDan12JaarFact(self):
        return self.law.getFact("[toeslag gehuwden/samenwonenden zonder kinderen of met uitsluitend kinderen jonger dan 12 jaar]")

    def toeslagGehuwdenOfSamenwonendenMetTenminste1KindVan12JaarOfOuderEnEenInkomenOnder110ProcentVanDeBijstandsnormVoorGehuwdenOfSamenwonendenFact(self):
        return self.law.getFact("[toeslag gehuwden/samenwonenden met tenminste 1 kind van 12 jaar of ouder en een inkomen onder 110% van de bijstandsnorm voor gehuwden/samenwonenden]")

    def toeslagGehuwdenOfSamenwonendenMetTenminste1KindVan12JaarOfOuderEnEenInkomenTussen110En125ProcentVanDeBijstandsnormVoorGehuwdenOfSamenwonendenFact(self):
        return self.law.getFact("[toeslag gehuwden/samenwonenden met tenminste 1 kind van 12 jaar of ouder en een inkomen tussen 110 en 125% van de bijstandsnorm voor gehuwden/samenwonenden]")

    def samenMetNietRechtHebbendeFact(self):
        return self.law.getFact("[SamenMetNietRechtHebbende]")

    def alleenstaandeOuderMetUitsluitendKinderenJongerDan12JaarFact(self):
        return self.law.getFact("[alleenstaande ouder met uitsluitend kinderen jonger dan 12 jaar]")

    def alleenstaandeOuderMetTenMinste1KindVan12JaarOfOuderFact(self):
        return self.law.getFact("[alleenstaande ouder met ten minste 1 kind van 12 jaar of ouder]")

    def effectivityFact(self):
        return self.law.getFact("[effectivity]")

    def indexationFact(self):
        return self.law.getFact("[indexation]")

    def geindexeerdeToeslagFact(self):
        return self.law.getFact("[geindexeerde toeslag]")

    def hardheidsclausuleFact(self):
        return self.law.getFact("[hardheidsclausule]")

    def validFromFact(self):
        return self.law.getFact("[validFrom]")

    def citeertitelFact(self):
        return self.law.getFact("[citeertitel]")

    def setPeilDatum(self, date):
        self.peilDatum = date
        self.refertePeriode = date - relativedelta(years=3)

    # functions to resolve facts
    def isMinderjarige(self, dateOfBirth):
        age = self.getAgeInYears(datetime.now(), dateOfBirth)
        if age < 18:
            return True
        return False

    def getAgeInYears(self, startDate, endDate):
        return relativedelta(startDate, endDate).years

    def isLangdurigLaagInkomenAlleenstaande(self):
        if self.inkomen - self.totaleKinderbijslag < 1.1 * self.bijstandsnormAlleenstaande:
            return True
        return False

    def isLangdurigLaagInkomenGezin(self):
        if self.inkomen - self.totaleKinderbijslag < 1.25 * self.bijstandsnormGehuwden:
            return True
        return False

    def toeslag(self):
        return self.toeslagAlleenstaande() + \
               self.toeslagAlleenstaandeOuderMetUitsluitendKinderenJongerDan12Jaar() + \
               self.toeslagAlleenstaandeOuderMetTenMinste1KindVan12JaarOfOuder() + \
               self.toeslagGehuwdenOfSamenwonendenZonderKinderenOfMetUitsluitendKinderenJongerDan12Jaar() + \
               self.toeslagGehuwdenOfSamenwonendenMetTenminste1KindVan12JaarOfOuderEnEenInkomenOnder110ProcentVanDeBijstandsnormVoorGehuwdenOfSamenwonenden() + \
               self.toeslagGehuwdenOfSamenwonendenMetTenminste1KindVan12JaarOfOuderEnEenInkomenTussen110En125ProcentVanDeBijstandsnormVoorGehuwdenOfSamenwonenden();


    # als alleenstaande: 225 euro
    def toeslagAlleenstaande(self):
        if self.isAlleenstaande:
            return 225.0;
        return 0.0

    def toeslagAlleenstaandeOuderMetUitsluitendKinderenJongerDan12Jaar(self):
        if not self.isAlleenstaande:
            return 0.0

        if self.birthdatesChildren == None or len(self.birthdatesChildren) == 0:
            return 0.0

        for i in range(len(self.birthdatesChildren)):
            if self.getAgeInYears(datetime.now(), self.birthdatesChildren[i]) >= 12:
                return 0.0
        return 50

    def toeslagAlleenstaandeOuderMetTenMinste1KindVan12JaarOfOuder(self):
        if not self.isAlleenstaande:
            return 0.0

        if self.birthdatesChildren == None or len(self.birthdatesChildren) == 0:
            return 0.0

        for i in range(len(self.birthdatesChildren)):
            if self.getAgeInYears(datetime.now(), self.birthdatesChildren[i]) >= 12:
                return 612.0
        return 0.0

    def toeslagGehuwdenOfSamenwonendenZonderKinderenOfMetUitsluitendKinderenJongerDan12Jaar(self):
        if self.isAlleenstaande:
            return 0.0

        if self.birthdatesChildren == None or len(self.birthdatesChildren) == 0:
            return 575.0

        for i in range(len(self.birthdatesChildren)):
            if self.getAgeInYears(datetime.now(), self.birthdatesChildren[i]) >= 12:
                return 0.0
        return 575.0

    def toeslagGehuwdenOfSamenwonendenMetTenminste1KindVan12JaarOfOuderEnEenInkomenOnder110ProcentVanDeBijstandsnormVoorGehuwdenOfSamenwonenden(self):
        if self.isAlleenstaande:
            return 0.0

        if self.inkomen >= 1.1 * self.bijstandsnormGehuwden:
            return 0.0

        if self.birthdatesChildren == None or len(self.birthdatesChildren) == 0:
            return 0.0

        for i in range(len(self.birthdatesChildren)):
            if self.getAgeInYears(datetime.now(), self.birthdatesChildren[i]) >= 12:
                return 815.0
        return 0.0

    def toeslagGehuwdenOfSamenwonendenMetTenminste1KindVan12JaarOfOuderEnEenInkomenTussen110En125ProcentVanDeBijstandsnormVoorGehuwdenOfSamenwonenden(self):
        if self.isAlleenstaande:
            return 0.0

        if self.inkomen < 1.1 * self.bijstandsnormGehuwden or self.inkomen > 1.25 * self.bijstandsnormGehuwden:
            return 0.0

        if self.birthdatesChildren == None or len(self.birthdatesChildren) == 0:
            return 0.0

        for i in range(len(self.birthdatesChildren)):
            if self.getAgeInYears(datetime.now(), self.birthdatesChildren[i]) >= 12:
                return 408.0
        return 0.0