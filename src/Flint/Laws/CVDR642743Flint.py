from datetime import date, datetime, timedelta
from dateutil.relativedelta import relativedelta

class CVDR642743Flint:
    def __init__(self, law):
        self.law = law

        self.peilDatum = None
        self.refertePeriode = None
        self.negeerbareKorteOnderbrekingLaagInkomen = None
        self.LaaginkomenGedurendeReferteperiodeAannemelijk = None
        self.voldoendeIngespannen = None
        self.GeenZichtOpInkomensverbetering = None
        self.volledigArbeidsOngeschikt = None
        self.actieveArbeidsverplichting = None
        self.sanctieOpgelegdGekregenVoorSchendingWegensSchendingVanDeArbeidsVerplichting = None
        self.reeleKansOpUitbreidingAantalArbeidsuren = None
        self.studiefinacieringOntvangenTijdensRefertePeriode = None
        self.inAfgelopen12MaandenAlReedsIITAangevraag = None

    def source(self):
        return self.law.getSource()

    def verordeningFact(self):
        return self.law.getAct("[verordening]")

    def arrangement4Fact(self):
        return self.law.getFact("[arrangement 4]")

    def individueleInkomenstoeslagFact(self):
        return self.law.getFact("[Individuele Inkomenstoeslag]")

    def inkomenFact(self):
        return self.law.getFact("[inkomen]")

    def wetFact(self):
        return self.law.getFact("[wet]")

    def inkomenBedoeldInArtikel3EersteLidVanDeVerordeningFact(self):
        return self.law.getFact("[inkomen bedoeld in artikel 3, eerste lid, van de verordening]")

    def peildatumFact(self):
        return self.law.getFact("[peildatum]")

    def referteperiodeFact(self):
        return self.law.getFact("[referteperiode]")

    def nadereRegelsFact(self):
        return self.law.getFact("[nadere regels]")

    def negeerbareKorteOnderbrekingLaagInkomenFact(self):
        return self.law.getFact("[NegeerbareKorteOnderbrekingLaagInkomen]")

    def laaginkomenGedurendeReferteperiodeAannemelijkFact(self):
        return self.law.getFact("[LaaginkomenGedurendeReferteperiodeAannemelijk]")

    def deKrachtenEnBekwaamhedenVanDePersoonFact(self):
        return self.law.getFact("[de krachten en bekwaamheden van de persoon]")

    def deInspanningenDieDePersoonHeeftVerrichtOmTotInkomensverbeteringTeKomenFact(self):
        return self.law.getFact("[de inspanningen die de persoon heeft verricht om tot inkomensverbetering te komen]")

    def geenZichtOpInkomensverbeteringFact(self):
        return self.law.getFact("[GeenZichtOpInkomensverbetering]")

    def arbeidsongeschiktOfArrangement4Fact(self):
        return self.law.getFact("[ArbeidsongeschiktOfArrangement4]")

    def voldoendeIngespannenFact(self):
        return self.law.getFact("[voldoendeingespannen]")

    def personenZonderActieveArbeidsverplichtingenFact(self):
        return self.law.getFact("[personen zonder actieve arbeidsverplichtingen]")

    def personenMetEenAanHetRechtOpUitkeringVerbondenActieveArbeidsverplichtingFact(self):
        return self.law.getFact("[personen met een aan het recht op uitkering verbonden actieve arbeidsverplichting]")

    def personenZonderReeleKansenOpUitbreidingVanHetAantalArbeidsurenFact(self):
        return self.law.getFact("[personen zonder reële kansen op uitbreiding van het aantal arbeidsuren.]")

    def inkomenOpGrondDeWetStudiefinancieringWSF2000OfDeWetTegemoetkomingOnderwijsbijdrageEnSchoolkostenWTOSFact(self):
        return self.law.getFact("[inkomen op grond de Wet Studiefinanciering (WSF 2000) of de Wet tegemoetkoming onderwijsbijdrage en schoolkosten (WTOS)]")

    def voltijdstudieFact(self):
        return self.law.getFact("[voltijdstudie]")

    def inkomensverbeteringWegensStudieFact(self):
        return self.law.getFact("[InkomensverbeteringWegensStudie]")

    def bijzondereOmstandighedenVolgensCollegeFact(self):
        return self.law.getFact("[BijzondereOmstandighedenVolgensCollege]")

    def aanvraagformulierFact(self):
        return self.law.getFact("[aanvraagformulier]")

    def aanvraagmethodeFact(self):
        return self.law.getFact("[aanvraagmethode]")

    def mogelijkNodigNaAanvraagFact(self):
        return self.law.getFact("[mogelijkNodigNaAanvraag]")

    def beslistermijnFact(self):
        return self.law.getFact("[beslistermijn]")

    def aanvraaglimietFact(self):
        return self.law.getFact("[aanvraaglimiet]")

    def validFromFact(self):
        return self.law.getFact("[validFrom]")

    def citeertitelFact(self):
        return self.law.getFact("[citeertitel]")

    def setPeilDatum(self, date):
        self.peilDatum = date
        self.refertePeriode = date - relativedelta(years=3)

    # aanvrager mag niet meer dan 1 maand een inkomen gehad hebben tijdens de referteperiode
    def askNegeerbareKorteOnderbrekingLaagInkomen(self, value):
        self.negeerbareKorteOnderbrekingLaagInkomen = value

    # aanvrager moet mbv bankafschriften gedurende de referteperiode duidelijk maken dat hij/zij van een laag inkomen moest rondkomen
    def askLaaginkomenGedurendeReferteperiodeAannemelijk(self, value):
        self.LaaginkomenGedurendeReferteperiodeAannemelijk = value

    # de aanvrager dient voldoende moeite gedaan te hebben om een hoger inkomen te krijgen
    def askVoldoendeIngespannen(self, value):
        self.voldoendeIngespannen = value

    def askGeenZichtOpInkomensverbetering(self, value):
        self.GeenZichtOpInkomensverbetering = value

    def askvolledigArbeidsOngeschikt(self, value):
        self.volledigArbeidsOngeschikt = value

    # de aanvrager kan een actieve arbeidsverplichting hebben. Zo ja, dan mag de aanvrager de verplichting niet schenden door niet naar werk te zoeken
    def askActieveArbeidsverplichting(self, value):
        self.actieveArbeidsverplichting = value

    def askSanctieOpgelegdGekregenVoorSchendingWegensSchendingVanDeArbeidsVerplichting(self, value):
        self.sanctieOpgelegdGekregenVoorSchendingWegensSchendingVanDeArbeidsVerplichting = value

    def askReeleKansOpUitbreidingAantalArbeidsuren(self, value):
        self.reeleKansOpUitbreidingAantalArbeidsuren = None

    def askStudiefinacieringOntvangenTijdensRefertePeriode(self, value):
        self.studiefinacieringOntvangenTijdensRefertePeriode = value

    def askInAfgelopen12MaandenAlReedsIITAangevraag(self, value):
        self.inAfgelopen12MaandenAlReedsIITAangevraag = value