import asyncio
import websockets
from websockets.exceptions import ConnectionClosed
import base64
import json
import pickle

class MyClient():
    def __init__(self, serverEndpoint, websocketEndpoint):
        self.serverEndpoint = serverEndpoint
        self.websocketEndpoint = websocketEndpoint

    async def claim(self, message):
        async with websockets.connect(self.websocketEndpoint) as websocket:
            send = ['claim', message]
            s = pickle.dumps(send)
            await websocket.send(s)
            greeting = await websocket.recv()
            s = pickle.loads(greeting)
            return s

    async def get(self, reference, pubkey, signature):
        async with websockets.connect(self.websocketEndpoint) as websocket:
            sendDict = {'reference': reference, 'pubkey': pubkey, 'signature': signature}
            send = ['get', sendDict]
            s = pickle.dumps(send)
            await websocket.send(s)
            greeting = await websocket.recv()
            s = pickle.loads(greeting)
            return s

    async def close(self):
        async with websockets.connect(self.websocketEndpoint) as websocket:
            send = ['close', None]
            s = pickle.dumps(send)
            await websocket.send(s)
            greeting = await websocket.recv()
            s = pickle.loads(greeting)

    async def echo(self, message):
        async with websockets.connect(self.websocketEndpoint) as websocket:
            send = ['echo', message]
            s = pickle.dumps(send)
            await websocket.send(s)
            greeting = await websocket.recv()
            s = pickle.loads(greeting)
            return s

    async def clean(self):
        async with websockets.connect(self.websocketEndpoint) as websocket:
            send = ['clean', None]
            s = pickle.dumps(send)
            await websocket.send(s)
            greeting = await websocket.recv()
            s = pickle.loads(greeting)

    async def reset(self):
        async with websockets.connect(self.websocketEndpoint) as websocket:
            send = ['reset', None]
            s = pickle.dumps(send)
            await websocket.send(s)
            greeting = await websocket.recv()
            s = pickle.loads(greeting)

    async def getPublicKey (self, claimId):
        async with websockets.connect(self.websocketEndpoint) as websocket:
            send = ['getPublicKey', claimId]
            s = pickle.dumps(send)
            await websocket.send(s)
            greeting = await websocket.recv()
            s = pickle.loads(greeting)
            return s

    async def getLatest (self, publicKey):
        async with websockets.connect(self.websocketEndpoint) as websocket:
            send = ['getLatest', publicKey]
            s = pickle.dumps(send)
            await websocket.send(s)
            greeting = await websocket.recv()
            s = pickle.loads(greeting)
            return s

    async def observe(self, pubkey, accessorpubkey, accessorsignature, claimFilter, historical):
        async with websockets.connect(self.websocketEndpoint) as websocket:
            sendDict = {'pubkey': pubkey, 'accessorpubkey': accessorpubkey, 'accessorsignature': accessorsignature, 'claimfilter': claimFilter, 'historical': historical}
            send = ['observe', sendDict]
            s = pickle.dumps(send)
            await websocket.send(s)
            greeting = await websocket.recv()
            s = pickle.loads(greeting)
            return s

    async def getObservedClaims(self, amount, ssid, accessorSsid):
        async with websockets.connect(self.websocketEndpoint) as websocket:
            sendDict = {'amount': amount, 'ssid': ssid, 'accessorSsid': accessorSsid}
            send = ['getObservedClaims', sendDict]
            s = pickle.dumps(send)
            await websocket.send(s)
            greeting = await websocket.recv()
            s = pickle.loads(greeting)
            return s

    async def subscribe(self, ssid, accessorSsid):
        async with websockets.connect(self.websocketEndpoint) as websocket:
            sendDict = {'ssid': ssid, 'accessorSsid': accessorSsid}
            send = ['subscribe', sendDict]
            s = pickle.dumps(send)
            await websocket.send(s)

    async def getGlobalObserverCount(self):
        async with websockets.connect(self.websocketEndpoint) as websocket:
            send = ['globalObserverCount', None]
            s = pickle.dumps(send)
            await websocket.send(s)
            greeting = await websocket.recv()
            s = pickle.loads(greeting)
            return s