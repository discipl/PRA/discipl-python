#import { filter, flatMap } from 'rxjs/operators'
from src.BaseConnector.Base_Connector import BaseConnector
from src.Ephemeral.EphemeralStorage import EphemeralStorage
from src.Ephemeral.EphemeralClient import EphemeralClient
from src.Ephemeral.crypto.IdentityFactory import IdentityFactory
from src.Ephemeral.util.DeepCopy import deepcopy
from reactivex import of, operators as op
import logging
import time
from src.Ephemeral.MyClient import MyClient

"""
 * The EphemeralConnector is a connector to be used in discipl-core. If unconfigured, it will use an in-memory
 * storage backend. If configured with endpoints, it will use the EphemeralServer as a backend.
"""
class EphemeralConnector(BaseConnector):
    def __init__(self, loglevel = 'warn', ephemeralClient = EphemeralStorage()):
        super()
        self.ephemeralClient = ephemeralClient
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.DEBUG)
        self.identityFactory = IdentityFactory()
        self.identityFactory.setConnector(self)
        self.myCache = {}
        self.caching = True

    """
    *  Returns the name of this connector. Mainly used in did and link constructions.
    *
    * @returns {string} The string 'ephemeral'.
    """
    def getName (self):
        return 'ephemeral'

    """
    * Configures the connector. If this function is called, it will connect to an instance of EphemeralServer.
    * If not, it will use an in-memory backend.
    *
    * @param {string} serverEndpoint - EphemeralServer endpoint for http calls
    * @param {string} websocketEndpoint - EphemeralServer endpoint for websocket connections
    * @param {object} w3cwebsocket - W3C compatible WebSocket implementation. In the browser, this is window.WebSocket.
    * For node.js, the `websocket` npm package provides a compatible implementation.
    * @param {string} loglevel - Loglevel of the connector. Default at 'warn'. Change to 'info','debug' or 'trace' to
    * get more information
    """
    '''
    def configure (self, serverEndpoint, websocketEndpoint, w3cwebsocket, caching):
        self.ephemeralClient = EphemeralClient(serverEndpoint, websocketEndpoint, w3cwebsocket)
        self.identityFactory.setClient(self.ephemeralClient)
        if caching != None:
            self.caching = caching
    '''
    def configure (self, serverEndpoint, websocketEndpoint, caching = True):
        self.ephemeralClient = MyClient(serverEndpoint, websocketEndpoint)
        self.identityFactory.setClient(self.ephemeralClient)
        self.caching = caching

    """" 
    * Looks up the corresponding did for a particular claim.
    * 
    * This information is saved in the backing memory on calls to claim (either directly, or indirectly through import)
    *
    * @param {string} link - Link to the claim
    * @returns {Promise<string>} Did that made this claim
    """
    async def getDidOfClaim (self, link):
        reference = BaseConnector.referenceFromLink(link)
        return self.didFromReference(await self.ephemeralClient.getPublicKey(reference))

    """
    * Returns a link to the last claim made by this did
    *
    * @param {string} did
    * @returns {Promise<string>} Link to the last claim made by this did
    """
    async def getLatestClaim (self, did):
        # return self.linkFromReference(await self.ephemeralClient.getLatest(BaseConnector.referenceFromDid(did)))
        latestClaim = await self.ephemeralClient.getLatest(BaseConnector.referenceFromDid(did))
        result = None
        if latestClaim != None:
            result = {'claim': {'data': latestClaim['data'],\
                                'signature': latestClaim['signature'],\
                                'previous': self.linkFromReference(latestClaim['previous'])\
                                }, 'pubkey': BaseConnector.referenceFromDid(did)}
        return result

    """
    * @typedef {Object} EphemeralSsid
    * @property {string} did - Did of the created identity
    * @property {string} privkey - PEM-encoded private key
    * @property {string} metadata.cert - PEM-encoded certificate of the identity
    """

    """
    * Generates a new ephemeral identity, backed by a cert generated with forge.
    *
    * @returns {Promise<EphemeralSsid>} ssid-object, containing both the did and the authentication mechanism.
    """
    async def newIdentity (self, options = {}):
        self.logger.info('Creating new identity')
        if 'cert' in options:
            if 'privkey' in options:
                identity = await self.identityFactory.fromCert(options['cert'], options['privkey'])
            else:
                identity = await self.identityFactory.fromCert(options['cert'], None)
        else:
            if 'did' in options:
                reference = BaseConnector.referenceFromDid (options['did'])
                if 'privkey' in options and 'signingkey' in options:
                    identity = await self.identityFactory.fromReference (reference, options['privkey'], options['signingkey'])
                    identity.ssid['did'] = self.didFromReference(reference)
                else:
                    identity = await self.identityFactory.fromReference(reference)
            else:
                identity = await self.identityFactory.newIdentity()

        return identity.ssid

    """
    * Expresses a claim
    *
    * The data will be serialized using a stable stringify that only depends on the actual data being claimed,
    * and not on the order of insertion of attributes.
    * If the exact claim has been made before, this will return the existing link, but not recreate the claim.
    *
    * @param {string} did - Identity that expresses the claim
    * @param {string} privkey - Base64 encoded authentication mechanism
    * @param {object} data - Arbitrary object that constitutes the data being claimed.
    * @param {object} [data.DISCIPL_ALLOW] - Special type of claim that manages ACL
    * @param {string} [data.DISCIPL_ALLOW.scope] - Single link. If not present, the scope is the whole channel
    * @param {string} [data.DISCIPL_ALLOW.did] - Did that is allowed access. If not present, everyone is allowed.
    * @returns {Promise<string>} link - Link to the produced claim
    """
    async def claim (self, did, privkey, signingkey, data):
        self.logger.info('Making a claim')
        # Sort the keys to get the same message for the same data

        reference = BaseConnector.referenceFromDid(did)

        identity = await self.identityFactory.fromDid(did, privkey, signingkey)
        signature = identity.sign(data)

        claim = { 'message': data, 'signature': signature, 'publicKey': reference}
        claimLink = await self.ephemeralClient.claim(claim)

        return self.linkFromReference(claimLink)

    """
    * @typedef {Object} ClaimInfo
    * @property {object} data - Data saved in the claim, can be an arbitrary object
    * @property {string|null} previous - Link to the previous claim, null if the claim is the first
    """

    """
    * Retrieve a claim by its link
    *
    * @param {string} link - Link to the claim
    * @param {string} did - Did that wants access
    * @param {string} privkey - Key of the did requesting access
    * @returns {Promise<ClaimInfo>} Object containing the data of the claim and a link to the
    * claim before it.
    """
    async def get (self, link, did = None, privkey = None, signingkey = None):
        retrievedObj = None
        if self.caching:
            cacheKey = link
            if did != None:
                cacheKey += did
            if cacheKey in self.myCache:
                retrievedObj = deepcopy(self.myCache[cacheKey])
            else:
                retrievedObj = []

        if retrievedObj == None or retrievedObj == []:
            reference = BaseConnector.referenceFromLink(link)
            pubkey = BaseConnector.referenceFromDid(did)

            signature = None
            if pubkey != None and privkey != None and signingkey != None:
                signIdentity = await self.identityFactory.fromDid(did, privkey, signingkey)
                signature = signIdentity.sign(reference)

            result = await self.ephemeralClient.get(reference, pubkey, signature)
            if not result or not 'data' in result:
                self.logger.info('Could not find data for %s', link)
                return None

            publicKeyFingerprint = await self.ephemeralClient.getPublicKey(reference)

            self.logger.debug('Retrieved fingerprint %s', publicKeyFingerprint)
            identity = await self.identityFactory.fromReference(publicKeyFingerprint)
            identity.verify(result['data'], reference)

            retrievedObj = {'claim': {'data': result['data'], 'signature': result['signature'] ,'previous': self.linkFromReference(result['previous'])}, 'pubkey': publicKeyFingerprint }
            if self.caching:
                self.myCache[cacheKey] = deepcopy(retrievedObj)
        return retrievedObj

    """
    * Deletes all key-value pairs from the myCache variable in the ephemeral connector.
    """
    async def deleteAllFromCache (self):
        self.myCache = {}

    """
    * Imports a claim that was exported from another ephemeral connector.
    *
    * This needs the signature on it, in the form of the link. The signature is verfied when using this method.
    *
    * @param {string} did - Did that originally made this claim
    * @param {string} link - Link to the claim, which contains the signature over the data
    * @param {object} data - Data in the original claim
    * @param {string} importerDid - Did that will automatically get access to imported claim
    * @returns {Promise<string>} - Link to the claim if successfully imported, null otherwise.
    """
    async def importConnector (self, did, link, data, importerDid = None):
        claim = { 'message': data, 'signature': BaseConnector.referenceFromLink(link), 'publicKey': BaseConnector.referenceFromDid(did)}

        if importerDid != None:
            claim['access'] = { 'scope': link, 'did': importerDid }
        return self.linkFromReference(await self.ephemeralClient.claim(claim))

    """
    * @typedef {object} ExtendedClaimInfo
    * @property {ClaimInfo} claim - The actual claim
    * @property {string} link - Link to this claim
    * @property {string} did - Did that made the claim
    """

    """
    * Observe claims being made in the connector
    *
    * @param {string} did - Only observe claims from this did
    * @param {object} claimFilter - Only observe claims that contain this data. If a value is null, claims with the key will be observed.
    * @param {string} accessorDid - Did requesting access
    * @param {string} accessorPrivkey - Private key of did requesting access
    * @returns {Promise<{observable: Observable<ExtendedClaimInfo>, readyPromise: Promise<>}>} -
    * The observable can be subscribed to. The readyPromise signals that the observation has truly started.
    """
    async def observe (self, did, claimFilter = {}, accessorDid = None, accessorPrivkey = None, accessorSigningkey = None, historical = False):
        pubkey = BaseConnector.referenceFromDid(did)
        accessorPubkey = BaseConnector.referenceFromDid(accessorDid)

        signature = None
        if accessorPubkey != None and accessorPrivkey != None:
            if pubkey == None:
                message = 'null'
            else:
                message = pubkey
            identity = await self.identityFactory.fromDid(accessorDid, accessorPrivkey, accessorSigningkey)
            signature = identity.sign(message)

        return await self.ephemeralClient.observe(pubkey, accessorPubkey, signature, claimFilter, historical)

    async def getObservedClaims(self, amount, ssid, accessorSsid = None):
        claims = await self.ephemeralClient.getObservedClaims(amount, ssid, accessorSsid)
        for i in range(len(claims)):
            claims[i]['claim']['previous'] = self.linkFromReference(claims[i]['claim']['previous'])
        return claims

    async def subscribe(self, ssid, accessorSsid):
        await self.ephemeralClient.subscribe(ssid, accessorSsid)

    async def getLastNotification(self, ssid, accessorSsid):
        return await self.ephemeralClient.getLastNotification(ssid, accessorSsid)

    async def getFirstClaim(self, did, accessorDid = None):
        pubkey = BaseConnector.referenceFromDid(did)
        accessorPubkey = BaseConnector.referenceFromDid(accessorDid)

        claim = await self.ephemeralClient.getFirstClaim(pubkey, accessorPubkey)
        claim['did'] = self.didFromReference(claim['pubkey'])

        return claim

    async def getLastClaim(self, did, accessorDid = None):
        pubkey = BaseConnector.referenceFromDid(did)
        accessorPubkey = BaseConnector.referenceFromDid(accessorDid)

        claim = await self.ephemeralClient.getLastClaim(pubkey, accessorPubkey)
        if claim == None:
            i = 0
            claim = await self.ephemeralClient.getLastClaim(pubkey, accessorPubkey)
        claim['did'] = self.didFromReference(claim['pubkey'])

        return claim

    async def getClaimsHistorical(self, amount, ssid, accessorSsid):
        return await self.ephemeralClient.getClaimsHistorical(self.getName(), amount, ssid, accessorSsid)

    async def getGlobalObserverCount(self):
        return await self.ephemeralClient.getGlobalObserverCount()