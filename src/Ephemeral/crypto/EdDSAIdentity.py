#import forge from 'node-forge'
#import CryptoUtil from './CryptoUtil'
from src.FileWriter import writeToFile, appendToFile
import sys
import nacl.utils
import base64
from nacl.public import PrivateKey, PublicKey
from nacl.signing import SigningKey, VerifyKey
from nacl.encoding import Base64Encoder
from src.Ephemeral.crypto.CryptoUtil import CryptoUtil

EC_PREFIX = 'ec:'

class EdDSAIdentity:
    def __init__(self, reference = None, privkey = None, signingkey = None):
        if reference == None:
            secretKey = PrivateKey.generate()
            secretSigningKey = SigningKey.generate()

            self.skey = secretKey
            self.pkey = secretKey.public_key

            self.ssigningkey = secretSigningKey
            self.psigningkey = secretSigningKey.verify_key

            self.reference = EC_PREFIX + self.getBase64StringFromPublicKey(self.pkey) + \
                             EC_PREFIX + self.getBase64StringFromPublicKey(self.psigningkey)

            self.ssid = { \
                'privkey': secretKey, \
                'signingkey': secretSigningKey, \
                'metadata': {} \
                }
        else:
            references = reference.split(EC_PREFIX)

            self.skey = privkey
            self.pkey = references[1]
            self.pkey = references[1]

            self.ssigningkey = signingkey
            self.psigningkey = references[2]

            self.ssid = { \
                'privkey': self.skey, \
                'signingkey': self.ssigningkey, \
                'metadata': {} \
                }

    def sign (self, data):
        md = CryptoUtil.objectToDigest(data)

        # Sign a message with the signing key
        signature = self.ssigningkey.sign(md)

        base64_bytes = base64.b64encode(signature.signature)
        base64_message = base64_bytes.decode('ascii')

        return base64_message

    def verify (self, data, signature):
        md = CryptoUtil.objectToDigest(data)

        base64_message = signature
        base64_bytes = base64_message.encode('ascii')
        message_bytes = base64.b64decode(base64_bytes)
        verify_key = self.getVerifyKeyFromBase64String(self.psigningkey)

        try:
            verify = verify_key.verify(md, message_bytes)
        except Exception:
            print('Invalid signature')
            return False

        return True

    def getBase64StringFromPublicKey(self, publicKey) -> str:
        return publicKey.encode(Base64Encoder).decode('utf8')

    def getPublicKeyFromBase64String(self, publicKeyInBase64: str) -> bytes:
        return PublicKey(self.base64_to_bytes(publicKeyInBase64))

    def getVerifyKeyFromBase64String(self, VerifyKeyInBase64: str) -> bytes:
        return VerifyKey(self.base64_to_bytes(VerifyKeyInBase64))

    def base64_to_bytes(self, key: str) -> bytes:
        return base64.b64decode(key.encode('utf8'))
#export default EdDSAIdentity
#export {
#  EC_PREFIX
#}
