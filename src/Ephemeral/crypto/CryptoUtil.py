"""
import forge from 'node-forge'
import stringify from 'json-stable-stringify'

class CryptoUtil {
  static objectToDigest (o) {
    const md = forge.md.sha256.create()
    md.update(stringify(o), 'utf8')
    return md
  }
}

export default CryptoUtil
"""
import nacl.encoding
import nacl.hash
import pickle
from nacl.encoding import Base64Encoder
import base64

class CryptoUtil:
    @staticmethod
    def objectToDigest (o):
        #const md = forge.md.sha256.create()
        #md.update(stringify(o), 'utf8')
        #return md

        #ascii_message = json.dumps(o).encode('ascii')
        #output_byte = base64.b64encode(ascii_message)
        output_byte = base64.b64encode(pickle.dumps(o))
        HASHER = nacl.hash.sha256
        digest = HASHER(output_byte, encoder=nacl.encoding.HexEncoder)
        return digest
