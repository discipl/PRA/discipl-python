from src.Ephemeral.crypto.RSAIdentity import RSAIdentity
from src.Ephemeral.crypto.EdDSAIdentity import EdDSAIdentity, EC_PREFIX
from src.BaseConnector.Base_Connector import BaseConnector
#import EdDSAIdentity, { EC_PREFIX } from './EdDSAIdentity'
#import { BaseConnector } from '@discipl/core-baseconnector'
#import * as log from 'loglevel'

import warnings

class IdentityFactory:
    def __init__(self):
        self.x = 10

    def setConnector (self, ephemeralConnector):
        self.ephemeralConnector = ephemeralConnector
        self.setClient(ephemeralConnector.ephemeralClient)

    def setClient (self, ephemeralClient):
        self.ephemeralClient = ephemeralClient

    async def fromCert (self, cert, privkey):
        identity = RSAIdentity(cert, privkey)
        await self.ephemeralClient.storeCert(identity.reference, identity.cert)

        identity.ssid['did'] = self.ephemeralConnector.didFromReference(identity.reference)

        return identity

    async def fromDid (self, did, privkey = None, signingkey = None):
        reference = BaseConnector.referenceFromDid(did)
        return await self.fromReference(reference, privkey, signingkey)

    async def fromReference (self, reference, privkey = None, signingkey = None):
      if reference.startswith(EC_PREFIX):
          return EdDSAIdentity(reference, privkey, signingkey)
      else:
          # Reference starts with CRT_PREFIX
          cert = await self.ephemeralClient.getCertForFingerprint(reference)
          warnings.warn('Retrieved cert ' + cert)
          return RSAIdentity(cert, privkey)

    async def newIdentity (self):
        identity = EdDSAIdentity()
        identity.ssid['did'] = self.ephemeralConnector.didFromReference(identity.reference)
        return identity

#export default IdentityFactory
