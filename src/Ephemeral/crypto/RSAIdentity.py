#import forge from 'node-forge'
#import * as log from 'loglevel'
#import CryptoUtil from './CryptoUtil'
import rsa
import base64

CRT_PREFIX = 'crt:'

class RSAIdentity:
    def __init__(self, cert, privkey = None):
        if type(cert) == str:
            #with open('private.pem', mode='rb') as privatefile:
            #(pubkey, privkey) = rsa.newkeys(512)
            # b = pubkey.save_pkcs1()
            #with open('readme.txt', 'wb') as f:
            #   f.write(b)

            parsedCert = rsa.PublicKey.load_pkcs1(cert)
            #parsedCert = forge.pki.certificateFromPem(cert)
        else:
            parsedCert = cert

        fingerprint = self.getFingerPrint(parsedCert)

        self.reference = fingerprint
        self.cert = parsedCert
        self.ssid = {\
            'privkey': privkey,
            'metadata': {'cert': rsa.PublicKey.save_pkcs1(parsedCert).decode("utf-8")}
        }

    def sign (self, data):
        privateKey = forge.pki.privateKeyFromPem(self.ssid.privkey)

        md = CryptoUtil.objectToDigest(data)
        return forge.util.encode64(privateKey.sign(md))


    def verify (data, signature):
        digest = CryptoUtil.objectToDigest(data)
        verify = self.cert.publicKey.verify(digest.digest().bytes(), forge.util.decode64(signature, 'base64'))

        if not verify:
            raise Exception('Invalid signature')

        return verify

    def getFingerPrint(self, parsedCert):
        i = parsedCert.save_pkcs1()
        i2 = base64.b64encode(i)
        return CRT_PREFIX + i2.decode("utf-8")

    def getPublicKey(self, fingerPrint):
        fingerPrintNoPrefix = fingerPrint[4:]
        i3 = base64.b64decode(fingerPrintNoPrefix)
        i4 = i3.decode("utf-8")
        return rsa.PublicKey.load_pkcs1(i4)