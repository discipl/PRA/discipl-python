#import express from 'express'
#import ws from 'ws'
#import EphemeralStorage from './EphemeralStorage'
#import stringify from 'json-stable-stringify'
#import forge from 'node-forge'
#import * as log from 'loglevel'
#import fs from 'fs'
#import https from 'https'

"""
 * EphemeralServer provides a http/ws interface for the logic contained in the EphemeralStorage class
"""
class EphemeralServer:
    def __init__(self, port, certificatePath = None, privateKeyPath = None, retentionTime = 24 * 3600):
        self.port = port
        self.storage = EphemeralStorage()
        self.websockets = {}
        self.timestamps = {}
        self.retentionTime = retentionTime
        self.certificatePath = certificatePath
        self.privateKeyPath = privateKeyPath

        self.logger = log.getLogger('EphemeralConnector')
        self.logger.setLevel('debug')

        # Set the interval to check at 1/10th of the retentionTime, such that we exceed retentionTime by at most 10%
        #self.cleanInterval = setInterval(() -> self.clean(), retentionTime * 100)

    def start ():
        app = express()
        app.use(express.json())
        app.post('/claim', lambda req, res: self.claim(req, res))
        app.post('/get', lambda req, res: self.get(req, res))
        app.post('/getLatest', lambda req, res: self.getLatest(req, res))
        app.post('/getPublicKey', lambda req, res: self.getPublicKey(req, res))
        app.post('/storeCert', lambda req, res: self.storeCert(req, res))
        app.post('/getCert', lambda req, res: self.getCert(req, res))
        app.post('/observe', lambda req, res: self.observe(req, res))
        """
        self.server = https.createServer({
          key: fs.readFileSync(self.privateKeyPath, { encoding: 'utf-8' }),
          cert: fs.readFileSync(self.certificatePath, { encoding: 'utf-8' })
        }, app).listen(self.port, null, 511, () => self.logger.info(`Secure ephemeral server listening on ${self.port}!`))

        const wss = new ws.Server({ server: self.server })
        wss.on('connection', (wsCon) => {
          wsCon.on('message', (nonce) => {
            self.websockets[JSON.parse(nonce)] = wsCon
          })
        })

        self.wss = wss
      }

  clean () {
    const now = new Date().getTime()
    for (const entry of Object.entries(self.timestamps)) {
      if (now - entry[1].getTime() > self.retentionTime * 1000) {
        self.storage.deleteIdentity(entry[0])
      }
    }
  }

  ping (pubkey) {
    self.timestamps[pubkey] = new Date()
  }

  async claim (req, res) {
    // Protect against non-memory access injection
    if (req.body.access) {
      delete req.body.access
    }
    try {
      const result = await self.storage.claim(req.body)
      self.ping(req.body.publicKey)
      res.send(stringify(result))
    } catch (e) {
      self.logger.warn('Error while claiming', e)
      res.status(401).send(e)
    }
  }

  async get (req, res) {
    try {
      const result = await self.storage.get(req.body.claimId, req.body.accessorPubkey, req.body.accessorSignature)
      self.ping(req.body.accessorPubkey)
      self.ping(await self.storage.getPublicKey(req.body.claimId))
      res.send(stringify(result))
    } catch (e) {
      self.logger.warn('Error while getting', e)
      res.status(401).send(e)
    }
  }

  async getLatest (req, res) {
    res.send(stringify(await self.storage.getLatest(req.body.publicKey)))
    self.ping(req.body.accessorPubkey)
  }

  async getPublicKey (req, res) {
    const result = await self.storage.getPublicKey(req.body.claimId)
    self.ping(result)
    res.send(stringify(result))
  }

  async storeCert (req, res) {
    self.logger.debug('Received request for certificate with fingerpint', req.body.fingerprint, 'through server')
    await self.storage.storeCert(req.body.fingerprint, forge.pki.certificateFromPem(req.body.cert))
    self.ping(stringify(req.body.fingerprint))
    res.send({})
  }

  async getCert (req, res) {
    const result = await self.storage.getCertForFingerprint(req.body.fingerprint)
    self.ping(req.body.fingerprint)
    res.send(stringify(forge.pki.certificateToPem(result)))
  }

  async observe (req, res) {
    if (!req.body.nonce || !Object.keys(self.websockets).includes(req.body.nonce)) {
      res.sendStatus(404)
      return
    }

    const observeResult = await self.storage.observe(req.body.scope, req.body.accessorPubkey, req.body.accessorSignature)

    self.ping(req.body.accessorPubkey)
    self.ping(req.body.scope)

    const subject = observeResult[0]

    const errorCallback = (error) => {
      if (error != null && !error.message.includes('WebSocket is not open')) {
        self.logger.error('Error while sending ws message:', error)
      }
    }

    const wsCon = self.websockets[req.body.nonce]

    const observer = {
      next: (value) => wsCon.send(stringify(value), {}, errorCallback)
    }

    subject.subscribe(observer)

    res.send({})
  }

  close () {
    self.logger.info('Stopping Ephemeral Server')
    clearInterval(self.cleanInterval)
    self.wss.close()
    self.server.close()
  }
}

export default EphemeralServer
"""