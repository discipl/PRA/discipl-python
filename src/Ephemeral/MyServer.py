import asyncio
import websockets
import json
import pickle
import datetime
from src.Ephemeral.EphemeralStorage import EphemeralStorage

class MyServer:
    def __init__(self, port, certificatePath=None, privateKeyPath=None, retentionTime=24 * 3600):
        self.storage = EphemeralStorage()
        self.timestamps = {}
        print("starting")
        asyncio.run(self.main())

    async def main(self):
        async with websockets.serve(self.handler, "localhost", 8000):
            self.fut = asyncio.Future()
            #await asyncio.Future()  # run forever
            await self.fut

    async def handler(self, websocket):
        async for message in websocket:
            mesReceived = pickle.loads(message)
            typeMessage = mesReceived[0]
            actualMes = mesReceived[1]

            if typeMessage == 'close':
                result = "closing"
                s = pickle.dumps(result)
                await websocket.send(s)
                self.fut.set_result(True)
            elif typeMessage == 'echo':
                result = actualMes + ' this is an echo'
                s = pickle.dumps(result)
                await websocket.send(s)
            elif typeMessage == 'claim':
                result = await self.claim(actualMes)
                s = pickle.dumps(result)
                await websocket.send(s)
            elif typeMessage == 'get':
                result = await self.get(actualMes['reference'], actualMes['pubkey'], actualMes['signature'])
                s = pickle.dumps(result)
                await websocket.send(s)
            elif typeMessage == 'getPublicKey':
                result = await self.getPublicKey(actualMes)
                s = pickle.dumps(result)
                await websocket.send(s)
            elif typeMessage == 'getLatest':
                result = await self.getLatest(actualMes)
                s = pickle.dumps(result)
                await websocket.send(s)
            elif typeMessage == 'observe':
                result = await self.observe(actualMes['pubkey'], actualMes['accessorpubkey'], actualMes['accessorsignature'], actualMes['claimfilter'], actualMes['historical'])
                s = pickle.dumps(result)
                await websocket.send(s)
            elif typeMessage == 'getObservedClaims':
                result = await self.getObservedClaims(actualMes['amount'], actualMes['ssid'], actualMes['accessorSsid'])
                s = pickle.dumps(result)
                await websocket.send(s)
            elif typeMessage == 'subscribe':
                await self.subscribe(actualMes['ssid'], actualMes['accessorSsid'])
            elif typeMessage == 'globalObserverCount':
                result = await self.getGlobalObserverCount()
                s = pickle.dumps(result)
                await websocket.send(s)
            elif typeMessage == 'clean':
                result = "cleaning"
                self.clean()
                s = pickle.dumps(result)
                await websocket.send(s)
            elif typeMessage == 'reset':
                result = "resetting"
                self.reset()
                s = pickle.dumps(result)
                print(result)
                await websocket.send(s)


    async def claim(self, req):
        try:
            result = await self.storage.claim(req)
            self.ping(req['publicKey'])
            return result
        except Exception as e:
            exception = str(e)

    async def get(self, reference, accessorpubkey, accessorsignature):
        try:
            result = await self.storage.get(reference, accessorpubkey, accessorsignature)
            self.ping(accessorpubkey)
            self.ping(await self.storage.getPublicKey(reference))
            return result
        except Exception as e:
            exception = str(e)

    async def getPublicKey(self, reference):
        result = await self.storage.getPublicKey(reference)
        self.ping(result)
        return result

    async def getLatest(self, publicKey):
        try:
            result = await self.storage.getLatest(publicKey)
            self.ping(publicKey)
            return result
        except Exception as e:
            exception = str(e)

    async def observe (self, publicKey, accessorPubkey, accessorSignature, claimFilter, historical):
        result = await self.storage.observe(publicKey, accessorPubkey, accessorSignature, claimFilter, historical)
        self.ping(accessorPubkey)
        self.ping(publicKey)
        return result

    async def getObservedClaims(self, amount, ssid, accessorSsid):
        return await self.storage.getObservedClaims(amount, ssid, accessorSsid)

    async def subscribe (self, ssid, accessorSsid):
        await self.storage.subscribe(ssid, accessorSsid)

    async def getGlobalObserverCount (self):
        return self.storage.getGlobalObserverCount()

    def ping(self, pubkey):
        self.timestamps[pubkey] = datetime.datetime.now()

    def reset(self):
        self.storage = EphemeralStorage()
        self.timestamps = {}

    def clean (self):
        now = datetime.datetime.now()
        delta = datetime.timedelta(microseconds = 1)
        deletedKeys = []

        for key, value in self.timestamps.items():
            if (now - value > delta):
                deletedKeys.append(key)

        for i in range(len(deletedKeys)):
            del self.timestamps[deletedKeys[i]]

        for i in range(len(deletedKeys)):
            self.storage.deleteIdentity(deletedKeys[i])

server = MyServer(8000)