#import { Subject } from 'rxjs'
#import { BaseConnector } from '@discipl/core-baseconnector'
#import * as log from 'loglevel'
#import IdentityFactory from './crypto/IdentityFactory'
#import { deepCopy } from './util/deepCopy'

from reactivex import Subject
from src.SubjectFilter import *
from src.BaseConnector.Base_Connector import BaseConnector
from src.Ephemeral.util.DeepCopy import deepcopy
from src.Ephemeral.crypto.IdentityFactory import IdentityFactory
import logging
import datetime

"""
* EphemeralStorage is responsible for managing claims. It validates the signature when the claim comes in.
"""
class EphemeralStorage:
    def __init__(self):
        self.storage = {}
        self.claimOwners = {}
        self.fingerprints = {}
        self.globalObservers = []
        self.subscribers = []
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.DEBUG)
        self.identityFactory = IdentityFactory()
        self.identityFactory.setClient(self)

    async def claim (self, claim):
        claimCopy = deepcopy(claim)
        verification = await self._verifySignature(claimCopy)

        if verification != True:
            self.logger.warning('Invalid signature on claim by pubkey %s', claimCopy.publicKey)
            print('verification went wrong')
            return None

        signature = claimCopy['signature']
        message = claimCopy['message']

        publicKey = claimCopy['publicKey']
        self._lazyInitStorage(publicKey)

        claimId = signature

        if claimId in self.storage[publicKey]['claims']:
            self.logger.info('Claim with id %s already existed', claimId)
            return claimId

        self.claimOwners[claimId] = publicKey
        self.storage[publicKey]['claims'][claimId] = { 'data': message, 'signature': signature, 'previous': self.storage[publicKey]['last'], 'access': [], 'datetime': datetime.datetime.now() }
        self.storage[publicKey]['last'] = claimId

        # claimCopy['access'] returns true if it contains elements, false if empty
        if BaseConnector.ALLOW() in message or ('access' in claimCopy and claimCopy['access']):
            # very weird syntax, boolean like but returns a dictionary
            # access = message[BaseConnector.ALLOW()] or ('access' in claimCopy and claimCopy['access'])
            # rewrote it to the following 5 lines:
            access = {}
            if BaseConnector.ALLOW() in message:
                access = message[BaseConnector.ALLOW()]
            else:
                if 'access' in claimCopy:
                    access = claimCopy['access']

            object = self.storage[publicKey]

            if 'scope' in access and BaseConnector.isLink(access['scope']) and self.claimOwners[BaseConnector.referenceFromLink(access['scope'])] == publicKey:
                object = self.storage[publicKey]['claims'][BaseConnector.referenceFromLink(access['scope'])]
                dt = datetime.datetime(1, 1, 1, 1, 1, 1, 1)
            else:
                dt = datetime.datetime.now()

            if 'datetime' in access:
                dt = access['datetime']

            if 'did' not in access:
                object['access'] = True
            else:
                if object['access'] != True:
                    if BaseConnector.isDid(access['did']):
                        object['access'].append({BaseConnector.referenceFromDid(access['did']): dt})

        for listener in self.storage[publicKey]['observers']:
            sourceClaim = self.storage[publicKey]['claims'][claimId]


            claimForObserver = {\
                'data': sourceClaim['data'],\
                'signature': sourceClaim['signature'],\
                'previous': sourceClaim['previous']}

            if self._hasAccessTo(claimId, listener['owner']):
                listener['subject'].on_next({ 'claim': claimForObserver, 'pubkey': publicKey })

        for listener in self.globalObservers:
            sourceClaim = self.storage[publicKey]['claims'][claimId]


            claimForObserver = {\
                'data': sourceClaim['data'],\
                'signature': sourceClaim['signature'],\
                'previous': sourceClaim['previous']}

            if self._hasAccessTo(claimId, listener['owner']):
                listener['subject'].on_next({ 'claim': claimForObserver, 'pubkey': publicKey })

        return claimId

    def _hasAccessTo (self, claimId, pubkey):
        claimPublicKey = self.claimOwners[claimId]

        if claimPublicKey == pubkey:
            return True

        if claimPublicKey == None:
            return False

        for accessObject in [self.storage[claimPublicKey]['access'], self.storage[claimPublicKey]['claims'][claimId]['access']]:
            if accessObject == True:
                return True
            else:
                if pubkey == None:
                    continue
                for i in range(len(accessObject)):
                    for key, value in accessObject[i].items():
                        if key.count(pubkey) > 0 and self.storage[claimPublicKey]['claims'][claimId]['datetime'] >= value:
                            return True
        return False

    async def get (self, claimId, accessorPubkey, accessorSignature):
        if accessorPubkey != None and accessorSignature != None:
            identity = await self.identityFactory.fromReference(accessorPubkey)
            if not identity.verify(claimId, accessorSignature):
                return None

        if claimId in self.claimOwners:
            publicKey = self.claimOwners[claimId]
        else:
            return None

        if publicKey in self.storage and claimId in self.storage[publicKey]['claims']:
            sourceClaim = self.storage[publicKey]['claims'][claimId]
            claim = {\
                'data': deepcopy(sourceClaim['data']),\
                'signature': sourceClaim['signature'],\
                'previous': sourceClaim['previous']}

            if self._hasAccessTo(claimId, accessorPubkey):
                return claim
            else:
                self.logger.warning('Entity with fingerprint %s tried to access %s and failed', accessorPubkey, claimId)

    async def getLatest (self, publicKey):
        if publicKey in self.storage and self.storage[publicKey]['last'] != None:
            claimId = self.storage[publicKey]['last']
            sourceClaim = self.storage[publicKey]['claims'][claimId]

            claim = { \
                'data': deepcopy(sourceClaim['data']), \
                'signature': sourceClaim['signature'], \
                'previous': sourceClaim['previous']}
            return claim


    async def getPublicKey (self, claimId):
        if claimId in self.claimOwners:
            return self.claimOwners[claimId]
        return None

    async def storeCert (self, reference, cert):
        self.logger.debug('Storing %s with %s', reference, cert)
        self.fingerprints[reference] = cert

    async def getCertForFingerprint (self, fingerprint):
        self.logger.debug('Requesting cert for fingerprint %s', fingerprint)
        return self.fingerprints[fingerprint]

    async def observe (self, publicKey = None, accessorPubkey = None, accessorSignature = None, claimFilter = {}, historical = False):
        if accessorPubkey != None and accessorSignature != None:
            if publicKey == None:
                message = 'null'
            else:
                message = publicKey

            identity = await self.identityFactory.fromReference(accessorPubkey)
            if not identity.verify(message, accessorSignature):
                return False

        subject = SubjectFilter(claimFilter)
        subject.historical = historical

        listener = {\
            'subject': subject,\
            'owner': accessorPubkey}
        if publicKey != None:
            self._lazyInitStorage(publicKey)

            self.storage[publicKey]['observers'].append(listener)
        else:
            self.globalObservers.append(listener)

        return True

    async def subscribe(self, ssid, accessorSsid):
        pubkey = None
        if ssid != None:
            pubkey = BaseConnector.referenceFromDid(ssid['did'])
        accessorPubkey = BaseConnector.referenceFromDid(accessorSsid['did'])

        subject = self.getSubject(pubkey, accessorPubkey)

        subscriber = SsidObserver(ssid, accessorSsid)
        self.subscribers.append(subscriber)

        await subject.subscribe(subscriber)

    async def getLastNotification(self, ssid, accessorSsid):
        for i in range(len(self.subscribers)):
            if ssid == self.subscribers[i].ssid and accessorSsid == self.subscribers[i].accessorSsid:
                return self.subscribers[i].getLastNotification()

        return None

    async def getFirstClaim(self, publicKey = None, accessorPubkey = None):
        return self.getSubject(publicKey, accessorPubkey).getFirstClaim()

    async def getLastClaim(self, publicKey=None, accessorPubkey=None):
        return self.getSubject(publicKey, accessorPubkey).getLastClaim()

    async def getObservedClaims(self, amount, ssid, accessorSsid = None):
        if ssid != None:
            pubkey = BaseConnector.referenceFromDid(ssid['did'])
        else:
            pubkey = None

        if accessorSsid != None:
            accessorPubkey = BaseConnector.referenceFromDid(accessorSsid['did'])
        else:
            accessorPubkey = None

        subject = self.getSubject(pubkey, accessorPubkey)
        if subject == None:
            print("subject = None")
            return None

        result = subject.getClaims(amount)
        return result

    async def getClaimsHistorical(self, connName, amount, ssid, accessorSsid):
        pubkey = BaseConnector.referenceFromDid(ssid['did'])
        accessorPubkey = BaseConnector.referenceFromDid(accessorSsid['did'])

        subject = self.getSubject(pubkey, accessorPubkey)

        result = await subject.getClaimsHistorical(connName, amount, ssid, self)
        return result

    def getSubject(self, publicKey = None, accessorPubkey = None):
        if publicKey != None:
            for listener in self.storage[publicKey]['observers']:
                if listener['owner'] == accessorPubkey:
                    return listener['subject']

        for listener in self.globalObservers:
            if listener['owner'] == accessorPubkey:
                return listener['subject']

        for listener in self.globalObservers:
            if listener['owner'] == None:
                return listener['subject']

    def getGlobalObserverCount(self):
        return len(self.globalObservers)

    async def _verifySignature (self, claim):
        identity = await self.identityFactory.fromReference(claim['publicKey'])
        return identity.verify(claim['message'], claim['signature'])

    def deleteIdentity (self, fingerprint):
        self.logger.info('Deleting information related to fingerprint %s', fingerprint)
        if fingerprint in self.storage:
            del self.storage[fingerprint]

        deletedKeys = []
        for key, value in self.claimOwners.items():
            self.logger.debug('Checking if %s %s is owned by %s', key, value, fingerprint)
            if value == fingerprint:
                self.logger.debug('Deleting claimOwner entry for claimId %s', key)
                deletedKeys.append(key)

        for key in deletedKeys:
            del self.claimOwners[key]

        self.logger.info('Deleting fingerprint', fingerprint)
        if fingerprint in self.fingerprints:
            del self.fingerprints[fingerprint]

        # Iterate backwards to prevent issues with modifying while looping
        for i in range(len(self.globalObservers) - 1, -1, -1):
            if self.globalObservers[i]['owner'] == fingerprint:
                self.globalObservers.pop(i)
        # Purposefully skip deleting the specific listeners, because iterating to them would take quite a lot of
        # time and they will get deleted when the key being listened to is no longer used.

    def _lazyInitStorage (self, publicKey):
        if publicKey not in self.storage:
            self.storage[publicKey] = { 'claims': {}, 'last': None, 'observers': [], 'access': [] }