from jsonc_parser.parser import JsoncParser
from src.DisciplLawReg.validationError import ValidationError

class ModelValidator:
    '''
    * Construct a new model validator from raw string data
    *
    * @param {string} rawData
    '''
    def __init__(self, rawData):
        self.errors = []
        # look into parsing here, what is rawData? A string, a fileneme?
        # self.model = jsonc.parse(rawData, self.errors)
        self.model =  JsoncParser.parse_str(rawData)

        if self._isValidJson():
            self.tree = jsonc.parseTree(rawData)
            self.rawData = rawData
            self.identifierPaths = {}
            self.referencePaths = {}
            self.multiIdentifierPaths = []

            # look into exact workings of this code in js
            identifierFields = [['acts', 'act'], ['facts', 'fact'], ['duties', 'duty']]
            '''
            for identifierField in identifierFields:
                self.identifierPaths = self.model[identifierField[0]].reduce((acc, _item, index) => {
                const path = [identifierField[0], index, identifierField[1]]
                const node = jsonc.findNodeAtLocation(self.tree, path)
                acc[node.value] = path
                return acc
                }, self.identifierPaths)
            '''
            self._populateMultiIdentifierPaths(identifierFields)

            indexedFields = [['acts', 'act'], ['acts', 'actor'], ['acts', 'object'], ['acts', 'recipient'],
                            ['acts', 'preconditions'],
                            ['facts', 'fact'], ['facts', 'function'],
                            ['duties', 'duty'], ['duties', 'duty-components'], ['duties', 'duty-holder'], ['duties', 'duty-holder'], ['duties', 'claimant'], ['duties', 'create'], ['duties', 'terminate']]
            # look into this code
            '''
            for indexField in indexedFields:
                if self.model[indexField[0]]:
                    self.referencePaths = self.model[indexField[0]].reduce((acc, item, index) => {
                        const path = [indexField[0], index, indexField[1]]
                        self._accumulateIdentifiers(path, acc)

                        return acc
                        }, self.referencePaths)

                indexedSubFields = [['acts', 'create'], ['acts', 'terminate']]
                for indexField in indexedSubFields:
                    self.referencePaths = self.model[indexField[0]].reduce((acc, item, index) => {
                    if item[indexField[1]]:
                        for subIndex in range(len(item[indexField[1]])):
                            path = [indexField[0], index, indexField[1], subIndex]
                            self._accumulateIdentifiers(path, acc)

                    return acc
                    }, self.referencePaths)
            '''
    def _isValidJson (self):
        return self.errors.length == 0

    '''
    * This method populates 'multiIdentifierPaths' with multiple positions of a identifier
    * as Key (String): Value (Collection).
    * @param {Array<Array<string>>} identifierFields
    * @private
    '''
    def _populateMultiIdentifierPaths (self, identifierFields):
        # look into this code
        '''
        for identifierField in identifierFields:
            self.multiIdentifierPaths = self.model[identifierField[0]].reduce((acc, _item, index) => {
                path = [identifierField[0], index, identifierField[1]]
            node = jsonc.findNodeAtLocation(self.tree, path)
            acc[node.value] = [*(acc[node.value] or []), path]
            return acc
            }, self.multiIdentifierPaths)
        '''
    def _accumulateIdentifiers (self, path, acc):
        node = jsonc.findNodeAtLocation(self.tree, path)
        if node:
            identifiers = self._extractIdentifiersFromString(node.value)
            if identifiers:
                for identifier in identifiers:
                    if acc[identifier]:
                        acc[identifier].append(path)
                    else:
                        acc[identifier] = [path]

    '''
    * Identifier information
    * @typedef  {Object} IdentifierInfo
    *
    * @property {string} identifier - The identifier in question
    * @property {number} offset - Start location in raw string
    '''

    '''
    * Finds definition for identifier located at a particular offset
    *
    * @param {number} offset - Offset that is located in the identifier
    * @return {IdentifierInfo|undefined} The identifier and offset of the definition if it exists,
    '''
    def getDefinitionForOffset (self, offset):
        if not self._isValidJson():
          return

        identifier = self._extractIdentifier(offset)
        if self.identifierPaths[identifier]:
            node = jsonc.findNodeAtLocation(self.tree, self.identifierPaths[identifier])
            dict_offset = {identifier}
            dict_offset['offset'] = node.offset
            return dict_offset

    def _extractIdentifiersFromString (self, str):
        regex = "/(\[.*\])|(<<.*>>)|(<.*>)/g"

        result = []
        match = regex.exec(str)
        while match:
            if not result.includes(match[0]):
                result.append(match[0])

            match = regex.exec(str)

        return result

    '''
    * Returns all definitions for a given type
    *
    * @param {('acts'|'facts'|'duties')} type
    * @return {IdentifierInfo[]} The identifier information for all defitions of the chosen type
    '''
    def getDefinitionsForType (self, type):
        if not self._isValidJson():
            return []
        # look at what code does here in js
        '''
        return Object.entries(self.identifierPaths).filter((identifierPath) => {
            return identifierPath[1][0] === type
              }).map((identifierPath) => {
                  const node = jsonc.findNodeAtLocation(self.tree, identifierPath[1])
                  return {
                      identifier: identifierPath[0],
                      offset: node.offset
            }
        })
        '''

    '''
    * Get all the references to an identifier located at the given offset
    *
    * @param {number} offset - offset located inside the identifier
    * @return {IdentifierInfo[]} The identifier information for all references to the identifier
    '''
    def getReferencesForOffset (self, offset):
        if not self._isValidJson():
            return []

        identifier = self._extractIdentifier(offset)
        if self.referencePaths[identifier]:
            i = 0
            # see what this does
            '''
            return self.referencePaths[identifier].map((referencePath) => {
                node = jsonc.findNodeAtLocation(self.tree, referencePath)

                return {identifier, offset: node.offset}
                })
            }
            '''

        return []

    '''
    * Get the identifier at a given offset
    * @param {number} offset
    * @returns {string | undefined}
    * @private
    '''
    def _extractIdentifier (self, offset):
        location = jsonc.getLocation(self.rawData, offset)
        if not location.previousNode:
            return

        value = location.previousNode.value
        offsetInValue = offset - location.previousNode.offset

        # Matches [facts], <<acts>> and <duties>
        regex = "/(\[.*\])|(<<.*>>)|(<.*>)/g"

        identifier
        while True:
            m = regex.exec(value)
            if not m:
                break
            if m.index <= offsetInValue and m.index + m[0].length >= offsetInValue:
                identifier = m[0]
                break

        return identifier

    '''
    * Get the validation errors for the model
    * @returns {ValidationError[]} Validation errors
    '''
    def getDiagnostics (self):
        if not self._isValidJson():
            return map(self._validationErrorFromParseError(error), self.errors)
            #self.errors.map(error => self._validationErrorFromParseError(error))

        actNameValidationErrors = self._checkIdentifiers('acts', 'act', "/^<<.+>>$/")
        factNameValidationErrors = self._checkIdentifiers('facts', 'fact', "/^\[.+\]$/")
        dutyNameValidationErrors = self._checkIdentifiers('duties', 'duty', "/^<.+>$/")
        duplicateIdentifiersValidationErrors = self._findOverallDuplicateIdentifiers()

        referenceErrors = self._checkReferences()

        return actNameValidationErrors.concat(factNameValidationErrors, dutyNameValidationErrors, referenceErrors, duplicateIdentifiersValidationErrors)

    '''
    * Does a overall look at duplicate identifiers,
    * e.g. <<act>> is not only findable on 'ACTS' field but as well
    * on 'FACTS' field and in between.
    *
    * @returns {ValidationError[]} Validation errors
    * @private
    '''
    def _findOverallDuplicateIdentifiers (self):
        validationError = []
        '''
        Object.keys(self.multiIdentifierPaths).filter(value =>
            self.multiIdentifierPaths[value].length > 1).forEach(key => {
                errors = self.multiIdentifierPaths[key].map(path => {
                    node = jsonc.findNodeAtLocation(self.tree, path)
                    beginPosition = node.offset
                    endPosition = node.offset + node.length
                    return new ValidationError(
                        'LR0003',
                        'Duplicate identifier',
                        [beginPosition, endPosition],
                        'ERROR',
                        key,
                        path
                        )
                    })
              validationError.push(errors)
          })
        '''
        #return validationError.flatMap(value => value)

    '''
    *
    * @param flintItems - Plural form of flint items to be checked
    * @param flintItem - Signular form of flint items to be checked
    * @param pattern - Regex that should match the identifier
    * @return {ValidationError[]} Validation errors
    * @private
    '''
    def _checkIdentifiers (self, flintItems, flintItem, pattern):
        '''
        return self.model[flintItems].filter((item) =>
            type(item[flintItem]) != str or not item[flintItem].match(pattern)).map((item) => {
            # console.log(item[flintItem])
                node = jsonc.findNodeAtLocation(self.tree, self.identifierPaths[item[flintItem]])
                beginPosition = node.offset
                endPosition = node.offset + node.length

                return new ValidationError(
                    'LR0001',
                    'Invalid name for identifier',
                    [beginPosition, endPosition],
                    'ERROR',
                    item[flintItem].toString(),
                    self.identifierPaths[item[flintItem]]
                    )
                })
        '''
    def _checkReferences (self):
        '''
        concat = (x, y) => x.concat(y)
        createTerminateErrors = self.model.acts.map((act) => {
            basePath = self.identifierPaths[act.act]
            createNode = jsonc.findNodeAtLocation(self.tree, [basePath[0], basePath[1], 'create'])
            terminateNode = jsonc.findNodeAtLocation(self.tree, [basePath[0], basePath[1], 'terminate'])

            createErrors = createNode ? self._checkCreateTerminate(act.create, createNode) : []
            terminateErrors = terminateNode ? self._checkCreateTerminate(act.terminate, terminateNode) : []
            return createErrors.concat(terminateErrors)
            }).reduce(concat, [])

        veryStrict = []
        lessStrict = ['']
        factStrict = ['[]']
        expressionCheckInfo = [['acts', 'actor', veryStrict], ['acts', 'object', veryStrict], ['acts', 'recipient', veryStrict],
            ['acts', 'preconditions', lessStrict], ['facts', 'function', factStrict]]

        expressionErrors = expressionCheckInfo.map((expressionCheckPath) => {
            return self.model[expressionCheckPath[0]].map((item, index) => {
                node = jsonc.findNodeAtLocation(self.tree, [expressionCheckPath[0], index, expressionCheckPath[1]])
                # console.log("ExpCheck en index", expressionCheckPath, index);
                if node and type(node.value) === str):
                    # console.log("Node", node);
                    return self._validateParsedExpression(node.value, node.offset, expressionCheckPath[2])
                else:
                    return self._validateParsedExpressionNode(node)
                }).reduce(concat, [])
            }).reduce(concat, [])

        return createTerminateErrors.concat(expressionErrors)
        '''
    '''
    * @typedef {Object} ParseError
    * @property {number} error
    * @property {number} offset
    * @property {number} length
    '''

    '''
    * Convert a parse error to a validation error.
    * @param parseError
    * @returns {ValidationError}
    * @private
    '''
    def validationErrorFromParseError (self, parseError):
        parseErrorMessages = [
            'Invalid symbol',
            'Invalid number format',
            'Property name expected',
            'Value expected',
            'Colon expected',
            'Comma expected',
            'Close brace expected',
            'Close bracket expected',
            'End of file expected',
            'Invalid comment token',
            'Unexpected end of comment',
            'Unexpected end of string',
            'Unexpected end of number',
            'Invalid unicode',
            'Invalid escape character',
            'Invalid character'
            ]
        return ValidationError(
            'LR0004',
            parseErrorMessages[parseError.error - 1],
            [parseError.offset, parseError.offset + parseError.length],
            'ERROR')

    def _checkCreateTerminate (self, referenceString, node):
        createTerminateErrors = []
        parsedReferences
        if type(referenceString) == str:
            i = 0
            #parsedReferences = referenceString.split(';').map(item => item.trim())
        else:
            parsedReferences = referenceString
        #parsedReferences = type(referenceString) == str ? referenceString.split(';').map(item => item.trim()) : referenceString

        for i in range(len(parsedReferences)):
            reference = parsedReferences[i]
            if reference.trim() == '':
                continue

            offset = jsonc.findNodeAtLocation(node, [i]).offset + 1
            error = self._validateReference(reference, offset)

            if error:
              createTerminateErrors.append(error)

        return createTerminateErrors

    def _validateReference (self, reference, beginOffset):
        if not self.identifierPaths[reference]:
            path = jsonc.getNodePath(jsonc.findNodeAtOffset(self.tree, beginOffset))
            return ValidationError(
                'LR0002',
                'Undefined item',
                [beginOffset, beginOffset + reference.length],
                'WARNING',
                reference,
                path)

    def _validateParsedExpressionNode (self, expression):
        errors = []
        operandsNode = jsonc.findNodeAtLocation(expression, ['operands'])

        if operandsNode:
            for subNode in operandsNode.children:
                errors = errors.concat(self._validateParsedExpressionNode(subNode))

        operandNode = jsonc.findNodeAtLocation(expression, ['operand'])
        expressionTypeNode = jsonc.findNodeAtLocation(expression, ['expression'])
        if operandNode and expressionTypeNode.value != 'LITERAL':
            errors = errors.concat(self._validateParsedExpressionNode(operandNode))

        itemsNode = jsonc.findNodeAtLocation(expression, ['items'])
        if itemsNode:
            errors = errors.concat(self._validateParsedExpressionNode(itemsNode))

        # expression.type == 'string' or expression.type == str ?????
        if expression and expression.type == str:
            error = self._validateReference(expression.value, expression.offset + 1)
            if error:
                errors.append(error)

        return errors

    def _validateParsedExpression (expression, beginOffset, exceptions = []):
        errors = []
        if type(expression) == str:
            if exceptions.includes(expression.trim()):
                return []

            error = self._validateReference(expression, beginOffset + 1)
            if error:
                errors.append(error)

        if expression.operands:
            for operand in expression.operands:
                errors = errors.concat(self._validateParsedExpression(operand, beginOffset, exceptions))

        if expression.operand:
            errors = errors.concat(self._validateParsedExpression(expression.operand, beginOffset, exceptions))

        return errors