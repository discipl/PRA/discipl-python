class ValidationError:
    '''
    * Construct a new validation error.
    * @param {string} code - Unique code to relate back to the error
    * @param {string} message - Human readable message describing the problem
    * @param {Array<number>} offset - Begin and end offset of the error
    * @param {('ERROR'|'WARNING')} severity - Severity of the error
    * @param {string|undefined} source - Text that relates to the cause of the error
    * @param {Array<string|number>|undefined} path - Path to the error
    '''
    def __init__(self, code, message, offset, severity, source, path):
        self.code = code
        self.message = message
        self.offset = offset
        self.severity = severity
        self.source = source
        self.path = path
