from src.DisciplLawReg.Expressions.baseSubExpressionChecker import BaseSubExpressionChecker

class AndExpressionChecker(BaseSubExpressionChecker):
    async def checkSubExpression (self, fact, ssid, context):
        hasUndefined = False
        for op in fact['operands']:
            newContext = self._getContextExplainer().extendContextWithExplanation(context)
            operandResult = await self._getExpressionChecker().checkExpression(op, ssid, newContext)
            self.logger.debug('OperandResult in AND', operandResult, 'for operand', op)
            if operandResult == False:
                self.logger.debug('Resolved AND as false, because', op, 'is false')
                self._getContextExplainer().extendContextExplanationWithResult(context, False)
                return False

            if operandResult == None:
                hasUndefined = True

        if hasUndefined:
            andResult = None
        else:
            andResult = True

        self.logger.debug('Resolved AND as', andResult)
        self._getContextExplainer().extendContextExplanationWithResult(context, andResult)
        return andResult