from src.DisciplLawReg.Expressions.baseSubExpressionChecker import BaseSubExpressionChecker
from src.DisciplLawReg.defaultFactResolver import DefaultFactResolver #wrapWithDefault
import src.DisciplLawReg.LawReg as lawReg# DISCIPL_FLINT_FACTS_SUPPLIED from '../index'
import asyncio
import functools

class ProjectionExpressionChecker(BaseSubExpressionChecker):
    '''
    * Create a SubExpressionChecker
    * @param {ServiceProvider} serviceProvider
    '''
    def __init__(self, serviceProvider):
        super().__init__(serviceProvider)
        self.scopeCheckers = {
            'single': SingleScopeProjectionExpressionChecker(serviceProvider),
            'all': AllScopeProjectionExpressionChecker(serviceProvider),
            'some': SomeScopeProjectionExpressionChecker(serviceProvider)
            }

    async def checkSubExpression (self, fact, ssid, context):
        if 'scope' in fact:
            scope = fact['scope']
        else:
            scope = 'single'
        self.logger.debug('Handling PROJECTION Expression with', scope, 'scope')
        return await self.scopeCheckers[scope].checkSubExpression(fact, ssid, context)

class BaseScopeProjectionExpressionChecker(BaseSubExpressionChecker):
    def _checkProjectionIsValid (self, fact):
        if not 'context' in fact or len(fact['context']) == 0:
            raise Exception('A \'context\' array must be given for the PROJECTION expression')

        if not 'operand' in fact:
            # TODO deprecate projection expression fact property
            if fact['fact'] != None:
                fact['operand'] = fact['fact']
            else:
                raise Exception('A \'operand\' must be given for the PROJECTION expression')

    '''
    * Get filtered creating acts
    * @param {object} fact - the create expression
    * @param {ssid} ssid - Identifies the actor
    * @param {Context} context - Context of the action
    * @return {Promise<CreatingAct[]>} - Object where key is the act link and value is the provided facts
    * @protected
    '''
    async def _getCreatingActs (self, fact, ssid, context):
        contextFact = fact['context'][0]
        creatingActs = await self._getFactChecker().getCreatingActs(contextFact, ssid, context)
        filteredActs = await self._filter(creatingActs, contextFact, ssid, context)
        result = []
        for act in filteredActs:
            result.append(await self._reduce(act, fact['context'][1:], ssid))
        return result

    '''
    * Reduce the create act using the context array to get the targets creating act
    * @param {CreatingAct}  creatingAct - The current creating act
    * @param {string[]} contextArray - The remaining context facts to reduce
    * @param {ssid} ssid - Identifies the actor
    * @return {Promise<CreatingAct>}
    * @protected
    '''
    async def _reduce (self, creatingAct, contextArray, ssid):
        self.logger.debug('Case for', creatingAct['contextFact'], 'is', creatingAct['link'], 'with facts', creatingAct['facts'])
        if len(contextArray) == 0:
            return creatingAct
        contextFact = contextArray[0]
        nextLink = creatingAct['facts'][contextFact]
        if nextLink:
            newCreatingAct = await self._getCreatingAct(nextLink, contextFact, ssid)
            return await self._reduce(newCreatingAct, contextArray[1:], ssid)
        return creatingAct

    '''
    * Get creating act for an act link
    * @param {string} actLink - Act link
    * @param {string} contextFact - The fact that was created by this act
    * @param {ssid} ssid - Identifies the actor
    * @return {Promise<CreatingAct>}
    * @private
    '''
    async def _getCreatingAct (self, actLink, contextFact, ssid):
        actData = await self._getAbundanceService().getCoreAPI().get(actLink, ssid)
        result = {}
        result['link'] = actLink
        result['contextFact'] = contextFact
        result['facts'] = actData['claim']['data'][lawReg.DISCIPL_FLINT_FACTS_SUPPLIED]
        return result

    '''
    * Resolve the value of a fact
    * @param {object} fact - the create expression
    * @param {ssid} ssid - Identifies the actor
    * @param {Context} context - Context of the action
    * @param {Object<string, *>} providedFacts - the facts provided by the create expression
    * @return {Promise<*>}
    * @protected
    '''
    async def _resolve (self, fact, ssid, context, providedFacts):
        newContext = dict(context)
        newContext['factResolver'] = DefaultFactResolver(context['factResolver'], providedFacts)
        result = await self._getFactChecker().checkFactWithResolver(fact['operand'], ssid, newContext)
        # noinspection JSUnresolvedVariable
        if type(result) is dict and result['expression']:
            result = await self._getFactChecker().checkFact(result, ssid, newContext)
        elif result == None:
            result = await self._getFactChecker().checkFact(fact['operand'], ssid, newContext)
        return result

    '''
    * Filter the creating acts (Should be implemented in sub expressions)
    * @param {CreatingAct[]} creatingActs - the creating acts
    * @param {string} contextFact - the context fact
    * @param {ssid} ssid - Identifies the actor
    * @param {Context} context - Context of the action
    * @return {Promise<CreatingAct[]>}
    * @protected
    '''
    async def _filter (self, creatingActs, contextFact, ssid, context):
        raise Exception('Not implemented')

    '''
    * Combine the values of creating acts into one provided facts object
    * @param {CreatingAct[]} creatingActs - the creating acts
    * @return {Object<string, *[]>} - object where key is the fact and value is an array of the facts values
    * @protected
    '''
    def _toProvidedFacts (self, creatingActs):
        return functools.reduce(lambda previousValue, currentValue: self.addCreatingAct(previousValue, currentValue), creatingActs, {})

    def addCreatingAct(self, providedFacts, creatingAct):
        facts = creatingAct['facts']
        for fact in facts:
            if fact in providedFacts:
                array = providedFacts[fact]
            else:
                array = []
            array.append(facts[fact])
            providedFacts[fact] = array
        return providedFacts

class SingleScopeProjectionExpressionChecker(BaseScopeProjectionExpressionChecker):
    async def checkSubExpression (self, fact, ssid, context):
        self._checkProjectionIsValid(fact)
        creatingActs = await self._getCreatingActs(fact, ssid, context)
        if len(creatingActs) != 1:
            context['searchingFor'] = fact['operand']
            return await self._checkAtLeastTypeOf(fact['operand'], ssid, context)

        return await self._resolve(fact, ssid, context, creatingActs[0]['facts'])

    '''
    * Check if current actor is an actor of type searchingFor
    * @param {string} searchingFor - The fact we are searching for
    * @param {ssid} ssid - Identifies the actor
    * @param {Context} context - Context of the action
    * @return {Promise<undefined|boolean>}
    * @private
    '''
    async def _checkAtLeastTypeOf (self, searchingFor, ssid, context):
        if 'myself' in context:
            self.logger.debug('Multiple creating acts found. Checking if you are at least a', searchingFor)
            isActorType = await self._getFactChecker().checkFact(searchingFor, ssid, context)
            self.logger.debug('Resolved you are a', searchingFor, 'as', isActorType)
            if isActorType:
                return None
            return False
        return None

    '''
    * Filter the creating acts
    * @param {CreatingAct[]} creatingActs - the creating acts
    * @param {string} contextFact - the context fact
    * @param {ssid} ssid - Identifies the actor
    * @param {Context} context - Context of the action
    * @return {Promise<CreatingAct[]>}
    * @protected
    '''
    async def _filter (self, creatingActs, contextFact, ssid, context):
        actLinks = list(map(lambda act: act['link'], creatingActs))
        resolverLink = await self._getFactChecker().checkFactWithResolver(contextFact, ssid, context, actLinks)
        return list(filter(lambda act: resolverLink == act['link'], creatingActs))

class AllScopeProjectionExpressionChecker(BaseScopeProjectionExpressionChecker):
    async def checkSubExpression (self, fact, ssid, context):
        self._checkProjectionIsValid(fact)
        creatingActs = await self._getCreatingActs(fact, ssid, context)
        if len(creatingActs) <= 0:
            return False
        providedFacts = self._toProvidedFacts(creatingActs)
        return await self._resolve(fact, ssid, context, providedFacts)

    '''
    * Filter the creating acts
    * @param {CreatingAct[]} creatingActs - the creating acts
    * @param {string} contextFact - the context fact
    * @param {ssid} ssid - Identifies the actor
    * @param {Context} context - Context of the action
    * @return {Promise<CreatingAct[]>}
    * @protected
    '''
    async def _filter (self, creatingActs, contextFact, ssid, context):
        return creatingActs

class SomeScopeProjectionExpressionChecker(BaseScopeProjectionExpressionChecker):
    async def checkSubExpression (self, fact, ssid, context):
        self._checkProjectionIsValid(fact)
        creatingActs = await self._getCreatingActs(fact, ssid, context)
        if len(creatingActs) <= 0:
            return False
        providedFacts = self._toProvidedFacts(creatingActs)
        return await self._resolve(fact, ssid, context, providedFacts)

    '''
    * Filter the creating acts
    * @param {CreatingAct[]} creatingActs - the creating acts
    * @param {string} contextFact - the context fact
    * @param {ssid} ssid - Identifies the actor
    * @param {Context} context - Context of the action
    * @return {Promise<CreatingAct[]>}
    * @protected
    '''
    async def _filter (self, creatingActs, contextFact, ssid, context):
        actLinks = list(map(lambda act: act['link'], creatingActs))
        resolverLinks = await self._getFactChecker().checkFactWithResolver(contextFact, ssid, context, actLinks)
        if not isinstance(resolverLinks, list):
            resolverLinks = []
        return list(filter(lambda act: act['link'] in resolverLinks, creatingActs))
