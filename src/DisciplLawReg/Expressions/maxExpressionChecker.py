from src.DisciplLawReg.Expressions.baseSubExpressionChecker import BaseSubExpressionChecker
from src.DisciplLawReg.Utils.Big_Util import BigUtil

class MaxExpressionChecker(BaseSubExpressionChecker):
    async def checkSubExpression (self, fact, ssid, context):
        maxResult = None
        for op in fact['operands']:
            newContext = self._getContextExplainer().extendContextWithExplanation(context)
            operandResult = await self._getExpressionChecker().checkExpression(op, ssid, newContext)
            self.logger.debug('OperandResult in MAX', operandResult, 'for operand', op)
            if operandResult == None:
                maxResult = None
                break
            elif not BigUtil.isNumeric(operandResult):
                maxResult = False
                break
            elif not BigUtil.isNumeric(maxResult) or BigUtil.greaterThan(operandResult, maxResult):
                maxResult = operandResult
        self.logger.debug('Resolved MAX as', maxResult)
        self._getContextExplainer().extendContextExplanationWithResult(context, maxResult)
        return maxResult

