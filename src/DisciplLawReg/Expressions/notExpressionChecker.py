from src.DisciplLawReg.Expressions.baseSubExpressionChecker import BaseSubExpressionChecker

class NotExpressionChecker(BaseSubExpressionChecker):
    async def checkSubExpression (self, fact, ssid, context):
        newContext = self._getContextExplainer().extendContextWithExplanation(context)
        value = await self._getExpressionChecker().checkExpression(fact['operand'], ssid, newContext)
        #notResult = typeof value === 'boolean' ? !value : undefined
        if type(value) == bool:
            notResult = not value
        else:
            notResult = None
        self._getContextExplainer().extendContextExplanationWithResult(context, notResult)
        return notResult