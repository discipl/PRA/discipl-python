from src.DisciplLawReg.Expressions.baseSubExpressionChecker import BaseSubExpressionChecker
from src.DisciplLawReg.Utils.Big_Util import BigUtil

class MinExpressionChecker(BaseSubExpressionChecker):
    async def checkSubExpression (self, fact, ssid, context):
        minResult = None
        for op in fact['operands']:
            newContext = self._getContextExplainer().extendContextWithExplanation(context)
            operandResult = await self._getExpressionChecker().checkExpression(op, ssid, newContext)
            self.logger.debug('OperandResult in MIN', operandResult, 'for operand', op)
            if operandResult == None:
                minResult = None
                break
            elif not BigUtil.isNumeric(operandResult):
                minResult = False
                break
            elif not BigUtil.isNumeric(minResult) or BigUtil.lessThan(operandResult, minResult):
                minResult = operandResult
        self.logger.debug('Resolved MIN as', str(minResult))
        self._getContextExplainer().extendContextExplanationWithResult(context, minResult)
        return minResult
