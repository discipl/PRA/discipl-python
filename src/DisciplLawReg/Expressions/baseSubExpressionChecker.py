from src.DisciplLawReg.Utils.Logging_Util import getDiscplLogger
# Improve intelisense
# eslint-disable-next-line no-unused-vars
from src.AbundanceService.AbundanceService import AbundanceService

class BaseSubExpressionChecker:
    '''
    * Create a SubExpressionChecker
    * @param {ServiceProvider} serviceProvider
    '''
    def __init__(self, serviceProvider):
        self.serviceProvider = serviceProvider
        self.logger = getDiscplLogger()

    '''
    * Get expression checker
    * @return {ExpressionChecker}
    * @protected
    '''
    def _getExpressionChecker (self):
        return self.serviceProvider.expressionChecker

    '''
    * Get context explainer
    * @return {ContextExplainer}
    * @protected
    '''
    def _getContextExplainer (self):
        return self.serviceProvider.contextExplainer

    '''
    * Get fact checker
    * @return {FactChecker}
    * @protected
    '''
    def _getFactChecker (self):
        return self.serviceProvider.factChecker

    '''
    * Get abundance service
    * @return {AbundanceService}
    * @protected
    '''
    def _getAbundanceService (self):
        return self.serviceProvider.abundanceService
