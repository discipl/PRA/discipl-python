from mpmath import *
from src.DisciplLawReg.Expressions.baseSubExpressionChecker import BaseSubExpressionChecker

class LiteralExpressionChecker(BaseSubExpressionChecker):
    async def checkSubExpression (self, fact, ssid, context):
        literalValue = fact['operand']
        if type(literalValue) == int or type(literalValue) == float or type(literalValue) == mpf:
            literalValue = mpf(literalValue)

        self._getContextExplainer().extendContextExplanationWithResult(context, literalValue)
        return literalValue
