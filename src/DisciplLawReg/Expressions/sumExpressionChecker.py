from src.DisciplLawReg.Expressions.baseSubExpressionChecker import BaseSubExpressionChecker
from src.DisciplLawReg.Utils.Big_Util import BigUtil

class SumExpressionChecker(BaseSubExpressionChecker):
    async def checkSubExpression (self, fact, ssid, context):
        sumResult = 0
        for op in fact['operands']:
            if not BigUtil.isNumeric(sumResult):
                break
            newContext = self._getContextExplainer().extendContextWithExplanation(context)
            operandResult = await self._getExpressionChecker().checkExpression(op, ssid, newContext)
            self.logger.debug('OperandResult in SUM', str(operandResult), 'for operand', op)
            if isinstance(operandResult, list):
                opArray = operandResult
            else:
                opArray = [operandResult]
            for arrayOp in opArray:
                self.logger.debug('ArrayOperandResult in SUM', str(arrayOp), 'for operand', op)
                if arrayOp == None:
                    sumResult = None
                    break
                elif not BigUtil.isNumeric(arrayOp):
                    sumResult = False
                    break
                else:
                    sumResult = BigUtil.add(sumResult, arrayOp)

        self.logger.debug('Resolved SUM as', str(sumResult))
        self._getContextExplainer().extendContextExplanationWithResult(context, sumResult)
        return sumResult
