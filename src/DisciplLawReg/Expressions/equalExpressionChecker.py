from src.DisciplLawReg.Utils.Big_Util import BigUtil
from src.DisciplLawReg.Expressions.baseSubExpressionChecker import BaseSubExpressionChecker
from src.DisciplLawReg.Utils.Logging_Util import logger

class EqualExpressionChecker(BaseSubExpressionChecker):
    async def checkSubExpression (self, fact, ssid, context):
        lastOperandResult = None
        for op in fact['operands']:
            newContext = self._getContextExplainer().extendContextWithExplanation(context)
            operandResult = await self._getExpressionChecker().checkExpression(op, ssid, newContext)
            logger.debug('OperandResult in EQUAL', str(operandResult), 'for operand', op)
            if operandResult == None:
                self.logger.debug('Resolved EQUAL as undefined because one of the operands was undefined')
                lastOperandResult = None
                break
            elif lastOperandResult != None and not BigUtil.equal(operandResult, lastOperandResult):
                self.logger.debug('Resolved EQUAL as false, because', str(lastOperandResult), 'does not equal', str(operandResult))
                lastOperandResult = False
                break
            else:
                lastOperandResult = operandResult

        #equalResult = lastOperandResult === undefined ? undefined : lastOperandResult !== false
        if lastOperandResult == None:
            equalResult = None
        else:
            equalResult = lastOperandResult != False

        if equalResult:
            self.logger.debug('Resolved EQUAL as', str(equalResult))

        self._getContextExplainer().extendContextExplanationWithResult(context, equalResult)
        return equalResult
