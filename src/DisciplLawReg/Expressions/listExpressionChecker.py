from src.DisciplLawReg.Expressions.baseSubExpressionChecker import BaseSubExpressionChecker

class ListExpressionChecker(BaseSubExpressionChecker):
    async def checkSubExpression (self, fact, ssid, context):
        hasUndefined = False
        if not 'listNames' in context:
            context['listNames'] = []
            context['listIndices'] = []

        if 'name' in fact:
            context['listNames'].append(fact['name'])
        else:
            context['listNames'].append('noName')

        context['listIndices'].append(0)
        listIndex = len(context['listIndices']) - 1
        listContentResult = []
        while True:
            op = fact['items']
            newContext = self._getContextExplainer().extendContextWithExplanation(context)
            operandResult = await self._getExpressionChecker().checkExpression(op, ssid, newContext)
            self.logger.debug('OperandResult in LIST', operandResult, 'for operand', op, 'and index', context['listIndices'][listIndex])

            if operandResult == False:
                self.logger.debug('Stopping LIST concatenation, because', op, 'is false')
                break

            listContentResult.append(operandResult)

            if operandResult == None:
                hasUndefined = True
                break

            context['listIndices'][listIndex] += 1

        context['listNames'].pop()
        resultIndex = context['listIndices'].pop()

        if hasUndefined:
            listResult = None
        elif resultIndex != 0:
            listResult = listContentResult
        else:
            listResult = False
        #listResult = hasUndefined ? undefined : (resultIndex !== 0 ? listContentResult : false)
        self.logger.debug('Resolved LIST as', listResult)
        self._getContextExplainer().extendContextExplanationWithResult(context, listResult)
        return listResult
