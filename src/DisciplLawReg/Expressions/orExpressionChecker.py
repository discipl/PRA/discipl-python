from src.DisciplLawReg.Expressions.baseSubExpressionChecker import BaseSubExpressionChecker

class OrExpressionChecker(BaseSubExpressionChecker):
    async def checkSubExpression (self, fact, ssid, context):
        hasUndefined = False
        for op in fact['operands']:
            newContext = self._getContextExplainer().extendContextWithExplanation(context)
            operandResult = await self._getExpressionChecker().checkExpression(op, ssid, newContext)

            if operandResult:
                self.logger.debug('Resolved OR as true, because', op, 'is true')
                self._getContextExplainer().extendContextExplanationWithResult(context, True)
                return True

            if operandResult == None:
                hasUndefined = True

        #orResult = hasUndefined ? undefined : false
        if hasUndefined:
            orResult = None
        else:
            orResult = False
        self._getContextExplainer().extendContextExplanationWithResult(context, orResult)
        self.logger.debug('Resolved OR as', orResult)
        return orResult
