import src.DisciplLawReg.LawReg as lawReg
from src.DisciplLawReg.Expressions.baseSubExpressionChecker import BaseSubExpressionChecker

class IsExpressionChecker(BaseSubExpressionChecker):
    async def checkSubExpression (self, fact, ssid, context):
        if not 'operand' in fact:
            raise Exception('A operand must be given for the IS expression')

        isAnyone = self._checkForIsAnyone(fact['operand'], context)
        if isAnyone:
            return True

        return self._checkForDidIdentification(fact['operand'], context, ssid)

    '''
    * Checks if fact is anyone marker
    *
    * @param {string} did - The did to check
    * @param {Context} context - Represents the context of the check
    * @returns {boolean} true if did is anyone marker
    '''
    def _checkForIsAnyone (self, did, context):
        if did == lawReg.DISCIPL_ANYONE_MARKER:
            self.logger.debug('Resolved IS as true, because anyone can be this')
            self._getContextExplainer().extendContextExplanationWithResult(context, True)
            return True
        return False

    '''
    * Checks if fact is `IS`-construction and if so returns if it's allowed
    *
    * @param {string} did - The did to check
    * @param {Context} context - Represents the context of the check
    * @param {object} ssid - Identity of the entity performing the check
    * @returns {boolean} true or false if fact is `IS`-construction else undefined
    '''
    def _checkForDidIdentification (self, did, context, ssid):
        if did != None:
            didIsIdentified = (ssid['did'] == did or not 'myself' in context)
            if 'myself' in context:
                text = 'did-identification'
            else:
                text = 'the concerned being someone else'
            self.logger.debug('Resolving fact IS as', didIsIdentified, 'by', text)
            self._getContextExplainer().extendContextExplanationWithResult(context, didIsIdentified)
            return didIsIdentified
        return None