from src.DisciplLawReg.Expressions.baseSubExpressionChecker import BaseSubExpressionChecker

class CreateExpressionChecker(BaseSubExpressionChecker):
    async def checkSubExpression (self, fact, ssid, context):
        finalCreateResult = await self._getFactChecker().checkCreatableFactCreated(context['previousFact'], ssid, context)

        if not finalCreateResult or not 'operands' in fact:
            self.logger.debug('Resolving fact', fact, 'as', finalCreateResult, 'by determining earlier creation')

            self._getContextExplainer().extendContextExplanationWithResult(context, finalCreateResult)
            return finalCreateResult

        self.logger.debug('Resolving fact', fact, 'as', finalCreateResult, 'by determining earlier creation')
        self._getContextExplainer().extendContextExplanationWithResult(context, finalCreateResult)

        return finalCreateResult