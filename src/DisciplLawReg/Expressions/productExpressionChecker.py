from src.DisciplLawReg.Utils.Big_Util import BigUtil
from src.DisciplLawReg.Expressions.baseSubExpressionChecker import BaseSubExpressionChecker

class ProductExpressionChecker(BaseSubExpressionChecker):
    async def checkSubExpression (self, fact, ssid, context):
        productResult = 1
        for op in fact['operands']:
            if not BigUtil.isNumeric(productResult):
                break
            newContext = self._getContextExplainer().extendContextWithExplanation(context)
            operandResult = await self._getExpressionChecker().checkExpression(op, ssid, newContext)
            self.logger.debug('OperandResult in PRODUCT', str(operandResult), 'for operand', op)
            #const opArray = Array.isArray(operandResult) ? operandResult : [operandResult]
            if isinstance(operandResult, list):
                opArray = operandResult
            else:
                opArray = [operandResult]

            for arrayOp in opArray:
                self.logger.debug('ArrayOperandResult in PRODUCT', str(arrayOp), 'for operand', op)
                if arrayOp == None:
                    productResult = None
                    break
                elif not BigUtil.isNumeric(arrayOp):
                    productResult = False
                    break
                else:
                    productResult = BigUtil.multiply(productResult, arrayOp)
        self.logger.debug('Resolved PRODUCT as', str(productResult))
        self._getContextExplainer().extendContextExplanationWithResult(context, productResult)
        return productResult