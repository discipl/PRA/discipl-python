from src.DisciplLawReg.Utils.Big_Util import BigUtil
from src.DisciplLawReg.Expressions.baseSubExpressionChecker import BaseSubExpressionChecker

class LessThanExpressionChecker(BaseSubExpressionChecker):
    async def checkSubExpression (self, fact, ssid, context):
        lastOperandResult = None
        for op in fact['operands']:
            newContext = self._getContextExplainer().extendContextWithExplanation(context)
            operandResult = await self._getExpressionChecker().checkExpression(op, ssid, newContext)
            self.logger.debug('OperandResult in LESS_THAN', operandResult, 'for operand', op)
            if operandResult == None:
                self.logger.debug('Resolved LESS_THAN as undefined because one of the operands was undefined')
                lastOperandResult = None
                break
            elif not BigUtil.isNumeric(operandResult):
                self.logger.debug('Resolved LESS_THAN as false because', str(operandResult), 'is not numeric')
                lastOperandResult = False
                break
            elif lastOperandResult != None and BigUtil.lessThan(operandResult, lastOperandResult):
                self.logger.debug('Resolved LESS_THAN as false, because', str(lastOperandResult), 'is not less than', str(operandResult))
                lastOperandResult = False
                break
            else:
                lastOperandResult = operandResult

        #lessThanResult
        if lastOperandResult == None:
            lessThanResult = None
        else:
            lessThanResult = lastOperandResult != False
        if lessThanResult:
            self.logger.debug('Resolved LESS_THAN as', str(lessThanResult))

        self._getContextExplainer().extendContextExplanationWithResult(context, lessThanResult)
        return lessThanResult