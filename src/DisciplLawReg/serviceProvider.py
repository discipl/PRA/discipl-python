# Improve intelisense
# eslint-disable-next-line no-unused-vars
from src.AbundanceService.AbundanceService import AbundanceService
from src.DisciplLawReg.Services.contextExplainer import ContextExplainer
from src.DisciplLawReg.Services.factChecker import FactChecker
from src.DisciplLawReg.Services.actionChecker import ActionChecker
from src.DisciplLawReg.Services.expressionChecker import ExpressionChecker
from src.DisciplLawReg.Services.actFetcher import ActFetcher
from src.DisciplLawReg.Services.dutyFetcher import DutyFetcher
from src.DisciplLawReg.Services.actionService import ActionService
from src.DisciplLawReg.Services.modelPublisher import ModelPublisher
from src.DisciplLawReg.Utils.Link_Util import LinkUtil

class ServiceProvider:
    '''
    * Create a ServiceProvider
    * @param {AbundanceService} abundanceService
    '''
    def __init__(self, abundanceService):
        self.abundanceService = abundanceService
        self.contextExplainer = ContextExplainer()
        self.linkUtil = LinkUtil(self)
        self.factChecker = FactChecker(self)
        self.actionChecker = ActionChecker(self)
        self.expressionChecker = ExpressionChecker(self)
        self.actFetcher = ActFetcher(self)
        self.dutyFetcher = DutyFetcher(self)
        self.actionService = ActionService(self)
        self.modelPublisher = ModelPublisher(self)

    '''
    * @type {ActionChecker}
    '''
    def getActionChecker (self):
        return self._actionChecker

    '''
    * @param {ActionChecker} value
    '''
    def setActionChecker (self, value):
        self._actionChecker = value

    '''
    * @type {ActFetcher}
    '''
    def getActFetcher (self):
        return self._actFetcher

    '''
    * @param {ActFetcher} value
    '''
    def setActFetcher (self, value):
        self._actFetcher = value

    '''
    * @type {LinkUtil}
    '''
    def getLinkUtil (self):
        return self._linkUtils

    '''
    * @param {LinkUtil} value
    '''
    def setLinkUtil (self, value):
        self._linkUtils = value

    '''
    * @type {ExpressionChecker}
    '''
    def getExpressionChecker (self):
        return self._expressionChecker

    '''
    * @param {ExpressionChecker} value
    '''
    def setExpressionChecker (self, value):
        self._expressionChecker = value

    '''
    * @type {AbundanceService}
    '''
    def getAbundanceService (self):
        return self._abundanceService

    '''
    * @param {AbundanceService} value
    '''
    def setAbundanceService (self, value):
        self._abundanceService = value

    '''
    * @type {FactChecker}
    '''
    def getFactChecker (self):
        return self._factChecker

    '''
    * @param {FactChecker} value
    '''
    def setFactChecker (self, value):
        self._factChecker = value

    '''
    * @type {ContextExplainer}
    '''
    def getContextExplainer (self):
        return self._contextExplainer

    '''
    * @param {ContextExplainer} value
    '''
    def setContextExplainer (self, value):
        self._contextExplainer = value

    '''
    * @type {DutyFetcher}
    '''
    def getDutyFetcher (self):
        return self._dutyFetcher

    '''
    * @param {DutyFetcher} value
    '''
    def setDutyFetcher (self, value):
        self._dutyFetcher = value

    '''
    * @type {ActionService}
    '''
    def getActionService (self):
        return self._actionService

    '''
    * @param {ActionService} value
    '''
    def setActionService (self, value):
        self._actionService = value

    '''
    * @type {ModelPublisher}
    '''
    def getModelPublisher (self):
        return self._modelPublisher

    '''
    * @param {ModelPublisher} value
    '''
    def setModelPublisher (self,value):
        self._modelPublisher = value

