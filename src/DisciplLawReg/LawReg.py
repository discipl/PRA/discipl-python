from src.AbundanceService.AbundanceService import AbundanceService
from src.DisciplLawReg.modelValidator import ModelValidator
from src.DisciplLawReg.serviceProvider import ServiceProvider

DISCIPL_ANYONE_MARKER = 'ANYONE'
DISCIPL_FLINT_FACT = 'DISCIPL_FLINT_FACT'
DISCIPL_FLINT_MODEL = 'DISCIPL_FLINT_MODEL'
DISCIPL_FLINT_ACT = 'DISCIPL_FLINT_ACT'
DISCIPL_FLINT_DUTY = 'DISCIPL_FLINT_DUTY'
DISCIPL_FLINT_ACT_TAKEN = 'DISCIPL_FLINT_ACT_TAKEN'
DISCIPL_FLINT_FACTS_SUPPLIED = 'DISCIPL_FLINT_FACTS_SUPPLIED'
DISCIPL_FLINT_GLOBAL_CASE = 'DISCIPL_FLINT_GLOBAL_CASE'
DISCIPL_FLINT_PREVIOUS_CASE = 'DISCIPL_FLINT_PREVIOUS_CASE'
DISCIPL_FLINT_MODEL_LINK = 'DISCIPL_FLINT_MODEL_LINK'

class LawReg:
    def __init__(self, abundanceService = AbundanceService()):
        self.serviceProvider = ServiceProvider(abundanceService)

    '''
    * Get abundance service
    * @return {AbundanceService}
    '''
    def getAbundanceService (self):
        return self.serviceProvider.abundanceService

    '''
    * Get action service
    * @return {ActionService}
    * @private
    '''
    def _getActionService (self):
        return self.serviceProvider.actionService

    '''
    * Get action checker
    * @return {ActionChecker}
    * @protected
    '''
    def _getActionChecker (self):
        return self.serviceProvider.actionChecker

    '''
    * Get act fetcher
    * @return {ActFetcher}
    * @private
    '''
    def _getActFetcher (self):
        return self.serviceProvider.actFetcher

    '''
    * Get expression checker
    * @return {ExpressionChecker}
    * @private
    '''
    def _getExpressionChecker (self):
        return self.serviceProvider.expressionChecker

    '''
    * Get duty fetcher
    * @return {DutyFetcher}
    * @private
    '''
    def _getDutyFetcher (self):
        return self.serviceProvider.dutyFetcher

    '''
    * Get model publisher
    * @return {ModelPublisher}
    * @private
    '''
    def _getModelPublisher (self):
        return self.serviceProvider.modelPublisher

    '''
    * Returns details of an act, as registered in the model
    *
    * @param {string} actLink - Link to the particular act
    * @param {ssid} ssid - Identity requesting the information
    * @returns {object}
    '''
    async def getActDetails (self, actLink, ssid):
        return await self._getActFetcher().getActDetails(actLink, ssid)

    '''
    * Returns the names of all acts that can be taken, given the current caseLink, ssid of the actor and a list of facts
    *
    * @param {string} caseLink - Link to the case, last action that was taken
    * @param {ssid} ssid - Identifies the actor
    * @param {string[]} facts - Array of true facts
    * @param {string[]} nonFacts - Array of false facts
    * @returns {Promise<Array>}
    '''
    async def getAvailableActs (self, caseLink, ssid, facts = [], nonFacts = []):
        return await self._getActFetcher().getAvailableActs(caseLink, ssid, facts, nonFacts)

    '''
    * Returns the names of all acts that can be taken, given the current caseLink, ssid of the actor and a list of facts
    *
    * @param {string} caseLink - Link to the case, last action that was taken
    * @param {ssid} ssid - Identifies the actor
    * @param {function} factResolver - Returns the value of a fact if known, and undefined otherwise
    * @returns {Promise<Array>}
    '''
    async def getAvailableActsWithResolver (self, caseLink, ssid, factResolver):
        return await self._getActFetcher().getAvailableActsWithResolver(caseLink, ssid, factResolver)

    '''
    * Returns the names of all acts that could be taken potentially if more facts are supplied,
    * given the current caseLink, ssid of the actor and a list of facts and nonFacts
    *
    * @param {string} caseLink - Link to the case, last action that was taken
    * @param {ssid} ssid - Identifies the actor
    * @param {string[]} facts - Array of true facts
    * @param {string[]} nonFacts - Array of false facts
    * @returns {Promise<Array>}
    '''
    async def getPotentialActs (self, caseLink, ssid, facts = [], nonFacts = []):
        return await self._getActFetcher().getPotentialActs(caseLink, ssid, facts, nonFacts)

    '''
    * Returns the names of all acts that could be taken potentially if more facts are supplied,
    * given the current caseLink, ssid of the actor and a factResolver
    *
    * @param {string} caseLink - Link to the case, last action that was taken
    * @param {ssid} ssid - Identifies the actor
    * @param {function} factResolver - Returns the value of a fact if known, and undefined otherwise
    * @returns {Promise<Array>}
    '''
    async def getPotentialActsWithResolver (self, caseLink, ssid, factResolver):
        return await self._getActFetcher().getPotentialActsWithResolver(caseLink, ssid, factResolver)

    '''
    * Returns the active duties that apply in the given case for the given ssid
    *
    * @param {string} caseLink - link to the current state of the case
    * @param {ssid} ssid - identity to find duties for
    * @returns {Promise<DutyInformation[]>}
    '''
    async def getActiveDuties (self, caseLink, ssid):
        return await self._getDutyFetcher().getActiveDuties(caseLink, ssid)

    '''
    * Publishes the FLINT model (as JSON) in linked verifiable claims (vc's)
    * in the channel of the given ssid. Each act, fact and duty is stored in a separate vc.
    * Returns a list to the claim holding the whole model with links to individual claims
    * Note that references within the model are not translated into links.
    *
    * @param {ssid} ssid SSID that publishes the model
    * @param {object} flintModel Model to publish
    * @param {object} factFunctions Additional factFunction that are declared outside the model
    * @return {Promise<string>} Link to a verifiable claim that holds the published model
    '''
    async def publish (self, ssid, flintModel, factFunctions = {}):
        modelPublisher = self._getModelPublisher()

        return await modelPublisher.publish(ssid, flintModel, factFunctions)

    '''
    * Returns all the actions that have been taken in a case so far
    *
    * @param {string} caseLink - Link to the last action in the case
    * @param {ssid} ssid - Identity used to get access to information
    * @returns {Promise<ActionInformation[]>}
    '''
    async def getActions (self, caseLink, ssid):
        return await self._getActionService().getActions(caseLink, ssid)

    '''
    * Denotes a given act in the context of a case as taken, if it is possible. See {@link ActionChecker.checkAction} is used to check the conditions
    *
    * @param {ssid} ssid - Identity of the actor
    * @param {string} caseLink - Link to the case, which is either an earlier action, or a need
    * @param {string} act - description of the act to be taken
    * @param {function} factResolver - Function used to resolve facts to fall back on if no other method is available. Defaults to always false
    * @returns {Promise<string>} Link to a verifiable claim that holds that taken actions
    '''
    async def take (self, ssid, caseLink, act, factResolver = lambda: False):
        return await self._getActionService().take(ssid, caseLink, act, factResolver)

    '''
    * Add the result of an action to the explanation part of the context. {@link ActionChecker.checkAction} is used to check the conditions.
    *
    * @param {ssid} ssid - Identity of the actor
    * @param {string} caseLink - Link to the case, which is either an earlier action, or a need
    * @param {string} act - description of the act to explain
    * @param {function} factResolver - Function used to resolve facts to fall back on if no other method is available. Defaults to always false
    * @returns {Promise<Explanation>} Explanation object from the context with the action result as value
    '''
    async def explain (self, ssid, caseLink, act, factResolver):
        return self._getActionService().explain(ssid, caseLink, act, factResolver)