class FactResolver():
    def __init__(self, facts, shouldUseSimpleResult = False, simpleResult = True):
        self.facts = facts
        self.shouldUseSimpleResult = shouldUseSimpleResult
        self.simpleResult = simpleResult

    async def resolveFacts(self, fact, listNames, listIndices, possibleCreatingActions):
        if self.shouldUseSimpleResult:
            return self.simpleResult

        if fact in self.facts:
            if isinstance(self.facts[fact], list):
                return self.facts[fact].pop(0)
            else:
                return self.facts[fact]
        return False
