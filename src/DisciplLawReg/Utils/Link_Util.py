import src.DisciplLawReg.LawReg as lawReg#import DISCIPL_FLINT_ACT, DISCIPL_FLINT_ACT_TAKEN, DISCIPL_FLINT_DUTY, DISCIPL_FLINT_MODEL, DISCIPL_FLINT_PREVIOUS_CASE from '../index'
from src.DisciplLawReg.Utils.Logging_Util import getDiscplLogger
# Improve intelisense
# eslint-disable-next-line no-unused-vars
from src.AbundanceService.AbundanceService import AbundanceService

class LinkUtil():
    '''
    * Create an LinkUtils
    * @param {ServiceProvider} serviceProvider
    '''
    def __init__(self, serviceProvider):
        self.logger = getDiscplLogger()
        self.serviceProvider = serviceProvider

    '''
    * Get abundance service
    * @return {AbundanceService}
    * @private
    '''
    def _getAbundanceService (self):
        return self.serviceProvider.abundanceService

    '''
    * Get model link from first case link and actor ssid
    * @param {string} firstCaseLink
    * @param {ssid} ssid
    * @return {Promise<string>}
    '''
    async def getModelLink (self, firstCaseLink, ssid):
        core = self._getAbundanceService().getCoreAPI()
        firstCase = await core.get(firstCaseLink, ssid)

        modelLink = firstCase['claim']['data']['need'][lawReg.DISCIPL_FLINT_MODEL_LINK]
        self.logger.debug('Determined model link to be', modelLink)
        return modelLink

    '''
    * Get first case link from a case link and actor ssid
    * @param {string} caseLink
    * @param {ssid} ssid
    * @return {Promise<string>}
    '''
    async def getFirstCaseLink (self, caseLink, ssid):
        core = self._getAbundanceService().getCoreAPI()
        caseClaim = await core.get(caseLink, ssid)
        isFirstActionInCase = not (lawReg.DISCIPL_FLINT_ACT_TAKEN in caseClaim['claim']['data'])
        #isFirstActionInCase = !Object.keys(caseClaim.data).includes(DISCIPL_FLINT_ACT_TAKEN)
        firstCaseLink = None
        if isFirstActionInCase:
            firstCaseLink = caseLink
        else:
            firstCaseLink = caseClaim['claim']['data'][lawReg.DISCIPL_FLINT_GLOBAL_CASE]
        #firstCaseLink = isFirstActionInCase ? caseLink : caseClaim.data[DISCIPL_FLINT_GLOBAL_CASE]
        self.logger.debug('Determined first case link to be', firstCaseLink)
        return firstCaseLink
