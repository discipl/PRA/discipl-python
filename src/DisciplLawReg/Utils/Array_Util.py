'''
 * Converts an array into an object
 *
 * @param {array} arr - array with objects in it
 * @returns {object} object instead of the given array
'''
def arrayToObject (arr):
    obj = {}
    for index, item in enumerate(arr):
        for key in item:
            obj[key] = item[key]
    return obj