import logging

logger = logging.getLogger('disciplLawReg')

'''
 * Get discpl logger
 * @return {object}
'''
def getDiscplLogger ():
    return logger
