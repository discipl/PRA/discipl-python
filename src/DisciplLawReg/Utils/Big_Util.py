from mpmath import *

class BigUtil():
    '''
    @staticmethod
    def genericOp (a, b, functionName, fallbackFunction, commutativeFunctionName = functionName):
        if (type(a) == NoneType or type(b) == NoneType):
            return None

        if (a[functionName]) {
          return a[functionName](b)
        }

        if (b[commutativeFunctionName]) {
          return b[commutativeFunctionName](a)
        }

        return fallbackFunction()
    }
    '''

    @staticmethod
    def isNumeric (n):
        if type(n) == int or type(n) == float or type(n) == mpf:
            return True
        return False

    @staticmethod
    def add (a, b):
        if a == None or b == None:
            return None
        if type(a) == mpf or type(b) == mpf:
            return fadd(a, b)
        else:
            return a + b

    @staticmethod
    def multiply (a, b):
        if a == None or b == None:
            return None
        if type(a) == mpf or type(b) == mpf:
            return fmul(a, b)
        else:
            return a * b

    @staticmethod
    def equal(a, b):
        if a == None or b == None:
            return None
        mp.dps = 5 # sets the precision (number of decimal points)
        return a == b

    @staticmethod
    def lessThan(a, b):
        if a == None or b == None:
            return None
        return a < b

    @staticmethod
    def greaterThan(a, b):
        if a == None or b == None:
            return None
        return a > b
