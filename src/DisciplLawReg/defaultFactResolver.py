from src.DisciplLawReg.Utils.FactResolver import FactResolver
import asyncio
'''
 * Create a default resolver and extend it's logic with additional facts and a fallback resolver. The fallback is used
 * when no other methods are available to resolve the supplied facts.
 *
 * @param {function} factResolver - Function used to resolve facts to fall back on if no other method is available
 * @param {Object} factsSupplied - Facts object
 * @return {function} Function used to resolve facts to fall back on if no other method is available
'''
class DefaultFactResolver:
    def __init__(self, factResolver, factsSupplied):
        self.factResolver = factResolver
        self.factsSupplied = factsSupplied
    '''  
    def wrapWithDefault (self, factResolver, factsSupplied):
        return async (fact, listNames, listIndices, possibleCreatingActions) => {
        let factsObject = factsSupplied
        for (let i = 0; i < listNames.length; i++) {
          const listName = listNames[i]
          factsObject[listName] = factsObject[listName] ? factsObject[listName] : []
          const listIndex = listIndices[i]
    
          factsObject[listName][listIndex] = factsObject[listName][listIndex] ? factsObject[listName][listIndex] : {}
    
          factsObject = factsObject[listName][listIndex]
        }
        let maybeCreatingAction = null
        if (possibleCreatingActions && possibleCreatingActions.length === 1) {
          maybeCreatingAction = possibleCreatingActions[0]
        }
        const result = factsObject[fact] || maybeCreatingAction || factResolver(fact, listNames, listIndices, possibleCreatingActions)
        factsObject[fact] = await result
        return result
    '''
    async def resolveFacts(self, fact, listNames, listIndices, possibleCreatingActions):
        factsObject = self.factsSupplied
        for i in range(len(listNames)):
            listName = listNames[i]
            if listName in factsObject:
                factsObject[listName] = factsObject[listName]
            else:
                factsObject[listName] = []
            #factsObject[listName] = factsObject[listName] ? factsObject[listName] : []
            listIndex = listIndices[i]
            if listIndex < len(factsObject[listName]):
                factsObject[listName][listIndex] = factsObject[listName][listIndex]
            else:
                factsObject[listName].append({})
            #factsObject[listName][listIndex] = factsObject[listName][listIndex] ? factsObject[listName][listIndex] : {}

            factsObject = factsObject[listName][listIndex]



        maybeCreatingAction = None
        if possibleCreatingActions != None:
            if len(possibleCreatingActions) == 1:
                maybeCreatingAction = possibleCreatingActions[0]

        if isinstance(fact, dict):
            return None

        if fact in factsObject and factsObject[fact] != None:
            result = factsObject[fact]
        else:
            result = maybeCreatingAction or await self.factResolver.resolveFacts(fact, listNames, listIndices, possibleCreatingActions)

        if asyncio.iscoroutinefunction(result):
            factsObject[fact] = await result
        else:
            factsObject[fact] = result

        return result
