import src.DisciplLawReg.LawReg as lawReg#  DISCIPL_FLINT_ACT, DISCIPL_FLINT_MODEL from '../index'
# Improve intelisense
# eslint-disable-next-line no-unused-vars
from src.AbundanceService.AbundanceService import AbundanceService
from src.DisciplLawReg.Utils.Logging_Util import getDiscplLogger
from src.DisciplLawReg.defaultFactResolver import DefaultFactResolver

class factResolver():
    def __init__(self, facts, nonFacts):
        self.facts = facts
        self.nonFacts = nonFacts

    async def resolveFacts(self, fact, listNames, listIndices, possibleCreatingActions):
        if fact in self.facts:
            return True
        if fact in self.nonFacts:
            return False

class ActFetcher():
    '''
    * Create an ActFetcher
    * @param {ServiceProvider} serviceProvider
    '''
    def __init__(self, serviceProvider):
        self.logger = getDiscplLogger()
        self.serviceProvider = serviceProvider

    '''
    * Get abundance service
    * @return {AbundanceService}
    * @private
    '''
    def _getAbundanceService (self):
        return self.serviceProvider.abundanceService

    '''
    * Get link utils
    * @return {LinkUtil}
    * @private
    '''
    def _getLinkUtils (self):
        return self.serviceProvider.linkUtil

    '''
    * Get action checker
    * @return {ActionChecker}
    * @private
    '''
    def _getActionChecker (self):
        return self.serviceProvider.actionChecker

    '''
    * Returns the names of all acts that can be taken, given the current caseLink, ssid of the actor and a list of facts
    *
    * @param {string} caseLink - Link to the case, last action that was taken
    * @param {ssid} ssid - Identifies the actor
    * @param {string[]} facts - Array of true facts
    * @param {string[]} nonFacts - Array of false facts
    * @returns {Promise<Array>}
    '''
    async def getAvailableActs (self, caseLink, ssid, facts = [], nonFacts = []):
        return await self.getAvailableActsWithResolver(caseLink, ssid, factResolver(facts, nonFacts))

    '''
    * Returns the names of all acts that can be taken, given the current caseLink, ssid of the actor and a list of facts
    *
    * @param {string} caseLink - Link to the case, last action that was taken
    * @param {ssid} ssid - Identifies the actor
    * @param {function} factResolver - Returns the value of a fact if known, and undefined otherwise
    * @returns {Promise<Array>}
    '''
    async def getAvailableActsWithResolver (self, caseLink, ssid, factResolver):
        core = self._getAbundanceService().getCoreAPI()

        firstCaseLink = await self._getLinkUtils().getFirstCaseLink(caseLink, ssid)
        modelLink = await self._getLinkUtils().getModelLink(firstCaseLink, ssid)

        model = await core.get(modelLink, ssid)

        acts = model['claim']['data'][lawReg.DISCIPL_FLINT_MODEL]['acts']

        defaultFactResolver = DefaultFactResolver(factResolver, {})

        allowedActs = []
        self.logger.debug('Checking', acts, 'for available acts')
        for actWithLink in acts:
            self.logger.debug('Checking whether', actWithLink, 'is an available option')

            for key, value in actWithLink.items():
                act = key
                link = value
                break
            #link = Object.values(actWithLink)[0]

            checkActionInfo = await self._getActionChecker().checkAction(modelLink, link, ssid, { 'factResolver': defaultFactResolver, 'caseLink': caseLink })
            if checkActionInfo['valid']:
                actionInformation = {
                    'act': act,
                    'link': link
                    }
                allowedActs.append(actionInformation)

        return allowedActs

    '''
    * Returns the names of all acts that could be taken potentially if more facts are supplied,
    * given the current caseLink, ssid of the actor and a list of facts and nonFacts
    *
    * @param {string} caseLink - Link to the case, last action that was taken
    * @param {ssid} ssid - Identifies the actor
    * @param {string[]} facts - Array of true facts
    * @param {string[]} nonFacts - Array of false facts
    * @returns {Promise<Array>}
    '''
    async def getPotentialActs (self, caseLink, ssid, facts = [], nonFacts = []):
        return await self.getPotentialActsWithResolver(caseLink, ssid, factResolver(facts, nonFacts))

    '''
    * Returns the names of all acts that could be taken potentially if more facts are supplied,
    * given the current caseLink, ssid of the actor and a factResolver
    *
    * @param {string} caseLink - Link to the case, last action that was taken
    * @param {ssid} ssid - Identifies the actor
    * @param {function} factResolver - Returns the value of a fact if known, and undefined otherwise
    * @returns {Promise<Array>}
    '''
    async def getPotentialActsWithResolver (self, caseLink, ssid, factResolver):
        core = self._getAbundanceService().getCoreAPI()

        firstCaseLink = await self._getLinkUtils().getFirstCaseLink(caseLink, ssid)
        modelLink = await self._getLinkUtils().getModelLink(firstCaseLink, ssid)

        model = await core.get(modelLink, ssid)

        acts = model['claim']['data'][lawReg.DISCIPL_FLINT_MODEL]['acts']

        allowedActs = []
        self.logger.debug('Checking', acts, 'for potentially available acts')
        for actWithLink in acts:
            unknownItems = []

            defaultFactResolver = DefaultFactResolver(factResolver, {})
            self.logger.debug('Checking whether', actWithLink, 'is a potentially available option')

            for key, value in actWithLink.items():
                localKey = key
                link = value
                break
            if localKey == "<<minister treft betalingsregeling voor het terugbetalen van de subsidie voor studiekosten>>":
                i = 0
            #link = Object.values(actWithLink)[0]
            checkActionInfo = await self._getActionChecker().checkAction(modelLink, link, ssid, { 'factResolver': defaultFactResolver, 'caseLink': caseLink })
            self.logger.debug('Unknown items', unknownItems)
            if checkActionInfo['valid'] == None:
                actionInformation = {
                    'act': localKey,
                    'link': link
                    }
                allowedActs.append(actionInformation)

        return allowedActs

    '''
    * Returns details of an act, as registered in the model
    *
    * @param {string} actLink - Link to the particular act
    * @param {ssid} ssid - Identity requesting the information
    * @returns {object}
    '''
    async def getActDetails (self, actLink, ssid):
        core = self._getAbundanceService().getCoreAPI()
        claimData = await core.get(actLink, ssid)
        return claimData['claim']['data'][lawReg.DISCIPL_FLINT_ACT]

