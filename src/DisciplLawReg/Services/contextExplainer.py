import copy

class ContextExplainer:
    '''
    * Extend context with explanation
    * @param {Context} context
    * @return {Context}
    '''
    def extendContextWithExplanation (self, context):
        if 'explanation' in context:
            newExplanation = {}
            if isinstance(context.explanation.operandExplanations, list):
                context.explanation.operandExplanations.push(newExplanation)
            else:
                context.explanation.operandExplanations = [newExplanation]
            contextDC = copy.deepcopy(context)
            contextDC['explanation'] = newExplanation
            return contextDC
        else:
            return context

    '''
    * Extend context explanation with result
    * @param {Context} context
    * @param {*} result
    * @return {Explanation}
    '''
    def extendContextExplanationWithResult (self, context, result):
        if 'explanation' in context and context['explanation'].value == None:
            if type(result) == dict:
                context['explanation'].value = str(result)
            else:
                context['explanation'].value = result