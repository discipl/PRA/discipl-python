import src.DisciplLawReg.LawReg as lawReg#import DISCIPL_FLINT_ACT, DISCIPL_FLINT_ACT_TAKEN, DISCIPL_FLINT_DUTY, DISCIPL_FLINT_MODEL, DISCIPL_FLINT_PREVIOUS_CASE from '../index'
from src.DisciplLawReg.Utils.Logging_Util import getDiscplLogger
from src.DisciplLawReg.Utils.FactResolver import FactResolver
# Improve intelisense
# eslint-disable-next-line no-unused-vars
from src.AbundanceService.AbundanceService import AbundanceService

from src.DisciplLawReg.defaultFactResolver import DefaultFactResolver#import wrapWithDefault from '../defaultFactResolver'
from src.DisciplLawReg.Utils.Identity_Util import IdentityUtil


class ActionService:
    '''
    * Create an ActionService
    * @param {ServiceProvider} serviceProvider
    '''
    def __init__(self, serviceProvider):
        self.logger = getDiscplLogger()
        self.serviceProvider = serviceProvider

    '''
    * Get abundance service
    * @return {AbundanceService}
    * @private
    '''
    def _getAbundanceService (self):
        return self.serviceProvider.abundanceService

    '''
    * Get link utils
    * @return {LinkUtil}
    * @private
    '''
    def _getLinkUtils (self):
        return self.serviceProvider.linkUtil

    '''
    * Get action checker
    * @return {ActionChecker}
    * @private
    '''
    def _getActionChecker (self):
        return self.serviceProvider.actionChecker

    '''
    * Returns all the actions that have been taken in a case so far
    *
    * @param {string} caseLink - Link to the last action in the case
    * @param {ssid} ssid - Identity used to get access to information
    * @returns {Promise<ActionInformation[]>}
    '''
    async def getActions (self, caseLink, ssid):
        core = self._getAbundanceService().getCoreAPI()
        actionLink = caseLink

        acts = []

        while actionLink != None:
            lastAction = await core.get(actionLink, ssid)
            if lawReg.DISCIPL_FLINT_ACT_TAKEN in lastAction['claim']['data']:
                actLink = lastAction['claim']['data'][lawReg.DISCIPL_FLINT_ACT_TAKEN]
            else:
                actLink = None

            if actLink != None:
                act = await core.get(actLink, ssid)

                if type(act['claim']['data'][lawReg.DISCIPL_FLINT_ACT]['act']) == str:
                    acts.insert(0, { 'act': act['claim']['data'][lawReg.DISCIPL_FLINT_ACT]['act'], 'link': actionLink })

            if lawReg.DISCIPL_FLINT_PREVIOUS_CASE in lastAction['claim']['data']:
                actionLink = lastAction['claim']['data'][lawReg.DISCIPL_FLINT_PREVIOUS_CASE]
            else:
                actionLink = None
        return acts

    '''
    * Denotes a given act in the context of a case as taken, if it is possible. See {@link ActionChecker.checkAction} is used to check the conditions
    *
    * @param {ssid} ssid - Identity of the actor
    * @param {string} caseLink - Link to the case, which is either an earlier action, or a need
    * @param {string} act - description of the act to be taken
    * @param {function} factResolver - Function used to resolve facts to fall back on if no other method is available. Defaults to always false
    * @returns {Promise<string>} Link to a verifiable claim that holds that taken actions
    '''
    async def take (self, ssid, caseLink, act, factResolver = FactResolver(None)):
        core, modelLink, actLink, firstCaseLink = await self._getModelAndActFromCase(caseLink, ssid, act)

        factsSupplied = {}

        defaultFactResolver = DefaultFactResolver(factResolver, factsSupplied)

        self.logger.debug('Checking if action', act, 'is possible from perspective of', ssid['did'])
        checkActionInfo = await self._getActionChecker().checkAction(modelLink, actLink, ssid, { 'factResolver': defaultFactResolver, 'caseLink': caseLink, 'factsSupplied': factsSupplied }, True)
        if checkActionInfo['valid']:
            self.logger.info('Registering act', actLink)
            return await core.claim(ssid, {
                lawReg.DISCIPL_FLINT_ACT_TAKEN: actLink,
                lawReg.DISCIPL_FLINT_GLOBAL_CASE: firstCaseLink,
                lawReg.DISCIPL_FLINT_PREVIOUS_CASE: caseLink,
                lawReg.DISCIPL_FLINT_FACTS_SUPPLIED: await self._addActorIsExpression(actLink, factsSupplied, ssid)})

        #raise Exception('Action ' + act + ' is not allowed due to ' + checkActionInfo['invalidReasons'])
        raise Exception('Action ' + act + ' is not allowed due to ')

    '''
    * Add actor IS expression to supplied facts
    *
    * @param {string} actLink - Link to the particular act
    * @param {ssid} ssid - Identity of the actor
    * @param {object} factsSupplied - The supplied facts
    * @returns {Promise<object>} The supplied facts
    * @private
    '''
    async def _addActorIsExpression (self, actLink, factsSupplied, ssid):
        core = self._getAbundanceService().getCoreAPI()
        actReference = await core.get(actLink, ssid)
        actor = actReference['claim']['data'][lawReg.DISCIPL_FLINT_ACT]['actor']
        factsSupplied[actor] = IdentityUtil.identityExpression(ssid['did'])
        return factsSupplied

    '''
    * Add the result of an action to the explanation part of the context. {@link ActionChecker.checkAction} is used to check the conditions.
    *
    * @param {ssid} ssid - Identity of the actor
    * @param {string} caseLink - Link to the case, which is either an earlier action, or a need
    * @param {string} act - description of the act to explain
    * @param {function} factResolver - Function used to resolve facts to fall back on if no other method is available. Defaults to always false
    * @returns {Promise<Explanation>} Explanation object from the context with the action result as value
    '''
    async def explain (self, ssid, caseLink, act, factResolver):
        modelLink, actLink = await self._getModelAndActFromCase(caseLink, ssid, act)

        defaultFactResolver = wrapWithDefault(factResolver, {})
        context = { 'factResolver': defaultFactResolver, 'caseLink': caseLink, explanation: {} }
        self.logger.debug('Checking if action is possible from perspective of', ssid.did)
        checkActionResult = await self._getActionChecker().checkAction(modelLink, actLink, ssid, context, true)
        context.explanation.value = checkActionResult.valid

        return context.explanation

    '''
    * Get model and act from case link, actor ssid and act
    * @param {string} caseLink
    * @param {ssid} ssid
    * @param {string} act
    * @return {Promise<{core: DisciplCore, modelLink: *, actLink: *, firstCaseLink: *}>}
    * @private
    '''
    async def _getModelAndActFromCase (self, caseLink, ssid, act):
        core = self._getAbundanceService().getCoreAPI()
        firstCaseLink = await self._getLinkUtils().getFirstCaseLink(caseLink, ssid)
        modelLink = await self._getLinkUtils().getModelLink(firstCaseLink, ssid)
        model = await core.get(modelLink, ssid)
        filteredActLinks = list(filter(lambda actWithLink: act in actWithLink, model['claim']['data'][lawReg.DISCIPL_FLINT_MODEL]['acts']))
        #filteredActLinks = model['claim']['data'][lawReg.DISCIPL_FLINT_MODEL]['acts'].filter(lambda actWithLink: act in actWithLink)
        #mappedActLinks = list(map(lambda actWithLink: filteredActLinks[0].value, filteredActLinks))
        for key in filteredActLinks[0]:
            actLink = filteredActLinks[0][key]
            break
        if actLink == None:
            raise Exception('Act not found ' + act)

        return core, modelLink, actLink, firstCaseLink
