import copy

import src.DisciplLawReg.LawReg as lawReg#import DISCIPL_FLINT_ACT, DISCIPL_FLINT_MODEL from '../index'
from src.DisciplLawReg.Utils.Logging_Util import getDiscplLogger
# Improve intelisense
# eslint-disable-next-line no-unused-vars
from src.AbundanceService.AbundanceService import AbundanceService
from src.DisciplLawReg.Utils.Array_Util import arrayToObject

class ActionChecker():
    '''
    * Create an ActionChecker
    * @param {ServiceProvider} serviceProvider
    '''
    def __init__(self, serviceProvider):
        self.logger = getDiscplLogger()
        self.serviceProvider = serviceProvider

    '''
    * Get abundance service
    * @return {AbundanceService}
    * @private
    '''
    def _getAbundanceService (self):
        return self.serviceProvider.abundanceService

    '''
    * Get fact checker
    * @return {FactChecker}
    * @private
    '''
    def _getFactChecker (self):
        return self.serviceProvider.factChecker

    '''
    * Get context explainer
    * @return {ContextExplainer}
    * @private
    '''
    def _getContextExplainer (self):
        return self.serviceProvider.contextExplainer

    '''
    * Checks if an action is allowed by checking if:
    * 1. The ssid can be the relevant actor
    * 2. The object exists
    * 3. The interested party exists
    * 4. The pre-conditions are fulfilled
    *
    * @param {string} modelLink - Link to a published FLINT model
    * @param {string} actLink - Link to the respective act
    * @param {ssid} ssid - Identity of intended actor
    * @param {Context} context - Context of the action
    * @param {boolean} earlyEscape - If true, will return a result as soon as one of the flint items is determined to be false
    * @returns {Promise<CheckActionResult>}
    '''
    async def checkAction (self, modelLink, actLink, ssid, context, earlyEscape = False):
        self.logger.debug('Checking action', actLink)
        core = self._getAbundanceService().getCoreAPI()
        modelReference = await core.get(modelLink, ssid)
        self.logger.debug('Obtained modelReference', modelReference)
        actReference = await core.get(actLink, ssid)
        factReference = arrayToObject(modelReference['claim']['data'][lawReg.DISCIPL_FLINT_MODEL]['facts'])
        self.logger.debug('Fact reference obtained from model', factReference)

        actor = actReference['claim']['data'][lawReg.DISCIPL_FLINT_ACT]['actor']

        invalidReasons = []

        actorContext = self._getContextExplainer().extendContextWithExplanation(context)

        self.logger.info('Checking if', actor, 'is', ssid['did'])

        #actorContextPlus = copy.deepcopy(actorContext)
        #actorContextPlus['facts'] = factReference
        #actorContextPlus['myself'] = True
        #checkedActor = await self._getFactChecker().checkFact(actor, ssid, actorContextPlus)

        actorContext['facts'] = factReference
        actorContext['myself'] = True
        checkedActor = await self._getFactChecker().checkFact(actor, ssid, actorContext)
        del actorContext['facts']
        del actorContext['myself']

        if not checkedActor:
            invalidReasons.append('actor')
            if earlyEscape:
                return {
                    'valid': False,
                    'invalidReasons': invalidReasons
                    }

        object = actReference['claim']['data'][lawReg.DISCIPL_FLINT_ACT]['object']

        self.logger.debug('Original object', object)

        objectContext = self._getContextExplainer().extendContextWithExplanation(context)

        #objectContextPlus = copy.deepcopy(objectContext)
        #objectContextPlus['facts'] = factReference

        #checkedObject = await self._getFactChecker().checkFact(object, ssid, objectContextPlus)
        #context = objectContextPlus

        objectContext['facts'] = factReference
        checkedObject = await self._getFactChecker().checkFact(object, ssid, objectContext)
        del actorContext['facts']

        if not checkedObject:
            invalidReasons.append('object')
            if earlyEscape:
                return {
                    'valid': False,
                    'invalidReasons': invalidReasons
                }

        recipient = actReference['claim']['data'][lawReg.DISCIPL_FLINT_ACT]['recipient']
        self.logger.debug('Original recipient', recipient)
        interestedPartyContext = self._getContextExplainer().extendContextWithExplanation(context)

        #interestedPartyContextPlus = copy.deepcopy(interestedPartyContext)
        #interestedPartyContextPlus['facts'] = factReference

        #checkedInterestedParty = await self._getFactChecker().checkFact(recipient, ssid, interestedPartyContextPlus)

        interestedPartyContext['facts'] = factReference
        checkedInterestedParty = await self._getFactChecker().checkFact(recipient, ssid, interestedPartyContext)
        del actorContext['facts']

        if not checkedInterestedParty:
            invalidReasons.append('recipient')
            if earlyEscape:
                return {
                    'valid': False,
                    'invalidReasons': invalidReasons
                    }

        preconditions = actReference['claim']['data'][lawReg.DISCIPL_FLINT_ACT]['preconditions']

        self.logger.debug('Original preconditions', preconditions)
        # Empty string, null, undefined are all explictly interpreted as no preconditions, hence the action can proceed
        preconditionContext = self._getContextExplainer().extendContextWithExplanation(context)
        if preconditions != '[]' and preconditions != None and preconditions != '':
            #preconditionContextPlus = copy.deepcopy(preconditionContext)
            #preconditionContextPlus['facts'] = factReference
            #checkedPreConditions = await self._getFactChecker().checkFact(preconditions, ssid, preconditionContextPlus)
            #context = preconditionContextPlus

            preconditionContext['facts'] = factReference
            checkedPreConditions = await self._getFactChecker().checkFact(preconditions, ssid, preconditionContext)
        else:
            checkedPreConditions = True

        if not checkedPreConditions:
            invalidReasons.append('preconditions')
            if earlyEscape:
                return {
                    'valid': False,
                    'invalidReasons': invalidReasons
                    }

        if checkedActor and checkedPreConditions and checkedObject and checkedInterestedParty:
            self.logger.info('Prerequisites for act', actLink, 'have been verified')
            return {
                'valid': True,
                'invalidReasons': []
                }

        definitivelyNotPossible = checkedActor == False or checkedObject == False or checkedInterestedParty == False or checkedPreConditions == False

        if definitivelyNotPossible:
            validity = False
        else:
            validity = None

        if definitivelyNotPossible:
            text =  'It is impossible.'
        else:
            text = 'It might work with more information'
        self.logger.info('Pre-act check failed due to', invalidReasons, text)
        return {
          'valid': validity,
          'invalidReasons': invalidReasons
            }