from src.DisciplLawReg.Utils.Logging_Util import getDiscplLogger
from src.DisciplLawReg.Expressions.andExpressionChecker import AndExpressionChecker
from src.DisciplLawReg.Expressions.createExpressionChecker import CreateExpressionChecker
from src.DisciplLawReg.Expressions.equalExpressionChecker import EqualExpressionChecker
from src.DisciplLawReg.Expressions.isExpressionChecker import IsExpressionChecker
from src.DisciplLawReg.Expressions.lessThanExpressionChecker import LessThanExpressionChecker
from src.DisciplLawReg.Expressions.listExpressionChecker import ListExpressionChecker
from src.DisciplLawReg.Expressions.literalExpressionChecker import LiteralExpressionChecker
from src.DisciplLawReg.Expressions.maxExpressionChecker import MaxExpressionChecker
from src.DisciplLawReg.Expressions.minExpressionChecker import MinExpressionChecker
from src.DisciplLawReg.Expressions.notExpressionChecker import NotExpressionChecker
from src.DisciplLawReg.Expressions.orExpressionChecker import OrExpressionChecker
from src.DisciplLawReg.Expressions.productExpressionChecker import ProductExpressionChecker
from src.DisciplLawReg.Expressions.projectionExpressionChecker import ProjectionExpressionChecker
from src.DisciplLawReg.Expressions.sumExpressionChecker import SumExpressionChecker

class ExpressionChecker:
    '''
    * Create a ExpressionChecker
    * @param {ServiceProvider} serviceProvider
    '''
    def __init__(self, serviceProvider):
        self.logger = getDiscplLogger()
        self.serviceProvider = serviceProvider

        self.subExpressionCheckers = {
            'AND': AndExpressionChecker(self.serviceProvider),
            'CREATE': CreateExpressionChecker(self.serviceProvider),
            'EQUAL': EqualExpressionChecker(self.serviceProvider),
            'IS': IsExpressionChecker(self.serviceProvider),
            'LESS_THAN': LessThanExpressionChecker(self.serviceProvider),
            'LIST': ListExpressionChecker(self.serviceProvider),
            'LITERAL': LiteralExpressionChecker(self.serviceProvider),
            'MAX': MaxExpressionChecker(self.serviceProvider),
            'MIN': MinExpressionChecker(self.serviceProvider),
            'NOT': NotExpressionChecker(self.serviceProvider),
            'OR': OrExpressionChecker(self.serviceProvider),
            'PRODUCT': ProductExpressionChecker(self.serviceProvider),
            'PROJECTION': ProjectionExpressionChecker(self.serviceProvider),
            'SUM': SumExpressionChecker(self.serviceProvider)
            }

    '''
    * Get fact checker
    * @return {FactChecker}
    * @private
    '''
    def _getFactChecker (self):
        return self.serviceProvider.factChecker

    '''
    * Get context explainer
    * @return {ContextExplainer}
    * @private
    '''
    def _getContextExplainer (self):
        return self.serviceProvider.contextExplainer

    '''
    * Checks a parsed expression by considering the atomic parts and evaluating them
    *
    * @param {ParsedExpression|string} fact - Parsed fact object (might be string if the object is an atomic fact)
    * @param {object} ssid - Identity doing the checking
    * @param {Context} context - Context of the check
    * @returns {Promise<boolean>}
    '''
    async def checkExpression (self, fact, ssid, context):
        if type(fact) == dict and 'expression' in fact:
            expr = fact['expression']
        else:
            expr = None
        self.logger.debug("Handling: ", expr)

        if expr != None:
            if not expr in self.subExpressionCheckers:
                raise Exception("Unknown expression detected")
            expressionChecker = self.subExpressionCheckers[expr]
        else:
            expressionChecker = None

        if 'explanation' in context and 'expresssion' in fact:
            context['explanation']['expression'] = fact['expression']
        if expressionChecker != None:
            return await expressionChecker.checkSubExpression(fact, ssid, context)
        else:
            if type(fact) == str:
                # Purposely do not alter context for explanation, self happens in checkFact
                result = await self._getFactChecker().checkFact(fact, ssid, context)

                self._getContextExplainer().extendContextExplanationWithResult(context, result)
                return result

            raise Exception('Unknown expression type ' + expr)
