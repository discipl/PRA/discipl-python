import src.DisciplLawReg.LawReg as lawReg#import DISCIPL_FLINT_ACT, DISCIPL_FLINT_ACT_TAKEN, DISCIPL_FLINT_DUTY, DISCIPL_FLINT_MODEL, DISCIPL_FLINT_PREVIOUS_CASE from '../index'
from src.DisciplLawReg.Utils.Logging_Util import getDiscplLogger
# Improve intelisense
# eslint-disable-next-line no-unused-vars
from src.AbundanceService.AbundanceService import AbundanceService
import copy

#from src.DisciplLawReg.defaultFactResolver import DefaultFactResolver#import wrapWithDefault from '../defaultFactResolver'
#from src.DisciplLawReg.Utils.Identity_Util import IdentityUtil

class ModelPublisher:
    '''
    * Create a ModelPublisher
    * @param {ServiceProvider} serviceProvider
    '''
    def __init__(self, serviceProvider):
        self.logger = getDiscplLogger()
        self.serviceProvider = serviceProvider

    '''
    * Get abundance service
    * @return {AbundanceService}
    * @private
    '''
    def _getAbundanceService (self):
        return self.serviceProvider.abundanceService

    '''
    * Publishes the FLINT model (as JSON) in linked verifiable claims (vc's)
    * in the channel of the given ssid. Each act, fact and duty is stored in a separate vc.
    * Returns a list to the claim holding the whole model with links to individual claims
    * Note that references within the model are not translated into links.
    *
    * @param {ssid} ssid SSID that publishes the model
    * @param {object} flintModel Model to publish
    * @param {object} factFunctions Additional factFunction that are declared outside the model
    * @return {Promise<string>} Link to a verifiable claim that holds the published model
    '''
    async def publish (self, ssid, flintModel, factFunctions = {}):
        self.logger.debug('Publishing model')
        core = self._getAbundanceService().getCoreAPI()
        result = { 'model': flintModel['model'], 'acts': [], 'facts': [], 'duties': [] }
        for fact in flintModel['facts']:
            resultFact = fact
            #if (fact.function === '[]' && factFunctions[fact.fact] != null) {
            #if type(fact['function']) == list and len(fact['function']) == 0 and factFunctions[fact['fact']] != None:
            if fact['function'] == '[]' and fact['fact'] in factFunctions:
                self.logger.debug('Setting function for', fact['fact'], 'to', factFunctions[fact['fact']])
                factPlus = copy.deepcopy(fact)
                factPlus['function'] = factFunctions[fact['fact']]
                resultFact = factPlus

            link = await core.claim(ssid, { lawReg.DISCIPL_FLINT_FACT: resultFact })
            result['facts'].append({ fact['fact']: link })

        for act in flintModel['acts']:
            link = await core.claim(ssid, { lawReg.DISCIPL_FLINT_ACT: act })
            result['acts'].append({ act['act']: link })

        for duty in flintModel['duties']:
            link = await core.claim(ssid, { lawReg.DISCIPL_FLINT_DUTY: duty })
            result['duties'].append({ duty['duty']: link })

        self.logger.debug('Done publishing')
        return await core.claim(ssid, { lawReg.DISCIPL_FLINT_MODEL: result })
