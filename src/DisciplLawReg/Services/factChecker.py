import src.DisciplLawReg.LawReg as lawReg#import DISCIPL_FLINT_ACT, DISCIPL_FLINT_ACT_TAKEN, DISCIPL_FLINT_FACT, DISCIPL_FLINT_FACTS_SUPPLIED, DISCIPL_FLINT_PREVIOUS_CASE
from src.DisciplLawReg.Utils.Logging_Util import getDiscplLogger
from src.DisciplLawReg.Utils.FactResolver import FactResolver
from mpmath import *
import copy

# Improve intelisense
# eslint-disable-next-line no-unused-vars
from src.AbundanceService.AbundanceService import AbundanceService

class FactChecker:
    '''
    * Create a ExpressionChecker
    * @param {ServiceProvider} serviceProvider
    '''
    def __init__(self, serviceProvider):
        self.logger = getDiscplLogger()
        self.serviceProvider = serviceProvider

    '''
    * Get abundance service
    * @return {AbundanceService}
    * @private
    '''
    def _getAbundanceService (self):
        return self.serviceProvider.abundanceService

    '''
    * Get expression checker
    * @return {ExpressionChecker}
    * @private
    '''
    def _getExpressionChecker (self):
        return self.serviceProvider.expressionChecker

    '''
    * Get expression checker
    * @return {ContextExplainer}
    * @private
    '''
    def _getContextExplainer (self):
        return self.serviceProvider.contextExplainer

    '''
    * Checks a fact by doing
    * 1. A lookup in the fact reference
    * 2. Checking if it is an expression, and parsing it
    *   a. If it is a simple expression, pass to the factResolver
    *   b. If it is a complex expression, parse it and evaluate it by parts
    *
    * @param {string|object} fact - fact or expression
    * @param {ssid} ssid - ssid representing the actor
    * @param {Context} context -
    * @returns {Promise<boolean>} - result of the fact
    '''
    async def checkFact (self, fact, ssid, context):
        self.logger.debug('Checking fact', fact)
        if type(fact) == str and 'facts' in context and fact in context['facts']:
            factLink = context['facts'][fact]
        else:
            factLink = None
        #factLink = context.facts ? context.facts[fact] : null

        printResult = lambda aResult: self.logger.debug('Resolved', fact, 'as', aResult)
        if factLink:
            if 'explanation' in context:
                context['explanation']['fact'] = fact

            newContext = self._getContextExplainer().extendContextWithExplanation(context)
            result = await self._checkFactLink(factLink, fact, ssid, newContext)

            self._getContextExplainer().extendContextExplanationWithResult(context, result)
            printResult(result)
            return result

        if type(fact) == str:
            if 'explanation' in context:
                context['explanation']['fact'] = fact
            newContext = self._getContextExplainer().extendContextWithExplanation(context)
            result = await self.checkFactWithResolver(fact, ssid, newContext)
            self._getContextExplainer().extendContextExplanationWithResult(context, result)
            printResult(result)
            return result
        else:
            result = await self._getExpressionChecker().checkExpression(fact, ssid, context)
            self._getContextExplainer().extendContextExplanationWithResult(context, result)
            printResult(result)
            return result

    '''
    * Checks a fact by using the factResolver from the context.
    * If an empty fact is to be checked, this is because a reference was followed. in this case we fall back
    * to the previousFact, which likely contains information that can be used to resolve this.
    *
    * @param {string} fact - Description of the fact, surrounded with []
    * @param {ssid} ssid - Identity of entity doing the checking
    * @param {Context} context - context of the checking
    * @param {string[]} possibleCreatingActions - Possible creating actions
    * @returns {Promise<*>}
    '''
    async def checkFactWithResolver (self, fact, ssid, context, possibleCreatingActions = []):
        # not fact returns True if fact is empty list
        if fact == '[]' or fact == '':
            factToCheck = context['previousFact']
        else:
            factToCheck = fact
        #factToCheck = fact === '[]' || fact === '' ? context.previousFact : fact
        if 'listNames' in context:
            listNames = context['listNames']
        else:
            listNames = []

        if 'listIndices' in context:
            listIndices = context['listIndices']
        else:
            listIndices = []
        #listNames = context.listNames or []
        #listIndices = context.listIndices or []

        if type(context['factResolver']) == FactResolver:
            if context['factResolver'].shouldUseSimpleResult:
                result = await context['factResolver'].resolveFacts(factToCheck, listNames, listIndices, possibleCreatingActions)
            else:
                result = await context['factResolver'].resolveFacts(factToCheck, listNames, listIndices, possibleCreatingActions)
        else:
            result = await context['factResolver'].resolveFacts(factToCheck, listNames, listIndices, possibleCreatingActions)
        resolvedResult = result
        if type(resolvedResult) == int or type(resolvedResult) == float:
            resolvedResult = mpf(resolvedResult)

        self.logger.debug('Resolving fact', fact, 'as', str(resolvedResult), 'via', factToCheck, 'by factresolver')
        self._getContextExplainer().extendContextExplanationWithResult(context, resolvedResult)
        return resolvedResult

    '''
    * Checks a fact link by checking created objects and passing the function to {@link checkFact}
    *
    * @param {string} factLink - Link to the fact
    * @param {string} fact - Name of the fact
    * @param {ssid} ssid - Identity of the entity performing the check
    * @param {Context} context - Represents the context of the check
    * @returns {Promise<boolean>}
    * @private
    '''
    async def _checkFactLink (self, factLink, fact, ssid, context):
        core = self._getAbundanceService().getCoreAPI()
        factReference = await core.get(factLink, ssid)
        functionRef = factReference['claim']['data'][lawReg.DISCIPL_FLINT_FACT]['function']

        context['previousFact'] = fact
        result = await self.checkFact(functionRef, ssid, context)
        self._getContextExplainer().extendContextExplanationWithResult(context, result)
        return result

    '''
    * Checks if a fact was created in a act that wasn't terminated yet that the given entity has access to.
    *
    * @param {string} fact - Description of the fact, surrounded with []
    * @param {ssid} ssid - Identity of entity doing the checking
    * @param {Context} context - context of the checking
    * @returns {Promise<boolean>} - true if the fact has been created
    '''
    async def checkCreatableFactCreated (self, fact, ssid, context):
        self.logger.debug('Checking if', fact, 'was created')
        creatingActions = list(map(lambda action: action['link'], await self.getCreatingActs(fact, ssid, context)))
        #creatingActions = (await self.getCreatingActs(fact, ssid, context)).map(action => action.link)
        if len(creatingActions) == 0:
            return False

        listNames = []
        if 'listNames' in context:
            listNames = context['listNames']
        listIndices = []
        if 'listIndices' in context:
            listIndices = context['listIndices']

        result = await context['factResolver'].resolveFacts(fact, listNames, listIndices, creatingActions)

        if not result in creatingActions and result != None:
            raise Exception('Invalid choice for creating action: ' + result)

        if result == None and 'myself' in context:
            actorType = context.searchingFor
            self.logger.debug('Multiple creating acts found. Checking if you are at least a', actorType)
            isActorType = await self.checkFact(actorType, ssid, context)
            self.logger.debug('Resolved you are a', actorType, 'as', isActorType)
            if isActorType:
                return None
            return False
            #return isActorType ? undefined : false

        return result

    '''
    * Get all creating acts where the fact was created and not terminated yet
    *
    * @param {string} fact - Description of the fact, surrounded with []
    * @param {ssid} ssid - Identity of entity getting the acts
    * @param {Context} context - context of the getting
    * @returns {Promise<CreatingAct[]>}
    '''
    async def getCreatingActs (self, fact, ssid, context):
        self.logger.debug('Getting creating actions for', fact)
        core = self._getAbundanceService().getCoreAPI()
        caseLink = context['caseLink']
        '''
        * @type {CreatingAct[]}
        '''
        possibleCreatingActions = []
        terminatedCreatingActions = []

        while caseLink != None:
            caseData = await core.get(caseLink, ssid)
            if lawReg.DISCIPL_FLINT_ACT_TAKEN in caseData['claim']['data']:
                lastTakenAction = caseData['claim']['data'][lawReg.DISCIPL_FLINT_ACT_TAKEN]
            else:
                lastTakenAction = None

            if lastTakenAction != None:
                actData = await core.get(lastTakenAction, ssid)
                act = actData['claim']['data'][lawReg.DISCIPL_FLINT_ACT]

                if 'create' in act and act['create'] != None and fact in act['create']:
                    self.logger.debug('Found possible creating act', act['act'])
                    possibleCreatingActions.append({
                        'link': caseLink,
                        'contextFact': fact,
                        'facts': caseData['claim']['data'][lawReg.DISCIPL_FLINT_FACTS_SUPPLIED]
                        })

                if 'terminate' in act and act['terminate'] != None and fact in act['terminate']:
                    self.logger.debug('Found possible terminating act', act['act'])
                    terminatedLink = caseData['claim']['data'][lawReg.DISCIPL_FLINT_FACTS_SUPPLIED][fact]
                    terminatedCreatingActions.append(terminatedLink)

            if lawReg.DISCIPL_FLINT_PREVIOUS_CASE in caseData['claim']['data']:
                caseLink = caseData['claim']['data'][lawReg.DISCIPL_FLINT_PREVIOUS_CASE]
            else:
                caseLink = None

        filtered = list(filter(lambda creatingAction: not creatingAction['link'] in terminatedCreatingActions, possibleCreatingActions))
        self.logger.debug('Creating acts', filtered)
        return filtered
