import re
import src.DisciplLawReg.LawReg as lawReg#import DISCIPL_FLINT_ACT, DISCIPL_FLINT_ACT_TAKEN, DISCIPL_FLINT_DUTY, DISCIPL_FLINT_MODEL, DISCIPL_FLINT_PREVIOUS_CASE from '../index'
from src.DisciplLawReg.Utils.Logging_Util import getDiscplLogger
# Improve intelisense
# eslint-disable-next-line no-unused-vars
from src.AbundanceService.AbundanceService import AbundanceService
from src.DisciplLawReg.Utils.Array_Util import arrayToObject

class DutyFetcher:
    '''
    * Create a DutyFetcher
    * @param {ServiceProvider} serviceProvider
    '''
    def __init__(self, serviceProvider):
        self.logger = getDiscplLogger()
        self.serviceProvider = serviceProvider

    '''
    * Get abundance service
    * @return {AbundanceService}
    * @private
    '''
    def _getAbundanceService (self):
        return self.serviceProvider.abundanceService

    '''
    * Get link utils
    * @return {LinkUtil}
    * @private
    '''
    def _getLinkUtils (self):
        return self.serviceProvider.linkUtil

    '''
    * Get fact checker
    * @return {FactChecker}
    * @private
    '''
    def _getFactChecker (self):
        return self.serviceProvider.factChecker

    '''
    * Returns the active duties that apply in the given case for the given ssid
    *
    * @param {string} caseLink - link to the current state of the case
    * @param {ssid} ssid - identity to find duties for
    * @returns {Promise<DutyInformation[]>}
    '''
    async def getActiveDuties (self, caseLink, ssid):
        core = self._getAbundanceService().getCoreAPI()
        firstCaseLink = await self._getLinkUtils().getFirstCaseLink(caseLink, ssid)
        modelLink = await self._getLinkUtils().getModelLink(firstCaseLink, ssid)

        model = await core.get(modelLink, ssid)

        duties = arrayToObject(model['claim']['data'][lawReg.DISCIPL_FLINT_MODEL]['duties'])

        factReference = arrayToObject(model['claim']['data'][lawReg.DISCIPL_FLINT_MODEL]['facts'])

        actionLink = caseLink

        terminatedDuties = []
        activeDuties = []

        while actionLink != None:
            lastAction = await core.get(actionLink, ssid)

            if lawReg.DISCIPL_FLINT_ACT_TAKEN in lastAction['claim']['data']:
                actLink = lastAction['claim']['data'][lawReg.DISCIPL_FLINT_ACT_TAKEN]
            else:
                actLink = None

            if actLink != None:
                act = await core.get(actLink, ssid)
                self.logger.debug('Found earlier act', act)

                if type(act['claim']['data'][lawReg.DISCIPL_FLINT_ACT]['create']) == str:
                    results = re.findall(r'(<.*?>)', act['claim']['data'][lawReg.DISCIPL_FLINT_ACT]['create'])
                    #results = re.findall('ver', act['claim']['data'][lawReg.DISCIPL_FLINT_ACT]['create'])
                    # If the duty is terminated, we should not include it as active
                    for result in results:
                        if not result in terminatedDuties:
                            activeDuties.append(result)
                    #activeDuties.append(*matches.filter(lambda duty: not duty in terminatedDuties))

                if type(act['claim']['data'][lawReg.DISCIPL_FLINT_ACT]['terminate']) == str:
                    results = re.findall(r'(<.*?>)', act['claim']['data'][lawReg.DISCIPL_FLINT_ACT]['terminate'])
                    #matches = act['claim']['data'][lawReg.DISCIPL_FLINT_ACT]['terminate'].match("/<[^>]+>/g") or []
                    for result in results:
                        terminatedDuties.append(result)
            if lawReg.DISCIPL_FLINT_PREVIOUS_CASE in lastAction['claim']['data']:
                actionLink = lastAction['claim']['data'][lawReg.DISCIPL_FLINT_PREVIOUS_CASE]
            else:
                actionLink = None

        self.logger.debug('Active duties', activeDuties, '. Checking ownership now.')
        ownedDuties = []

        for duty in activeDuties:
            dutyLink = duties[duty]

            if dutyLink != None:
                dutyInformation = (await core.get(dutyLink, ssid))['claim']['data'][lawReg.DISCIPL_FLINT_DUTY]

                dutyHolder = dutyInformation['duty-holder']

                if dutyHolder != None:
                    self.logger.debug('Checking duty-holder')
                    checkActor = await self._getFactChecker().checkFact(dutyHolder, ssid, { 'facts': factReference, 'myself': True, 'caseLink': caseLink })
                    if checkActor:
                        self.logger.info('Duty', duty, 'is held by', dutyHolder)
                        ownedDuties.append({
                            'duty': duty,
                            'link': dutyLink
                            })

        return ownedDuties

