from reactivex import of, operators as op
import sys
from src.SsidObserver import *
from src.BaseConnector.Base_Connector import BaseConnector
from src.Ephemeral.crypto.IdentityFactory import IdentityFactory

class SubjectFilter(Subject):
    def __init__(self, claimFilter = {}):
        Subject.__init__(self)
        self.claims = []
        self.claimFilter = claimFilter
        self.identityFactory = IdentityFactory()
        self.historical = False

    def on_next(self, value):
        self.claims.append(value)
        Subject.on_next(self, value)

    def on_error(self, value):
        Subject.on_error(self, value)
        print("error")

    def on_completed(self):
        Subject.on_completed(self)
        print("done")

    def getFirstClaim (self):
        result = self.applyFilter(self.claims)
        if len(result) > 0:
            return result[0]
        return None

    def getLastClaim (self):
        result = self.applyFilter(self.claims)
        if len(result) > 0:
            return result[-1]
        return None

    def getClaims (self, amount):
        result = self.applyFilter(self.claims)
        if len(result) > 0:
            return result[:amount]
        return None

    async def getClaimsHistorical(self, connName, amount, ssid, client):
        if not self.historical:
            return None

        result = None
        if len(self.claims) == 0:
            result = [{'claim': await client.getLatest(BaseConnector.referenceFromDid(ssid['did']))}]
        else:
            result = [self.claims[-1]]

        if result == None:
            return None

        i = 0
        current = result[i]['claim']
        while 'previous' in current and current['previous'] != None:
            previousLink = current['previous']
            if not previousLink.startswith('link'):
                previousLink = "link:discipl:" + connName + ":" + previousLink
                current['previous'] = previousLink
            reference = BaseConnector.referenceFromLink(previousLink)
            pubkey = BaseConnector.referenceFromDid(ssid['did'])

            signature = await self.getSignature(reference, ssid['did'], pubkey, ssid['privkey'], ssid['signingkey'])
            result.append({'claim': await client.get(reference, pubkey, signature)})

            i += 1
            current = result[i]['claim']

        result = self.applyFilter(result)
        result.reverse()
        return result[:amount]

    async def getSignature(self, reference, did, pubkey, privkey, signingkey):
        signature = None
        if pubkey != None and privkey != None and signingkey != None:
            signIdentity = await self.identityFactory.fromDid(did, privkey, signingkey)
            signature = signIdentity.sign(reference)

        return signature

    def applyFilter(self, claimResults):
        if not self.claimFilter:
            return claimResults

        filteredClaims = []
        for claim in claimResults:
            claimData = claim['claim']['data']
            test = True
            for predicate in self.claimFilter:
                if predicate not in claimData:
                    test = False
                    break

                if self.claimFilter[predicate] != None and self.claimFilter[predicate] != claimData[predicate]:
                    test = False
                    break
            if test == True:
                filteredClaims.append(claim)
        return filteredClaims

    async def take (self, amount):
        resultPromise = self.subject.pipe(op.take(amount))
        return resultPromise

    async def subscribe (self, observer):
        Subject.subscribe(self, observer)