import importlib.util
import sys

CONNECTOR_MODULE_PREFIX = '@discipl/core-'

#
# loads a connector based on module name using a dynamic import
#
async def loadConnector(connectorName):
    module = await importlib.import_module(CONNECTOR_MODULE_PREFIX + connectorName)
    print(module)
    return module.default