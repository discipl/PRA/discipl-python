import json

DID_DELIMITER = ':'
LINK_PREFIX = 'link' + DID_DELIMITER + 'discipl' + DID_DELIMITER
DID_PREFIX = 'did' + DID_DELIMITER + 'discipl' + DID_DELIMITER

class BaseConnector:
    @staticmethod
    def ALLOW ():
        return 'DISCIPL_ALLOW'

    def __init__(self):
        if type(self) == BaseConnector:
            raise Exception('BaseConnector must be overridden')

        methods = ['getName', 'getDidOfClaim', 'getLatestClaim', 'newIdentity', 'claim', 'get', 'observe']
        for method in methods:
            if self[method] == None or not callable(self[method]):
                raise Exception('Method ' + method + ' should be implemented')

    """
    * Converts platform-specific reference to a discipl-link to a claim.
    *
    * Can be reverted with {@link BaseConnector.referenceFromLink}
    *
    * @param {string} claimReference Reference to the claim
    * @returns {string} The link if the reference is a non-empty string. Null otherwise.
    """
    def linkFromReference (self, claimReference):
        if claimReference != None and type(claimReference) == str:
            return LINK_PREFIX + self.getName() + DID_DELIMITER + claimReference
        return None

    """
    * Converts platform-specific reference to a discipl-did
    *
    * Can be reverted with {@link BaseConnector.referenceFromDid}
    *
    * @param {string} reference Reference to the identity
    * @returns {string} The link if the reference is a non-empty string. Null otherwise.
    """
    def didFromReference (self, reference):
        if reference != None and type(reference) == str:
            return DID_PREFIX + self.getName() + DID_DELIMITER + reference
        return None

    """
    * Extracts connector name from a did or link
    *
    * @param {string} linkOrDid String from which the connector needs to be extracted
    * @returns {string} The name of the connector, if the input is a valid link or did. Null otherwise.
    """
    @staticmethod
    def getConnectorName (linkOrDid):
        if BaseConnector.isDid(linkOrDid) or BaseConnector.isLink(linkOrDid):
            splitted = linkOrDid.split(DID_DELIMITER)
            return splitted[2]
        return None

    """
    * Extracts the reference from a link
    *
    * @param {string} link
    * @returns {string} Reference part of the link, if the link is valid. Null otherwise
    """
    @staticmethod
    def referenceFromLink (link):
        if BaseConnector.isLink(link):
            splitted = link.split(DID_DELIMITER)
            return DID_DELIMITER.join(splitted[3:])
        return None

    """
    * Extracts the reference from a did
    *
    * @param {string} did
    * @returns {string} Reference part of the did, if the did is valid. Null otherwise
    """
    @staticmethod
    def referenceFromDid (did):
        if BaseConnector.isDid(did):
            splitted = did.split(DID_DELIMITER)
            return DID_DELIMITER.join(splitted[3:])
        # test_list[K:]
        return None

    """
    * @param {string} str Potential link
    * @returns {boolean} True if and only if the input is a valid link
    """
    @staticmethod
    def isLink (string):
        return type(string) == str and string.startswith(LINK_PREFIX)

    """
    * @param {string} str Potential did
    * @returns {boolean} True if and only if the input is a valid did
    """
    @staticmethod
    def isDid (string):
        return type(string) == str and string.startswith(DID_PREFIX)

    """
    * Verifies existence of a claim with the given data in the channel of the given did
    *
    * @param {string} did - That might have claimed the data
    * @param {object} data - Data that needs to be verified
    * @param {string} verifierDid - Did that wants to verify (used for access management)
    * @param {string} verifierPrivkey - Private key of the verifier
    * @returns {Promise<string>} Link to claim that proves self data, null if such a claim does not exist
    """
    async def verify (self, did, data, verifierDid = None, verifierPrivkey = None, verifierSignkey = None):
        claim = await self.getLatestClaim(did)

        current = None
        if claim != None:
            signature = claim['claim']['signature']
            current = self.linkFromReference(signature)

        while current != None:
            res = await self.get(current, verifierDid, verifierPrivkey, verifierSignkey)
            if res != None and json.dumps(data) == json.dumps(res['claim']['data']):
                return current

            if res != None:
                current = res['claim']['previous']
            else:
                break
        return None

    async def importConnector (self, did, link, data):
        raise Exception('Claim import is not supported')

# export { BaseConnector }
