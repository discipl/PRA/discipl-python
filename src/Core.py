from src.BaseConnector.Base_Connector import BaseConnector
from .Connector_Loader import loadConnector
from reactivex import Observable
from reactivex import concat, operators as op
from .SubjectFilter import SubjectFilter
from src.Ephemeral.EphemeralConnector import EphemeralConnector

import warnings

MAX_DEPTH_REACHED = 'MAX_DEPTH_REACHED'
REVOKE_PREDICATE = 'revoke'

"""
 @module discipl-core
 
"""
class DisciplCore:
    def __init__(self):
        self.disciplCoreConnectors = {}

    """ 
    @typedef {Object} ssid - Identifier of an actor
    @property {string} ssid.did - Did of the actor
    @property {string} ssid.privkey - Private key belonging to the did
    """

    """
    Requires and holds in memory the given discipl connector (if not done before)
    
    @param {string} connectorName
    @returns {Promise<void>}
    """
    async def initializeConnector (self, connectorName):
        if connectorName not in self.disciplCoreConnectors:
            #ConnectorModuleClass = await loadConnector(connectorName)
            #self.registerConnector(connectorName, ConnectorModuleClass())
            connector = EphemeralConnector()
            self.registerConnector(connectorName, connector)
    """
    * Returns a instance of the given discipl connector and automatically lazy loads the corresponding module
    *
    * @param {string} connectorName Name of the connector to load
    * @returns {Promise<*>} Connector instance that extends from {@link BaseConnector}
    """
    async def getConnector (self, connectorName):
        await self.initializeConnector(connectorName)
        return self.disciplCoreConnectors[connectorName]

    """
    * Extracts the connector name from a link or did and return a instance of the connector
    *
    * @param {string} linkOrDid String from which the connector needs to be extracted
    * @returns {Promise<*>} Connector instance that extends from {@link BaseConnector}
    """
    async def getConnectorForLinkOrDid (self, linkOrDid):
        connectorName = BaseConnector.getConnectorName(linkOrDid)
        return await self.getConnector(connectorName)

    """"
    * Registers a connector explicitly.
    *
    * @param {string} name - Name of the connector
    * @param {object} connector - Instantiated object representing the connector
    """
    def registerConnector (self, name, connector):
        self.disciplCoreConnectors[name] = connector


    """"
    * Retrieves the did that made the claim referenced in the given link
    *
    * @param {string} link
    * @returns {Promise<string>} Did that made the claim
    """
    async def getDidOfLinkedClaim (self, link):
        conn = await self.getConnectorForLinkOrDid(link)
        return await conn.getDidOfClaim(link)


    """
    * Generates a new ssid using the specified connector
    *
    * @param {string} connectorName - Name of the connector used
    * @returns {Promise<ssid>} Created ssid
    """
    async def newSsid (self, connectorName):
        conn = await self.getConnector(connectorName)
        return await conn.newIdentity()


    """"
    * Adds a claim to the (end of the) channel of the given ssid.
    *
    * @param {ssid} ssid
    * @param {object} data - Data to be claimed
    * @param {object} attester - Ssid of the attester, used for access management
    * @returns {Promise<string>} Link to the made claim
    """
    async def claim (self, ssid, data, attester = None):
        connector = await self.getConnectorForLinkOrDid(ssid['did'])
        if attester is not None:
            return connector.claim(ssid['did'], ssid['privkey'], ssid['signingkey'], data, attester.did)
        return await connector.claim(ssid['did'], ssid['privkey'], ssid['signingkey'], data)


    """
    * Adds an attestation claim for a given link. The link will be be added to the channel of the given
    * ssid referenced by the given predicate
    *
    * @param {ssid} ssid
    * @param {string} predicate - Statement being made about the claim linked
    * @param {string} link - Object of the attestation
    * @returns {Promise<string>} Link to the made attestation
    """
    async def attest (self, ssid, predicate, link):
        attest = {}
        attest[predicate] = link
        return await self.claim(ssid, attest)

    """
    * Adds a claim to the (end of the) chanel of the given ssid with a specified scope and/or did
    *
    * @param {ssid} ssid
    * @param {string} scope - Scope of this claim. If not present, the scope is the whole channel
    * @param {string} did - Did that is allowed access. If not present, everyone is allowed.
    """
    async def allow (self, ssid, scope = None, did = None):
        allowConfiguration = {}
        if scope is not None:
            allowConfiguration['scope'] = scope

        if did is not None:
            allowConfiguration['did'] = did

        return await self.claim(ssid, { BaseConnector.ALLOW(): allowConfiguration })

    """
    * Will verify existence of an attestation of the claim referenced in the given link and mentioning the given predicate.
    * If the referenced claim or an attestation itself are revoked, the claim will be treated as "not attested" and thus the
    * method will return null
    *
    * @param {string} predicate - Attestation predicate
    * @param {string} link - Link to the claim to verify attestation for
    * @param {string[]} dids - Candidates that might attested the claim
    * @param {ssid} verifierSsid - Ssid object that grants access to the relevant claims
    * @returns {Promise<string>} The first did that attested, null if none have
    """
    async def verify (self, predicate, link, dids, verifierSsid = { 'did': None, 'privkey': None, 'signingkey': None }):
        for did in dids:
            if not isinstance(did, str):
                continue

            connector = await self.getConnectorForLinkOrDid(did)
            attestation = { predicate: link }
            attestationLink = await connector.verify(did, attestation, verifierSsid['did'], verifierSsid['privkey'], verifierSsid['signingkey'])

            if attestationLink is not None:
                if await self.verify(REVOKE_PREDICATE, attestationLink, [did], verifierSsid) == None:
                    if predicate == REVOKE_PREDICATE or await self.verify(REVOKE_PREDICATE, link, [await self.getDidOfLinkedClaim(link)], verifierSsid) == None:
                        return did
        return None

    """
    * Retrieves the data of the claim a given link refers to, along with a link to the previous claim in the same channel
    *
    * @param {string} link - link to the claim of which the data should be retrieved
    * @param {ssid} [ssid] - Optional authorization method if the claim in question is not publically visible
    * @return {Promise<{data: object, previous: string}>} Claim data
    """
    async def get (self, link, ssid = None):
        conn = await self.getConnectorForLinkOrDid(link)

        if ssid != None:
            return await conn.get(link, ssid['did'], ssid['privkey'], ssid['signingkey'])
        return await conn.get(link)

    """
    * Observe for new claims that will be made by a specified did
    *
    * @param {string} did - Filter for claims made by a did
    * @param {ssid} observerSsid - Ssid to allow access to claims
    * @param {object} claimFilter - Filters by the content of claims
    * @param {boolean} historical - DEPRECATED if true, the result will start at the beginning of the channel
    * @param {object} connector - Needs to be provided in order to listen platform-wide without ssid
    * @returns {ObserveResult}
    """
    async def observe (self, did, observerSsid = { 'did': None, 'privkey': None, 'signingkey': None}, claimFilter = {}, historical = False, connector = None):
        if historical:
            warnings.warn('Historical observation is deprecated and may be buggy')

        if connector != None and did == None:
            return await self.observeAll(connector, claimFilter, observerSsid)

        if did == None:
            raise Exception('Observe without did or connector is not supported')

        connector = await self.getConnectorForLinkOrDid(did)
        await connector.observe(did, claimFilter, observerSsid['did'], observerSsid['privkey'], observerSsid['signingkey'], historical)

    async def getObservedClaims(self, amount, ssid, accessorSsid=None):
        connector = await self.getConnectorForLinkOrDid(ssid['did'])
        await connector.getObservedClaims(amount, ssid, accessorSsid)

    async def subscribe(self, ssid, accessorSsid):
        connector = await self.getConnectorForLinkOrDid(ssid['did'])
        await connector.subscribe(ssid, accessorSsid)

    async def getLastNotification(self, ssid, accessorSsid):
        connector = await self.getConnectorForLinkOrDid(ssid['did'])

        return await connector.getLastNotification(ssid, accessorSsid)

    async def getFirstClaim(self, did, accessorDid = None):
        connector = await self.getConnectorForLinkOrDid(did)

        return await connector.getFirstClaim(did, accessorDid)

    async def getLastClaim(self, did, accessorDid=None):
        connector = await self.getConnectorForLinkOrDid(did)

        return await connector.getLastClaim(did, accessorDid)

    async def getClaimsHistorical(self, amount, ssid, accessorSsid):
        connector = await self.getConnectorForLinkOrDid(ssid['did'])

        return await connector.getClaimsHistorical(amount, ssid, accessorSsid)

    """
    * Observe for new claims that will be made by anyone
    *
    * @param {object} connector - needs to be provided in order to listen platform-wide without ssid
    * @param {object} claimFilter - Filters by the content of claims
    * @param {ssid} observerSsid - Ssid to allow access to claims
    * @returns {ObserveResult}
    """
    async def observeAll (self, connector, claimFilter, observerSsid):
        return await connector.observe(None, claimFilter, observerSsid['did'], observerSsid['privkey'], observerSsid['signingkey'])

    """"
    * Observe for verification requests for a given did.
    *
    * @param {object} did - Did to observe verification requests for
    * @param {object} claimFilter
    * @param {string} claimFilter.did - Filter incomming verification requests on did
    * @param {object} observerSsid - The ssid that is observing, used for access management
    * @returns {ObserveResult}
    """
    async def observeVerificationRequests (self, did, claimFilter = None, observerSsid = { 'did': None, 'privkey': None }):
        connector = await this.getConnectorForLinkOrDid(did)

        if not callable(connector.observeVerificationRequests):
            raise Exception("The 'observeVerificationRequests' method is not supported for the '" + connector.getName() + "' connector")

        currentObservableResult = await connector.observeVerificationRequests(did, claimFilter, observerSsid.did, observerSsid.privkey)

        return SubjectFilter(currentObservableResult.observable, currentObservableResult.readyPromise)


    """
    * Exports linked claim data starting with the claim the given link refers to.
    * Links contained in the data of the claim are exported also in a value alongside of the link and links in data of those claims are processed in a same way too etc.
    * By default, expansion like this is done at most three times. You can alter this depth of the export by setting the second argument. If the maximum depth is reached the exported data
    * will contain the value MAX_DEPTH_REACHED alongside of the link instead of an exported dataset. You can use this method to iteratively expand the dataset using the link that was not followed.
    * A claim is never exported twice; circulair references are not followed.
    """
    async def exportLD (self, didOrLink, exporterSsid = { 'did': None, 'privkey': None }, maxdepth = 4, ssid = None, visitedStack = [], withPrevious = False):
        isDidBool = BaseConnector.isDid(didOrLink)
        isLinkBool = BaseConnector.isLink(didOrLink)
        if isDidBool:
            withPrevious = True

        if not isDidBool and not isLinkBool:
            return didOrLink

        connector = await self.getConnectorForLinkOrDid(didOrLink)

        if isDidBool:
            currentDid = didOrLink
        else:
            currentDid = await connector.getDidOfClaim(didOrLink)
            if currentDid == None:
                return {}

        if isLinkBool:
            currentLink = didOrLink
        else:
            lastClaim = await connector.getLatestClaim(didOrLink)
            if lastClaim != None:
                currentLink = connector.linkFromReference(lastClaim['claim']['signature'])
            else:
                currentLink = None

        if len(visitedStack) >= maxdepth:
            return { didOrLink: MAX_DEPTH_REACHED }

        if not withPrevious:
            visitedStack.append(currentLink)

        channelData = []

        if currentLink != None:
            res = await self.get(currentLink, exporterSsid)
        else:
            res = None

        if res != None:
            data = res['claim']['data']

            if res['claim']['previous'] and withPrevious:
                prevData = await self.exportLD(res['claim']['previous'], exporterSsid, maxdepth, currentDid, visitedStack, True)
                channelData = prevData[currentDid]

            linkData = {}
            if type(data) is not str and isinstance(data, list):
                linkData = []

            if isinstance(data, dict):
                for elem in data:
                    #if data.hasOwnProperty(elem):
                    #if hasattr(data, elem):

                    value = data[elem]
                    exportValue = await self.exportLD(value, exporterSsid, maxdepth, ssid, visitedStack, False)

                    if type(data) is not str and isinstance(data, list):
                        linkData.append(exportValue)
                    else:
                        linkData[elem] = exportValue
            else:
                for i in range(len(data)):
                    # if data.hasOwnProperty(elem):
                    # if hasattr(data, elem):

                    value = data[i]
                    exportValue = await self.exportLD(value, exporterSsid, maxdepth, ssid, visitedStack, False)

                    if type(data) is not str and isinstance(data, list):
                        linkData.append(exportValue)
                    else:
                        linkData[elem] = exportValue

            #channelData.append({ [currentLink]: linkData })
            channelData.append({currentLink: linkData})

        #return { [currentDid]: channelData }
        return { currentDid: channelData}

    """
    * Imports claims given a dataset as returned by exportLD. Claims linked in the claims are not imported (so max depth = 1)
    * Not all connectors will support this method and its functioning may be platform specific. Some may actually let you
    * create claims in bulk through this import. Others will only check for existence and validate.
    *
    * @param {*} data
    * @param {*} importerDid
    * @return {boolean}
    """
    async def importLD (self, data, importerDid = None):
        succeeded = None

        for did in data:
            if not BaseConnector.isDid(did):
                continue

            for dict1 in data[did]:
                for k, v in dict1.items():
                    link = k
                    claim = v
                    for k2, v2 in v.items():
                        predicate = k2

                res = await self.importLD(claim[predicate], importerDid)
                if res != None:
                    for k2, v2 in claim[predicate].items():
                        nestedDid = k2
                    #nestedDid = Object.keys(claim[predicate])[0]

                    for k2, v2 in claim[predicate][nestedDid][0].items():
                        l = k2
                    #l = Object.keys(claim[predicate][nestedDid][0])[0]

                    claim = { predicate: l }

                connector = await self.getConnectorForLinkOrDid(did)
                result = await connector.importConnector(did, link, claim, importerDid)

                if result == None:
                    return None

                succeeded = True
        return succeeded

    """
    * Adds a revocation attestation to the channel of the given ssid. Effectively revokes the claim the given link refers to. Subsequent verification of the claim will not succeed.
    *
    * @param {ssid} ssid - The ssid makes the revoke attestation
    * @param {string} link - The link to the claim (or attestation) that should be attested as being revoked. Note that this claim must be in the channel of the given ssid to be effectively seen as revoked.
    * @return {Promise<string>} Link to the attestation
    """
    async def revoke (self, ssid, link):
         return await self.attest(ssid, REVOKE_PREDICATE, link)